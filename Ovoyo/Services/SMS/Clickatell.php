<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

require_once 'Ovoyo/Services/SMS/Abstract.php';
require_once 'Ovoyo/Services/SMS/ClickatellSession.php';
require_once 'Zend/Http/Client.php';
require_once 'Zend/Mail.php';
require_once 'Zend/Mail/Transport/Sendmail.php';

/**
 * Clickatell integration class
 *
 * @package	Ovoyo_Services
 * @subpackage	SMS
 */
class Ovoyo_Services_SMS_Clickatell extends Ovoyo_Services_SMS_Abstract
{
    /**
     * Defines if SMS messages should be logged
     * @var boolean
     */
    public $logMessages;

    /**
     * Name of the site using the class - used for admin emails
     * @var string
     */
    public $siteName;

    /**
     * The Clickatell Session object
     * @var object
     */
    public $session;

    /**
     * clickatell username
     * @var string
     */
    public $username;

    /**
     * clickatell password
     * @var string
     */
    public $password;

    /**
     * clickatell API ID
     * @var string
     */
    public $apiId;

    /**
     * Mobile number of recipient
     * @var string
     */
    public $recipient_number;

    /**
     * Message text of SMS
     * @var string
     */
    public $message_text;

    /**
     * The message ID of the message once sent
     * @var string
     */
    public $message_id;

    // Optional parameters

    /**
     * SMS sender
     * @var string
     */
    public $from;

    public $to;

    public $text;

    /*
     * The Zend_HTTP client. Make this a class variable so we can keep
     * the connection alive over multiple requests.
     */

    private $httpClient;

    /*
     * constants used by the clickatell API
     */
    const LOCAL_COUNTRY_CODE = 44;

    // HTTP Interfaces and other limits
    const SMS_MAX_FROM_LENGTH = 11;      /* The maximum length of the string that can be used in the From field */
    const SMS_MAX_MESSAGE_LENGTH = 160;  /* The maximum length of an SMS message                                */
    const SMS_MAX_MESSAGE_LENGTH_WITH_UNSUB = 130; /* The maximum length of an SMS message when unsubscribe info is attached */

    const CLICKATELL_SMS_SESSION_TIMEOUT = 15;
    const CLICKATELL_SMS_ERROR_SESSION_INVALID = '003';
    const CLICKATELL_SMS_ERROR_SESSION_MISSING = '005';
    const CLICKATELL_SMS_ERROR_CONNECTION_ISSUE = 100;

    const CLICKATELL_HTTP_SESSION_INTERFACE = 'http://api.clickatell.com/http/auth';
    const CLICKATELL_HTTP_PING_INTERFACE = 'http://api.clickatell.com/http/ping';
    const CLICKATELL_HTTP_SEND_INTERFACE = 'http://api.clickatell.com/http/sendmsg';
    const CLICKATELL_HTTP_BALANCE_INTERFACE = 'http://api.clickatell.com/http/getbalance';

    const SMS_HTTP_HOST = 'api.clickatell.com';
    const SMS_HTTP_PORT = 80;
    const SMS_HTTP_TIMEOUT = 30;
    const SMS_HTTP_METHOD = 'GET';
    const SMS_HTTP_VERSION = '1.1';
    const SMS_RESULT_SIZE = 128;

    const SMS_ADMIN_EMAIL_ADDRESS = 'antony@totallycommunications.com';
    const SMS_ADMIN_EMAIL_FROM = 'Clickatell SMS Mailer';
    const SMS_ADMIN_EMAIL_INSUFFICIENT_SUBJECT = 'Clickatell WARNING: Insufficient SMS credits';
    const SMS_ADMIN_EMAIL_DWINDLING_SUBJECT = 'Clickatell WARNING: Dwindling SMS credits';
    const SMS_LOW_CREDIT_THRESHOLD = 20;

    /**
     * SMS Class Constructor
     * @param string $siteName The name of the site using the class - used for the admin emails
     * @param boolean $logMessages Set to true if sent messages should be logged. If it is true, SMS_DB_TABLE_SENT must be defined
     */
    public function __construct($siteName, $apiId, $username, $password, $logMessages = true)
    {
        $this->siteName    = $siteName;
        $this->apiId       = $apiId;
        $this->username     = $username;
        $this->password     = $password;
        $this->logMessages = $logMessages;
        $this->message_id   = null;

        $this->session = new Ovoyo_Services_SMS_ClickatellSession;
        $this->session->setParams($apiId, $username, $password);

        $httpConfig = array('timeout' => self::SMS_HTTP_TIMEOUT, 'keepalive' => true);
        $this->httpClient = new Zend_Http_Client(self::CLICKATELL_HTTP_SEND_INTERFACE, $httpConfig);
    }

    /**
     * Creates a new SMS to be sent
     *
     * @access public
     * @param int $to The recipient's number in full international format (no '+' or '00')
     * @param string $text The message body text
     * @param string $from Optional SMS sender which gets displayed on the recipient's handset. Max of SMS_MAX_FROM_LENGTH characters. Not that Premium SMS cannot have a personalised SMS sender. They must appear to come from the authorised shortcode.
     * @return void
     */
    public function send($to, $text, $from = null) {
        $this->to   = $to;
        $this->text = $text;
        $this->from = (strlen($from) > self::SMS_MAX_FROM_LENGTH) ? substr($from, 0, self::SMS_MAX_FROM_LENGTH) : $from;

        return $this->doSend();
    }

    /**
     * Sends the SMS and logs the send in the database
     *
     * @access public
     * @param boolean $check_pre_balance Set to true if the account balance should be checked before attempting to send the message
     * @param boolean $check_post_balance Set to true if the account balance should be checked after sending the message
     * @return boolean TRUE if send was successful, FALSE if otherwise
     */
    public function doSend ($check_pre_balance = false, $check_post_balance = false, $recreated_session = false)
    {
        if (!$this->isValid()) {
            return false;
        }

        if (!$this->session->create()) {
            return false;
        }

        if ($check_pre_balance && !$this->fetchBalance()) {
            $this->sendInsufficientCreditsEmail('antony@totallycommunications.com');
            return false;
        }

        $parameters = array(
            'user'      =>  $this->username,
            'password'  =>  $this->password,
            'api_id'    =>  $this->apiId,
            'to'        =>  $this->to,
            'text'      =>  $this->text
        );

        /* Add optional parameters */
        if ($this->from) {
            $parameters['from'] = $this->from;
        }

        $response = $this->sendHttpRequest(self::CLICKATELL_HTTP_SEND_INTERFACE, $parameters);

        // Check the success of the request
        if (substr($response, 0, 3) ==  'ERR') {
            $error_message = substr($response, 5);
            $error_code = substr($error_message, 0, 3);

            // If the session id is invalid then recreate a session and resend (but only once)
            if (!$recreated_session && ($error_code < self::CLICKATELL_SMS_ERROR_CONNECTION_ISSUE)) {
                $this->session->create(true);
                return $this->doSend($check_pre_balance, $check_post_balance, true);
            }

            return false;
        }

        $this->message_id = substr($response, 4);

        if ($this->logMessages) {
            //@todo logg messages somewhere
        }

        // Check the remaining balance, and alert the admin if necessary
        if ($check_post_balance) {
            $balance = $this->fetchBalance();
            if ($balance  < (int)self::SMS_LOW_CREDIT_THRESHOLD) {
                $this->sendDwindlingCreditsEmail(self::SMS_ADMIN_EMAIL_ADDRESS, $balance);
            }
        }

        return true;
    }

    /**
     * Checks the validity of the data before sending an SMS.
     *
     * Checks for required fields, and makes sure a network is set if SMS is premium
     *
     * @access public
     * @return boolean TRUE if SMS is valid, FALSE if otherwise
     */
    public function isValid()
    {
        $valid = true;

        if (!$this->to) {
            Ovoyo_ErrorHandler::addError('No number specified');
            $valid = false;
        }

        if (!$this->text) {
            Ovoyo_ErrorHandler::addError('Message is empty');
            $valid = false;
        }

        return $valid;
    }

    /**
     * Fetches the SMS account balance
     *
     * Returns the account balance in credits if fetch was successful, or FALSE if incorrect access details were sent
     *
     * @access public
     * @param string $username The username for access to the SMS system
     * @param string $password The password for access to the SMS system
     * @param string $account The account for access to the SMS system
     * @return mixed integer account balance if successful fetch, boolean FALSE if incorrect access details were sent
     */
    public function fetchBalance()
    {
        if (!is_object($this->session)) {
            return false;
        }

        $parameters = array('session_id' => $this->session->sessionId);

        $balance_return = $this->sendHttpRequest(self::CLICKATELL_HTTP_BALANCE_INTERFACE, $parameters);

        return (substr($balance_return, 0, 3) == 'ERR' || !$balance_return) ? false : substr($balance_return, 8);
    }

    /**
     * Sends an HTTP request to the specific URL, and attaches any parameters
     *
     * @access public
     * @param array Associative array of parameters to pass
     * @return mixed String of the returned value of the HTTP request, FALSE if there was an error sending the HTTP request
     */
    public function sendHttpRequest($url, &$parameters)
    {
        $client = $this->httpClient;
        $client->resetParameters();

        $client->setUri($url);

        $client->setParameterGet($parameters);

        $response = $client->request();

        if($response->isError()){
            return false;
        }

        return $response->getBody();
    }

    /**
     * Send an email stating that the SMS account has run out of credits
     *
     * @access private
     * @param string $email_address Email address to which the email should be sent
     * @return boolean TRUE if mail sent successful, FALSE if otherwise
     */
    public function sendInsufficientCreditsEmail($email_address) {
        $host       = $_SERVER['HTTP_HOST'];
        $from_email = self::SMS_ADMIN_EMAIL_FROM.'<'.self::SMS_ADMIN_EMAIL_ADDRESS.'>';
        $subject    = self::SMS_ADMIN_EMAIL_INSUFFICIENT_SUBJECT;

        // Text version of mail
        $text.= 'Warning, the SMS sender for '.$this->siteName." has run out of credits. ".$this->fetchBalance($this->username, $this->password, $this->account)."\n\n";

        // HTML version of mail
        $html.= '<html><body><font face="arial" size="-1" color="red">Warning, the SMS sender for '.$this->siteName." has run out of credits.".$this->fetchBalance($this->username, $this->password, $this->account)."\n\n</font></body></html>";

        $mail = new Zend_Mail();
        $tr = new Zend_Mail_Transport_Sendmail('-f'.$from_email);
        Zend_Mail::setDefaultTransport($tr);

        $mail->setFrom($from_email);
        $mail->setSubject(stripslashes($subject));

        $mail->setBodyText($text);
        $mail->setBodyHtml($html);

        $mail->addTo($email_address);

        return $mail->send();
    }

    /**
     * Send an email stating that the SMS account is nearly out of credits
     *
     * @access private
     * @param string $email_address Email address to which the email should be sent
     * @return boolean TRUE if mail sent successful, FALSE if otherwise
     */
    public function sendDwindlingCreditsEmail ($email_address, $balance) {
        $host       = $_SERVER['HTTP_HOST'];
        $from_email = self::SMS_ADMIN_EMAIL_FROM.'<'.self::SMS_ADMIN_EMAIL_ADDRESS.'>';
        $subject    = self::SMS_ADMIN_EMAIL_DWINDLING_SUBJECT;

        // Text version of mail
        $text.= 'Warning, the SMS sender for '.$this->siteName." is nearly out of credits\n\n";

        // HTML version of mail
        $html.= '<html><body><font face="arial" size="-1" color="red">Warning, the SMS sender for '.$this->siteName." is nearly out of credits - there are $balance left.\n\n</font></body></html>";

        $mail = new Zend_Mail();
        $tr = new Zend_Mail_Transport_Sendmail('-f'.$from_email);
        Zend_Mail::setDefaultTransport($tr);

        $mail->setFrom($from_email);
        $mail->setSubject(stripslashes($subject));

        $mail->setBodyText($text);
        $mail->setBodyHtml($html);

        $mail->addTo($email_address);

        return $mail->send();
    }

}

