<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Services_PaymentProvider_Abstract
 */
require_once 'Ovoyo/Services/PaymentProvider/Abstract.php';

/**
 * Epdq class
 *
 * @package    Ovoyo_Services_PaymentProvider
 * @TODO remove comments/var dump
 */
class Ovoyo_Services_PaymentProvider_Invoice extends Ovoyo_Services_PaymentProvider_Abstract
{
    public $paymentDetails = array(); // populated after callback
    protected $_callbackUrl;

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::_init()
     */
    protected function _init()
    {

    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract#sendPayment()
     */
    public function sendPayment(array $vars)
    {
        if ($this->_removeCallbackHttp) {
            $this->_callbackUrl = str_replace('http://', '', $this->_callbackUrl);
        }

        $this->_callbackUrl .= '?paymentId=' . $vars['paymentId']
                            .= '&providerAccountId=' . $vars['providerAccountId'];
        header("Location: $this->_callbackUrl");
        exit;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_Abstract::validateCallback()
     */
    public function validateCallback($data = array())
    {
        $valid = false;

        // check url has come from website
        $url = 'http:\/\/' . $_SERVER['HTTP_HOST'];
        if (preg_match("/^$url/", $_SERVER['HTTP_REFERER'])) {
            $valid = true;
        }

        return $valid;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract#handleCallback()
     */
    public function handleCallback(array $data)
    {
        // fetch the payment
        $payment = Ovoyo_Services_PaymentProvider::getPayment(
            array('connName' => $this->_connName)
        );
        if (!$payment->fetch($data['paymentId'])) {
            return false;
        }

        $this->paymentId = $payment->paymentId;

        // update payment details
        $payment->status = 'Y';
        $payment->callbackData = $data;
        $payment->paid = 1;
        $payment->paidDate = new Zend_Date;

        if (!$payment->update()) {
            return false;
        }

        $this->amount = $payment->amount;
        $this->paymentDetails = $payment->details;
        $this->paymentDetails['paymentId'] = $payment->paymentId;

        return $payment->paid;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract#completePayment()
     */
    public function completePayment()
    {
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::processPaymentInfo()
     */
    public function processPaymentInfo(array $data)
    {
        $paid = $this->handleCallback($data);
        if ($paid) {
            $result = array();
            $result['paymentId'] = $this->paymentId;
        } else {
            $result = false;
        }

        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::postConvertToOrder()
     */
    public function postConvertToOrder($config)
    {

    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::prepareData()
     */
    public function prepareData(array $data)
    {
        return $data;
    }
}
