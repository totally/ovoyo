<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * User interface
 *
 * @package    Ovoyo_Authentication
 * @subpackage  Interface
 */
interface Ovoyo_Authentication_Interface_User
{
    /**
     * Set hash algorithm to use when encrypting/decrypting passwords
     *
     * @param string $hashType
     * @return void
     */
    public function setHashType($hashType);
    
    /**
     * Set user session instance
     *
     * @param Ovoyo_Authentication_UserSession $userSession
     * @return void
     */
    public function setUserSession($userSession);
    
    /**
     * Perform login
     *
     * @param string $email Email
     * @param string $password Password
     * @param string $sessionIdentifier Session identifier
     * @return boolean
     */
    public function login($email, $password, $sessionIdentifier);
    
    
    /**
     * Perform logout
     *
     * @param string $sessionIdentifier Session identifier
     * @return boolean
     */
    public function logout($sessionIdentifier);

    /**
     * Pre login actions
     *
     * @return void
     */
    public function preLogin();
    
    /**
     * Post login actions
     *
     * @return void
     */
    public function postLogin();
    
    /**
     * Pre logout actions
     *
     * @return void
     */
    public function preLogout();
    
    /**
     * Post logout actions
     *
     * @return void
     */
    public function postLogout();
}