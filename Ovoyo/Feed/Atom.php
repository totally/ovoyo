<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Feed_Abstract
 */
require_once 'Ovoyo/Feed/Abstract.php';
require_once 'Zend/Feed.php';

/**
 * Atom Feed class
 *
 * @package    Ovoyo_Feed
 */
class Ovoyo_Feed_Atom extends Ovoyo_Feed_Abstract
{
    
    /** 
     * Converts Data to a Zend RSS Feed
     *
     * @param   array $data Formatted Feed Array
     * @return  void
     */
    public function convertDataToFeed($data)
    {

        $this->feed = Zend_Feed::importArray($data, 'atom');
     
    }

}
