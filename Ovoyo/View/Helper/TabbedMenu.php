<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Class which will output a menu 
 *
 * @package    Ovoyo_View
 */
class Ovoyo_View_Helper_TabbedMenu
{
    /**
     * Constructor
     *
     * @param   Ovoyo_Menu $menu Menu data
     * @param   Ovoyo_View $view
     */
    public function tabbedMenu($menu, $view)
        {
        list($selected, $selectedId, $selectedElement) = $menu->getSelected();
        $html = '<ul id="tabbedMenu' . ucfirst($menu->id) . '" class="tabs';

        if ($menu->class) {
            $html.= ' '.$menu->class;
        }

        $html.= '">'."\n";

        // parse elements
        if (is_array($menu->elements)) {
            foreach ($menu->elements AS $id => $element) {
                $selected = (!is_null($selectedId) && $id == $selectedId) ? true : false;
                if ($element instanceof Ovoyo_Menu_Element) {
                    $element->setViewHelper('tabbedMenuElement');
                    $html.= $element->render($view, $selected);
                }
            }
        }

        $html.= "</ul>\n";

        return $html;
    }
}
