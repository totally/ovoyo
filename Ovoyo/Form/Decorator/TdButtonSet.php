<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Zend_Form_Decorator_Abstract
 */
require_once 'Zend/Form/Decorator/Abstract.php';

/**
 * For displaying a set of buttons in a table
 *
 * Single static method to take a Ovoyo_Model instance (or sub class of)
 * and create a Zend_Form instance from it. Based on code found in the following article:
 * http://codecaine.co.za/posts/form-generation-with-zend-form-part-2/
 *
 * @package    Ovoyo_Form
 */
class Ovoyo_Form_Decorator_TdButtonSet extends Zend_Form_Decorator_Abstract
{
    private $_secondarySeparator = '|';

    /**
     * Render
     *
     * @param  string $content
     * @return string
     */
    public function render($content)
    {
        $displayGroup = $this->getElement();
        $view         = $displayGroup->getView();
        if (null === $view) {
            return $content;
        }

        $elements = $displayGroup->getElements();

        // separate primary and seconday elements
        $primary   = array();
        $secondary = array();
        foreach ($elements AS $element) {
            if (!$element instanceof Zend_Form_Element_Submit &&
                !$element instanceof Zend_Form_Element_Button &&
                !$element instanceof Zend_Form_Element_Reset) { continue; }

            $attribs = $element->getAttribs();
            $type = ($attribs['class']) ? $attribs['class'] : 'primary';
            if ($type == 'primary') {
                $primary[] = $element;
            } else {
                $secondary[] = $element;
            }
        }

        $label = $displayGroup->getAttrib('label');
        $label = ($label) ? $label . ' &rsaquo;&rsaquo;' : '&nbsp;';
        
        $buttonSet = '<tr class="spacer"><td colspan="2"></td></tr>' . "\n"
                   . '<tr class="buttonSet">' . "\n"
                   . "\t" . '<td class="label">' . $label . '</td>' . "\n"
                   . "\t" . '<td class="element">' . "\n";

        // display primary elements
        foreach ($primary AS $element) {
            $element->setDecorators(array('FormElements', 'ViewHelper'));

            $buttonSet.= "\t\t"
                       . $element->render($view)
                       . "\n";
        }

        // display secondary elements
        foreach ($secondary AS $element) {
            $element->setDecorators(array('FormElements', 'ViewHelper'));

            $buttonSet.= "\t\t"
                       . $this->_secondarySeparator . ' '
                       . $element->render($view)
                       . "\n";
        }


        return $buttonSet;
    }
}
