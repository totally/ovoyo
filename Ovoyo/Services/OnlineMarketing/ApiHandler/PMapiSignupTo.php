<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Signupto v1.0 Persist Marketing API handler
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage ApiHandler
 */
class Ovoyo_Services_OnlineMarketing_ApiHandler_PMapiSignupTo
      extends Ovoyo_Services_OnlineMarketing_ApiHandler
{
    protected $uid;
    protected $cid;
    protected $hash;
    protected $endpoint;
    protected $rawResponse;
    protected $headers;
    protected $_baseApiParams;
    
    const SUT_API_HASH_LEN = 32;
    const DATE_RFC1123 = '%a, %d %h %Y %H:%M:%S';
    
    const API_SCHEME = 'https';
    const API_SERVER = 'api.sign-up.to';
    const API_VERSION = 1;
    const RESPONSE_FORMAT = 'application/json';
    
    /**
     * Constructor
     * 
     * @param   array $config Config options
     * @return  void
     */
    public function __construct($config)
    {
        // check for required options
        if (!isset($config['cid'])) {
            throw new Exception('cid is a required config option');
        }
        
        if (!isset($config['uid'])) {
            throw new Exception('uid is a required config option');
        }
        
        $this->cid  = (int) $config['cid'];
        $this->uid  = (int) $config['uid'];
        $hash = $config['hash'];
        
        if ((strlen($hash) != self::SUT_API_HASH_LEN)
            || (strspn($hash, '0123456789abcdef') != self::SUT_API_HASH_LEN)) {
            throw new Exception("Invalid API hash value $hash");
        }
        
        $this->hash = $hash;
        $this->verb = (isset($config['verb']) ? $config['verb'] : 'post');
        
        $this->_baseApiParams = array(
            'cid' => $this->cid,
            'hash' => $this->hash
        );
    }
    
    /**
     * Get Header for api call
     *
     * @param   string $verb call type to call
     * @param   array $params Optional params to add as querystring to API url
     * @return  array Headers
     */
    protected function _getHeaders($verb, $version, $endpoint)
    {
        $headers = array(
            'Date'             => $this->_getDate(),
            'X-SuT-CID'        => $this->cid,
            'X-SuT-UID'        => $this->uid,
            'X-SuT-Nonce'      => $this->_getNonce(),
        );
        
        $headers_ = array(strtoupper($verb) . " /v$version/$endpoint");
        foreach ($headers as $header => $value) {
            $headers_[] = "$header: $value";
        }
        $headers_[] = $this->hash;
        
        $sig_string = join("\r\n", $headers_);
        $signature = sha1($sig_string);
        
        $headers['Authorization'] = "SuTHash signature=\"$signature\"";
        
        return $headers;
    }
    
    /**
     * Make a call to api
     *
     * @param   string $url Url to call
     * @param   array $params Optional params to add as querystring to API url
     * @return  string Response from API call
     * @throws  Exception
     */
    public function callApi($endpoint, array $params = null)
    {
        if (!isset($endpoint)) {
            throw new Exception("Endpoint isn't defined");
            return false;
        }
        
        foreach ($params as $key => $val) {
            if (!strlen($key)) {
                throw new Exception("Invalid argument '$key' and value '$val'");
            }
        }
        
        $response = null;
        $this->endpoint = $endpoint;
        
        $url = self::API_SCHEME . "://" . self::API_SERVER . "/v" . self::API_VERSION . "/{$this->endpoint}";
        $verb = $this->verb;
        
        $headers = $this->_getHeaders($verb, self::API_VERSION, $this->endpoint);
        $headers['Accept'] = self::RESPONSE_FORMAT;
        
        $curl = curl_init();
        $curl_opts = array();
        
        switch($verb) {
            case 'post':
                $curl_opts[CURLOPT_POST] = true;
                if (!empty($params)) {
                    $curl_opts[CURLOPT_POSTFIELDS] = json_encode($params);
                    $headers['Content-Type'] = 'application/json';
                }
                break;
            
            case 'put':
                $fp = fopen('php://temp', 'w');
                fwrite($fp, json_encode((array) $params));
                $len = ftell($fp);
                fseek($fp, 0);
                $curl_opts[CURLOPT_PUT] = 1;
                $curl_opts[CURLOPT_INFILE] = $fp;
                $curl_opts[CURLOPT_INFILESIZE] = $len;
                $headers['Content-Type'] = 'application/json';
                break;
            
            case 'delete':
            case 'head':
                $curl_opts[CURLOPT_CUSTOMREQUEST] = strtoupper($verb);
            case 'get':
                if (count($params)) {
                    $url .= '?' . http_build_query((array) $params);
                }
                break;
        }
        
        $headers_ = array();
        foreach ($headers as $k => $v) {
            $headers_[] = strtoupper(
                str_replace('X_SUT', 'HTTP_X_SUT', str_replace('_', '-', $k))) . ": $v";
        }
        
        $curl_opts += array(
            CURLOPT_URL              => $url,
            CURLOPT_HTTPHEADER       => $headers_,
            CURLOPT_WRITEFUNCTION    => array($this, 'receiveResponse'),
            CURLOPT_HEADERFUNCTION   => array($this, 'receiveHeaders'),
        );
        
        if ($this->debugMode) {
            $curl_opts[CURLOPT_SSL_VERIFYPEER] = false;
        }
        
        curl_setopt_array($curl, $curl_opts);
        
        if (!curl_exec($curl)) {
            throw new Exception(curl_error($curl), curl_errno($curl));
        }
        
        $this->finalise(curl_getinfo($curl, CURLINFO_HTTP_CODE));
        
        return $this->response;
    }
    
    /**
     * Finalise the curl request
     *
     * @param   string $url Url to call
     * @param   array $params Optional params to add as querystring to API url
     * @return  string Response from API call
     * @throws  Exception
     */
    public function finalise($status)
    {
        $this->status = $status;
        
        if (($this->verb == 'head') && !strlen($this->response)) {
            return;        // No response text expected for HEAD requests
        }
        
        if (!($this->response = json_decode($this->rawResponse, true))) {
            throw new Exception("Invalid server response: empty response");
        }
        
        $this->rawResponse = null;
        
        // Validate response
        foreach (array('status', 'response') as $required) {
            if (!isset($this->response[$required])) {
                throw new Exception("Invalid server response: missing field [$required]");
            }
            
            if (!is_array($this->response['response'])) {
                throw new Exception("Invalid server response: [response] is not an array");
            }
            
            $resp =& $this->response['response'];
            switch ($this->response['status']) {
                case 'ok':
                    foreach (array('count', 'data') as $required) {
                        if (!isset($resp[$required])) {
                            throw new Exception("Invalid server response: missing field [response][$required]");
                        }
                        
                        if ($this->verb == 'get') {
                            if (!array_key_exists('next', $resp)) {   // resp[next] can be null
                                throw new Exception("Invalid server response: missing field [response][next]");
                            }
                            
                            if (!is_array($resp['data'])) {
                                throw new Exception("Invalid server response: is not an array");
                                
                                $this->count = (int) $resp['count'];
                                $this->next = $resp['next'];
                                $this->isError = false;
                            }
                        }
                    }
                    break;
                    
                case 'error':
                    $this->isError = true;
                    $this->error = (isset($resp['message']) ? $resp['message'] : self::UNKNOWN_ERROR_MSG);
                    break;
                
                default:
                    throw new Exception("Invalid server response: unknown status '{$this->response['status']}'");
            }
        }
    }
    
    /**
     * cURL callback: receives raw response text from peer server.
     *
     * @param   string $url Url to call
     * @param   array $params Optional params to add as querystring to API url
     * @return  integer string length
     */
    public function receiveResponse($curl, $data)
    {
        $this->rawResponse .= $data;
        return strlen($data);
    }
    
    /**
     * cURL callback: receives header data from peer server.
     *
     * @param   obj $curl 
     * @param   array $data
     * @return  integer string length
     */
    public function receiveHeaders($curl, $data)
    {
        $this->headers .= $data;
        return strlen($data);
    }
    
    /**
     * Create random number for securtiy validation
     *
     * @return  string Generated string
     */
    protected function _getNonce()
    {
        return sha1(uniqid(mt_rand(), true));
    }
    
    /**
     * Get date
     *
     * @return  string date
     */
    protected function _getDate()
    {
        // The HTTP "Date:" header must contain an English-language string.  If the current date/
        // time locale is not en_*, attempt to change it in order to format the "Date:" header
        // correctly.
        $locale_changed = false;
        $time_locale = setlocale(LC_TIME, '0');
        if (strpos('en_', $time_locale) !== 0) {
            // Try to select any English-language locale for date/times.
            if (!setlocale(
                    LC_TIME, 
                    array(
                        'en_US', 
                        'en_US.utf8', 
                        'en_GB', 
                        'en_GB.utf8', 
                        'en_CA', 
                        'en_CA.utf8', 
                        'en_AU', 
                        'en_AU.utf8', 
                        'en_NZ', 
                        'en_NZ.utf8'
                    ))) {
                
                throw new Exception("Cannot set en_* locale for request");
            }
            
            $locale_changed = true;
        }
        
        $sDate = gmstrftime(self::DATE_RFC1123 . ' GMT');
        
        // If the date/time locale was changed above, attempt to restore the original setting.
        if ($locale_changed) {
            setlocale(LC_TIME, $time_locale);
        }
        
        return $sDate;
    }
}
