<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Class for accessing the SamKnows API
 * SamKnows.com is an on-line broadband availability checker.
 *
 * @package Ovoyo_Services
 * @subpackage ???
 */
class Ovoyo_Services_SamKnows 
{
    /**
     * Provider status constants.
     */
    const STATUS_DISABLED     = 1;
    const STATUS_ENABLED      = 2;
    const STATUS_RFS_DATE_SET = 4;
    
    const RETURN_RAW = 1;
    const RETURN_EXCHANGE = 2;
    const RETURN_PROVIDERS = 3;
    const RETURN_BOTH = 4;

    /**
     * @var string The userName that will be used for all API requests.
     */
    protected $_userName;

    /**
     * @var string The password that will be used for all API requests.
     */
    protected $_password;

    /**
     * @var string The description of the last error that occured.  Blank
     *   if no request has been made, or if the last request was successful.
     */
    protected $_lastError = "";
    
////////////////////// CONSTRUCTOR
    
    /**
     * Constructor
     * Initialise the class with the appropriate API connection details.
     * You will need to {@link http://www.samknows.com/broadband/dataoutput contact SamKnows}
     * to apply for access, which will be granted after signing of a contract,
     * which they will send to you by return of e-mail.
     * 
     * @param string $userName The user name (normally an e-mail address) that
     *   will be used to access the API.
     * @param string $password The password that will be used to access the API.
     */
    public function __construct($userName, $password)
    {
        $this->_userName = $userName;
        $this->_password = $password;
    }
    
    
    /**
     * Returns the full URL that should be used to access the
     * SamKnows API using the given arguments.
     * 
     * @param array $args An array of key => value pairs, which will
     *   be included in the query string.  See the SamKnows API
     *   documentation for details.  'user', 'pass', 'output' and 'checks'
     *   should not be included, and will be ignored if present.
     *   The correct values for these fields will be added automatically.
     *   Note that args don't need tidying - the API handles
     *   variation in postcode/phone/etc. caused by white-space,
     *   casing or extra characters (e.g. brackets in phone number).
     * @param array $providers (optional) An array of provider IDs (as defined
     *   in the SamKnows API document).
     */
    public function _getRequestUrl($args, $providers = array()) {
        // Set the base URL to be used for the request.
        $url = "http://api.samknows.com/checker.do";
        
        // Add the authentication parameters to the argument array.
        $args['user'] = $this->_userName;
        $args['pass'] = $this->_password;
        $args['output'] = "php";
        
        // If the $providers array is non-empty, add the list of providers
        // as the 'check' argument (it's a comma-separated string).
        if (count($providers) > 0) {
            $args['checks'] = implode(",", $providers);
        // Otherwise, unset the 'checks' argument, if present.
        } else {
            unset($args['checks']);
        }
        
        // Add all request parameters to the URL.
        // We first populate the $args array with the URL-encoded
        // parameter/value pairs, and then implode this into a query string
        // which is appended to the URL.
        foreach ($args as $parameter => $value) {
            $args[$parameter] = rawurlencode($parameter) . "="
            . rawurlencode($value);
        }
        $url .= "?" . implode("&", $args);
        
        return $url;
    }
    
    /**
     * _performRequest()
     * Returns the raw response, as returned by the SamKnows API, or false
     * on error.  If an error occurs, then {@see getLastError()} may
     * be called to get the error description.
     * Note: For simplicity we use the 'php' output format.  However the 'xml'
     *       format provides a small amount of additional data (e.g. error
     *       messages as well as error codes) and may be better from a security
     *       point of view.
     * TODO: Consider switching to the XML output format.
     *
     * @param array $args {@see _getRequestUrl()}.
     * @param array $providers (optional)  {@see _getRequestUrl()}.
     * @return boolean|array Returns false on error, or the unserialized
     *   array, as returned by SamKnows.  Consult their API docs for more
     *   information.
     */
    protected function _performRequest($args, $providers = array()) {
        // Initialise the last error to "" to indicate success.
        // Fail conditions will over-ride this with the appropriate message.
        $this->_lastError = "";
    
        // Remove any blank arguments from the input array.
        foreach ($args as $key => $value) {
            if ($value == "") {
                unset($args[$key]);
            }
        }
    
        // Check that either the 'phone' or 'postcode' argument is set.
        // One of these is required.
        if (!isset($args['phone']) && !isset($args['postcode'])) {
            $this->_lastError = "Either phone or postcode must be provided.";
            return false;
        }
    
        // Check phone-number validity.
        if (isset($args['phone'])) {
            // Tidy string.  Not required for submission to SamKnows, but
            // simplifies the checks we are about to perform.
            // This code removes all characters except the numbers 0-9 and
            // the + sign.
            $args['phone'] = preg_replace('/[^+0-9]/', "", $args['phone']);
    
            // Convert international-style UK phone-numbers into
            // non-international variants, by replacing +44 with 0.
            $args['phone'] = preg_replace('/^\+44/', "0", $args['phone']);
    
            // Report any other international numbers as errors.
            if (substr($args['phone'], 0, 1) == "+") {
                $this->_lastError = "Must be a UK phone number.";
                return false;
            }
    
            // Report any non-geographic numbers.
            $arrValidPrefixes = array(
                                    "01", // Geographic area codes
                                    "02", // New geographic area codes
                                    "04", // Reserved (future-compatibility)
                                    "05", // Corporate numbering
                                    "06", // Unused (future compatibility)
                                );
            if (!in_array(substr($args['phone'], 0, 2), $arrValidPrefixes)) {
                $this->_lastError = "Must be a landline.";
                return false;
            }
        }
    
        // Get the API URL
        $url = $this->_getRequestUrl($args, $providers);
    
        // Call the API.
        // We use @ to suppress any errors, as they will be reported via
        // our existing mechanisms, and should not raise PHP errors.
        // The result must be a non-blank string, otherwise there was a
        // communication problem.
        $result = @file_get_contents($url);
        if (!is_string($result) || $result == "") {
            $this->_lastError = "Unable to contact server.";
            return false;
        }
    
        // Try to unserialize the result.
        // If this fails then we didn't get a valid PHP serialized string, as
        // expected, so report the error.  We use @ to suppress any errors
        // generated by the unserialize() function, as we are checking for
        // them manually.
        $result = @unserialize($result);
        if (!is_array($result)) {
            $this->_lastError = "Malformed response.";
            return false;
        }
    
        // Check for common error codes.
        // If a phone number was supplied, but an error occurred, report it.
        if ($result['inputs']['phone']['phone'] != ""
            && $result['inputs']['phone']['errorcode'] != 0)
        {
            $this->_lastError = "Bad phone number.";
            return false;
        }
        // If a postcode was supplied, but an error occurred, report it.
        if ($result['inputs']['postcode']['errorcode'] != 0
            && $result['inputs']['postcode']['postcode'] != "")
        {
            $this->_lastError = "Bad postcode.";
            return false;
        }

        // Return the unserialized PHP array.
        return $result;
    }
    
    /**
     * getLastError()
     * 
     * @return string Returns the text description of the last error that
     *   occurred.  Will return an empty string if the last API request was
     *   successful, or no API request has been run.
     */
    public function getLastError() 
    {
        return $this->_lastError;
    }
    
    /**
     * Perform raw query of API
     *
     * $args - please see the API documentation for full list of args.  Some of
     * the key fields are:
     *   'phone' - must be supplied if 'postcode' is blank
     *   'postcode' - must be supplied if 'phone' is blank
     *   'buildingname'
     *   'buildingnum'
     *   'street'
     *   'town'
     *   'city'
     *   
     * $returnType - specifies what information should be returned.  One of:
     *   Ovoyo_Services_SamKnows::RETURN_RAW - return raw result from api call
     *   Ovoyo_Services_SamKnows::RETURN_EXCHANGE - return just exchange info
     *   Ovoyo_Services_SamKnows::RETURN_PROVIDERS - return just provider info
     *   Ovoyo_Services_SamKnows::RETURN_BOTH - return both exchange and
     *       provider info
     *   
     * $options - allows restrictions to be passed on which providers to fetch.
     * Allowed values are:
     *  'providers' - an array of providers to limit results to. In addition, 
     *     the following short-hand codes are provided:
     *         'all' - Include all providers, as defined at date of writing.
     *         'bt'  - Include all BT Wholesale providers in the results.
     *  'includeRFSPending'- if set to true to also include providers with 
     *     RFS (Ready For Service) date set.  These providers are not 
     *     currently enabled, but may be enabled soon.  Note that RFS dates 
     *     are fairly speculative, and may change at any point
     *     (in either direction) so should not be relied upon if you need to
     *     guarantee future availability.  Defaults to false, if omitted.
     *
     * @param array $args
     * @param int $returnType 
     * @param array $options
     * @return false|array
     */
    public function queryApi($args, $returnType = self::RETURN_BOTH, $options = array())
    {
        $providerAliases = self::getProviderAliases();
        
        // If $providers is not already an array, turn it into one.
        // This means that you can pass in a string instead of a single-element
        // array, which is a little simpler for this common use-case.
        $providers = $options['providers'];
        if (!is_array($providers)) {
            // If item is not a string, or is the empty string, then set the
            // array to be empty.
            if (!is_string($providers) || $providers === "") {
                $providers = array();
            // Otherwise, convert the argument into an array.
            } else {
                $providers = array($providers);
            }
        }

        // If there are no providers specified, we default to 'all'.
        if (count($providers) == 0) {
            $providers[] = 'all';
        }
        
        // Expand any aliases.
        foreach ($providerAliases as $alias => $replacements) {
            if (in_array($alias, $providers)) {
                unset($providers[$alias]);
                $providers = array_merge($providers, $replacements);
            }
        }

        // Remove any duplicates from the providers array.
        $providers = array_unique($providers);
        
        // Run the query.
        $apiCallResult = $this->_performRequest($args, $providers);
        if ($apiCallResult == false) {
            return $apiCallResult;
        }
        
        // return raw result
        if ($returnType == self::RETURN_RAW) {
            return $apiCallResult;
        }
        
        $result = array();
        
        // return exchange
        if ($returnType == self::RETURN_EXCHANGE || $returnType == self::RETURN_BOTH) {
            $result['exchange'] = $apiCallResult['exchange'];
        }
        
        // return providers
        if ($returnType == self::RETURN_PROVIDERS || $returnType == self::RETURN_BOTH) {
            $providers = $apiCallResult['checks'];
        
            // Remove any providers who are not enabled at the exchange.
            foreach ($providers as $provider => $data) {
                switch ($data['status']) {
        
                    // If the provider is disabled, remove them.
                    case self::STATUS_DISABLED:
                        unset($providers[$provider]);
                        break;
        
                        // If the provider has an RFS (Ready For Service) date
                        // set, but is not currently enabled, remove them unless
                        // the $includeRFSPending flag has been set.
                    case self::STATUS_RFS_DATE_SET:
                        if (!$options['includeRFSPending']) {
                            unset($providers[$provider]);
                        }
                        break;
                }
            }
        
            // Sort the provider list by key (which is the provider ID as
            // passed into the function).
            ksort($providers);
            
            $result['providers'] = $providers;
        }
        
        return $result;
    }
    
    /**
     * Shorthand method for fetching just the exchange details for a location
     *   
     * @param array $args {@see queryApi()}.
     * @return false|array
     */
    public function getExchange($args) 
    {
        return $this->queryApi($args, self::RETURN_EXCHANGE);
    }

    /**
     * Shorthand method for fetching just the provider details for a location
     * 
     * @param array $args {@see queryApi()}.
     * @param array $options {@see queryApi()}.
     * @return false|array
     */
    public function getProviders($args, $options = array())
    {
       return $this->queryApi($args, self::RETURN_PROVIDERS, $options);
    }

    /**
     * Get list of provider aliases
     * 
     * @return array
     */
    public static function getProviderAliases()
    {
        
        // Define our aliases as a static array (so that it doesn't need to
        // be redefined on subsequent calls).
        // Note that these may go out-of-date as the SamKnows provider
        // list changes, so keep an eye on that!
        static $aliases = array(
            'all' => array(
                'adsl', 'adslmax', 'sdsl', 'wbc', '21cn', 'fttc', 'ol',
                'be', 'bulldog', 'cpw', 'edge',
                'easynet', 'homechoice', 'lumison',
                'onlincs', 'kijoma', 'emnet', 'uwimax',
                'ltt', 'node4', 'now', 'ntl', 'pipex',
                'smallworld_llu', 'smallworld_cable',
                'telewest', 'tiscali', 'virgin',
                'wanadoo', 'wbi', 'zen'
            ),
            'bt' => array(
                'adsl', 'adslmax', 'sdsl',  'wbc', '21cn',
                'fttc'
            )
        );
        
        return $aliases;
    }
}
