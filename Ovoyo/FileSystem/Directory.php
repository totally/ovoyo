<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_ErrorHandler
 */
require_once 'Ovoyo/ErrorHandler.php';

/**
 * Directory class
 *
 * @package    Ovoyo_FileSystem
 */
class Ovoyo_FileSystem_Directory
{
    public $directory;
    
    const UMASK = 0755;
    
    /**
     * Constructor
     *
     * @param   string $directory Path to directory
     * @param   array $config
     */
    public function __construct($directory)
    {   
        // check directory does not have any ".." calls in it, this could be 
        // a security threat so disallow
        if (preg_match("/\.\./", $directory)) {
            throw new Exception('Invalid directory name provided - possible security threat.');
        }
        
        $this->directory = preg_replace(
            '/\\' . DIRECTORY_SEPARATOR . '{1,}$/', '', $directory
        );
    }
    
    /**
     * Fetch list of items in a directory
     *
     * $returnType values:
     * - 0 = everything (default)
     * - 1 = directoriess only, 
     * - 2 = files only
     *
     * @param   int $returnType 
     * @return  array
     */
    public function getContents($returnType = 0)
    {
        $directoryContents = array();
        if (!is_dir($this->directory)) { 
            return $directoryContents; 
        }
        
        $contents    = scandir($this->directory);
        $directories = array();
        $files       = array();
        foreach ($contents AS $item) {
            // ignore hidden files
            if (preg_match("/^\./", $item)) { 
                continue; 
            }
            
            $itemPath = $this->directory . DIRECTORY_SEPARATOR . $item;
            
            // return only directorys
            if ($returnType == 1 && !is_dir($itemPath)) { 
                continue; 
            }
            
            // return only files
            if ($returnType == 2 && !is_file($itemPath)) { 
                continue; 
            }
            
            $info = array();
            $info['name'] = $item;
            $info['path'] = $itemPath;
            $info['type'] = self::getItemType($item);
            $info['mime_type'] = Ovoyo_MimeType::getMimeType($itemPath);
            $info['size'] = self::getItemSize($item);
            $info['lastUpdated'] = self::getItemLastUpdated($item);
            
            if (is_dir($itemPath)) {
                $directories[$item] = $info;
            } else {
                $files[$item] = $info;
            }
        }
        
        ksort($directories);
        ksort($files);
        
        // build contents manually instead of using array_merge otherwise causes 
        // issues with directories with numerical names
        $directoryContents = array();
        foreach ($directories AS $item => $info) {
            $directoryContents[$item] = $info;
        }
        foreach ($files AS $item => $info) {
            $directoryContents[$item] = $info;
        }
        
        return $directoryContents;
    }
    
    /**
     * Create new sub-directory
     *
     * @param   string $directoryName New directory name
     * @param   string $umask
     * @return  boolean
     */
    public function createSubDirectory($directoryName, $umask = self::UMASK)
    {
        // make sure we have a directory name
        if (empty($directoryName)) {
            Ovoyo_ErrorHandler::addError('Directory name is required');
            return false;
        }
        
        // make sure directory only contains alpha numeric chars
        require_once 'Zend/Validate/Regex.php';
        $validator = new Zend_Validate_Regex("/^([A-Za-z]|[0-9]|_|-|\s)+$/");
        if (!$validator->isValid($directoryName)) {
            $error = 'Directory name can only contain letters, numbers, ' 
                   . 'spaces, underscores and hyphens';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }
        
        $directory = $this->directory . DIRECTORY_SEPARATOR . $directoryName;
        if (is_dir($directory)) {
            $error = 'A directory already exists with the same name';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }
        
        // create new directory
        @mkdir($directory);
        if (!is_dir($directory)) {
            $exception = 'An unknown problem occurred when trying to add new' 
                       . 'directory';
            throw new Exception($exception);
        }
        @chmod($directory, $umask);
        
        return true;
    }
    
    /**
     * Rename directory
     *
     * @param   string $directoryName New directory name
     * @return  boolean
     */
    public function rename($directoryName)
    {
        $oldName = basename($this->directory);
        $directory = preg_replace(
            '/' . $oldName . "$/", $directoryName, $this->directory
        );
                
        // make sure we have a directory name
        if (empty($directoryName)) {
            Ovoyo_ErrorHandler::addError('Directory name is required');
            return false;
        }
        
        // check if name has changed
        if ($directory == $this->directory) {
            Ovoyo_ErrorHandler::addError('Name is the same as original');
            return false;
        }
        
        // make sure directory only contains alpha numeric chars
        require_once 'Zend/Validate/Regex.php';
        $validator = new Zend_Validate_Regex("/^([A-Za-z]|[0-9]|_|-|\s)+$/");
        if (!$validator->isValid($directoryName)) {
            $error = 'Directory name can only contain letters, numbers, ' 
                   . 'spaces, underscores and hyphens';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }
        
        if (is_dir($directory)) {
            $error = 'A directory already exists with the same name';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }
        
        // create new directory
        if (!rename($this->directory, $directory)) {
            $exception = 'An unknown problem occurred when trying to rename' 
                       . 'directory';
            throw new Exception($exception);
        }
        
        $this->directory = $directory;
        
        return true;
    }
    
    /**
     * Alias of rename
     * 
     * @param   string $destination
     * @return  boolean
     */
    public function move($destination)
    {
            return $this->rename($destination);
    }
    
    /**
     * Copy one directory to another
     * 
     * Will copy contents of one directory to another
     * 
     * @param   string|Ovoyo_FileSystem_Directory $destination
     * @return  boolean
     * @throws  Exception
     */
    public function copy($destination)
    {
            if ($destination instanceof Ovoyo_FileSystem_Directory) {
               if (is_dir($destination->directory)) {
               $toDir = $destination;
               } else {
                   $toDir = Ovoyo_FileSystem_Directory::create(
                       $destination->directory
                   );
               if (!$toDir) { 
                   return false; 
               }
               }
            } else if (!is_dir($destination)) {
                    if (!$toDir = Ovoyo_FileSystem_Directory::create($destination)) { 
                        return false; 
                    }
            } else {
            $toDir = new Ovoyo_FileSystem_Directory($destination);
            }
            
            $contents = $this->getContents();
            foreach ($contents AS $item) {
                    if ($item['type'] == 'file') {
                            $destinationFile = $toDir->directory . DIRECTORY_SEPARATOR 
                                             . $item['name'];
                            @copy($item['path'], $destinationFile);
                            if (!is_file($destinationFile)) {
                                    throw new Exception('Copy failed');
                            }
                    } else {
                        $subDirectoryPath = $this->directory . DIRECTORY_SEPARATOR 
                                          . $item['name'];
                            $subDirectory = new Ovoyo_FileSystem_Directory(
                    $subDirectoryPath
                );
                            if (!$subDirectory->copy($subDirectoryPath)) {
                    throw new Exception('Copy failed');
                            }
                    }
            }
            
            return true;
    }
    
    /**
     * Get parent directory
     *
     * False is returned if no parent directory
     *
     * @return  Pelorous_MediaManager_Directory|false
     */
    public function getParent()
    {
        $parentDirectory = preg_replace(
            '/\/' . basename($this->directory) . '$/', '',  $this->directory
        );
        $parent = new Ovoyo_FileSystem_Directory($parentDirectory);
        
        return $parent;
    }
    
    /**
     * Create a new directory
     * 
     * @param   string $path Full path to directory
     * @param   string $umask
     * @return  Ovoyo_FileSystem_Directory|false
     * @throws  Exception
     */
    public static function create($path, $umask = self::UMASK)
    {
            // make sure we have a directory name
        if (empty($path)) {
            throw new Exception('Directory path is required');
        }
        
        // make sure directory only contains alpha numeric chars
        require_once 'Zend/Validate/Regex.php';
        $validator = new Zend_Validate_Regex("/^([A-Za-z]|[0-9]|_|-|\s)+$/");
        if (!$validator->isValid(basename($path))) {
            $exception = 'Directory name can only contain letters, numbers, ' 
                       . 'spaces, underscores and hyphens';
            throw new Exception($exception);
        }
        
        if (is_dir($path)) {
            $exception = 'A directory already exists with the same name';
            throw new Exception($exception);
        }
        
            $parts = explode(DIRECTORY_SEPARATOR, $path);
            $directory = '';
            foreach ($parts AS $part) {
                    if ($part == '') { continue; }
                    $directory.= DIRECTORY_SEPARATOR . $part;
                    if (is_dir($directory)) { continue; }
                    
                // create new directory
                @mkdir($directory);
                if (!is_dir($directory)) {
                $exception = 'An unknown problem occurred when trying to add '
                           . 'new directory';
                    throw new Exception($exception);
                }
                chmod($directory, $umask);
            }
            
            return new Ovoyo_FileSystem_Directory($path);
    }
    
    /**
     * Delete a directory or just its contents
     *
     * Will attempt to delete all files and sub-directories. If $itemsOnly 
     * is set to true, will only delete sub-items and not directory itself  
     *
     * @param   boolean $itemsOnly 
     * @return  boolean
     * @throws  Exception
     */
    public function delete($itemsOnly = false)
    {
        if (!is_dir($this->directory)) { return true; }
        
        $contents = $this->getContents();
        
        foreach ($contents AS $id => $item) {
            if (is_file($item['path'])) {
                if (!@unlink($item['path'])) {
                    $exception = 'Operation could not complete, some files '
                               . 'could not be deleted';
                    throw new Exception($exception);
                }
            } else {
                try {
                    $directory = new Ovoyo_FileSystem_Directory($item['path']);
                    $directory->delete();
                    
                } catch (Exception $e) {
                    throw $e;
                }
            }
        }
        
        if (!$itemsOnly && !@rmdir($this->directory)) {
            $exception = 'Operation could not complete, directory or '
                       . 'sub-directory could not be deleted';
            throw new Exception($exception);
        }
        
        return true;
    }
    
    /**
     * Delete all items within a directory
     * 
     * @return  boolean
     */
    public function clearContents()
    {
            return $this->delete(true);
    }
        
    /**
     * Sync one directory to another
     * 
     * Will copy contents of one directory to another.  If $delete is set to
     * true will delete files that exist at $destination and not at $source
     * 
     * @param   Ovoyo_FileSystem_Directory $source
     * @param   Ovoyo_FileSystem_Directory $destination
     * @param   boolean $delete
     * @return  boolean
     * @throws  Exception
     */
    public static function rsync($source, $destination, $delete = true)
    {
        // check rsync in installed and accessible
        $result = `rsync --version`;
        if (!preg_match('/^rsync/', $result)) {
            $error = '<b>rsync</b> must be installed and accessible by your '
                   . 'webserver';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }
    
        if (!is_dir($source->directory)) {
            throw new Exception('Source directory does not exist');
        }
        
        if (!is_dir($destination->directory)) {
            if (!Ovoyo_FileSystem_Directory::create($destination->directory)) { 
                return false; 
            }
        }
        
        $command = 'rsync -aqz';
        if ($delete) {
            $command.= ' --delete';
        }
        $command.= ' ' 
                  . $source->directory . '/ ' 
                 . $destination->directory . '/';
        
        $result = `$command`;
        
        return true;
    }
    
    /**
     * Get the type of an item within a directory
     *
     * @param   string $item Name of item
     * @return  string
     */
    public function getItemType($item)
    {
        $fullPath = $this->directory . DIRECTORY_SEPARATOR . $item;
        
        return (is_dir($fullPath)) ? 'dir' : 'file';
    }
    
    /**
     * Get the size of an item within a directory
     *
     * @param   string $item Name of item
     * @return  string
     */
    public function getItemSize($item)
    {
        $fullPath = $this->directory . DIRECTORY_SEPARATOR . $item;
        
        if (is_dir($fullPath)) { return '----'; }
        
        $size = sprintf("%u", filesize($fullPath));
        
        // display in Gb, Mb, Kb or b
        $kb = 1024;
        $mb = 1024 * $kb;
        $gb = 1024 * $mb;
        
        if ($size > $gb) {
            $size = round(($size / $gb), 2);
            $size.= 'Gb';
        } else if ($size > $mb) {
            $size = round(($size / $mb), 2);
            $size.= 'Mb';
        } else if ($size > $kb) {
            $size = round(($size / $kb), 2);
            $size.= 'Kb';
        } else {
            $size.= 'b';
        }
        
        return $size;
    }
    
    /**
     * Get the last updated date of an item within a directory
     *
     * @param   string $item Name of item
     * @return  string
     */
    public function getItemLastUpdated($item)
    {
        $fullPath = $this->directory . DIRECTORY_SEPARATOR . $item;
        
        return date("D dS M Y H:i:s", fileatime($fullPath));
    }
}
