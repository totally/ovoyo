<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Class for performing common authentication actions
 *
 * @package    Ovoyo_Authentication
 */
class Ovoyo_Authentication
{
    /**
     * Factory for Ovoyo_Authentication_Adapter classes
     *
     * First argument is the name of the adataper to use
     * e.g. 'Ovoyo/Application/Authentication' corresponds to class Ovoyo_Application_Authentication.  This
     * is case-sensitive and the trailing '.php' is not required.
     *
     * @param   mixed $adapter String name of base adapter class, or Zend_Config object.
     * @param   Zend_Config $defaults Default options
     * @param   Zend_Config $options Authentication options
     * @return  obj Ovoyo_Authentication_Adapter sub-class
     * @throws  Ovoyo_Authentication_Exception
     */
    public static function factory($adapter, $defaults = null, $options = null)
    {
        require_once 'Ovoyo/Authentication/Exception.php';

        // check an adapter has been specified.
        if (!is_string($adapter) || empty($adapter)) {
            throw new Ovoyo_Authentication_Exception("Adapter ($adapter) is not valid");
        }

        // if adapter class does not already exist, try and include it
        if (!class_exists($adapter)) {
            $adapterInclude = preg_replace('/_/', '/', $adapter);

            include_once "$adapterInclude.php";
            if (!class_exists($adapter)) {
                throw new Ovoyo_Authentication_Exception("Adapter ($adapter) is not valid");
            }
        }

        $authenticationAdapter = new $adapter($defaults, $options);

        // check we have a valid adapter
        if (!$authenticationAdapter instanceof Ovoyo_Authentication_Adapter) {
            require_once 'Ovoyo/Authentication/Exception.php';
            throw new Ovoyo_Authentication_Exception("Adapter class '$className' does not extend Ovoyo_Authentication_Adapter");
        }

        return $authenticationAdapter;
    }
}
