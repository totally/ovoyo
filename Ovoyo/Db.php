<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Zend_Db
 */
require_once 'Zend/Db.php';

/**
 * @see Zend_Registry
 */
require_once 'Zend/Registry.php';

/**
 * Ovoyo Db class
 *
 * Provides a wrapper to Zend_Db class and adds facility for nested transactions
 *
 * @package    Ovoyo_Db
 */
class Ovoyo_Db
{
    private $_adapter;
    private $_adapterId;
    private $_firebugProfilerEnabled = false;

    /**
     * Constructor
     *
     * @param  obj $adapter Zend_Config object.
     * @throws Zend_Db_Exception
     */
    public function __construct($adapter)
        {
        try {
            $this->_adapter = Zend_Db::factory($adapter);

            $config = $this->_adapter->getConfig();

            // create id from md5 hash of config info
            $this->_adapterId = md5(serialize($config));

            // store transaction count
            if (!Zend_Registry::isRegistered($this->_adapterId)) {
                Zend_Registry::set($this->_adapterId, 0);
            }

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
    * Get database adapter
    *
    * @return Zend_Db_Adapter instance
    */
    public function getAdapter()
    {
        return $this->_adapter;
    }
    
    /**
     * Check to see if we can begin a transaction
     *
     * @return  boolean
     */
    public function canBeginTransaction()
    {
        return ($this->_getTransactionCount() == 0);
    }
    
    /**
     * Begin a transaction
     *
     * @return bool
     */
    public function beginTransaction()
    {
        // can only start a transaction if one has not already been started
        if ($this->canBeginTransaction()) {
            if (!$this->_adapter->beginTransaction()) { return false; }
        }
        $this->_incrementTransactionCount();

        return true;
    }
    
    /**
     * Check to see if we can commit
     *
     * @return  boolean
     */
    public function canCommit()
    {
        return ($this->_getTransactionCount() == 1);
    }

    /**
     * Commit a transaction
     *
     * @return bool
     */
    public function commit()
    {
        // can only commit if we are the owner of the transaction
        if ($this->canCommit()) {
            if (!$this->_adapter->commit()) { return false; }
        }
        $this->_decrementTransactionCount();

        return true;
    }
    
    /**
     * Check to see if we can rollback
     *
     * @return  boolean
     */
    public function canRollback()
    {
        return ($this->_getTransactionCount() == 1);
    }

    /**
     * Rollback a transaction
     *
     * @return bool
     */
    public function rollback()
    {
        // can only rollback if we are the owner of the transaction
        if ($this->canRollback()) {
            if (!$this->_adapter->rollback()) { return false; }
        }

        $this->_decrementTransactionCount();

        return true;
    }
    
    /**
     * Get current transaction count
     *
     * @return int
     */
    protected function _getTransactionCount()
    {
        return Zend_Registry::get($this->_adapterId);
    }

    /**
     * Increment transaction count
     *
     * return void
     */
    protected function _incrementTransactionCount()
    {
        $count = $this->_getTransactionCount();
        Zend_Registry::set($this->_adapterId, $count + 1);
    }

    /**
     * Decrement transaction count
     *
     * @return int
     */
    protected function _decrementTransactionCount()
    {
        $count = $this->_getTransactionCount();
        Zend_Registry::set($this->_adapterId, $count - 1);
    }

    
    /**
     * check the charset on a db and change connection appropriately
     * 
     * @param boolean $usePear
     * @param database obj $db
     */
    public static function restoreCharacterSet($db, $usePear = false)
    {
        $config = array();
        if (!$usePear) {
            $config = $db->getAdapter()->getConfig();
        }

        if (isset($config['charset'])) {
            $charSet = $config['charset'];
        } else {
            $findCharSet = 'SHOW VARIABLES LIKE "character_set_server"';
            if ($usePear) {
                $result = $db->getRow($findCharSet, array(), DB_FETCHMODE_ASSOC);
                $charSet = $result['Value'];
            } else {
                $result = $db->fetchAll($findCharSet);
                $charSet = $result[0]['Value'];
            }
        }

        if (isset($config['collate'])) {
            $collate = $config['collate'];
        } else {
            $findCollation = 'SHOW VARIABLES LIKE "collation_server"';
            if ($usePear) {
                $result = $db->getRow($findCollation, array(), DB_FETCHMODE_ASSOC);
                $collate = $result['Value'];
            } else {
                $result = $db->fetchAll($findCollation);
                $collate = $result[0]['Value'];
            }
        }
        
        $db->query('SET NAMES "' . $charSet . '" COLLATE "' . $collate . '"');
    } 
    
    /**
     * Enable firephp profiler
     *
     * @return  void
     */
    public function enableFirebugProfiler()
    {
        if ($this->_firebugProfilerEnabled) { return; }
        
        require_once 'Zend/Db/Profiler/Firebug.php';
        $conf = $this->_adapter->getConfig();
        $profiler = new Zend_Db_Profiler_Firebug('DB Queries for ' . $conf['dbname'] . ' on ' . $conf['host']);
        $profiler->setEnabled(true);
        $this->setProfiler($profiler);
        $this->_firebugProfilerEnabled = true;
    }

    /**
     * Apply some magic function calls
     *
     * Any method that doesn't exist, try calling via $adapter
     *
     * @param string $method Name of method
     * @param array $arguments List of arguments to pass to method
     * @return result of function call
     * @throws Exception
     */
    public function __call($method, $arguments)
    {
        try {
            if (!method_exists($this->_adapter, $method)) {
                throw new Exception("method $method not found");
            }

            return call_user_func_array(array($this->_adapter, $method), $arguments);
        } catch (Exception $e) {
            throw $e;
        }
    }
}
