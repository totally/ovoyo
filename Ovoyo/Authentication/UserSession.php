<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_ErrorHandler
 */
require_once 'Ovoyo/ErrorHandler.php';

/**
 * User session class
 *
 * @package    Ovoyo_Authentication
 */
class Ovoyo_Authentication_UserSession
    implements Ovoyo_Authentication_Interface_UserSession
{
    public $sessionId;
    public $email;

    protected $_session;
     
    /**
     * Constructor
     *
     */
    public function __construct()
    {
        /**
         * @see Zend_Session_session
         */
        require_once 'Zend/Session/Namespace.php';

        // create session name space and fetch an existing messages
        $this->_session = new Zend_Session_Namespace(
            'Ovoyo_Authentication_SimpleAuth'
        );
    }

    /**
     * Start new session for user
     *
     * @param   string|int $userId User identifier
     * @param   string $sessionIdentifier Session identifier
     * @return  boolean
     */
    public function startSession($userId, $sessionIdentifier)
    {
        // add user to list of users in session
        $this->email = $userId;
        $this->sessionId = $this->_createSessionId();
        $this->_session->{$this->sessionId} = $this->email;

        // set expiry time of 10 mins (600s)
        $this->_session->setExpirationSeconds(600, $this->sessionId);

        $this->dropSessionIdCookie($sessionIdentifier);

        return true;
    }
    
    /**
     * Refresh session
     * 
     * @return  void
     */
    public function refresh()
    {
        $this->_session->setExpirationSeconds(600, $this->sessionId);
    }

    /**
     * Fetch user session
     * 
     * @param   string $sessionId
     * @return  void
     */
    public function fetch($sessionId)
    {
        if (isset($this->_session->{$sessionId})) {
            $this->email = $this->_session->{$sessionId};
            $this->sessionId = $sessionId;
            
            return true;
        }
        
        return false;
    }

    /**
     * Create a random session ID string
     *
     * @return string a random session ID string 64 characters in length
     */
    private final function _createSessionId()
    {
        $sessionId = '';

        srand((double) microtime() * 1000000);

        for ($n = 0; $n < 64; $n++) {
            $varchar = rand(1,3);
            switch ($varchar) {
                case 1: $sessionId.= chr(rand(97,122));
                break;
                case 2: $sessionId.= chr(rand(48,57));
                break;
                case 3: $sessionId.= chr(rand(65,90));
                break;
            }
        }

        return $sessionId;
    }

    /**
     * Drop cookie containing a session ID
     *
     * @param   string $sessionIdentifier Session identifier
     * @return  void
     */
    public function dropSessionIdCookie($sessionIdentifier)
    {
        $secure_cookie = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? true : false;
        setcookie($sessionIdentifier, $this->sessionId, 0, "/", '', $secure_cookie);
    }

    /**
     * Delete session
     *
     * @param   string $sessionIdentifier Session identifier
     * @return  void
     */
    public function end($sessionIdentifier)
    {
        $this->_session->setExpirationSeconds(1, $this->sessionId);

        $secure_cookie = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? true : false;
        setcookie($sessionIdentifier, null, 0, "/", '', $secure_cookie);
        unset($_COOKIE[$sessionIdentifier]);
        
        return true;
    }
}
