<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Email marketing list class
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage List
 */
class Ovoyo_Services_OnlineMarketing_List_MailChimp
      extends Ovoyo_Services_OnlineMarketing_List_Abstract
{
    /**
     * Contruct method
     * @param array $config
     */
    public function __construct($config)
    {
        foreach ($config AS $option => $value) {
            $this->{$option} = $value;
        }

        $this->setApiHandler($config);
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::create()
     */
    public function create($name)
    {
        // not possible with current MailChimp API
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::delete()
     */
    public function delete()
    {
        // not implemented
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::getList()
     */
    public function getList()
    {
        // get subscriber count
        $apiHandler = $this->getApiHandler();

        $params = array(
            'method' => 'lists',
            'filters' => array('list_id' => $this->listId)
        );

        $response = $apiHandler->callApi($params);

        if (!@unserialize($response)) {
            return false;
        }

        $rawList = unserialize($response);
        $list = array();

        $list['name'] = $rawList['data'][0]['name'];
        $list['numSubscribers'] = $rawList['data'][0]['stats']['member_count'];
        $list['data'] = $rawList['data'][0];

        return $list;
    }

    /**
     * Get available lists
     *
     * @return  array
     */
    public function getLists()
    {
        $apiHandler = $this->getApiHandler();

        $params = array(
            'method' => 'lists',
        );

        $response = $apiHandler->callApi($params);

        if (!@unserialize($response)) {
            return false;
        }

        $listItems = unserialize($response);

        return $listItems;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::addSubscriber()
     */
    public function addSubscriber($data)
    {
        $apiHandler = $this->getApiHandler();

        $params = array(
            'method' => 'listSubscribe',
            'id' => $this->listId
        );

        $params['send_welcome'] = ($this->sendWelcomeEmailOnImport)
                                ? true : false;

        $params['double_optin'] = ($this->doubleOptIn)
                                ? true : false;

        $params['email_address'] = $data['email'];

        $params['merge_vars'] = array(
            'FNAME' => $data['FNAME'],
            'LNAME' => $data['LNAME']
        );

        $response = $apiHandler->callApi($params);

        return $response;
    }

    /**
     * Batch version of addSubscriber
     */
    public function addSubscribers($data)
    {
        $apiHandler = $this->getApiHandler();

        $params = array(
            'method' => 'listBatchSubscribe',
            'id' => $this->listId
        );

        $params['replace_interests'] = false;
        $params['update_existing'] = false;

        $params['double_optin'] = ($this->doubleOptIn)
        ? true : false;

        $params['batch'] = $data;

        $response = $apiHandler->callApi($params);

        return $response;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::getSubscribers()
     */
    public function getSubscribers()
    {
        $apiHandler = $this->getApiHandler();

        $params = array(
            'method' => 'listMembers',
            'id' => $this->listId
        );

        $response = $apiHandler->callApi($params);

        return $response;
    }

    /**
     * Import data into list
     *
     * If list of fields are not provided, data is assumed to only contain
     * email addresses
     *
     * @param   array $data Data to import
     * @param   array $fields List of fields to be imported
     * @return  int Import id | false
     */
    public function import(array $data, $fields = null)
    {
        if (count($data) == 0) {
            Ovoyo_ErrorHandler::addError('List does not contain any data');
            return false;
        }

        if (!is_array($fields)) {
            $fields = array('email');
            if (!$data[0]['email']) {
                $newData = array();
                foreach ($data AS $email) {
                    $newData[] = array('email' => $email);
                }
                $data = $newData;
            }
        }

        if (count($fields) > 1) {
            $this->addSubscribers($data);
        } else {
            foreach ($data AS $subscriber) {
                $this->addSubscriber($subscriber);
            }
        }

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::hasImportCompleted()
     */
    public function hasImportCompleted($importId = null)
    {
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::postImport()
     */
    public function postImport($importId = null)
    {
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::setApiHandler()
     */
    public function setApiHandler($config = null)
    {
        $this->_apiHandler = new Ovoyo_Services_OnlineMarketing_ApiHandler_MailChimp($config);
    }
}