<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

require_once 'Zend/Form/Element.php';

/**
 * Assignable selects element
 *
 * @package    Ovoyo_Form
 */
class Ovoyo_Form_Element_MultipleCheckbox extends Zend_Form_Element
{
    protected $_isArray = false;
    
    /**
     * Use formHidden view helper by default
     * @var string
     */
    public $helper = 'formMultipleCheckbox';

    /**
     * Set element name
     *
     * @param  string $name
     * @return Zend_Form_Element
     */
    public function setName($name)
    {
        if ('' === $name) {
            require_once 'Zend/Form/Exception.php';
            throw new Zend_Form_Exception('Invalid name provided; must contain only valid variable characters and be non-empty');
        }

        $this->_name = $name;
        return $this;
    }
}
