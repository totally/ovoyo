<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Email marketing campaign class
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage Campaign
 */
class Ovoyo_Services_OnlineMarketing_Campaign_VerticalResponse
      extends Ovoyo_Services_OnlineMarketing_Campaign_Abstract
{
    public $sendTime;
    
    /**
     * Constructor
     *
     * @param array $config
     */
    public function __construct($config)
    {
        foreach ($config AS $option => $value) {
            $this->{$option} = $value;
        }
        
        $this->setApiHandler($config);
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::create()
     */
    public function create($name, $headers, $content)
    {
        $apiHandler = $this->getApiHandler();
        
        // email content cannot be supplied as a URL so fetch the body contents
        // here and supply as an HTML string
        $html = file_get_contents($content['htmlUrl'], 'r');
        $hasBody = preg_match('/<body>(.*)<\/body>/s', $html, $body);
        if ($hasBody) {
            $html = $body[0];
        }
        
        $text = $html;
        if (isset($content['textUrl'])) {
            $text = file_get_contents($content['textUrl'], 'r');
        }
        
        // required params
        $params = array(
            'email_campaign' => array(
                'name' => substr($name,0,39),
                'type' => 'freeform',
                'from_label' => $headers['fromName'],
                'support_email' => $headers['replyToAddress'],
                'send_friend' => 'false',
                'contents' => array(
                    array(
                        'type' => 'freeform_html',
                        'copy' => $html,
                    ),
                    array(
                        'type' => 'freeform_text',
                        'copy' => $text,
                    ),
                    array(
                        'type' => 'subject',
                        'copy' => $headers['subject'],
                    ),
                    array(
                        'type' => 'unsub_message',
                        'copy' => 'You requested these emails. To be removed from the list ',
                    )
                )
            )
        );
        
        return $apiHandler->callApi('createEmailCampaign', $params);
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::update()
     */
    public function update($name, $headers, $content)
    {
        $apiHandler = $this->getApiHandler();
        
        // each campaign attribute must be updated individually.....
        $params = array(
            'campaign_id' => $this->campaignId,
            'name' => 'name',
            'value' => substr($name,0,39),
        );
        
        $apiHandler->callApi('setEmailCampaignAttribute', $params);
        
        $params = array(
            'campaign_id' => $this->campaignId,
            'name' => 'from_label',
            'value' => $headers['fromName'],
        );
        
        $apiHandler->callApi('setEmailCampaignAttribute', $params);
        
        $params = array(
            'campaign_id' => $this->campaignId,
            'name' => 'support_email',
            'value' => $headers['replyToAddress'],
        );
        
        $apiHandler->callApi('setEmailCampaignAttribute', $params);
        
        // email content cannot be supplied as a URL so fetch the body contents
        // here and supply as an HTML string
        $html = file_get_contents($content['htmlUrl'], 'r');
        $hasBody = preg_match('/<body>(.*)<\/body>/s', $html, $body);
        if ($hasBody) {
            $html = $body[0];
        }
        
        $params = array(
            'campaign_id' => $this->campaignId,
            'content' => array(
                    'type' => 'freeform_html',
                    'copy' => $html,
            )
        );

        $apiHandler->callApi('setEmailCampaignContent', $params);
        
        $text = $html;
        if (isset($content['textUrl'])) {
            $text = file_get_contents($content['textUrl'], 'r');
        }
        
        $params = array(
            'campaign_id' => $this->campaignId,
            'content' => array(
                'type' => 'freeform_text',
                'copy' => $text,
            )
        );
        
        $apiHandler->callApi('setEmailCampaignContent', $params);
        
        $params = array(
            'campaign_id' => $this->campaignId,
            'content' => array(
                'type' => 'subject',
                'copy' => $headers['subject'],
            )
        );
        
        $apiHandler->callApi('setEmailCampaignContent', $params);
        
        $params = array(
            'campaign_id' => $this->campaignId,
            'content' => array(
                'type' => 'unsub_message',
                'copy' => 'You requested these emails. To be removed from the list ',
            )
        );
        
        $apiHandler->callApi('setEmailCampaignContent', $params);
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::delete()
     */
    public function delete()
    {
        $apiHandler = $this->getApiHandler();
        
        $params = array(
            'campaign_id' => $this->campaignId
        );

        $response = $apiHandler->callApi('deleteCampaign', $params);
        
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::send()
     */
    public function send($lists)
    {
        $apiHandler = $this->getApiHandler();
        
        $params = array(
            'campaign_id' => $this->campaignId
        );
        
        foreach ($lists as $listId) {
            $params['list_ids'][] = $listId;
        }
        
        try {
            $response = $apiHandler->callApi('setCampaignLists', $params);
            
            $params = array(
                'campaign_id' => $this->campaignId
            );
            
            $response = $apiHandler->callApi('launchEmailCampaign', $params);
        } catch (Exception $e) {
            throw $e;
        }
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::sendPreview()
     */
    public function sendPreview($sendTo)
    {
        $apiHandler = $this->getApiHandler();
        
        $emails = explode(',', $sendTo);
        
        $params = array(
            'campaign_id' => $this->campaignId
        );
        
        foreach ($emails AS $email) {
            $params['recipients'][] = array(
                array(
                    'name' => 'email_address',
                    'value' => $email
                )
            );
        }

        $response = $apiHandler->callApi('sendEmailCampaignTest', $params);

        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::setApiHandler()
     */
    public function setApiHandler($config = null)
    {
        $this->_apiHandler = new Ovoyo_Services_OnlineMarketing_ApiHandler_VerticalResponse($config);
    }
}
