<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Model_Exception
 */
require_once 'Ovoyo/Model/Exception.php';

/**
 * @see Ovoyo_WebContainer
 */
require_once 'Ovoyo/WebContainer.php';

/**
 * @see Ovoyo_Db_Table
 */
require_once 'Ovoyo/Db/Table.php';

/**
 * @see Zend_Date
 */
require_once 'Zend/Date.php';

/**
 * Model
 *
 * Abstract class providing some automated database interaction
 *
 * @package    Ovoyo_Model
 */
abstract class Ovoyo_Model
{
    protected $_connName;
    protected $_tablePrefix;
    protected $_deleteMethod;
    protected $_dbTableConfig;
    
    protected $_fetchOptions = array();
    protected $_searchOptions = array();

    protected $_tableInfo;

    protected $_relationships        = array();
    protected $_relationshipLinks    = array();
    protected $_ignoredRelationships = array();
    protected $_ignoreRelations      = false;

    public $__parent;
    public $__isParent = false;
    //public $__child;

    public $requiredFields = array();
    public $invalidFields = array();
    
    const RELATIONSHIP_TYPE_ONE2ONE   = '1:1';
    const RELATIONSHIP_TYPE_ONE2MANY  = '1:M';
    const RELATIONSHIP_TYPE_MANY2MANY = 'M:M';
    
    const MODEL_DELETE_METHOD_PHYSICAL = 'physical';
    const MODEL_DELETE_METHOD_LOGICAL = 'logical';
    
    const DB_NAME_STYLE_CAMEL_CASE = 'camelCase';
    const DB_NAME_STYLE_UNDERSCORE = 'underscore';

    /**
     * Constructor
     *
     * @param   array|Zend_Config $options Possible options are 'connName' - connection name and 'tablePrefix' - db table prefix to use
     */
    public function __construct($options = array())
    {
        if (is_array($options) || $options instanceof Zend_Config) {
            foreach ($options AS $option => $value) {
                $this->{"_$option"} = ($value) ? $value : null;
            }
        }

        $this->init();
        
        // if not defined, set delete type to physical as default
        if (!isset($this->_deleteMethod)) {
            $this->_deleteMethod = self::MODEL_DELETE_METHOD_PHYSICAL;
        }
        
        $this->_setup();
    }

    /**
     * Init method
     *
     * Sub classes should extend this method and define the correct db connection name and db table config options,
     * see Zend_DB_Table for info on possible options for db table config
     */
    public function init()
    {
        // set some defaults
        $this->_connName = (isset($this->_connName)) ? $this->_connName : 'default';
        $this->_tablePrefix = (isset($this->_tablePrefix)) ? $this->_tablePrefix : '';
    }

    /**
     * Setup model based on config options
     *
     * @return void
     */
    private function _setup()
    {
        $tableInfo = $this->tableInfo();
        $defaultAlias = preg_replace(
            "/$this->_tablePrefix/", '', $tableInfo['name']
        );
        $defaultAlias = substr($defaultAlias, 0, 1);
        
        // set fetch options
        if (!isset($this->_fetchOptions['tableAlias'])) {
            $this->_fetchOptions['tableAlias'] = $defaultAlias;
        }
        
        // set search options
        if (!isset($this->_searchOptions['tableAlias'])) {
            $this->_searchOptions['tableAlias'] = $defaultAlias;
        }
        
        // add any implied parent relationship
        $parentClass = get_parent_class($this);
        if ($parentClass && $parentClass != 'Ovoyo_Model') {
            // can not instantiate abstract classes
            $class = new ReflectionClass($parentClass);
            if ($class->isInstantiable()) {
                $options = $this->getModelOptions();
                $options['ignoreRelations'] = true;
                $this->__parent = Ovoyo_Model::factory($parentClass, $options);
                $this->__parent->__isParent = true;
                $parentVars = $this->__parent->toArray();
                foreach ($parentVars AS $variable => $value) {
                    if (isset($this->{$variable})) {
                        $currentValue = $this->{$variable};
                        $this->{$variable} = &$this->__parent->{$variable};
                        $this->{$variable} = $currentValue;
                    } else {
                        $this->{$variable} = null;
                        $this->{$variable} = &$this->__parent->{$variable};
                    }
                }
            }
        }

        // process any relationships
        foreach ($this->_relationships AS $name => $mapping) {
            $this->_addRelationship($name, $mapping);
        }

        // set up date vars
        $tableInfo =& $this->tableInfo();
        foreach ($tableInfo['metadata'] AS $var => $varInfo) {
            if (!is_null($this->{$var})) { continue; }
            $this->{$var} = null;
            
            if ($varInfo['DATA_TYPE'] == 'date') {
                $this->{$var} = 0;
            } else if ($varInfo['DATA_TYPE'] == 'datetime') {
                $this->{$var} = 0;
            }

            // for enum fields, fetch possible values and store in an array
            if (preg_match('/^enum/i', $varInfo['DATA_TYPE'])) {
                preg_match_all('/\'(.*?)\'/', $varInfo['DATA_TYPE'], $matches);

                $options = array();
                foreach ($matches[1] as $match) {
                    $options[$match] = $match;
                }

                $this->{$var . 'Options'} = $options;
            }
        }
    }
    
    /**
     * Factory for Ovoyo_Authentication_Model classes
     *
     * See public factory() method more more info
     *
     * @param   string $className Name of class to create
     * @param   array $options Possible options are 'connName' - connection name and 'tablePrefix' - db table prefix to use
     * @param   boolean $validateOnly
     * @returns obj|boolean An object which is a subclass of Ovoyo_Model or boolean if $validateOnly is set to true
     * @throws Exception
     */
    private static function _factory($className, $options = array(), $validateOnly = false)
    {
        require_once 'Ovoyo/ClassLoader.php';

        // if class already exists, simply return new instance
        if (@class_exists($className)) {
            if ($validateOnly) {
                try {
                    $model = new $className($options);
                    return true;
                } catch (Exception $e) {
                    return false;
                }
            }
            $model = new $className($options);
            return $model;
        }

        // if class physically exists but hasn't been loaded, then load it
        if (Ovoyo_ClassLoader::exists($className)) {
            if ($validateOnly) {
                try {
                    $model = new $className($options);
                    return true;
                } catch (Exception $e) {
                    return false;
                }
            }            $model = new $className($options);
            return $model;
        }
        
        // set default values to get rid of php warnings
        $options['tablePrefix'] = (isset($options['tablePrefix'])) ? $options['tablePrefix'] : '';
        $options['ignoreRelations'] = (isset($options['ignoreRelations'])) ? $options['ignoreRelations'] : '';

        // class definition
        $class = 'class ' . $className . ' extends Ovoyo_Model {}';
     /*          . 'public function init() {'
               . '$this->_connName = "' . $options['connName'] . '";'
               . '$this->_tablePrefix = "' . $options['tablePrefix'] . '";'
               . '$this->_ignoreRelations = "' . $options['ignoreRelations'] . '";';

        if (isset($options['requiredFields']) && is_array($options['requiredFields'])){
            foreach($options['requiredFields'] AS $fieldName){
                $class.= '$this->requiredFields[] = "'.$fieldName.'";';
            }
        }

        $class.= '}}';*/

        // create class
        eval($class);

        if (!class_exists($className)) {
            throw new Ovoyo_Model_Exception("failed to create class ($className)");
        }

        // validate only - check to see if we can get db table info
        if ($validateOnly) {
            try {
                $model = new $className;

                return true;
            } catch (Exception $e) {
                return false;
            }
        }

        $model = new $className($options);
        return $model;
    }

    /**
     * Factory for Ovoyo_Authentication_Model classes
     *
     * Takes the name of a required class and will attempt to dynamically create a class
     * which extends Ovoyo_Model and return and instance of new class.  If $validateOnly param is
     * set to true, will just check to see if model class can be created and not perform creation.  Useful
     * to test for existence of db tables
     *
     * @param   string $className Name of class to create
     * @param   array $options Possible options are 'connName' - connection name and 'tablePrefix' - db table prefix to use
     * @param   boolean $validateOnly
     * @returns obj|boolean An object which is a subclass of Ovoyo_Model or boolean if $validateOnly is set to true
     * @throws Exception
     */
    public static final function factory($className, $options = array(), $validateOnly = false)
    {
        return self::_factory($className, $options, $validateOnly);
    }

    /**
     * Update a class variables from the contents of an array
     *
     * @param   array $variables Array containing values for instance variables
     * @return  void
     */
    public function updateVariables($variables)
    {
        $this->preUpdateVariables($variables);
        
        $tableInfo =& $this->tableInfo();
        
        // get class vars using foreach instead of get_class_vars, so that
        // any vars created by $this->__parent will be detected
        $classVars = array();
        foreach ($this AS $var => $value) {
            $classVars[$var] = 1;
        }
        
        // update class variables
        if (is_array($variables)) {
            foreach ($variables AS $variable => $value) {
                $dataType = (isset($tableInfo['metadata'][$variable])) ? $tableInfo['metadata'][$variable]['DATA_TYPE'] : null;

                // date vars we can identify, should be set as date objects
                if ($dataType == 'date') {

                    if (is_array($value)) {
                        $this->{$variable} = new Zend_Date($value);

                    } else {
                        if ($value && $value != '0000-00-00') {
                            $this->{$variable} = new Zend_Date($value,
                                Zend_Date::ISO_8601
                            );
                        }
                    }

                // do the same with date times
                } else if ($dataType == 'datetime') {
                    if (is_array($value)) {
                        $this->{$variable} = new Zend_Date($value);
                    } else {
                        if ($value && $value != '0000-00-00 00:00:00') {
                            $this->{$variable} = new Zend_Date($value,
                                Zend_Date::ISO_8601
                            );
                        }
                    }

                // if var is a relation, pass data to relation(s)
                } else if (isset($this->_relationships[$variable])) {
                    $this->_updateRelationVariable($variable, $value);
                // if var is relation linking data, update relation links
                } else if (isset($this->_relationshipLinks[$variable])) {
                    $this->_updateLinkedRelations($variable, $value);

                // other vars just set normally
                } else if (isset($tableInfo['metadata'][$variable])
                           || array_key_exists($variable, $classVars)) {
                    $this->{$variable} = $value;
                }
            }
        }
        
        $this->postUpdateVariables($variables);
    }
    /**
     * Pre-update variables method
     *
     * Any pre-update actions should go here - expects to be overridden
     *
     * @param array $variables Array containing values for instance variables
     * @return  void
     */
    public function preUpdateVariables($variables)
    { }

    /**
     * Post-update variables method
     *
     * Any post-update actions should go here - expects to be overridden
     *
     * @param array $variables Array containing values for instance variables
     * @return  void
     */
    public function postUpdateVariables(&$variables)
    { }

    /**
     * Update a relation's variables from the contents of an array
     *
     * @param string $relationshipVar Name of variable holding relation
     * @param array $variables Array containing values to update in relation
     * @return void
     */
    public function _updateRelationVariable($relationshipVar, $variables)
    {
        if ($this->_ignoreRelations || !is_array($variables)) {
            return;
        }

        if (array_key_exists($relationshipVar, $this->_ignoredRelationships)) {
            return;
        }

        $tableInfo = $this->tableInfo();

        $mapping = $this->_relationships[$relationshipVar];

        $foreignKeys = $mapping['foreignKeys'];

        // one-to-one mapping
        if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2ONE) {
            // relation exists so just update
            if ($this->$relationshipVar instanceof $mapping['relation']) {
                $this->$relationshipVar->updateVariables($variables);

            // relation does not exist, so create instance
            } else {
                $model = Ovoyo_Model::factory(
                    $mapping['relation'], $mapping['options']
                );
                $model->updateVariables($variables);

                foreach ($foreignKeys AS $foreignKey) {
                    $model->$foreignKey = $this->$foreignKey;
                }

                $this->$relationshipVar = $model;
            }

        // one-to-many mapping
        } else if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2MANY) {
            // if no relationKey, create new relations array
            if (!$mapping['relationKey']) {
                $newRelations = array();
                foreach ($variables AS $itemId => $values) {
                    $model = Ovoyo_Model::factory(
                        $mapping['relation'], $mapping['options']
                    );
                    $model->updateVariables($values);

                    foreach ($foreignKeys AS $foreignKey) {
                        $model->$foreignKey = $this->$foreignKey;
                    }

                    $newRelations[] = $model;
                }

                $this->{$relationshipVar} = $newRelations;

            } else {
                foreach ($variables AS $itemId => $values) {
                    // relation exists so just update
                    if ($this->{$relationshipVar}[$itemId] instanceof $mapping['relation']) {
                        $this->{$relationshipVar}[$itemId]->updateVariables($values);

                    // relation does not exist, so create instance
                    } else {
                        $model = Ovoyo_Model::factory(
                            $mapping['relation'], $mapping['options']
                        );
                        $model->updateVariables($values);

                        foreach ($foreignKeys AS $foreignKey) {
                            $model->$foreignKey = $this->$foreignKey;
                        }

                        $this->{$relationshipVar}[] = $model;
                    }
                }
            }
        }
    }

    /**
     * Update links between many-to-many relationships
     *
     * @param   string $linkedRelationsArrayId Id of array variable holding relation link data
     * @param   array $linkedItems Array containing list of ids of related items
     * @return  void
     * @throws  Ovoyo_Model_Exception
     */
    public function _updateLinkedRelations($linkedRelationsArrayId, $linkedItems)
    {
        if ($this->_ignoreRelations) {
            return;
        }

        $relationshipVar = $this->_relationshipLinks[$linkedRelationsArrayId];
        $mapping     = $this->_relationships[$relationshipVar];

        if (array_key_exists($relationshipVar, $this->_ignoredRelationships)) {
            return;
        }

        // store new linked items
        $this->{$linkedRelationsArrayId} = $linkedItems;

        // remove items from relationship array that are no longer linked
        $relations = array();
        foreach ($this->{$relationshipVar} AS $id => $model) {
            if (!$linkedItems[$id]) { continue; }
            $relations[$id] = $model;
        }
        $this->{$relationshipVar} = $relations;

        // add new relationships
        if (is_array($linkedItems)) {
            foreach ($linkedItems AS $itemId) {
                if ($this->{$relationshipVar}[$itemId]) { continue; }

                $model = Ovoyo_Model::factory(
                    $mapping['relation'], $mapping['options']
                );

                // fetch item
                $fetch = 'fetchBy' . ucfirst($mapping['relationKey']);
                $model->$fetch($itemId);

                $this->{$relationshipVar}[$itemId] = $model;
            }
        }
    }

    /**
     * Fetch table info
     *
     * Only fetches info once and caches data in instance variable
     *
     * @param boolean $flushCache Flag to specify that cached copy should be flushed and refreshed
     * @return array Array holding table info
     * @throws Exception if _dbTable has not been defined
     */
    public function tableInfo($flushCache = false)
    {
        // return cached copy
        if (is_array($this->_tableInfo) && !$flushCache) {
            return $this->_tableInfo;
        }

        try {
            // check _dbTable has been defined
            if (!$this->_dbTable instanceOf Zend_Db_Table_Abstract) {
                throw new Exception("_dbTable has not been defined");
            }
            $this->_tableInfo = $this->_dbTable->info();

        } catch (Exception $e) {
            throw $e;
        }

        return $this->_tableInfo;
    }

    /**
     * Fetch table metadata
     *
     * Will optionally fetch parent class data
     *
     * @param   boolean $includeParent
     * @return  array Array holding table metadata
     * @throws  Exception
     */
    public function tableMetaData($includeParent = false)
    {
        $metaData = array();

        if ($includeParent && $this->__parent instanceOf Ovoyo_Model) {
            $metaData = $this->__parent->tableMetaData($includeParent);
        }

        $tableInfo = $this->tableInfo();
        foreach ($tableInfo['metadata'] AS $key => $val) {
            $metaData[$key] = $val;
        }

        return $metaData;
    }

    /**
     * Fetch item from DB using primary key
     *
     * The argument specifies one or more primary key value(s). To find
     * multiple rows by primary key, the argument must be an array.
     *
     * @param mixed $itemId ID of item to fetch
     * @return boolean
     */
    public function fetch($itemId)
    {
        // check _dbTable has been defined
        if (!$this->_dbTable instanceOf Zend_Db_Table_Abstract) {
            throw new Ovoyo_Model_Exception("_dbTable has not been defined");
        }
        
        // do nothing if no id has been passed in
        if (!isset($itemId)) {
            return true;
        }
        
        $primaryKeys = $this->getPrimaryKeys();
        if (count($primaryKeys) > 1) {
            $exception = 'Can not perform a fetch on '
                       . get_class($this) . ' instance, which has more '
                       . 'than one primary key';
            throw new Ovoyo_Model_Exception($exception);
        }
        
        $fetchCol = $this->_fetchOptions['tableAlias'] . '.'
                  . current($primaryKeys);
        
        // build query
        $select = $this->_db->select();
        $select->from(array(
            $this->_fetchOptions['tableAlias'] => $this->_fullTableName())
        );
        
        if (!$this->_fetchOptions['ignore']) {
            if (is_array($this->_fetchOptions['joins'])) {
                foreach ($this->_fetchOptions['joins'] AS $table => $joins) {
                    if (!is_numeric(key($joins))) {
                        $joins = array($joins);
                    }
                    foreach ($joins AS $join) {
                        $alias = ($join['alias']) ? $join['alias']
                                                  : substr($table, 0, 1);
                        $type = ($join['type']) ? $join['type'] : 'join';
                        
                        if (!preg_match('/^join/', $type)) {
                            $type = 'join' . ucfirst($type);
                        }
                        
                        $select->{$type}(
                            array("$alias" => $table),
                            $join['on'], $join['cols']
                        );
                        $i++;
                    }
                }
            }
        }
        
        // add where clause
        $select->where($fetchCol . ' = ?', $itemId);
        
        // do select
        $stmt = $this->_db->query($select);
        $results = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

        // throw exception if no items returned
        if (count($results) == 0) {
            return false;
        }

        // get item and update instance
        $this->updateVariables($results[0]);

        $this->_fetchOvoyoModelParent();

        // fetch related data
        if (!$this->_fetchRelations()) {
            return false;
        }

        if (!$this->__isParent) {
            $this->postFetch();
        }

        return true;
    }

    /**
     * Post-fetch method
     *
     * Expects to be overridden
     *
     * @return void
     */
    public function postFetch()
    { }

    /**
     * Fetch item from DB using given fields
     *
     * @param array $fetchBy List of fields and their values to fetch data by
     * @return bool
     * @throws Zend_Db_Table_Exception, Exception
     */
    public function fetchBy(array $fetchBy)
    {
        try {
            // build query
            $select = $this->_db->select();
            $select->from(array(
                $this->_fetchOptions['tableAlias'] => $this->_fullTableName())
            );
            
            // add any joins
            if (!$this->_fetchOptions['ignore']) {
                if (is_array($this->_fetchOptions['joins'])) {
                    foreach ($this->_fetchOptions['joins'] AS $table => $joins) {
                        if (!is_numeric(key($joins))) {
                            $joins = array($joins);
                        }
                        foreach ($joins AS $join) {
                            $alias = ($join['alias']) ? $join['alias']
                                                      : substr($table, 0, 1);
                            $type = ($join['type']) ? $join['type'] : 'join';
                            
                            if (!preg_match('/^join/', $type)) {
                                $type = 'join' . ucfirst($type);
                            }
                            
                            $select->{$type}(
                                array("$alias" => $table),
                                $join['on'], $join['cols']
                            );
                            $i++;
                        }
                    }
                }
            }
            
            // add where clause
            $tableInfo =& $this->tableInfo();
            foreach ($fetchBy AS $field => $value) {
                $fetchCol = $field;
                if (isset($tableInfo['metadata'][$field])) {
                    $fetchCol = $this->_fetchOptions['tableAlias']
                              . '.' . $fetchCol;
                }
                $select->where($fetchCol . ' = ?', $value);
            }
            
            // do select
            $stmt = $this->_db->query($select);
            $results = $stmt->fetchAll(Zend_Db::FETCH_ASSOC);

            if (count($results) == 0) {
                return false;
            }

            $this->updateVariables($results[0]);

            $this->_fetchOvoyoModelParent();

            // fetch related data
            $this->_fetchRelations();

            if (!$this->__isParent) {
                $this->postFetch();
            }

        } catch (Exception $e) {
            // if fetch failed and we have a parent, try fetching parent by supplied
            // criteria and then loading child
            if ($this->__parent) {
                if ($this->__parent->fetchBy($fetchBy)) {
                    // get parent cols
                    $parentCols = $this->__parent->getPrimaryKeyWhere(true);

                    $tableInfo = $this->tableInfo();

                    $newFetchBy = array();
                    foreach ($tableInfo['metadata'] AS $colInfo) {
                        $colName = $colInfo['COLUMN_NAME'];
                        if (isset($parentCols[$colName])) {
                            $newFetchBy[$colName] = $parentCols[$colName];
                        }
                    }

                    return $this->fetchBy($newFetchBy);

                } else {
                    return false;
                }
            } else {
                throw $e;
            }
        }

        return true;
    }

    /**
     * Fetch parent
     *
     * @return  boolean
     */
    private function _fetchOvoyoModelParent()
    {
        if (!($this->__parent instanceOf Ovoyo_Model)) {
            return false;
        }

        $parentPrimaryKeys = $this->__parent->getPrimaryKeys();
        $fetchBy = array();
        foreach ($parentPrimaryKeys AS $primaryKey) {
            if (isset($this->{$primaryKey})) {
                $fetchBy[$primaryKey] = $this->{$primaryKey};
            }
        }
        if (!$this->__parent->fetchBy($fetchBy)) {
            return false;
        }

        $classVars = get_class_vars(get_class($this));

        $parentVars = $this->__parent->toArray();
        foreach ($parentVars AS $variable => $value) {
            if (!isset($this->{$variable})) {
                $this->{$variable} = null;
                $this->{$variable} = &$this->__parent->{$variable};
            }
        }
    }


    /**
     * Fetch related data
     *
     * @param  string|array $relationsToFetch Optional list of relations to fetch
     * @return boolean
     * @throws Exception
     */
    protected function _fetchRelations($relationsToFetch = null)
    {
        if ($this->_ignoreRelations) {
            return true;
        }

        if (!is_array($this->_relationships)) {
            return true;
        }

        $tableInfo =& $this->tableInfo();

        if ($relationsToFetch !== null) {
            if (!is_array($relationsToFetch)) {
                $relationsToFetch = array($relationsToFetch);
            }
            $relationsToFetch = array_flip($relationsToFetch);
        }
        

        // fetch relationships
        foreach ($this->_relationships AS $relationshipName => $mapping) {
            // if supplied with a list, only fetch requested relations
            if (is_array($relationsToFetch)) {
                if (!array_key_exists($relationshipName, $relationsToFetch)) { continue; }

            // otherwise, ignore anything in our _ignoredRelationships array
            } else if (array_key_exists($relationshipName, $this->_ignoredRelationships)) {
                continue;
            }

            // one-to-one
            if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2ONE) {
                $fetchBy = array($mapping['relationKey'] => $this->{$mapping['relationKey']});
                foreach ($mapping['where'] AS $field => $value) {
                    $fetchBy[$field] = $value;
                }

                $model = Ovoyo_Model::factory(
                    $mapping['relation'], $mapping['options']
                );
                $model->fetchBy($fetchBy);

                $this->{$relationshipName} = $model;

            // one-to-many / many-to-many
            } else if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2MANY ||
                       $mapping['type'] == self::RELATIONSHIP_TYPE_MANY2MANY) {

                $oneToMany  = ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2MANY) ? true : false;
                $manyToMany = ($mapping['type'] == self::RELATIONSHIP_TYPE_MANY2MANY) ? true : false;

                // if many-to-many use relatedBy table
                if ($manyToMany) {
                    $linkingArray = key($mapping['relatedBy']);
                    $relatedByClass   = current($mapping['relatedBy']);

                    $model = Ovoyo_Model::factory(
                        $relatedByClass, $mapping['options']
                    );

                // otherwise assume direct link with relation
                } else {
                    $model = Ovoyo_Model::factory(
                        $mapping['relation'], $mapping['options']
                    );
                }

                // get info about about relation model db table
                $modelTableInfo = $model->tableInfo();

                // build options to pass to our data select
                $whereArray = array();
                foreach ($mapping['foreignKeys'] AS $foreignKey) {
                    $whereArray[$foreignKey] = $this->{$foreignKey};
                }
                foreach ($mapping['where'] AS $field => $value) {
                    $whereArray[$field] = $value;
                }
                $options = array('where' => $whereArray);

                // if we have a relationKey, get relations by id
                $relations = array();
                if (isset($mapping['relationKey'])) {
                    $options['key']   = $mapping['relationKey'];
                    $options['order'] = (isset($mapping['order'])) ? $mapping['order'] : '';
                    $relationIds = $model->dataSetAsArray($options);

                    // for many-to-many, store ids in linking array
                    if ($manyToMany) {
                        $this->{$linkingArray} = $relationIds;
                        $this->_relationshipLinks[$linkingArray] = $relationshipName;
                    }

                    // fetch each relation by id
                    $fetchBy = ($oneToMany) ? $whereArray : array();
                    foreach ($relationIds AS $relationId) {
                        $fetchBy[$mapping['relationKey']] = $relationId;
                        $relation = Ovoyo_Model::factory(
                            $mapping['relation'], $mapping['options']
                        );
                        $relation->fetchBy($fetchBy);

                        $relations[$relationId] = $relation;
                    }

                // otherwise fetch by foreign key(s)
                } else {
                    // first find all matching items
                    $stmt = $model->search(
                        $whereArray, $mapping['order'],
                        $modelTableInfo['primary']
                    );
                    $data = $stmt->fetchAll(Zend_Db::FETCH_OBJ);

                    // now fetch each item as model object
                    foreach ($data AS $row) {
                        $relation = Ovoyo_Model::factory(
                            $mapping['relation'], $mapping['options']
                        );

                        $fetchBy = array();
                        foreach ($modelTableInfo['primary'] AS $key) {
                            $fetchBy[$key] = $row->{$key};
                        }
                        $relation->fetchBy($fetchBy);


                        if (count($fetchBy) == 1) {
                            $relationKey = key($fetchBy);
                            $relationId  = $relation->{$relationKey};

                            $relations[$relationId] = $relation;
                        } else {
                            $relations[] = $relation;
                        }
                    }
                }

                $this->{$relationshipName} = $relations;
            }
        }

        return true;
    }

    /**
     * Insert new item into DB
     *
     * @return boolean
     * @throws Exception
     */
    public function insert()
    {
        $this->beginTransaction();
        try {
            if (!$this->preInsert()) {
                $this->rollback();
                return false;
            }

            if (!$this->_save(1)) {
                $this->rollback();
                return false;
            }

            if (!$this->postInsert()) {
                $this->rollback();
                return false;
            }

            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }

        return true;
    }

    /**
     * Pre-insert
     *
     * Expects to be overridden
     *
     * @return boolean
     */
    public function preInsert()
    {
        return true;
    }

    /**
     * Post-insert
     *
     * Expects to be overridden
     *
     * @return boolean
     */
    public function postInsert()
    {
        return true;
    }

    /**
     * Update an existing item to DB
     *
     * @return boolean
     * @throws Exception
     */
    public function update()
    {
        $this->beginTransaction();
        try {
            if (!$this->preUpdate()) {
                $this->rollback();
                return false;
            }

            // set values for updatedDate and updatedBy fields if they exist
            $tableInfo = $this->tableInfo();

            if(in_array('updatedDate', $tableInfo['cols'])){
                $this->updatedDate = new Zend_Date();
            }
            if(in_array('updatedBy', $tableInfo['cols'])){
                $user = $this->webContainer->getUser();
                $this->updatedBy = $user->firstName.' '.$user->lastName;
            }

            if (!$this->_save()) {
                $this->rollback();
                return false;
            }

            if (!$this->postUpdate()) {
                $this->rollback();
                return false;
            }

            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }

        return true;
    }

    /**
     * Pre-update
     *
     * Expects to be overridden
     *
     * @return boolean
     */
    public function preUpdate()
    {
        return true;
    }

    /**
     * Post update
     *
     * Expects to be overridden
     *
     * @return boolean
     */
    public function postUpdate()
    {
        return true;
    }

    /**
     * Save item to DB
     *
     * @param   int forceInsert Flag to specify if an insert should be forced or not
     * @return  bool
     * @throws  Exception
     */
    protected function _save($forceInsert = 0)
    {
        // only perform preSave() if not a parent class, since postSave()
        // will be called by child instance, which may have overridden it
        if (!$this->__isParent) {
            if (!$this->preSave()) {
                return false;
            }
        }

        // check _dbTable has been defined
        if (!$this->_dbTable instanceOf Zend_Db_Table_Abstract) {
            throw new Exception("_dbTable has not been defined");
        }

        // only perform isValid() if not a parent class, since isValid()
        // will be called by child instance, which may have overridden it
        if (!$this->__isParent && !$this->isValid()) {
            return false;
        }
        
        $this->beginTransaction();

        // do insert or update
        try {

            // get table info and build where clause from primary fields
            $tableInfo = $this->tableInfo();

            // special case for parent class relation
            $hasParent = false;
            $sameTableParent = false;
            if ($this->__parent instanceof Ovoyo_Model) {
                $parentTableInfo = $this->__parent->tableInfo();
                if ($parentTableInfo['name'] == $tableInfo['name']) {
                    $sameTableParent = true;
                }
            
                if (!$this->__parent->_save($forceInsert)) {
                    $this->rollback();
                    return false;
                }
            }
            
            $alreadyExists = $this->alreadyExists($tableInfo['primary'], null, true);
            
            // if we have a parent class which uses the same db table as child,
            // we don't need to do any insert or update at this point since this
            // will already have been performed by parent
            if (!$hasParent && !$sameTableParent) {

                $doUpdate = $alreadyExists;
    
                // insert new item
                if (!$doUpdate || $forceInsert) {
                    if(in_array('createdDate', $tableInfo['cols'])){
                        $this->createdDate = new Zend_Date();
                    }
    
                    if(in_array('updatedBy', $tableInfo['cols'])){
                        $user = $this->webContainer->getUser();
                        $this->updatedBy = $user->firstName.' '.$user->lastName;
                    }
                }
    
                // build date for save
                $data = array();
                foreach ($tableInfo['metadata'] AS $colInfo) {
                    $colName = $colInfo['COLUMN_NAME'];
                    $colValue = null;
                    
                    // for inserts, ignore primary keys that are
                    // auto incrementing and empty
                    if ((!$doUpdate || $forceInsert) && $colInfo['IDENTITY']) {
                        if (!$this->{$colName}) {
                            continue;
                        }
                    }
    
                    // if date field, get date value
                    if ($colInfo['DATA_TYPE'] == 'date'
                        && $this->{$colName} instanceOf Zend_Date) {
                        $colValue = $this->{$colName}->get('yyyyMMdd');
                        
                    } else if ($colInfo['DATA_TYPE'] == 'datetime'
                        && $this->{$colName} instanceOf Zend_Date) {
                        $colValue = $this->{$colName}->get('yyyyMMddHHmmss');
                        
                    } else {
                        $colValue = (isset($this->{$colName})) ? $this->{$colName}
                                                               : $colInfo['DEFAULT'];
                    }
                    
                    if (!is_null($colValue)) {
                        $data[$colName] = $colValue;
                    } else {
                        if ($colInfo['NULLABLE']) {
                            $data[$colName] = new Zend_Db_Expr('NULL');
                        }
                    }
                }
                
                // insert new item
                if (!$doUpdate || $forceInsert) {
                    $this->_dbTable->insert($data);
    
                    // if only one primary key - fetch last id
                    if (count($tableInfo['primary']) == 1) {
                        reset($tableInfo['primary']);
                        $primaryKey = current($tableInfo['primary']);
                        if (!$this->{$primaryKey}) {
                            $this->{$primaryKey} = $this->_db->lastInsertId();
                        }
                    }
    
                // update existing item
                } else {
                    $where = $this->getPrimaryKeyWhere();
    
                    if (!$where) {
                        throw new Exception("could not build where clause");
                    }
    
                    $this->_dbTable->update($data, $where);
                }
            }

            // update related items
            if (count($this->_relationships) > 0) {
                if (!$this->_saveRelations($alreadyExists, $forceInsert)) {
                    $this->rollback();
                    return false;
                }
            }

            // only perform postSave() if not a parent class, since postSave()
            // will be called by child instance, which may have overridden it
            if (!$this->__isParent) {
                if (!$this->postSave()) {
                    $this->rollback();
                    return false;
                }
            }

            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }


        return true;
    }

    /**
     * Pre-save
     *
     * Expects to be overridden
     *
     * @return boolean
     */
    public function preSave()
    {
        return true;
    }

    /**
     * Post-save
     *
     * Expects to be overridden
     *
     * @return boolean
     */
    public function postSave()
    {
        return true;
    }

    /**
     * Save relations
     *
     * @param boolean $existingItem Should be set to true if item we are saving already exists
     * @param int forceInsert Flag to specify if an insert should be forced or not
     * @return boolean
     * @throws Ovoyo_Model_Exception
     */
    protected function _saveRelations($existingItem, $forceInsert = 0)
    {
        if ($this->_ignoreRelations) {
            return true;
        }

        try {
            $this->beginTransaction();

            foreach ($this->_relationships AS $relationshipVar => $mapping) {
                if (!($mapping['cascade']['update'])) { continue; }

                // one-to-one
                if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2ONE) {
                    $relation = $this->{$relationshipVar};
                    foreach ($mapping['foreignKeys'] AS $foreignKey) {
                        $relation->{$foreignKey} = $this->{$foreignKey};
                    }
                    if ($relation instanceOf Ovoyo_Model && !$relation->_save($forceInsert)) {
                        $this->rollback();
                        return false;
                    }

                // one-to-many
                } else if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2MANY) {
                    $relations = $this->{$relationshipVar};

                    // delete previous relations
                    if ($existingItem && $mapping['cascade']['delete']) {
                        $relationModel = Ovoyo_Model::factory($mapping['relation'], $mapping['options']);

                        $whereArray = array();
                        foreach ($mapping['foreignKeys'] AS $foreignKey) {
                            $whereArray[$foreignKey] = $this->{$foreignKey};
                        }
                        
                        $relationModel->deleteBy($whereArray);
                    }

                    if (is_array($relations)) {
                        foreach ($relations AS $relation) {
                            foreach ($mapping['foreignKeys'] AS $foreignKey) {
                                $relation->{$foreignKey} = $this->{$foreignKey};
                            }

                            if (!$relation->_save($forceInsert)) {
                                $this->rollback();
                                return false;
                            }
                        }
                    }

                // many-to-many - only saving linking data
                } else if ($mapping['type'] == self::RELATIONSHIP_TYPE_MANY2MANY) {
                    $linkingArray   = key($mapping['relatedBy']);
                    $relatedByClass = current($mapping['relatedBy']);

                    $linkingModel = Ovoyo_Model::factory($relatedByClass, $mapping['options']);

                    // delete existing links
                    if ($existingItem) {
                        $whereArray = array();
                        foreach ($mapping['foreignKeys'] AS $foreignKey) {
                            $whereArray[$foreignKey] = $this->{$foreignKey};
                        }
                        $linkingModel->_deleteMethod = self::MODEL_DELETE_METHOD_PHYSICAL;
                        $linkingModel->deleteBy($whereArray);
                    }

                    // save current links
                    if (is_array($this->{$linkingArray})) {
                        foreach ($this->{$linkingArray} AS $linkedItemId) {
                            $model = Ovoyo_Model::factory($relatedByClass, $mapping['options']);
                            $model->{$mapping['relationKey']} = $linkedItemId;
                            foreach ($mapping['foreignKeys'] AS $foreignKey) {
                                $model->{$foreignKey} = $this->{$foreignKey};
                            }
                            $model->insert();
                        }
                    }
                }
            }

            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }

        return true;
    }

    /**
     * Check whether item can be deleted
     *
     * Should be overridden by sub-classes where required
     *
     * @return  boolean
     */
    protected function _canDelete()
    {
        return true;
    }

    /**
     * Delete item from DB
     *
     * @param   array|string $where Optional where clause
     * @return  bool
     * @throws  Exception
     */
    public function delete($where = null)
    {
        // check _dbTable has been defined
        if (!$this->_dbTable instanceOf Zend_Db_Table_Abstract) {
            throw new Exception("_dbTable has not been defined");
        }

        if (!$this->_canDelete()) { return false; }

        $deleteWhere = '';
        
        // where clause has been supplied as array - expecting key => value pairs
        if (is_array($where)) {
            $i++;
            foreach ($where AS $field => $value) {
                if ($i > 1) {
                    $deleteWhere.= ' AND ';
                }
                $deleteWhere.= $this->_db->quoteInto("$field = ?", $value);
                $i++;
            }

        // where clause has been supplied as a string
        } else if (is_string($where)) {
            $deleteWhere = $where;

        // no where clause has been supplied, so construct from primary key(s)
        } else {
            $deleteWhere = $this->getPrimaryKeyWhere();

        }

        if (!$deleteWhere) {
            $exception = 'Could not build where clause for delete of '
                       . get_class($this) . ' object, and no where clause '
                       . 'supplied';
            throw new Ovoyo_Model_Exception($exception);
        }
        
        $callParentDelete = false;

        // delete item
        $this->beginTransaction();
        try {
            if (!$this->preDelete()) {
                $this->rollback();
                return false;
            }

            // for logical deletes, just do an update
            if ($this->_deleteMethod == self::MODEL_DELETE_METHOD_LOGICAL) {
                $logicalDeleteData = $this->_getLogicalDeleteData();
                
                // sanitize data
                $data = array();
                $tableInfo = $this->tableInfo();
                foreach ($logicalDeleteData AS $fieldName => $value) {
                    $colInfo = $tableInfo['metadata'][$fieldName];
                    if (!$colInfo) {
                        continue;
                    }
    
                    // if date field, get date value
                    if ($colInfo['DATA_TYPE'] == 'date'
                        && $value instanceOf Zend_Date) {
                        $value = $value->get('yyyyMMdd');
                        
                    } else if ($colInfo['DATA_TYPE'] == 'datetime'
                        && $value instanceOf Zend_Date) {
                        $value = $value->get('yyyyMMddHHmmss');
                        
                    }
                    
                    if (!is_null($value)) {
                        $data[$fieldName] = $value;
                    }
                }
                
                if (count($data) == 0) {
                    $message = "Invalid logical delete fields they dont't"
                             . " exist or have no values set against them";
                    throw new Exception($message);
                }
                
                $this->_dbTable->update($data, $deleteWhere);
                
            // else physical delete
            } else {
                $this->_dbTable->delete($deleteWhere);
            }

            // delete related items
            if (count($this->_relationships) > 0) {
                $this->_deleteRelations();
            }
        
            if (!$this->postDelete()) {
                $this->rollback();
                return false;
            }

            $this->commit();
        } catch (Exception $e) {
            if ($this->__parent instanceOf Ovoyo_Model) {
                $callParentDelete = true;
            } else {
                $this->rollback();
                throw $e;
            }
        }
        
        // delete caused exception, so try calling delete on parent instance
        if ($callParentDelete) {
            try {
                if (!$this->__parent->delete($where)) {
                    $this->rollback();
                    return false;
                }
                
                if (!$this->postDelete()) {
                    $this->rollback();
                    return false;
                }
                
                $this->commit();
            } catch (Exception $e) {
                $this->rollback();
                throw $e;
            }
        }

        return true;
    }
    
    /**
     * Get data to update during a logical delete
     *
     * @return array
     */
    protected function _getLogicalDeleteData()
    {
        return $this->toArray();
    }

    /**
     * Pre-delete
     *
     * Actions to perform prior to call to delete() - expects to be overridden
     *
     * @return boolean
     */
    public function preDelete()
    {
        return true;
    }

    /**
     * Post-delete
     *
     * Actions to perform after call to delete() - expects to be overridden
     *
     * @return boolean
     */
    public function postDelete()
    {
        return true;
    }

    /**
     * Delete relations
     *
     * @return boolean
     * @throws Ovoyo_Model_Exception
     */
    protected function _deleteRelations()
    {
        // do nothing if doing a logical delete
        if ($this->_deleteMethod == self::MODEL_DELETE_METHOD_LOGICAL) {
            return true;
        }
        
        try {
            $this->beginTransaction();

            foreach ($this->_relationships AS $relationshipVar => $mapping) {
                if (!($mapping['cascade']['delete'])) { continue; }

                // one-to-one
                if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2ONE) {
                    $relation = $this->{$relationshipVar};
                    $relation->delete();

                // one-to-many
                } else if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2MANY) {
                    $relations = $this->{$relationshipVar};
                    if (is_array($relations)) {
                        foreach ($relations AS $relation) {
                            $relation->delete();
                        }
                    }
                    unset($this->{$relationshipVar});

                // many-to-many
                } else if ($mapping['type'] == self::RELATIONSHIP_TYPE_MANY2MANY) {
                    $linkingArray   = key($mapping['relatedBy']);
                    $relatedByClass = current($mapping['relatedBy']);

                    $linkingModel = Ovoyo_Model::factory($relatedByClass, $mapping['options']);

                    // delete links to relations only, not the relations themselves
                    $whereArray = array();
                    foreach ($mapping['foreignKeys'] AS $foreignKey) {
                        $whereArray[$foreignKey] = $this->{$foreignKey};
                    }
                    $linkingModel->deleteBy($whereArray);

                    unset($this->{$linkingArray});
                    unset($this->{$relationshipVar});
                }
            }

            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }

        return true;
    }

    /**
     * Delete item from DB using given fields
     *
     * @param   array $deleteBy List of fields and their values to delete data by
     * @return  boolean
     * @throws  Zend_Db_Table_Exception, Exception
     */
    public function deleteBy(array $deleteBy)
    {
        $callParentDelete = false;
        
        $this->beginTransaction();
        try {
            $deleteWhere = array();

            foreach($deleteBy AS $colName => $value) {
                if (is_numeric($colName)) {
                    $deleteWhere[] = $this->_db->quoteInto($value);
                } else {
                    $deleteWhere[] = $this->_db->quoteInto($colName. ' = ?', $value);
                }
            }

            // do logical delete
            if ($this->_deleteMethod == self::MODEL_DELETE_METHOD_LOGICAL) {
                $logicalDeleteData = $this->_getLogicalDeleteData();
                if (!is_array($logicalDeleteData)) {
                    $exception = 'Trying to do logical delete on '
                               . get_class($this) . ' object but no '
                               . 'update data has been provided';
                    throw new Ovoyo_Model_Exception($exception);
                }

                // sanitize data
                $data = array();
                $tableInfo = $this->tableInfo();
                foreach ($logicalDeleteData AS $fieldName => $value) {
                    $colInfo = $tableInfo['metadata'][$fieldName];
                    if (!$colInfo) {
                        continue;
                    }
    
                    // if date field, get date value
                    if ($colInfo['DATA_TYPE'] == 'date'
                        && $value instanceOf Zend_Date) {
                        $value = $value->get('yyyyMMdd');
                        
                    } else if ($colInfo['DATA_TYPE'] == 'datetime'
                        && $value instanceOf Zend_Date) {
                        $value = $value->get('yyyyMMddHHmmss');
                        
                    }
                    
                    if (!is_null($value)) {
                        $data[$fieldName] = $value;
                    }
                }
            
                if (count($data) > 0) {
                    $this->_dbTable->update($data, $deleteWhere);
                }
                
            // else physical delete
            } else {
                $this->_dbTable->delete($deleteWhere);
            }

            $this->commit();
        } catch (Exception $e) {
            if ($this->__parent instanceOf Ovoyo_Model) {
                $callParentDelete = true;
            } else {
                $this->rollback();
                throw $e;
            }
        }
        
        // delete caused exception, so try calling delete on parent instance
        if ($callParentDelete) {
            try {
                if (!$this->__parent->deleteBy($deleteBy)) {
                    $this->rollback();
                    return false;
                }
                
                $this->commit();
            } catch (Exception $e) {
                $this->rollback();
                throw $e;
            }
        }

        return true;
    }

    /**
     * Fetch data set from database and return as an associative array
     *
     * The following options may be specified:
     *
     * 'key' - string - name of col to use as key in return results (if not supplied primary key is used)
     * 'cols' - array - list of cols to fetch (if not supplied only primary key is fetched)
     * 'keyFormat' - string - format of key field, this can be any string, any instance of a column name prefixed with a '%' will be replaced with the value from that column e.g. '%firstName-$lastName'. If not supplied, primary key is used
     * 'valueFormat' - string - format of value field, this can be any string, any instance of a column name prefixed with a '%' will be replaced with the value from that column e.g. 'Name for id (%userId) is %firstName $lastName'. If not supplied, primary key is used
     * 'where' - array - array key/value pairs to build where clause from
     * 'order' - string - order by string (if not supplied, will order by value field)
     *
     * @param array $options Additional options
     * @return array Associative array
     * @throws Exception
     */
    public function dataSetAsArray($options = array())
    {
        $fullTableName = $this->_fullTableName();

        $tableInfo = $this->tableInfo();

        // get cols to fetch
        $cols = (isset($options['cols']) && is_array($options['cols'])) ? $options['cols'] : $tableInfo['primary'];
        if (count($cols) == 0) {
            $exception = 'failed to fetch data set - no columns to fetch';
            throw new Ovoyo_Model_Exception($exception);
        }

        // get our key field
        if (isset($options['key'])) {
            $key = $options['key'];
        } else {
            $key = implode(':', $tableInfo['primary']);
        }

        // build regex patterns to be replaced when building data set array
        $patterns = array();
        foreach ($cols AS $colName) {
            $patterns[] = "/%$colName/";
        }

        // format of array key field
        $keyFormat = (isset($options['keyFormat'])) ? $options['keyFormat'] : "%$key";

        // format of array value field
        $valueFormat = (isset($options['valueFormat'])) ? $options['valueFormat'] : "%$key";

        // start build of select statement
        $select = $this->_dbTable->select();
        $select->from($fullTableName, $cols);

        // add any where clauses
        if (isset($options['where']) && is_array($options['where'])) {
            foreach ($options['where'] AS $colName => $value) {
                if (!is_int($colName)) {
                    $select->where($colName . ' = ?', $value);
                } else {
                    $select->where($value);
                }
            }
        }

        // add order string
        if (isset($options['order'])) {
            $select->order($options['order']);
        }

        // fetch data
        $rows = $this->_dbTable->fetchAll($select);

        // build data set
        $dataSet = array();
        foreach ($rows AS $row) {
            $replacements = array();
            foreach ($cols AS $colName) {
                $replacements[] = $row->$colName;
            }

            $formattedKey   = preg_replace($patterns, $replacements, $keyFormat);
            $formattedValue = preg_replace($patterns, $replacements, $valueFormat);
            $dataSet[$formattedKey] = $formattedValue;
        }

        return $dataSet;
    }

    /**
     * Check if similar item already exists in the db
     *
     * @param string|array $cols Column name to check against or array of column names
     * @param array $additionalWhere
     * @param boolean $includePrimaryKeys If set to true, will also search for items with matching primary keys
     * @return boolean
     * @throws Exception
     */
    public function alreadyExists($cols, $additionalWhere = array(), $includePrimaryKeys = false)
    {
        try {
            // if no cols provided, use primary keys
            if ($cols == '' || (is_array($cols) && count($cols) == 0)) {
                $cols= $this->getPrimaryKeys();
            }
    
            if (!is_array($cols)) {
                $cols = array($cols);
            }
    
            // start build of select statement
            $select = $this->_db->select();
            $select->from($this->_fullTableName());
    
            // add any where clauses
            foreach ($cols AS $colName) {
                $value = (isset($this->{$colName})) ? $this->{$colName} : '';
                $select->where('`' . $colName. '` = ?', $value);
            }
    
            if (!$includePrimaryKeys) {
                $primaryKeyVals = $this->getPrimaryKeyWhere(true);
                foreach ($primaryKeyVals AS $colName => $value) {
                    if (!isset($value)) { continue; }
                    $select->where('`' . $colName. '` != ?', $value);
                }
            }
    
            if (is_array($additionalWhere)) {
                foreach ($additionalWhere AS $colName => $value) {
                    if (is_numeric($colName)) {
                        $select->where($value);
                    } else {
                        $select->where('`' . $colName. '` = ?', $value);
                    }
                }
            }
    
            $select->limit(1);
    
            // fetch data
            $row = $this->_db->fetchRow($select);
    
            return (!$row) ? false : true;
        } catch (Exception $e) {
            if ($this->__parent) {
                if ($this->__parent->alreadyExists($cols, $additionalWhere, $includePrimaryKeys)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                throw $e;
            }
        }
    }

    /**
     * Return model values as an array
     *
     * @param   boolean $ignoreRelations
     * @return  array
     */
    public function toArray($ignoreRelations = false)
    {
        $tableInfo = $this->tableInfo();

        $data = array();
        $returnedFields = array();

        if ($this->__parent instanceOf Ovoyo_Model) {
            $data = $this->__parent->toArray();
        }

        foreach ($tableInfo['metadata'] AS $colInfo) {
            $colName = $colInfo['COLUMN_NAME'];

            if ($this->{$colName} instanceof Zend_Date) {
                if ($colInfo['DATA_TYPE'] == 'date') {
                    $data[$colName] = $this->{$colName}->toString('yyyy-MM-dd');
                } elseif ($colInfo['DATA_TYPE'] == 'datetime') {
                    $data[$colName] = $this->{$colName}->toString('yyyy-MM-dd HH:mm:ss');
                }
            } elseif (($colInfo['DATA_TYPE'] == 'date' || $colInfo['DATA_TYPE'] == 'datetime') && $this->{$colName} == 0) {
                $data[$colName] = '';
            } else {
                $data[$colName] = $this->{$colName};
            }
            $returnedFields[$colName] = 1;
        }
    
        // get value of other non-relationship, public vars defined in class
        foreach ($this AS $var => $value) {
            if (!array_key_exists($var, $data)
                && !array_key_exists($var, $this->_relationships)
                && !preg_match('/^_/' ,$var)) {
                    
                $data[$var] = $value;
            }
        }

        if ($ignoreRelations) { return $data; }

        foreach ($this->_relationships AS $relationshipVar => $mapping) {
            $returnedFields[$relationshipVar] = 1;
            if (!isset($mapping['type'])) { continue; }
            
            // one-to-one
            if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2ONE) {
                $relation = $this->{$relationshipVar};
                $data[$relationshipVar] = (!is_null($relation)) ? $relation->toArray() : array();

            // one-to-many
            } else if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2MANY) {
                $relations = $this->{$relationshipVar};
                if (is_array($relations)) {
                    foreach ($relations AS $key => $relation) {
                        $data[$relationshipVar][$key] = $relation->toArray();
                    }
                }

            // many-to-many - only saving linking data
            } else if ($mapping['type'] == self::RELATIONSHIP_TYPE_MANY2MANY) {
                $linkingArray   = key($mapping['relatedBy']);
                $data[$linkingArray] = (is_array($this->{$linkingArray})) ? $this->{$linkingArray} : array();
            }
        }

        return $data;
    }

    /**
     * Begin a transaction
     *
     * @return boolean
     */
    public function beginTransaction()
    {
        return $this->_db->beginTransaction();
    }

    /**
     * Commit transaction
     *
     * @return boolean
     */
    public function commit()
    {
        return $this->_db->commit();
    }

    /**
     * Rollback a transaction
     *
     * @return boolean
     */
    public function rollback()
    {
        return $this->_db->rollback();
    }

    /**
     * Get options related to this model
     *
     * @deprecated Please use getModelOptions();
     * @return array
     */
    protected function _modelOptions()
    {
        return $this->getModelOptions();
    }
    
    /**
     * Get options related to this model
     *
     * @return array
     */
    public function getModelOptions()
    {
        $options = array(
            'connName' => $this->_connName,
            'tablePrefix' => $this->_tablePrefix,
            'deleteMethod' => $this->_deleteMethod
        );
        
        return $options;
    }
    
    /**
     * Override db connection
     *
     * Will replace $this->_db with the connection provided
     *
     * @param   string $connName
     * @return  void
     */
    public function overrideDbConnection($connName)
    {
        $db = Ovoyo_WebContainer::getDbConnection($connName);
        
        $this->_connName = $connName;
        $this->_db = $db;
        unset($this->_dbTable);
        $this->_dbTableConfig = array();
        $this->tableInfo(true);
        
        // update parent
        if ($this->__parent instanceof Ovoyo_Model) {
                $this->__parent->overrideDbConnection($connName);
        }
        
        // update relations
        foreach ($this->_relationships AS $relationshipVar => $mapping) {
            // one-to-one
            if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2ONE) {
                $relation = $this->{$relationshipVar};
                if ($relation instanceOf Ovoyo_Model) {
                        $relation->overrideDbConnection($connName);
                }
    
            // one-to-many
            } else if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2MANY) {
                $relations = $this->{$relationshipVar};
                if (is_array($relations)) {
                    foreach ($relations AS $relation) {
                            if ($relation instanceOf Ovoyo_Model) {
                                $relation->overrideDbConnection($connName);
                            }
                    }
                }
    
            // many-to-many
            } else if ($mapping['type'] == self::RELATIONSHIP_TYPE_MANY2MANY) {
                $mapping['options']['connName'] = $connName;
                $this->_relationships[$relationshipVar] = $mapping;
            }
        }
    }

    /**
     * Add a relationship mapping
     *
     * Takes an array and adds to list of mapped relationships.  Array can contain:
     * 'type' - (optional) string - relationship type (one of RELATIONSHIP_TYPE_xxx constants)
     * 'relation' - (required) string - name of related class
     * 'relatedBy' - (optional) array - 'listName' => 'className' - listName is the name of the var to store mapped items list and className is the name of the class representing the related items
     * 'foreignKeys' - (optional) array - foreign keys in relation that link back to this class - defaults to primary keys of this class
     * 'relationKey' - (optional) string - the relationKey specifies which relation field to use as the identifying key for each relation.  Required for many-to-many, and if not supplied, relation primary key will be used.
     * 'cascade' - (optional) array - cascade operations to perform e.g. array('update' => true, 'delete' => true). Updates are not relevant for many-to-many relationships.  Default is as per the example
     * 'order' - (optional) string - SQL string to use when fetching mapped items
     * 'options' - (options) array - options to pass to Model::factory when creating objects of this relationship
     *
     * @parma   string $name Name of mapping and also name of class variable used to store relationship instance(s)
     * @param   array $mapping Array holding mapping info
     * @return  void
     * @throws  Ovoyo_Model_Exception
     */
    public function addRelationship($name, $mapping)
    {
        // only add relationship if not already done so
        if (isset($this->_relationships[$name])) {
            $exception = "relationship ($name) has already been defined";
        }

        if (isset($exception)) {
            require_once 'Ovoyo/Model/Exception.php';
            throw new Ovoyo_Model_Exception($exception);
        }

        $this->_relationships[$name] = $mapping;
    }

    /**
     * Add a relationship mapping
     *
     * See public method addRelationship() for parameter info.  This method was added so that we can do the error
     * checking and tidying up, once the Model's _setup() method has been called.
     *
     * @parma   string $name Name of mapping and also name of class variable used to store relationship instance(s)
     * @param   array $mapping Array holding mapping info
     * @return  void
     * @throws  Ovoyo_Model_Exception
     */
    protected function _addRelationship($name, $mapping)
    {
        if ($this->_ignoreRelations) { return; }
        if (array_key_exists($name, $this->_ignoredRelationships)) { return; }

        $tableInfo = $this->tableInfo();
        $exception = '';

        // can not add a relationship if no primary keys
        if (count($tableInfo['primary']) == 0) {
            $exception = "could not add relationship ($name) - no primary keys "
                       . "could be found for current class ("
                       . get_class($this) .")";
        }

        if (!empty($this->{$name})) {
            $exception = "could not add relationship ($name) - an instance "
                       . "variable already exists with the same name";
        }

        // define default options for model
        if (!isset($mapping['options']) || !is_array($mapping['options'])) {
            $mapping['options'] = $this->getModelOptions();
        }

        // if relationship is with self, then add to ignored relationship list
        if ($mapping['relation'] == get_class($this)) {
            $this->ignoreRelationships($mapping['relation']);
            return;
        }

        // add table info for mapped relationship
        $relationModel = Ovoyo_Model::factory($mapping['relation'], $mapping['options']);
        $relationTableInfo = $relationModel->tableInfo();
        $mapping['relationTableInfo'] = $relationTableInfo;

        // make sure mapping has a type
        if (!isset($mapping['type'])) {
            if ($mapping['relatedBy']) {
                $mapping['type'] = self::RELATIONSHIP_TYPE_MANY2MANY;
            } else {
                $mapping['type'] = self::RELATIONSHIP_TYPE_ONE2ONE;
            }
        }

        // make sure mapping has valid type
        if ($mapping['type'] == self::RELATIONSHIP_TYPE_MANY2MANY && !$mapping['relatedBy']) {
            $exception = "relationship ($name) can not be added - "
                       . "many-to-many requires relatedBy details";
        }

        // make sure we have foreignKeys
        if (!isset($mapping['foreignKeys']) || !is_array($mapping['foreignKeys'])) {
            $mapping['foreignKeys'] = array();
            foreach ($tableInfo['primary'] AS $foreignKey) {
                $mapping['foreignKeys'][$foreignKey] = $foreignKey;
            }
        }

        /*        // make sure we have a relationKey
        if (!$mapping['relationKey']) {
            if (count($relationTableInfo['primary']) == 1) {
                $mapping['relationKey'] = current($relationTableInfo['primary']);
            } else {
                $exception = "could not determine relationKey for relationship ($name)";
            }
        }*/


        // check if a relation key has been provided, and if so exists as
        // a field in relation
        if (isset($mapping['relationKey'])) {
            $keyExists = false;
            foreach ($relationTableInfo['metadata'] AS $colInfo) {
                if ($mapping['relationKey'] == $colInfo['COLUMN_NAME']) {
                    $keyExists = true;
                    break;
                }
            }
            if (!$keyExists) {
                $exception = "provided relationKey ("
                           . $mapping['relationKey']
                           . ") does not exist in relation model";
            }

        // if no relation key has been provided, attempt to use primary key
        // from relation table if not one-to-many
        } else if ($mapping['type'] != self::RELATIONSHIP_TYPE_ONE2MANY) {
            if (count($relationTableInfo['primary']) == 1) {
                $mapping['relationKey'] = current($relationTableInfo['primary']);
            } else {
                $exception = "relationKey could not be determined "
                           . "automatically, please specify which field to "
                           . "use as identifier for each relation";
            }
        }

        // define cascade operations
        if (!isset($mapping['cascade']) || !is_array($mapping['cascade'])) {
            if ($mapping['type'] == self::RELATIONSHIP_TYPE_MANY2MANY) {
                $mapping['cascade'] = array('update' => true, 'delete' => true);
            } else {
                $mapping['cascade'] = array('update' => false, 'delete' => false);
            }
        }

        // make sure optional where clause is an array
        if (!isset($mapping['where']) || !is_array($mapping['where'])) {
            $mapping['where'] = array();
        }

        if ($exception) {
            require_once 'Ovoyo/Model/Exception.php';
            throw new Ovoyo_Model_Exception($exception);
        }

        $this->_relationships[$name] = $mapping;

        if ($mapping['type'] == self::RELATIONSHIP_TYPE_ONE2ONE) {
            $this->{$name} = null;
        } else if ($mapping['type'] == self::RELATIONSHIP_TYPE_MANY2MANY) {
            $linkingArray = key($mapping['relatedBy']);
            $this->_relationshipLinks[$linkingArray] = $name;
            $this->{$linkingArray} = array();
            $this->{$name} = array();
        } else {
            $this->{$name} = array();
        }
    }

    /**
     * Specify list of relationships to ignore when peforming db operations
     *
     * @param string|array $name Name or names of relationships to ignore
     * @return void
     */
    public function ignoreRelationships($name)
    {
        if (is_string($name)) {
            $this->_ignoredRelationships[$name] = $name;
        } else if (is_array($name)) {
            foreach ($name AS $toIgnore) {
                $this->_ignoredRelationships[$toIgnore] = $toIgnore;
            }
        }
    }

    /**
     * Simple search method
     *
     * Default search filter method allows for filterOptions to be provided as
     * an array of key/values pairs or a string
     *
     * @param mixed $filterOptions Options to pass to $this->_searchFilter()
     * @param array|string $order Order options as a string or key/value pairs
     * @param array $cols
     * @param array|string $group
     * @param string $limit
     * @return Zend_Db_Statement instance
     */
    public function search($filterOptions = null, $order = null, $cols = null, $group = null, $limit = null)
    {
        // build query
        $select = $this->_db->select();

        // add cols if provided, otherwise select all
        if (is_array($cols) && count($cols) > 0) {
            $select->from(array($this->_searchOptions['tableAlias'] => $this->_fullTableName()), $cols);
        } else {
            $select->from(array($this->_searchOptions['tableAlias'] => $this->_fullTableName()));
        }
        
        // add any joins
        if (!$this->_searchOptions['ignore']) {
            if (is_array($this->_searchOptions['joins']) && !$ignoreJoins) {
                foreach ($this->_searchOptions['joins'] AS $table => $joins) {
                    if (!is_numeric(key($joins))) {
                        $joins = array($joins);
                    }
                    foreach ($joins AS $join) {
                        $alias = ($join['alias']) ? $join['alias']
                                                  : substr($table, 0, 1);
                        $type = ($join['type']) ? $join['type'] : 'join';
                        
                        if (!preg_match('/^join/', $type)) {
                            $type = 'join' . ucfirst($type);
                        }
                        
                        $select->{$type}(
                            array("$alias" => $table),
                            $join['on'], $join['cols']
                        );
                        $i++;
                    }
                }
            }
        }

        $this->_searchFilter($select, $filterOptions);
        $this->_searchGroup($select, $group);
        $this->_searchOrder($select, $order);
        $this->_searchLimit($select, $limit);
//echo $select;
        // return statement
        return $this->_db->query($select);
    }
    
    /**
     * Filter search
     *
     * @param Zend_Db_Select $select
     * @param array|string $filterOptions Search options - can be an array of key/values pairs or a string
     * @return void
     */
    protected function _searchFilter($select, $filterOptions)
    {
        // where clause has been supplied as array - expecting key => value pairs
        if (is_array($filterOptions)) {
            foreach ($filterOptions AS $field => $value) {
                if ($field === 'having') {
                    continue;
                }
                if (is_numeric($field)) {
                    $select->where($value);
                } else {
                    $select->where($field . ' = ?', $value);
                }
            }

            // make sure 'having' exists and is and array
            if (!array_key_exists('having', $filterOptions)
                || !is_array($filterOptions['having'])) {

                $filterOptions['having'] = array();
            }

            foreach ($filterOptions['having'] AS $field => $value) {
                if (is_numeric($field)) {
                    $select->having($value);
                } else {
                    $select->having($field . ' = ?', $value);
                }
            }

        // where clause has been supplied as a string
        } else if (is_string($filterOptions)) {
            $select->where($filterOptions);

        }
    }
    
    /**
     * Order search
     *
     * @param   Zend_Db_Select $select
     * @param   array|string $order Order options as a string or key/value pairs
     * @return  void
     */
    protected function _searchOrder($select, $order = null)
    {
        if (is_array($order)) {
            foreach ($order AS $orderString) {
                $select->order($orderString);
            }
        } else if ($order != '') {
            $select->order($order);
        }
    }
    
    /**
     * Group search
     *
     * @param   Zend_Db_Select $select
     * @param   array|string $group
     * @return  void
     */
    protected function _searchGroup($select, $group)
    {
        if (is_array($group)) {
            foreach ($group AS $groupBy) {
                $select->group($groupBy);
            }
            
        } else if (isset($group)) {
            $select->group($group);
        }
    }
    
    /**
     * Limit search
     *
     * @param   Zend_Db_Select $select
     * @param   int|array $limit
     * @return  void
     */
    protected function _searchLimit($select, $limit)
    {
        if (!isset($limit)) {
            return;
        }
        
        if (is_array($limit)) {
            $select->limit($limit[0], $limit[1]);
        } else {
            $select->limit($limit);
        }
    }
    
    /**
     * Set joins to use in search
     *
     * @deprecated see setSearchJoins()
     * @param   array $joins
     * @return  void
     */
    protected function _setSearchJoins(array $joins)
    {
            $this->_searchOptions['joins'] = $joins;
    }
    
    
    /**
     * Get joins used in searches
     *
     * @return array
     */
    public function getSearchJoins()
    {
        return $this->_searchOptions['joins'];
    }
    
    /**
     * Set joins to use in search
     *
     * @param   array $joins
     * @return  void
     */
    public function setSearchJoins(array $joins)
    {
        $this->_searchOptions['joins'] = $joins;
    }
    
    /**
     * Get joins used when doing a fetch
     *
     * @return array
     */
    public function getFetchJoins()
    {
        return $this->_fetchOptions['joins'];
    }
    
    /**
     * Set joins to use when doing a fetch
     *
     * @param array $joins
     * @return void
     */
    public function setFetchJoins(array $joins)
    {
        $this->_fetchOptions['joins'] = $joins;
    }

    /**
     * Set joins to use when doing a fetch
     *
     * @deprecated see setFetchJoins()
     * @param   array $joins
     * @return  void
     */
    protected function _setFetchJoins(array $joins)
    {
        $this->_fetchOptions['joins'] = $joins;
    }

    /**
     * Add joins to use in search
     *
     * @param   array $joins
     * @return  void
     */
    protected function _addSearchJoins(array $joins)
    {
        foreach ($joins AS $table => $joinConfig) {
            $this->_searchOptions['joins'][$table] = $joinConfig;
        }
    }

    /**
     * Get full table name in schema.tableName format
     *
     * This method should be used when created SQL statements as we may not know the
     * full table name e.g. a different schema may be in use, or table prefixes
     *
     * @param   string $table Optional
     * @param   boolean $ignorePrefix
     * @return  string full table name
     */
    protected final function _fullTableName($table = null, $ignorePrefix = false)
    {
        $tableInfo = $this->tableInfo();

        $schema = $tableInfo['schema'];
        if (!is_null($table)) {
            $name = ($ignorePrefix) ? $table : $this->_tablePrefix . $table;
        } else {
            $name = $tableInfo['name'];
        }

        return ($schema) ? "$schema.$name" : $name;
    }

    /**
     * Get db schema
     *
     * @return string Db schema
     */
    protected final function _dbSchema()
    {
        $tableInfo = $this->tableInfo();

        return $tableInfo['schema'];
    }

    /**
     * Get primary key where clause
     *
     * @param   boolean $returnArray Option to return where clause as associative array
     * @return  array|string Primary key where clause
     * @deprecated  Use public method getPrimaryKeyWhere() instead
     */
    protected function _primaryKeyWhere($returnArray = false)
    {
        return $this->getPrimaryKeyWhere($returnArray);
    }

    /**
     * Get primary key where clause
     *
     * @param   boolean $returnArray Option to return where clause as associative array
     * @return  array|string Primary key where clause
     */
    public function getPrimaryKeyWhere($returnArray = false)
    {
        // get table info and build where clause from primary fields
        $tableInfo = $this->tableInfo();

        // return as array
        if ($returnArray) {
            $where = array();
            foreach ($tableInfo['primary'] AS $field) {
                $where[$field] = $this->{$field};
            }

        // return as string
        } else {
            $where = '';
            $i = 1;
            foreach ($tableInfo['primary'] AS $field) {
                if ($i > 1) {
                    $where.= ' AND ';
                }
                $where.= $this->_db->quoteInto("$field = ?", $this->{$field});
                $i++;
            }
        }

        return $where;
    }

    /**
     * Get list of primary keys
     *
     * @return  array
     */
    public function getPrimaryKeys()
    {
        $tableInfo = $this->tableInfo();
        return $tableInfo['primary'];
    }

    /**
     * Method for checking item validation
     *
     * @return  boolean
     */
    public function isValid()
    {
        $valid = true;

        // check for required fields
        if (is_array($this->requiredFields)) {
            foreach ($this->requiredFields AS $key => $value) {
                // check if we have been given a Zend_Validate object
                if ($value instanceof Zend_Validate_Abstract){
                    $validator = $value;
                    $field = $key;
                    if (!$validator->isValid($this->{$field})) {
                        $valid = false;
                        $this->invalidFields[$field] = $this->{$field};
                        // field is invalid, add error messages
                        foreach ($validator->getMessages() as $messageId => $message) {
                            Ovoyo_ErrorHandler::addError($message);
                        }
                    }
                } else {    // validate string as not empty
                    $field = $value;
                    require_once 'Zend/Validate/NotEmpty.php';
                    $validator = new Zend_Validate_NotEmpty();
                    if (!$validator->isValid($this->{$field}) && !is_int($this->{$field})) {
                        $friendlyField = ucfirst(strtolower(preg_replace("/([a-z])([A-Z])/", "$1 $2", $field)));
                        $friendlyField = preg_replace("/_/", " ", $friendlyField);
                        Ovoyo_ErrorHandler::addError("$friendlyField is a required field");
                        $this->invalidFields[$field] = $this->{$field};
                        $valid = false;
                    }
                }
            }
        }

        return $valid;
    }

    /**
     * Convert model instance to Zend_Form instance
     *
     * See Ovoyo_ModelToForm for $options and $config options
     *
     * @param   array $config Config
     * @param   array $options Options
     * @return  Zend_Form instance
     */
    public function toForm($config = array(), $options = array())
    {
        require_once 'Ovoyo/ModelToForm.php';
        return Ovoyo_ModelToForm::create($this, $config, $options);
    }

    /**
     * Apply some magic function calls
     *
     * Supported magic calls - fetchByxxx()
     *
     * @param string $method Name of method
     * @param array $arguments List of arguments to pass to method
     * @return result of function call
     * @throws Exception
     */
    public function __call($method, $arguments)
    {
        try {
            // fetchByxxx() magic method
            if (substr($method, 0, 7) == 'fetchBy') {
                $field = substr($method, 7);

                if (true === function_exists('lcfirst')) {
                    $field = lcfirst($field);
                } else {
                    $field = (string)(strtolower(substr($field, 0, 1)).substr($field, 1));
                }

                $fetchBy = array($field => $arguments[0]);
                return $this->fetchBy($fetchBy);

            // deleteByxxx()
            } else if (substr($method, 0, 8) == 'deleteBy') {
                $field = substr($method, 8);

                if (true === function_exists('lcfirst')) {
                    $field = lcfirst($field);
                } else {
                    $field = (string)(strtolower(substr($field, 0, 1)).substr($field, 1));
                }

                $deleteBy = array($field => $arguments[0]);
                return $this->deleteBy($deleteBy);

            } else if (!method_exists($this->_dbTable, $method)) {
                throw new Exception("method $method() not found");
            }

            return call_user_func_array(
                array($this->_dbTable, $method), $arguments
            );
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Simple magic __get call to provide access to $this->webContainer for backwards compatibility
     *
     * @param   string $variable
     * @return  mixed
     */
    function __get($variable)
    {
        // web container
        if ($variable == 'webContainer') {
            return Ovoyo_WebContainer::getInstance();

        // db handler
        } else if ($variable == '_db') {
            if (!isset($this->_db)) {
                $this->_db = Ovoyo_WebContainer::getInstance()->getDbConnection($this->_connName);
            }
            
            return $this->_db;

        // db table instamce
        } else if ($variable == '_dbTable') {
            if (!isset($this->_dbTable)) {
                // get db config
                $dbConfig = Ovoyo_WebContainer::getInstance()->dbConnectionManager->getDbConfig($this->_connName);

                // if table name has not been supplied, try to work it out
                if (!$this->_dbTableConfig['name']) {
                    $dbName = $dbConfig->params->dbname;
    
                    // get db table name from class
                    $dbTableName = preg_replace("/.*_/", '', get_class($this));
                    
                    if ($dbConfig->params->nameStyle == self::DB_NAME_STYLE_UNDERSCORE) {
                        $dbTableName = strtolower(
                            preg_replace(
                                '/([a-z])([A-Z])/', '$1_$2', $dbTableName
                            )
                        );
                    } else {
                        $dbTableName = substr_replace(
                            $dbTableName,
                            strtolower(substr($dbTableName, 0, 1)),
                            0, 1
                        );
                    }
    
                    // add table prefix if supplied
                    if ($this->_tablePrefix) {
                        $dbTableName = $this->_tablePrefix . $dbTableName;
                    }
                    $dbTableName = "$dbName.$dbTableName";
    
                    $this->_dbTableConfig['name'] = $dbTableName;
                }
    
                // get db adapter to pass as connection info
                $this->_dbTableConfig['db'] = $this->_db->getAdapter();
    
                // enable meta-data caching if cache is available
                $zendCache = Ovoyo_WebContainer::getInstance()->cache;
                if ($zendCache !== null) {
                    $this->_dbTableConfig['metadataCache'] = $zendCache;
                }

                $this->_dbTable = new Ovoyo_Db_Table($this->_dbTableConfig);
                $this->_dbTable->setOptions(array(Zend_Db_Table::SCHEMA => $dbConfig->params->dbname));
            }
            
            return $this->_dbTable;
        } else {
            return (isset($this->{$variable})) ? $this->{$variable} : null;
        }
    }

}

