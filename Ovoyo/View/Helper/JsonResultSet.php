<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Paginator
 */
require_once 'Ovoyo/Paginator.php';

/**
 * @see Zend_Json
 */
require_once 'Zend/Json.php';

/**
 * Class which will create a JSON result set from a data array
 *
 * @package    Ovoyo_View
 */
class Ovoyo_View_Helper_JsonResultSet
{
    /**
     * Constructor
     *
     * @param   array $data Data to create JSON result set from
     * @param   string $collectionName Name of collection to return
     * @param   int $pageNum Current page number
     * @param   int $itemsPerPage Number of items per page
     */
    public function jsonResultSet($data, $collectionName = 'items', $pageNum = 1, $itemsPerPage = 0, $metaData = array())
    {
        $itemsPerPage = ($itemsPerPage > 0) ? $itemsPerPage : count($data);

        $paginator = new Ovoyo_Paginator($pageNum, $itemsPerPage);
        $paginator->setPageData($data);

        $pageData = $paginator->getPageData();
       /* $i    = 0;
        $json = '';
        foreach ($paginator AS $item) {
            $json.= ($i > 0) ? ',' : '';
            $json.= Zend_Json::encode($item);
            $i++;
        }*/
        
        $resultSet = array('totalCount' => count($data),
                           $collectionName => $pageData);
        
/* REMOVED - commit d504947eb605f570b53446dfd8539424ef198b3e by Simon
 * "fixed bug, meta data causing dates in grid not to show up correctly."
 * Therefore no automatic meta-data is output.  We still allow meta-data
 * to be manually output on a case-by-case basis, by passing it into
 * the function.
 * TODO: Investigate the bug alluded to above, and resolve so that it is
 *       safe to output meta-data in all instances.
 */
/*******************
        $fields = (count($pageData) > 0) ? array_keys($pageData[0]) : array();
        $extraMetaData = array(
                                'totalProperty' => 'totalCount',
                                'root' => $collectionName,
                                'id' => 'workspaceId',
                                'fields' => $fields
                            );
        $metaData = array_merge($extraMetaData, $metaData);
*******************/

        if (count($metaData) > 0) {
            $resultSet['metaData'] = $metaData;
        }
        
        header("Content-Type: application/json");
        
        return Zend_Json::encode($resultSet);
    }
}
