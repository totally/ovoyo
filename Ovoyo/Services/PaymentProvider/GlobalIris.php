<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Services_PaymentProvider_
 */
require_once 'Ovoyo/Services/PaymentProvider/Abstract.php';

/**
 * HSBC Global Iris class
 *
 * @package    Ovoyo_Services_PaymentProvider
 */
class Ovoyo_Services_PaymentProvider_GlobalIris extends Ovoyo_Services_PaymentProvider_Abstract
{
    // The payment page URL
    CONST PAYMENT_URL = 'https://redirect.globaliris.com/epage.cgi';
    
    // Success codes
    CONST RESULT_OK = '00';
    CONST PAYER_SETUP_OK = '00';
    CONST PMT_SETUP_OK = '00';
    
    // Failure codes - payer already exists
    CONST PAYER_SETUP_PAYER_ALREADY_EXISTS = '508';
    CONST PMT_SETUP_PAYER_ALREADY_EXISTS = '508';
    CONST PAYER_SETUP_MSG_ALREADY_EXISTS = 'That payer reference already exists';
    CONST PMT_SETUP_MSG_ALREADY_EXISTS = 'Error creating payer - payment method aborted';
    
    // Failure codes - Payment Reference Already Exists
    CONST PAYER_SETUP_REF_ALREADY_EXISTS = '00';
    CONST PMT_SETUP_REF_ALREADY_EXISTS = '508';
    CONST PAYER_SETUP_MSG_REF_ALREADY_EXISTS = 'Successful';
    CONST PMT_SETUP_MSG_REF_ALREADY_EXISTS = 'That payment method already exists';
    
    // Failure codes - Payer And Payment Setup Fail
    CONST PAYER_SETUP_FAIL = '508';
    CONST PMT_SETUP_FAIL = '508';
    CONST PAYER_SETUP_MSG_FAIL = 'Error creating payer';
    CONST PMT_SETUP_MSG_FAIL = 'Error creating payment method';
    
    protected $_paymentUrl;

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_::_init()
     */
    protected function _init()
    {
        $this->_paymentUrl = self::PAYMENT_URL;
        // $this->_paymentUrl = '/checkout/callback/';
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_::sendPayment()
     */
    public function sendPayment(array $vars, $controller = null)
    {
        if ($controller) {
            $renderer = $controller->getHelper('viewRenderer');
            $renderer->setNoRender(true);
        }
        
        $this->_generateExtraVars(&$vars);
        
        $form = $this->_createPaymentForm($vars);
        $formId = $form->getAttrib('id');
        
        $script = '<script type="text/javascript">'."\n"
                . 'document.getElementById("' . $formId . '").submit();'
                ."\n" . '</script>'."\n";

        if ($controller) {
            $view = $controller->view;
            $controller->getResponse()->setBody($form->render($view) . $script);
        } else {
            echo $form->render() . $script;
            exit;
        }
    }
    
    /**
     * Generate a timestamp, a MD5 Hash and an order id 
     * to be given to the payment provider.
     * 
     * @return string
     */
    protected function _generateExtraVars($vars)
    {
        $timestamp = strftime("%Y%m%d%H%M%S");
        mt_srand((double)microtime()*1000000);
        
        $orderId = $vars['paymentId'];
        $merchantId = $vars['MERCHANT_ID'];
        // the amount is multiplicated by 100 to not show decimals as Global Iris
        // doesn't accept them. it will do the conversion automatically
        $vars['AMOUNT'] = $vars['AMOUNT'] * 100;
        $amount = $vars['AMOUNT'];
        $curr = $this->_currency;
        
        $tmp = "$timestamp.$merchantId.$orderId.$amount.$curr";
        
        $md5hash = md5($tmp);
        $tmp = "$md5hash.$this->_sharedSecret";
        $md5hash = md5($tmp);
        
        $vars['ORDER_ID'] = $orderId;
        $vars['TIMESTAMP'] = $timestamp;
        $vars['MD5HASH'] = $md5hash;
        $vars['originalTimestamp'] = $timestamp;
        $vars['originalHash'] = $md5hash;
    }

    /**
     * Creates a Zend_Form from supplied variables to submit to Committed
     * Giving
     *
     * @param array $vars
     */
    protected function _createPaymentForm($vars)
    {
        $form = new Zend_Form;
        $form->setAction($this->_paymentUrl);
        $form->setMethod('post');
        $form->setAttrib('id', 'globalIrisPaymentForm');
        $form->setAttrib('style', 'display: none;');

        foreach ($vars AS $name => $value) {
            $form->addElement('hidden', $name, array('value' => $value));
        }

        $form->addElement('submit', '_submitPayment');

        return $form;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_::handleCallback()
     */
    public function handleCallback(array $data)
    {
        $timestamp = $data['TIMESTAMP'];
        $result = $data['RESULT'];
        $orderId = $data['ORDER_ID'];
        $message = $data['MESSAGE'];
        $authcode = $data['AUTHCODE'];
        $pasref = $data['PASREF'];
        $realexmd5 = $data['MD5HASH'];
        
        $merchantId = $data['MERCHANT_ID'];
        $amount = $data['AMOUNT'];
        $currency = $this->_currency;
        
        // re-create the md5 to check that the realexmd5 field is correct
        //$tmp = "$timestamp.$merchantid.$orderid.$result.$message.$pasref.$authcode";
        //$tmp = "$timestamp.$merchantId.$orderId.$amount.$currency";
        $tmp = "$timestamp.$merchantId.$orderId.$result.$message.$pasref.$authcode";
        
        $md5hash = md5($tmp);
        $tmp = "$md5hash.$this->_sharedSecret";
        $md5hash = md5($tmp);
        
        // check to see if hashes match or not
        if ($md5hash != $realexmd5) {
            Zend_Debug::dump($data);
            Zend_Debug::dump("$timestamp.$merchantId.$orderId.$amount.$currency");
            Zend_Debug::dump($this->_sharedSecret);
            throw new Ovoyo_Exception(
                $md5hash . ' Hashes don\'t match - response not authenticated! ' . $realexmd5
            );
        }
        
        $resultCode = $data['RESULT'];
        
        // need to process the payment here
        $paymentId = $data['paymentId'];

        // fetch the payment
        $payment = Ovoyo_Services_PaymentProvider::getPayment(
            array('connName' => $this->_connName)
        );
        if (!$paymentId || !$payment->fetch($paymentId)) {
            return false;
        }

        $this->paymentId = $payment->paymentId;
        
        // update payment details
        $payment->status = $resultCode;
        $payment->reference = $pasref;
        $payment->callbackData = $data;
        $payment->paid = ($resultCode == self::RESULT_OK) ? 1 : 0;
        $payment->paidDate = new Zend_Date;
        
        $this->message = $data['MESSAGE'];
        $this->result = $data['RESULT'];
        $this->reference = $pasref;

        if (!$payment->update()) {
            return false;
        }

        $this->amount = $payment->amount;
        $this->details = $payment->details;
        

        return $payment->paid;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_::completePayment()
     */
    public function completePayment()
    {
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_::processPaymentInfo()
     */
    public function processPaymentInfo(array $data)
    {
        $paid = $this->handleCallback($data);
        
        if ($paid) {
            $result = array();
            $result['paymentId'] = $this->paymentId;
        } else {
            $result = false;
        }

        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_::postConvertToOrder()
     */
    public function postConvertToOrder($config)
    {
        $output = '';
        
        if ($this->result == '00') {
            $output.= "<h1>Your transaction has been processed</h1>";
        } else {
            $output.= "<h1>There has been a problem while processing your payment</h1>";
            $output.= '<p>Error ' . $this->result . ' - ' . $this->message . ' (reference: ' . $this->reference . ')</p>';
        }
        
        $websiteUrl = $config['host'] . $config['paymentCompleteUrl'];
        
        $output.= "<p>Please <a href='" 
                . $websiteUrl 
                . "'>click here</a> to return to the merchant website and finish</p>";
        
        echo $output;
        
        exit;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_::validateCallback()
     */
    public function validateCallback($data = array())
    {
        return array_key_exists('RESULT', $data);
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_::prepareData()
     */
    public function prepareData(array $data)
    {
        $preparedData = array(
            'MERCHANT_ID'       => $data['config']['merchantId'],
            'CURRENCY'          => $this->_currency,
            'AMOUNT'            => $data['amount'],
            'AUTO_SETTLE_FLAG'  => '1',
            'paymentId'         => $data['paymentId'],
            'userId'            => $data['userId'],
        );
        
        return $preparedData;
    }

}