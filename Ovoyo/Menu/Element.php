<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Menu element class
 *
 * @package    Ovoyo_Menu
 */
class Ovoyo_Menu_Element extends Ovoyo_Menu
{
    protected $_viewHelper = 'menuElement';

    public $id;
    public $type;
    public $label;
    public $link;
    public $info;
    public $target;

    /**
     * Constructor
     *
     * @param array $config
     */
    public function __construct($config = array())
    {
        foreach ($config AS $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Check to see if an element is currently selected
     *
     * A menu element is considered selected if it's link or any of its submenu
     * items' links match the current url path - a score is given to the
     * relevancy of the match
     *
     * @param string $currentMatchLink
     * @param int $currentMatchScore
     * @return int Match score
     */
    public function selected($currentMatchLink = '', $currentMatchScore = 0)
    {
        $urlPath = Ovoyo_WebContainer::getInstance()->getParam('urlPath');
        
        // check for home url above all else
        if ($this->link == '/' && ($urlPath == '/' || $urlPath == "")) { return 1; }

        // link to compare to urlPath
        $link = preg_replace("/\?.*$/", "", $this->link);
        $link = rtrim($link, "/");
        if (strlen($link) == 0) { return 0; }

        // if link matches, return score based on string position
        if ($urlPath == $link) {
            $score = 1;
        } else {
            $score = strpos($urlPath, $link);
            $score = (is_int($score)) ? $score + 2 : 0;
            
            if (($currentMatchScore > 0 && $score > 0) && (strlen($link) > strlen($currentMatchLink))) {
                $score--;
            }
        }
       
        return ($score > 0 && ($score < $currentMatchScore || $currentMatchScore == 0)) ? $score : 0;
    }
    
    /**
     * Get menu element as an array
     *
     * @return array
     */
    public function toArray()
    {
        return array (
            'id' => $this->id,
            'info' => $this->info,
            'label' => $this->label,
            'link' => $this->link,
            'target' => $this->target,
            'type' => $this->type
        );
    }

    /**
     * Render menu
     *
     * @param Ovoyo_View $view
     * @param boolean $selected Mark item as selected
     * @return string
     */
    public function render($view, $selected = false)
    {
        return $view->{$this->_viewHelper}($this, $view, $selected);
    }

   /**
     * Set view helper
     *
     * @param string $viewHelper
     * @return void
     */
    public function setViewHelper($viewHelper)
    {
        $this->_viewHelper = $viewHelper;
    }
}
