<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Feed
 *
 * A class for performing basic feed manipulation
 *
 * @package    Ovoyo_Feed
 */
abstract class Ovoyo_Feed_Abstract
{
    protected $dataArray;
    protected $feed;
    
    /**
     * Displays the XML feed and generates correct headers
     *
     * @param   string $file Feed XML
     * @return  null
     * @throws  Ovoyo_Feed_Exception
     */
    abstract public function displayFeed($data, $metaData);

    /**
     * Formats database model input into valid Zend_Feed Array
     *
     * @param   array $data Database result arrays
     * @return  null
     * @throws  Ovoyo_Feed_Exception
     */
    abstract public function formatData($data, $metaData);

    /**
     * Converts arrya data to a Zend RSS Feed
     *
     * @return  void
     */
    abstract public function convertDataToFeed();
     
}
