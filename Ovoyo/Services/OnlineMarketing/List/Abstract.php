<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Email marketing list class
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage List
 */
abstract class Ovoyo_Services_OnlineMarketing_List_Abstract
{
    public $listId;

    protected $_apiHandler;

    /**
     * Create list
     *
     * @param   string $name Name of list
     * @return  boolean
     */
    abstract public function create($name);

    /**
     * Delete list
     *
     * @return  boolean
     */
    abstract public function delete();

    /**
     * Get list
     *
     * @return  array|false
     */
    abstract public function getList();

    /**
     * Get available lists
     *
     * @return  array|false
     */
    abstract public function getLists();

    /**
     * Add subscriber
     *
     * @param   array $data
     * @return  boolean
     */
    abstract public function addSubscriber($data);

    /**
     * Get subscribers
     *
     * @return  mixed
     */
    abstract public function getSubscribers();

    /**
     * Import data into list
     *
     * If list of fields are not provided, data is assumed to only contain
     * email addresses
     *
     * @param   array $data Data to import
     * @param   array $fields List of fields to be imported
     * @return  boolean
     */
    abstract public function import(array $data, $fields = null);

    /**
     * Check list import has completed
     *
     * @param   int|string $importId
     * @return  boolean
     */
    abstract public function hasImportCompleted($importId = null);

    /**
     * Any post import processing
     *
     * @param   int|string $importId
     * @return  boolean
     */
    abstract public function postImport($importId = null);

    /**
     * Set api handler
     *
     * @param   array $config
     * @return  void
     */
    abstract public function setApiHandler($config = null);

    /**
     * Get api handler
     *
     * @return  Ovoyo_Services_OnlineMarketing_ApiHandler
     */
    public function getApiHandler()
    {
        if (!isset($this->_apiHandler)) {
            throw new Exeption('API handler has not been defined');
        }
        return $this->_apiHandler;
    }
}
