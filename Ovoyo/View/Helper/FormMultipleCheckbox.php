<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */


/**
 * @see Zenk_View_Helper_FormElement
 */
require_once 'Zend/View/Helper/FormElement.php';

/**
 * Display a nicely layed out set of checboxes
 *
 * @package    Ovoyo_View
 */
class Ovoyo_View_Helper_FormMultipleCheckbox extends Zend_View_Helper_FormElement
{
    protected $_inputType = 'checkbox';
    protected $_isArray = true;
    
    /**
     * Constructor
     *
     * Possible options are:
     *
     * - 'name' string name to use for checboxes
     * - 'cols' int number of cols
     * - 'checkboxes' array holding:
     *    - 'multiOptions' array holding checkbox values and labels and key/value pairs
     *    - 'attribs' array holding additional shared attributes for checkboxes
     *    - 'checkedOptions' array holding checked options for each checkbox - keyed by checkbox value
     * - 'checked' array holding list of items that should be checked by default
     * - 'lineBreaks' boolean if set to true will add line breaks to html to make easy to read/debug
     * - 'style' string css style to use for container
     *
     * @param   string $name Name of assignable select
     * @param   array|string $selected Currently selected items
     * @param   array $options Optional array holding options for 'assigned' and 'unassigned' selects
     * @return  void
     */
    public function formMultipleCheckbox($name, $selected = null, $options = null)
        {
            $view = new Ovoyo_View;
            
           // var_dump($options);
            // get number of cols (default to 1) and work out widths
            $numCols  = ($options['cols']) ? $options['cols'] : 1;
            $colWidth = (floor(100 / $numCols));
            $lastColWidth = $colWidth + (100 - ($numCols * $colWidth));

            // create checkboxes
        $checkboxes = (array) $options['checkboxes']['multiOptions'];
                
        $html = '<div style="' . $options['style'] . '">' ."\n";

        $colNum = 1;
        $i = 1;
 
        if ($options['addHidden']) {
            $html .= $view->formHidden($name, $value);
        }
        
        foreach ($checkboxes AS $value => $label) {
            $width = ($colNum == $numCols) ? $lastColWidth : $colWidth;
                      
            $checkboxName = (!preg_match("/]$/", $name)) ? $name .  '[]' : $name;
            $id = preg_replace("/[\[|\]]/", '_', $checkboxName);
            $id = preg_replace("/_{1,}$/", '', $id);
            $id.= '_' . $value;
            
            $attribs = $options['checkboxes']['attribs'];
            
            // set some defaults to prevent php notices
            $attribs['style'] = (isset($attribs['style'])) ? $attribs['style'] : '';
        
            $attribs['id']    = $id;
            $attribs['value'] = $value;
            $attribs['style'].= 'vertical-align: middle;';
            
            if (!$selected) {
                $attribs['checked'] = (is_array($options['checked'])) ? in_array($value, $options['checked']) : false;
            } else {
                $attribs['checked'] = (is_array($selected)) ? in_array($value, $selected) : false;
            }
            
            if (!$options['noEscapeLabel']) {
                $label = $view->escape($label);
            }
            
            $html.= '<div style="float: left; width: ' . $width . '%;">' . "\n"
              #    . '    <div style="margin-left: 25px; text-indent: -25px;">' . "\n"
                  . '    <div style="">' . "\n"
                  . '        ' . $view->formCheckbox($checkboxName, $value, $attribs) . "\n"
                  . '        <label for="' . $id . '">' . $label . '</label>' . "\n"
                  . '    </div>' . "\n"
                  . '</div>' . "\n";

            if ($colNum == $numCols && $i < count($checkboxes)) {
                $html.= '<div style="clear: both; margin-bottom: 5px;"></div>' ."\n";
                $colNum = 1;
            } else {
                $colNum++;
            }
            $i++;
        }
        
        $html.= '<div style="clear: both;"></div>' . "\n"
              . '</div><div style="clear: both;"></div>';
        
        if (!isset($options['lineBreaks'])) {
            $html = preg_replace("/\n/", '', $html);
            $html = preg_replace("/\s{3,}/", '', $html);
        }

        return $html;
    }
}
