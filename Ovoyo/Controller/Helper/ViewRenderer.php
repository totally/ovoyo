<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Zend_Controller_Action_Helper_ViewRenderer
 */
require_once 'Zend/Controller/Action/Helper/ViewRenderer.php';

/**
 * @see Ovoyo_View
 */
require_once 'Ovoyo/View.php';

/**
 * Simple abstract controller class to provide CRUD activities
 *
 * @package     Ovoyo_Controller
 * @subpackage  Helper
 */
class Ovoyo_Controller_Helper_ViewRenderer 
    extends Zend_Controller_Action_Helper_ViewRenderer
{
    /**
     * See Zend_Controller_Action_Helper_ViewRenderer initView() for more info
     *
     * @param  string $path
     * @param  string $prefix
     * @param  array  $options
     * @throws Zend_Controller_Action_Exception
     * @return void
     */
    public function initView($path = null, $prefix = null, array $options = array())
    {
        if (!isset($options['noController'])) {
            $options['noController'] = true;
        }
        
        if (null === $this->view) {
            $this->setView(new Ovoyo_View());
            $this->view->setScriptPath($this->_getBasePath());
        }
        
        parent::initView($path, $prefix, $options);
    }
    
    /**
     * This overrides the standard _translateSpec and provides a
     * different paradigm for view script discovery
     *
     * @param  array $vars
     * @return string
     */
    protected function _translateSpec(array $vars = array())
    {
        $request = $this->getRequest();
        $suffix  = '.' . $this->getViewSuffix();
        $action  = $this->getScriptAction();
        
        $pathInfo = $request->getPathInfo();
        $basePath = $this->_getBasePath();
        
        $haveViewScript = false;
        
        $moduleName = $request->getModuleName();
        
        // first try and match to action
        if (!empty($action)) {
            $script = preg_replace(array("/\/{0,}$/", "/^\/{1,}/", "/$suffix/"), array('', '', ''), $action);
            
            if ($moduleName) {
                $script = preg_replace("/^\/{0,}" . $moduleName . "/", '', $script);
            }
            
            if (is_file($basePath . '/' . $script . $suffix)) {
                return $script . $suffix;
            } else if (is_file($basePath . '/' . $script . '/index' . $suffix)) {
                return $script . '/index' . $suffix;
            } else {
                return $script . $suffix;
            }
        }
            
        // otherwise try and work out from path info
        if (!$haveViewScript) {
            
            if ($moduleName) {
                $pathInfo = preg_replace("/^\/{0,}" . $moduleName . "/", '', $pathInfo);
            }

            $script = preg_replace(array("/\/{0,}$/", "/^\/{1,}/"), array('', ''), $pathInfo);
            if ($script == '') {
                $script = 'index';
            }
            
            // if view does not exist, try using index.php in same location
            if (!is_file($basePath . '/' . $script . $suffix)) {
                if (is_file($basePath . '/' . $script . '/index' . $suffix)) {
                    $script = $script . '/index';
                }
            }
        }

        return $script . $suffix;
    }
}
