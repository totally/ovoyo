<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Class which will output a menu element
 *
 * @package    Ovoyo_View
 */
class Ovoyo_View_Helper_MenuElement
{
    /**
     * Constructor
     *
     * @param Ovoyo_Menu_Element $element
     * @param Ovoyo_View $view
     * @param boolean $selected
     * @return string
     */
    public function menuElement($element, $view, $selected = false)
    {
        $html = '<li class="menuItem ';
        if ($selected) {
            $html.= ' selected';
        }
        $html.= '" role="menuitem"';
        if ($selected) {
            $html.= ' aria-checked="true"';
        } else {
            $html.= ' aria-checked="false"';
        }

        if ($element->id) {
            $html.= ' id="menuElement' . $element->id . '"';
        }

        $html.= ">\n";

        if ($element->link) {
            $html.= '<a href="' . $element->link . '">';
        }

        $html.= $view->escape($element->label);

        if ($element->link) {
            $html.= "</a>\n";
        }

        $html.= "</li>\n";

        return $html;
    }
}
