<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Services
 *
 * A class for connecting to 3rd Party web services
 *
 * @package    Ovoyo_Services
 */
class Ovoyo_Services
{
    /**
     * Factory for Ovoyo_Image classes
     *
     * @param   string $type Type of image class to create
     * @return  obj An object which is a subclass of Ovoyo_Image_Abstract
     * @throws  Exception
     */
    public static final function factory($type = 'Gd')
    {     
        $className = 'Ovoyo_Services_' . $type;
        
        // if class already exists, simply return new instance
        if (class_exists($className)) {
            $model = new $className();
            return $model;
        }

        // if class physically exists but hasn't been loaded, then load it
        require_once 'Ovoyo/ClassLoader.php';
        if (Ovoyo_ClassLoader::exists($className)) {
            $model = new $className();
            return $model;
        }
        
        // class does not exist, so throw error
        require_once 'Ovoyo/Image/Exception.php';
        throw new Ovoyo_Services_Exception("Unable to create image class of type '$type'");
    }

}
