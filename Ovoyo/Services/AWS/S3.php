<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

require_once 'Ovoyo/Services/AWS/S3Crypt.php';
require_once 'Ovoyo/Services/AWS/External/S3.php';

/**
 * Class to process file information
 *
 * @package	Ovoyo_Services
 * @subpackage	AWS
 */
class Ovoyo_Services_AWS_S3 extends S3
{
        
        public $serviceUrl;
        public $accessKeyId;
        public $secretKey;
        
        public function __construct($accessKeyId, $secretKey, $serviceUrl="http://s3.amazonaws.com/") {
                
                $this->serviceUrl = $serviceUrl;
                $this->accessKeyId = $accessKeyId;
                $this->secretKey = $secretKey;
                
                parent::__construct($accessKeyId, $secretKey);                
        }

        /**
    * queryStringGet - returns a amazon url mask
    * @param   string $bucket the bucket Id
    * @param   string $key the key
    * @param   string $expires how long the lin kis valid for
    * @return  string $queryString the URL to the requested file
    */  
        public function queryStringGet($bucket, $key, $expires) {
                
                $expires = time() + $expires;
                $stringToSign = "GET\n\n\n" . $expires . "\n/" . $bucket . "/" . $key;
                $signature = urlencode($this->constructSig($stringToSign));
                
                $queryString = "http://s3.amazonaws.com/" . $bucket . "/" . $key . "?AWSAccessKeyId=" . $this->accessKeyId;
                $queryString .= "&Expires=" . $expires . "&Signature=" . $signature;
                
                return $queryString;         
    }

    /**
    * _hex2b64 - converts hex to 64 bit
    *
    * @param   string $str the string to convert
    * @return  string the converted string
    */  
        public function hex2b64($str) {
                
                $raw = '';
                
                for ($i=0; $i < strlen($str); $i+=2) {
                        $raw .= chr(hexdec(substr($str, $i, 2)));
                }
                
                return base64_encode($raw);
                
        }

        /**
    * _constructSig - builds the signature
    *
    * @param   string $str the string to build the signature from
    * @return  string the signature
    */  
        public function constructSig($str) {
                
                $hasher = new Ovoyo_Services_AWS_S3Crypt($this->secretKey, "sha1");
                
                $signature = $this->hex2b64($hasher->hash($str));
                
                return $signature;
                
        }
           
}
