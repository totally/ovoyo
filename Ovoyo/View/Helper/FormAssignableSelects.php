<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */


/**
 * @see Zend_View_Helper_FormElement
 */
require_once 'Zend/View/Helper/FormElement.php';

/**
 * Class which will output a menu
 *
 * @package     Ovoyo_View
 * @subpackage  Helper
 */
class Ovoyo_View_Helper_FormAssignableSelects extends Zend_View_Helper_FormElement
{
    /**
     * Constructor
     *
     * Possible options are:
     *
     * - 'assigned' array Array holding options for assigned select
     *   - 'name' string Name of assigned select element
     *   - 'id' string Id of assigned select object
     *   - 'options' array Values for assigned select in key/value pair format
     *   - 'attribs' array Optional attributes for assigned select
     * - 'unassigned' array Array holding options for unassigned select
     *   - 'name' string Name of unassigned select element
     *   - 'id' string Id of unassigned select object
     *   - 'options' array Values for unassigned select in key/value pair format
     *   - 'attribs' array Optional attributes for unassigned select
     * - 'pack' boolean Whether or not to pack the associated javascript
     * - 'xhr' boolean If set, will output javascript to a hidden div so that it can eval'd at a later stage
     * - 'lineBreaks' boolean If set to true will add line breaks to html to make easy to read/debug
     *
     * @param   string $name Name of assignable select
     * @param   string $value Ignored in this implementation
     * @param   array $options Optional array holding options for 'assigned' and 'unassigned' selects
     * @return  void
     */
    public function formAssignableSelects($name, $value = null, $options = null)
        {
            $view = new Ovoyo_View;
            
        $options['assigned']   = (is_array($options['assigned'])) ? $options['assigned'] : array();
        $options['unassigned'] = (is_array($options['unassigned'])) ? $options['unassigned'] : array();
            
            // always pack js by default
            $options['pack'] = (isset($options['pack'])) ? $options['pack'] : true;
            
            $assignedHeader   = ($options['assigned']['header']) ? $options['assigned']['header'] : 'Assigned';
            $unassignedHeader = ($options['unassigned']['header']) ? $options['unassigned']['header'] : 'Unassigned';
            
            // set values for assigned select
            $assignedAttribs = (is_array($options['assigned']['attribs'])) ?  $options['assigned']['attribs'] : array();
            $assignedAttribs['multiple'] = 'multiple';
            
            $elementName = ($assignedAttribs['name']) ? $assignedAttribs['name'] : 'assigned';
            $elementId   = ($assignedAttribs['id']) ? $assignedAttribs['id'] : $elementName;
            if (!preg_match("/\[\]$/", $elementName)) {
                $elementName.= '[]';
            } else if ($elementName == $elementId) {
                $elementId = preg_replace("/\[\]$/", "", $elementId);
            }
            
            $assignedAttribs['id']   = preg_replace("/[\[|\]]/", '', $elementId);
            $assignedAttribs['name'] = $elementName;
            
            // set values for unassigned select
        $unassignedAttribs = (is_array($options['unassigned']['attribs'])) ?  $options['unassigned']['attribs'] : array();
        $unassignedAttribs['multiple'] = 'multiple';
        
        $elementName = ($unassignedAttribs['name']) ? $unassignedAttribs['name'] : 'unassigned';
        $elementId   = ($unassignedAttribs['id']) ? $unassignedAttribs['id'] : $elementName;
        if (!preg_match("/\[\]$/", $elementName)) {
            $elementName.= '[]';
        } else if ($elementName == $elementId) {
            $elementId = preg_replace("/\[\]$/", "", $elementId);
        }
        
        $unassignedAttribs['id']   = preg_replace("/[\[|\]]/", '', $elementId);
        $unassignedAttribs['name'] = $elementName;
        
        // set values for buttons
        $assignButtonText = ($options['assignButtonText']) ? $options['assignButtonText'] : '>>';
        $unassignButtonText = ($options['unassignButtonText']) ? $options['unassignButtonText'] : '<<';
        
        $assignButtonId   = $name . 'AssignButton';
        $unassignButtonId = $name . 'UnassignButton';
            
        
        // header row
        $html = "\n"
                  . '<table id="' . $name . '" class="ovoyoAssignableSelect">'."\n"
                  . "    <tr>\n"
              . "        <th>" . $assignedHeader . "</th>\n"
              . "        <th>&nbsp;</th>\n"
              . "        <th>" . $unassignedHeader . "</th>\n"
              . "    </tr>\n";
              
        // assigned col
        $html.= "    <tr>\n"
              . "        <td class=\"assignedCol\">\n"
              . "            " . $view->formSelect($assignedAttribs['name'], null, $assignedAttribs, $options['assigned']['options']) . "\n"
              . "        </td>\n";

        // assign buttons col
        $html.= "        " . '<td class="buttonCol">' . "\n"
              . "            " . $view->formButton($assignButtonId, $assignButtonText, array('id' => $assignButtonId)) . "<br />\n"
              . "            " . $view->formButton($unassignButtonId, $unassignButtonText, array('id' => $unassignButtonId)) . "<br />\n"
              . "        </td>\n";
        
        // unassigned col
        $html.= "        <td class=\"unassignedCol\">\n"
              . "            " . $view->formSelect($unassignedAttribs['name'], null, $unassignedAttribs, $options['unassigned']['options']) . "\n"
              . "        </td>\n"
              . "    </tr>\n"
              . "</table>\n";
        
        // add jquery code
        $jquery = "var $name = new function() {\n"
                . "    this.form = $('#" . $unassignButtonId . "').parents('form');\n"
                . "    this.init = function() {\n"
                . "        $('#" . $assignButtonId . "').click(function() { $('#" . $assignedAttribs['id'] . " option:selected').remove().appendTo('#" . $unassignedAttribs['id'] . "'); });\n"
                . "        $('#" . $unassignButtonId . "').click(function() { $('#" . $unassignedAttribs['id'] . " option:selected').remove().appendTo('#" . $assignedAttribs['id'] . "'); });\n"
                . "        this.form.submit(function() { " . $name . ".selectAll(); });\n"
                . "    };\n"
                . "    this.selectAll = function() {\n"
                . "        $('#" . $assignedAttribs['id'] . " option').each(function(i) { $(this).attr('selected', 'selected'); });\n"
                . "        $('#" . $unassignedAttribs['id'] . " option').each(function(i) { $(this).attr('selected', 'selected'); });\n"
                . "    };\n"
                . "    this.unselectAll = function() {\n"
                . "        $('#" . $assignedAttribs['id'] . " option').each(function(i) { $(this).removeAttr('selected'); });\n"
                . "        $('#" . $unassignedAttribs['id'] . " option').each(function(i) { $(this).removeAttr('selected'); });\n"
                . "    };\n"
                . "};\n"
                . "$(document).ready(function() {\n"
                . "    " . $name . ".init();\n"
                . "});\n";

        if (!$options['lineBreaks']) {
            $html = preg_replace("/\n/", '', $html);
            $html = preg_replace("/\s{3,}/", '', $html);
        }
        
        // pack/pack
            if ($options['pack']) {
            include_once 'Ovoyo/Utilities/External/JavaScriptPacker.php';
            $packer = new JavaScriptPacker($jquery);
            $jquery = $packer->pack();
            }
            
            if ($options['xhr']) {
            $html.= '<div id="' . $name . 'Js" style="display: none;">' . $jquery . '</div>';
        } else {
            $html.= '<script type="text/javascript">' ."\n"
                  . "    //<!--\n"
                  . "    $jquery\n"
                  . "    //-->\n"
                  . "</script>\n";
        }
      
        return $html;
    }
}
