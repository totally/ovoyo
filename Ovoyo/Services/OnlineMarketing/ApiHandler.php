<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Api Handler class
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage List
 */
class Ovoyo_Services_OnlineMarketing_ApiHandler
{
}
