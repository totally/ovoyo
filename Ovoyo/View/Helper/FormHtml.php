<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Zend_View_Helper_FormElement
 */
require_once 'Zend/View/Helper/FormElement.php';

/**
 * Simple way to inject HTML directly into a form.
 * 
 * Usage: $form->addElement('html', $name, array('value' => $htmlToDisplay));
 * 
 * Works for Ovoyo_Form objects without requiring any further configuration.
 * To use with Zend_Form objects, you will need to add the necessary Ovoyo 
 * directory to the path, (using addPrefixPath()) so that the element can be
 * found.
 * 
 * @package    Ovoyo_View
 */
class Ovoyo_View_Helper_FormHtml extends Zend_View_Helper_FormElement
{

    /**
     * Constructor
     *
     * @param   string $name Name (ID) used to identify the content.
     * @param   string $value HTML to be displayed.
     * @param   array $attribs Optional array holding options.  The following
     *   attributes have been added by this sub-class:
     *   'escape': (boolean) Set to true to escape the value prior to output.
     *             This over-rides the default setting for this attribute
     *             (for other form elements it normally defaults to true).
     * @return  HTML to be output.
     */
    public function formHtml($name, $value = null, $attribs = null) {
        // The 'escape' attribute defaults to false, so if it is not explicitly
        // set, fill in the default value.  We need to do this here, as 
        // _getInfo() initialises it to true if not explicitly set.
        if (!isset($attribs['escape'])) {
            $attribs['escape'] = false;
        }
        
        // Extract the element parameters.
        $info = $this->_getInfo($name, $value, $attribs);
        extract($info); // name, value, attribs, options, listsep, disable

        // If the 'escape' setting was set to true, escape the value prior
        // to output.
        if ($escape) {
            $value = $this->view->escape($value);
        }

        // Render the HTML within a container div.
        $output = '<div id="' . $this->view->escape($id) . '"'
                . $this->_htmlAttribs($attribs) . '>'
                . $value
                . '</div>';
        
        return $output;
    }

}
