<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Class to paginate array data
 *
 * @package    Ovoyo_Paginator
 */
class Ovoyo_Paginator implements Iterator
{
    protected $_dataSet;
    protected $_index;

    public $itemsPerPage;
    public $itemsOnPage;
    public $pageNum;
    public $numItems;
    public $firstItem;
    public $lastItem;

    /**
     * Constructor
     *
     * @param   int $pageNum Current page number
     * @param   int $itemsPerPage Number of items per page
     */
    public function __construct($pageNum, $itemsPerPage)
        {
        $this->pageNum      = $pageNum;
        $this->itemsPerPage = $itemsPerPage;
        $this->firstItem    = (($this->pageNum - 1) * $this->itemsPerPage) + 1;
    }

    /**
     * Set data to be paginated
     *
     * @param   mixed $items Items to be paginated
     */
    public function setPageData(&$dataSet)
    {
        $this->_dataSet    =& $dataSet;
        $this->numItems    = $this->_numItems();
        $this->lastItem    = $this->_lastItem();
        $this->itemsOnPage = ($this->lastItem - $this->firstItem) + 1;
        #echo "$this->numItems - $this->firstItem - $this->lastItem - $this->itemsPerPage - $this->itemsOnPage<BR>";
    }
    
    /**
     * Get data for current page
     *
     * @return  array
     */
    public function getPageData()
    {
        $data = array();
        foreach ($this AS $item) {
            $data[] = (array) $item;
        }
        return $data;
    }

    /**
     * Number of items in data set
     *
     * @return  int Number of items
     */
    protected function _numItems()
    {
        return count($this->_dataSet);
    }

    /**
     * Work out last item on page
     *
     * @return  int Number of last item on page
     */
    protected function _lastItem()
    {
        $lastItem = $this->pageNum * $this->itemsPerPage;
        $lastItem = ($lastItem > $this->numItems) ? $this->numItems : $lastItem;

        return $lastItem;
    }
    
    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function current()
    {
        return current($this->_dataSet);
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function key()
    {
        return key($this->_dataSet);
    }

    /**
     * Defined by Iterator interface
     *
     */
    public function next()
    {
        next($this->_dataSet);
        $this->_index++;
    }

    /**
     * Defined by Iterator interface
     *
     */
    public function rewind()
    {
        reset($this->_dataSet);
        $this->_index = 1;
        while ($this->_index < $this->firstItem) {
            $this->next();
        }
    }

    /**
     * Defined by Iterator interface
     *
     * @return boolean
     */
    public function valid()
    {
        return $this->_index < $this->lastItem + 1;
    }

}
