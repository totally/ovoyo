<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Class which will output a menu
 *
 * @package    Ovoyo_View
 */
class Ovoyo_View_Helper_Menu
{
    /**
     * Constructor
     *
     * @param Ovoyo_Menu $menu Menu data
     * @param Ovoyo_View $view
     * @param booled $checkSelected Flag to prevent menu item trying to find selected item
     */
    public function menu($menu, $view, $checkSelected = true)
    {
        $selected   = false;
        $selectedId = null;
        
        // only work out selected it allowed to - this is to prevent matches with a higher score being selected when the shouldn't be
        if ($checkSelected) {
        list($selected, $selectedId, $selectedElement, $score) = $menu->getSelected();
        } else {
            $checkSelected = false;
        }
            
        $html = '<div id="menu' . $menu->id . '"';

        if ($menu->class || $selected) {
            $html.= ' class="';
            if ($menu->class) {
                $html.= $menu->class;
            }

            if ($selected) {
                $html.= " selected";
            }

            $html.= '"';
        }

        $html.= ">\n";


        if (is_array($menu->elements)) {
            $html.= '<ul class="menu" role="menubar">';
            foreach ($menu->elements AS $id => $element) {
                $selected = (!is_null($selectedId) && $id == $selectedId) ? true : false;
                if ($element instanceof Ovoyo_Menu_Element) {
                    $html.= $element->render($view, $selected);
                } else if ($element instanceof Ovoyo_Menu) {
                    $html.= $element->render($view, $selected, $checkSelected);
                } else {
                    $html.= '<li class="separator">&nbsp;</li>'."\n";

                }
            }
            $html.= "</ul>\n";
        }
        $html.= "</div>\n";

        return $html;
    }
}
