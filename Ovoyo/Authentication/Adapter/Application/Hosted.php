<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Authentication_Adapter_Application
 */
require_once 'Ovoyo/Authentication/Adapter/Application.php';

/**
 * Application authentication
 *
 * @package     Ovoyo_Authentication
 * @subpackage  Adapter
 */
class Ovoyo_Authentication_Adapter_Application_Hosted
    extends Ovoyo_Authentication_Adapter_Application
{
    protected $_workspaceClass;
    
    protected $_workspaceSelectUrl = '/login/';
    protected $_workspace;

    /**
     * Init
     *
     */
    public function init()
    {
        if (!isset($this->_userClass)) {
            $this->_userClass = 'Ovoyo_Application_Hosted_User';
        }
        if (!isset($this->_userSessionClass)) {
            $this->_userSessionClass = 'Ovoyo_Application_Hosted_UserSession';
        }
        if (!isset($this->_workspaceClass)) {
            $this->_workspaceClass = 'Ovoyo_Application_Hosted_Workspace';
        }
        if (!isset($this->_idField)) {
            $this->_idField = 'userId';
        }
    }

    /**
     * Authenticate
     *
     * Check we have a valid, logged in, user
     */
    protected function _authenticate()
    {
        $webContainer = Ovoyo_WebContainer::getInstance();

        // if we have a login request, call login()
        if (isset($_REQUEST['_login'])) {
            $this->_login();
            return;
        }
        
        // if we have a valid session id, try fetching logged in user
        $sessionId = $_REQUEST[$this->_sessionIdentifier];
        if (!isset($sessionId)) {
            $sessionId = $_COOKIE[$this->_sessionIdentifier];
        }
        if (isset($sessionId)) {
            $dropCookie = false;

            // if session id has come via get or post, need to flag to drop cookie
            if (!$_COOKIE[$this->_sessionIdentifier]) {
                $_COOKIE[$this->_sessionIdentifier] = $sessionId;
                $dropCookie = true;
            }
    
            // fetch user from session id
            $userSession = $this->getUserSession();
            $userSession->fetch($sessionId);

            $user = $this->getUser();
            $fetchBy = array(
                $this->_idField => $userSession->{$this->_idField}
            );
            if ($userSession->{$this->_idField} && $user->fetchBy($fetchBy)) {
                if (!$userSession->refresh()) {
                    $this->_logout();
                    return;
                }
            }
    
            // drop cookie if required
            if ($dropCookie) {
                $userSession->dropSessionIdCookie();
            }
            
            // destroy session cookie if we don't have a valid user
            if (!isset($user->{$this->_idField})) {
                $secure_cookie = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? true : false;
                setcookie($this->_sessionIdentifier, null, 0, "/", '', $secure_cookie);
                unset($_COOKIE[$this->_sessionIdentifier]);
            }
        }
        
        // if url is an allowed exception - do nothing further
        if ($this->allowedException()) {
            // make user available globally
            if (isset($user)) {
                $webContainer->setUser($user);
            }
            
            if (isset($userSession->workspaceId)) {
                /**
                 * @see Ovoyo_ClassLoader
                 */
                require_once 'Ovoyo/ClassLoader.php';
                Ovoyo_ClassLoader::load($this->_userClass);
                
                $modelOptions = $this->_authModelOptions();
        
                $workspace = new $this->_workspaceClass($modelOptions);
                $workspace->fetch($userSession->workspaceId);
                $workspace->register();

                $this->workspace =& $workspace;
            }
            
            return;
        }

        // if we have a logout request, call logout()
        if (isset($_REQUEST['_logout'])) {
            $this->_logout();
            return;
        }

        // if we have a workspace select request, call selectWorkspace()
        if (isset($_REQUEST['_loginSelectWorkspace'])) {
            $this->_selectWorkspace();
            return;
        }
        
        // if we don't have logged in user then redirect to login screen
        // or simply return if authentication is not being enforced
        if (!isset($user->{$this->_idField})) {
            if ($this->_enforcing) {
                $this->_redirect($this->_loginUrl);
            }
            return;
        }

        // make user workspace available globally
        $webContainer->setUser($user);

        // check to see if user needs to select an application workspace
        if ($this->_multiWorkspaceApplication()) {
            // check user has access to one or more workspaces
            if (count($user->userWorkspaces) == 0 && !$user->superuser) {
                $error = 'You do not have access to any workspaces';
                Ovoyo_ErrorHandler::addError($error);
                return;

            // user needs to select a workspace
            } else if (!$userSession->workspaceId) {
                $workspaces = $user->allowedWorkspaces();
                // user has access to only one workspace so auto log them in
                if (count($workspaces) == 1) {
                    $this->_selectWorkspace(key($workspaces));
                } else {
                    $this->_redirect($this->_workspaceSelectUrl);
                    return;
                }

            // workspace has already been selected so create workspace
            } else {
                /**
                 * @see Ovoyo_ClassLoader
                 */
                require_once 'Ovoyo/ClassLoader.php';
                Ovoyo_ClassLoader::load($this->_userClass);
        
                $modelOptions = $this->_authModelOptions();
                
                Ovoyo_ClassLoader::load($this->_workspaceClass);
        
                $workspace = new $this->_workspaceClass($modelOptions);
                $workspace->fetch($userSession->workspaceId);
                $workspace->register();

                $this->workspace =& $workspace;
            }
        }

        // finally if logged in and on login page, redirect to login success
        $urlPath = preg_replace("/\?.*$/", "", $this->_loginUrl);
        $urlPath = rtrim($urlPath, "/");

        if ($urlPath == Ovoyo_WebContainer::getInstance()->getParam('urlPath')) {
            header("Location: $this->_loginSuccessUrl");
            exit;
        }
    }

    /**
     * Select application workspace
     *
     * @param   int $workspaceId Optional workspace id, if not supplied will use $_POST['workspaceId']
     * @return  void
     * @throws  Exception
     */
    protected function _selectWorkspace($workspaceId = 0)
    {
        $workspaceId = ($workspaceId > 0) ? $workspaceId : $_POST['workspaceId'];
        
        $userSession = $this->getUserSession();
        $userSession->workspaceId = $workspaceId;
        if ($userSession->update()) {
            $url = $this->_loginSuccessUrl;
            $url.= '?loginSuccess=1';
            header("Location: $url");
            exit;
        }
    }

    /**
     * Check if application serves multiple workspaces
     *
     * @return bool
     */
    private function _multiWorkspaceApplication()
    {
        /**
         * @see Ovoyo_Application_Hosted_Workspace
         */
        require_once 'Ovoyo/Application/Hosted/Workspace.php';

        $workspace = new Ovoyo_Application_Hosted_Workspace;
        $workspaceIds = $workspace->dataSetAsArray();
        
        return (count($workspaceIds)) > 0 ? true : false;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo/Authentication/Ovoyo_Authentication_Adapter#getDefaultControllerClass()
     */
    public static function getDefaultControllerClass()
    {
        return 'Ovoyo_Controller_Authentication_Application_Hosted';
    }
}
