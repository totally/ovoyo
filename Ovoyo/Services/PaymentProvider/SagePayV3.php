<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Services_PaymentProvider_Abstract
 */
require_once 'Ovoyo/Services/PaymentProvider/Abstract.php';

/**
 * SagePay class
 *
 * @package    Ovoyo_Services_PaymentProvider
 */
class Ovoyo_Services_PaymentProvider_SagePayV3 extends Ovoyo_Services_PaymentProvider_Abstract
{

    const PROTX_TEST_URL = 'https://test.sagepay.com/gateway/service/vspform-register.vsp';
    const PROTX_LIVE_URL = 'https://live.sagepay.com/gateway/service/vspform-register.vsp';

    const PROTX_TRANS_SUCCESS = 'OK';
    const PROTX_TRANS_CANCELLED = 'ABORT';
    const PROTX_TRANS_FAILED = 'NOTAUTHED';

    const PROTX_PROTOCOL = '3.00';
    const PROTX_TRANS_TYPE_PAYMENT = 'PAYMENT';
    const PROTX_TRANS_TYPE_DEFERRED = 'DEFERRED';
    const PROTX_TRANS_TYPE_PREAUTH = 'PREAUTH';

    // request field
    const PROTX_FIELD_PROTOCOL = 'VPSProtocol';
    const PROTX_FIELD_TRANS_TYPE = 'TxType';
    const PROTX_FIELD_VENDOR = 'Vendor';
    const PROTX_FIELD_CRYPT = 'Crypt';

    /* Required */

    const PROTX_CRYPT_FIELD_INTERNAL_TRANS_ID    = 'VendorTxCode';
    const PROTX_CRYPT_FIELD_AMOUNT               = 'Amount';
    const PROTX_CRYPT_FIELD_CURRENCY             = 'Currency';
    const PROTX_CRYPT_FIELD_DESCRIPTION          = 'Description';
    const PROTX_CRYPT_FIELD_SUCCESS_URL          = 'SuccessURL';
    const PROTX_CRYPT_FIELD_FAILURE_URL          = 'FailureURL';

    const PROTX_CRYPT_FIELD_BILLING_SURNAME      = 'BillingSurname';
    const PROTX_CRYPT_FIELD_BILLING_FIRSTNAMES   = 'BillingFirstNames';
    const PROTX_CRYPT_FIELD_BILLING_ADDRESS      = 'BillingAddress1';
    const PROTX_CRYPT_FIELD_BILLING_CITY         = 'BillingCity';
    const PROTX_CRYPT_FIELD_BILLING_POSTCODE     = 'BillingPostCode';
    const PROTX_CRYPT_FIELD_BILLING_COUNTRY      = 'BillingCountry';

    const PROTX_CRYPT_FIELD_DELIVERY_SURNAME     = 'DeliverySurname';
    const PROTX_CRYPT_FIELD_DELIVERY_FIRSTNAMES  = 'DeliveryFirstNames';
    const PROTX_CRYPT_FIELD_DELIVERY_ADDRESS     = 'DeliveryAddress1';
    const PROTX_CRYPT_FIELD_DELIVERY_CITY        = 'DeliveryCity';
    const PROTX_CRYPT_FIELD_DELIVERY_POSTCODE    = 'DeliveryPostCode';
    const PROTX_CRYPT_FIELD_DELIVERY_COUNTRY     = 'DeliveryCountry';

    /* Optional */

    const PROTX_CRYPT_FIELD_CUSTOMER_NAME        = 'CustomerName';
    const PROTX_CRYPT_FIELD_CUSTOMER_EMAIL       = 'CustomerEMail';
    const PROTX_CRYPT_FIELD_VENDOR_EMAIL         = 'VendorEMail';
    const PROTX_CRYPT_FIELD_EMAIL_MESSAGE        = 'eMailMessage';

    const PROTX_CRYPT_FIELD_SEND_EMAIL           = 'SendEMail';
    const PROTX_CRYPT_FIELD_BILLING_ADDRESS_2    = 'BillingAddress2';    
    const PROTX_CRYPT_FIELD_BILLING_STATE        = 'BillingState';
    const PROTX_CRYPT_FIELD_BILLING_PHONE        = 'BillingPhone';

    const PROTX_CRYPT_FIELD_DELIVERY_ADDRESS_2   = 'DeliveryAddress2';
    const PROTX_CRYPT_FIELD_DELIVERY_STATE       = 'DeliveryState';
    const PROTX_CRYPT_FIELD_PHONE                = 'DeliveryPhone';

    const PROTX_CRYPT_FIELD_BASKET               = 'Basket';
    const PROTX_CRYPT_FIELD_ALLOW_GIFTAID        = 'AllowGiftAid';
    const PROTX_CRYPT_FIELD_APPLY_3D_SECURE      = 'Apply3dSecure';
    const PROTX_CRYPT_FIELD_APPLY_ACSCV2         = 'ApplyAVSCV2';

    const PROTX_CRYPT_FIELD_BASKET_XML           = 'BasketXML';
    const PROTX_CRYPT_FIELD_CUSTOMER_XML         = 'CustomerXML';
    const PROTX_CRYPT_FIELD_SURCHARGE_XML        = 'SurchargeXML';
    const PROTX_CRYPT_FIELD_VENDOR_DATA          = 'VendorData';   

    const PROTX_CRYPT_FIELD_REFERRER_ID          = 'ReferrerID';
    const PROTX_CRYPT_FIELD_LANGUAGE             = 'Language';   
    const PROTX_CRYPT_FIELD_WEBSITE              = 'Website';   

    /* End of optional */

    const PROTX_CRYPT_FIELD_CURRENCY_GBP = 'GBP';
    const PROTX_CRYPT_FIELD_CURRENCY_USD = 'USD';
    const PROTX_CRYPT_FIELD_CURRENCY_EUR = 'EUR';

    // response field
    const PROTX_FIELD_STATUS  = 'Status';
    const PROTX_FIELD_STATUS_DETAIL = 'StatusDetail';
    const PROTX_FIELD_TRANS_ID = 'VPSTxId';
    const PROTX_FIELD_INTERNAL_TRANS_ID = 'VendorTxCode';
    const PROTX_FIELD_AUTH_CODE = 'TxAuthNo';
    const PROTX_FIELD_AMOUNT = 'Amount';

    /**
     * Protocol
     * @var string
     */
    protected $_protocol = self::PROTX_PROTOCOL;

    /**
     * Payment url
     * @var string
     */
    protected $_paymentUrl = '';

    /**
     * Payment type
     * @var string
     */
    protected $_paymentType = self::PROTX_TRANS_TYPE_PAYMENT;

    /**
     * Vendor login name
     * @var string
     */
    protected $_vendor = '';

    /**
     * Password (used for live transactions)
     * @var string
     */
    protected $_password = '';

    /**
     * the currency
     * @var string
     */
    protected $_Currency = self::PROTX_CRYPT_FIELD_CURRENCY_GBP;

    /**
     * Request fields, encrypted before transmission
     * @var array
     */
    public $requestFields = array();

    /**
     * Response fields
     * @var array
     */
    public $responseFields = array();

    /**
     * Error messages
     * @var array
     */
    public $errors = array();

    /**
     * Array containing the fields which are required before being sent to Protx
     * @var array
     */
    public $requiredFields = array(self::PROTX_CRYPT_FIELD_INTERNAL_TRANS_ID => "Transaction id missing",
                                   self::PROTX_CRYPT_FIELD_AMOUNT => "Amount missing",
                                   self::PROTX_CRYPT_FIELD_CURRENCY => "Currency missing",
                                   self::PROTX_CRYPT_FIELD_SUCCESS_URL => "Successful URL missing",
                                   self::PROTX_CRYPT_FIELD_FAILURE_URL => "Failure URL missing"
                                   );

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::_init()
     */
    protected function _init()
    {
        if($this->_useTestPayment) {
            $this->_paymentUrl = self::PROTX_TEST_URL;
        } else {
            $this->_paymentUrl = self::PROTX_LIVE_URL;
        }
    }
    /**
     * Set a request field
     *
     * @param string $name
     * @param string $value
     * @return void
     */
    protected function _setRequestField($name, $value)
    {
        $this->requestFields[$name] = $value;
    }

    /**
     * Set a request fields
     *
     * @param array $data
     * @return void
     */
    protected function _setRequestFields($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $item) {
                $this->_setRequestField($key, $item);
            }
        }

    }

    /**
     * Get a response field value
     *
     * @param string field name
     * @return string value
     */
    protected function _getResponseField($field)
    {
        return $this->response_fields[$field];
    }

    /**
     * Checks whether the current request is valid for transmission
     *
     * @returns bool
     */
    protected function _isValidRequest()
    {
        foreach ($this->requiredFields as $field => $err_msg) {
            if (!$this->requestFields[$field]) {
                echo $err_msg;
                $this->errors[] = $err_msg;
            }
        }
        return (!empty($this->errors)) ? false : true;
    }

    public function decode($strIn)
    {
        $decodedString =  $this->decodeAndDecrypt($strIn);
        parse_str($decodedString, $sagePayResponse);
        return $sagePayResponse;
    }

    protected function decodeAndDecrypt($strIn) 
    {
        $strIn = ltrim($strIn, '@');
        $strIn = pack('H*', $strIn);
        return 
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_128,
                 $this->_password,
                  $strIn,
                   MCRYPT_MODE_CBC,
                    $this->_password);
    }

    /**
     * Encrypts string using mcrypt, uses current
     * password as key
     *
     * @param string to encrypt
     * @return string encrypted
     */
    protected function _encryptAndEncode($strIn) 
    {
        $strIn  =$this->_pkcs5_pad($strIn, 16);
        $crypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->_password, $strIn, MCRYPT_MODE_CBC, $this->_password);
        return "@".bin2hex($crypt);
    }

    /**
     * Uses DIY PKCS#5 padding as mcrypt API does not support it
     *
     * @param string to pad
     * @param int as blocksize
     * @return string padded
     */
    protected function _pkcs5_pad($input) 
    {
        $block_size = 16;
        $padding = "";
        // Pad input to an even block size boundary
        $pad_length = $block_size - (strlen($input) % $block_size);
        for($i = 1; $i <= $pad_length; $i++) {
            $padding .= chr($pad_length);
        }
        return $input . $padding;
    }    

    protected function removePKCS5Padding($input)
    {
        $char = ord($input[strlen($input) - 1]);
        return substr($input, 0, -$char);
    }

    /**
     * Sets up a payment, ready to send to Protx
     *
     * @access public
     * @param string form id
     * @param string text to appear on submit button if javascript is not enabled
     * @return mixed string The HTML form
     */
    protected function _createPaymentForm()
    {
        if (!$this->_isValidRequest()) {
            foreach($this->errors as $errorMsg){
                Ovoyo_ErrorHandler::addError($errorMsg);
            }
            return false;
        }

        // now we need to build the crypt field
        $crypt = array();
        foreach ($this->requestFields as $name => $value) {
            $crypt[] = "$name=$value";
        }
        $crypt = implode("&", $crypt);
        //var_dump($crypt);exit;
        $crypt = $this->_encryptAndEncode($crypt);

        $form = new Ovoyo_Form();
        $form->setAttrib('id', 'sagePaymentForm')
             ->setAttrib('name', 'sagePaymentForm')
             ->setAttrib('action', $this->_paymentUrl)
             ->setMethod('post');

        $form->addElement('hidden', self::PROTX_FIELD_PROTOCOL,
                          array('value' => $this->_protocol,
                                'required' => true
                          ));

        $form->addElement('hidden', self::PROTX_FIELD_TRANS_TYPE,
                          array('value' => $this->_paymentType,
                                'required' => true
                          ));

        $form->addElement('hidden', self::PROTX_FIELD_VENDOR,
                          array('value' => $this->_vendor,
                                'required' => true
                          ));

        $form->addElement('hidden', self::PROTX_FIELD_CRYPT,
                          array('value' => $crypt,
                                'required' => true
                          ));
        return $form;
    }

    /**
     * send a payment off to a provider
     *
     * @param array $vars The payment info
     * @param object instanceof Ovoyo_Controller
     * @return void Callback is provided through sagepay
     */
    public function sendPayment(array $vars, $controller = null)
    {
        if($controller) {
            $renderer = $controller->getHelper('viewRenderer');
            $renderer->setNoRender(true);
        }

        $this->_setRequestFields($vars);

        $script = '<script type="text/javascript">'."\n";
        $script .= 'document.getElementById("sagePaymentForm").submit();'."\n";
        $script .= '</script>'."\n";

        $form = $this->_createPaymentForm();
        if(!$form) {
            return false;
        }
        if($controller) {
            $view = $controller->view;
            $controller->getResponse()->setBody($form->render($view) . $script);
        }else {
            echo $form->render() . $script;
            exit;
        }
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_Abstract::validateCallback()
     */
    public function validateCallback($data = array())
    {
        $valid = false;

        if (array_key_exists('crypt', $data)) {
            $valid = true;
        }

        return $valid;
    }

    /**
     * handle a callback from payment provider
     *
     * @param array $data callback data
     * @return bool
     * @throws Ovoyo_Services_PaymentProvider_Exception
     */
    public function handleCallback(array $data)
    {
        $responseArray = $this->decode($data['crypt']);

        if ($responseArray["Status"] === "OK") {
            //Success
        } elseif ($responseArray["Status"] === "ABORT") {
            //Payment Cancelled
        } else {
            // Payment failed
        }

        $params = $responseArray;

        $this->response_fields = $params;

        // retrieving the payment id from the internal trans id
        $internalTransId = $this->_getResponseField(self::PROTX_FIELD_INTERNAL_TRANS_ID);
        list($timestamp, $paymentId) = explode('-', $internalTransId);

        $callback = array(
            'paymentId' => $paymentId,
            'transactionId' => $this->_getResponseField(self::PROTX_FIELD_TRANS_ID),
            'status' => $this->_getResponseField(self::PROTX_FIELD_STATUS),
            'paymentCancelled' => ($params[self::PROTX_FIELD_STATUS] == self::PROTX_TRANS_CANCELLED) ? true : false,
            'success' => ($params[self::PROTX_FIELD_STATUS] == self::PROTX_TRANS_SUCCESS) ? true : false
        );

        return $callback;
    }

    /**
     * Complete the payment
     * @return void
     */
    public function completePayment()
    {

    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::processPaymentInfo()
     */
    public function processPaymentInfo(array $data)
    {
        $callbackData = $this->handleCallback($data);
        $payment = Ovoyo_Services_PaymentProvider::getPayment(
            array('connName' => $this->_connName)
        );
        if ($payment->fetch($callbackData['paymentId'])) {
            $payment->complete($callbackData['success'],
                $callbackData['transactionId'], $callbackData['status']
            );
            $result = array();
            $result['paymentId'] = $callbackData['paymentId'];
        } else {
            $result = false;
        }
        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::postConvertToOrder()
     */
    public function postConvertToOrder($config)
    {
        if($config['paymentCompleteUrl'] && !headers_sent()) {
            header('Location: ' . $config['paymentCompleteUrl'] . '/');
            exit;
        }
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::prepareData()
     */
    public function prepareData(array $data)
    {
        // collate some customer info
        $name = $data['firstName'] . ' ' . $data['lastName'];

        $address = $data['address1'];
        $address.= ($data['address2']) ? ", \n" . $data['address2']  . ", \n" : '';
        $address.= ($data['address3']) ? ", \n" . $data['address3']  . ", \n" : '';

        $deliveryAddress = "";
        $deliveryAddressFields = array(
            "deliveryAddress1",
            "deliveryAddress3"
        );

        foreach($deliveryAddressFields as $deliveryAddressField) {
            if(!array_key_exists($deliveryAddressField, $data)) {
                continue;
            }
            $deliveryAddress .= $data[$deliveryAddressField] . ", \n";
        }

        $billingCity = $data['townCity'] ? $data['townCity'] : $data['city'];

        //$address.= $data['townCity'] . ", \n";
        //$address.= ($data['countryName']) ? $data['countryName'] : $data['country'];

        $contactNumber = ($data['telephone']) ? $data['telephone'] : $data['mobile'];

        // building the unique string to be passed as the internal trans id
        $internalTransId = time() . '-' . $data['paymentId'];

        $preparedData = array(
            
            self::PROTX_CRYPT_FIELD_CUSTOMER_NAME          => $name,
            self::PROTX_CRYPT_FIELD_CUSTOMER_EMAIL         => $data['email'],
            self::PROTX_CRYPT_FIELD_INTERNAL_TRANS_ID      => $internalTransId,
            self::PROTX_CRYPT_FIELD_AMOUNT                 => $data['amount'],
            self::PROTX_CRYPT_FIELD_VENDOR_EMAIL           => $data['config']['vendorEmail'],
            self::PROTX_CRYPT_FIELD_EMAIL_MESSAGE          => $data['emailMessage'],

            self::PROTX_CRYPT_FIELD_DESCRIPTION            => $data['description'],
            self::PROTX_CRYPT_FIELD_CURRENCY               => 'GBP',
            self::PROTX_CRYPT_FIELD_SUCCESS_URL            => $data['callbackUrl'],
            self::PROTX_CRYPT_FIELD_FAILURE_URL            => $data['callbackUrl'],

            self::PROTX_CRYPT_FIELD_BILLING_SURNAME        => $data['lastName'],
            self::PROTX_CRYPT_FIELD_BILLING_FIRSTNAMES     => $data['firstName'],
            self::PROTX_CRYPT_FIELD_BILLING_ADDRESS        => $address,
            self::PROTX_CRYPT_FIELD_BILLING_CITY           => $data['townCity'] ? $data['townCity'] : $data['city'],
            self::PROTX_CRYPT_FIELD_BILLING_POSTCODE       => $data['postcode'],
            self::PROTX_CRYPT_FIELD_BILLING_COUNTRY        => $data['country'],

            self::PROTX_CRYPT_FIELD_DELIVERY_SURNAME        => $data['lastName'],
            self::PROTX_CRYPT_FIELD_DELIVERY_FIRSTNAMES     => $data['firstName'],
            self::PROTX_CRYPT_FIELD_DELIVERY_ADDRESS        => $deliveryAddress ? $deliveryAddress : $address,
            self::PROTX_CRYPT_FIELD_DELIVERY_CITY           => $data['deliveryCity'] ? $data['deliveryCity'] : $billingCity,
            self::PROTX_CRYPT_FIELD_DELIVERY_POSTCODE       => $data['deliveryPostcode'] ? $data['deliveryPostcode'] : $data['postcode'],
            self::PROTX_CRYPT_FIELD_DELIVERY_COUNTRY        => $data['deliveryCountry'] ? $data['deliveryCountry'] : $data['country'],

            // Optional fields are below

            self::PROTX_CRYPT_FIELD_SEND_EMAIL           => $data['sendEMail'],
            self::PROTX_CRYPT_FIELD_BILLING_ADDRESS_2    => $data['billingAddress2'], 
            self::PROTX_CRYPT_FIELD_BILLING_STATE        => $data['billingState'],
            self::PROTX_CRYPT_FIELD_BILLING_PHONE        => $data['billingPhone'],

            self::PROTX_CRYPT_FIELD_DELIVERY_ADDRESS_2   => $data['deliveryAddress2'],
            self::PROTX_CRYPT_FIELD_DELIVERY_STATE       => $data['deliveryState'],
            self::PROTX_CRYPT_FIELD_PHONE                => $data['deliveryPhone'],

            self::PROTX_CRYPT_FIELD_ALLOW_GIFTAID        => $data['allowGiftAid'],
            self::PROTX_CRYPT_FIELD_APPLY_3D_SECURE      => $data['apply3dSecure'],
            self::PROTX_CRYPT_FIELD_APPLY_ACSCV2         => $data['applyAVSCV2'],

            self::PROTX_CRYPT_FIELD_BASKET               => $data['basket'],
            self::PROTX_CRYPT_FIELD_ALLOW_GIFTAID        => $data['allowGiftAid'],
            self::PROTX_CRYPT_FIELD_APPLY_3D_SECURE      => $data['apply3dSecure'],
            self::PROTX_CRYPT_FIELD_APPLY_ACSCV2         => $data['applyAVSCV2'],

            self::PROTX_CRYPT_FIELD_BASKET_XML           => $data['basketXML'],
            self::PROTX_CRYPT_FIELD_CUSTOMER_XML         => $data['customerXML'],
            self::PROTX_CRYPT_FIELD_SURCHARGE_XML        => $data['surchargeXML'],
            self::PROTX_CRYPT_FIELD_VENDOR_DATA          => $data['vendorData'] ,

            self::PROTX_CRYPT_FIELD_REFERRER_ID          => $data['referrerID'],
            self::PROTX_CRYPT_FIELD_LANGUAGE             => $data['language'] ,
            self::PROTX_CRYPT_FIELD_WEBSITE              => $data['website'],
        );
        return $preparedData;
    }

}
