<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Class which will output a menu element
 *
 * @package    Ovoyo_View
 */
class Ovoyo_View_Helper_TabbedMenuElement
{
    /**
     * Constructor
     *
     * @param   Ovoyo_Menu_Element $element 
     * @param   Ovoyo_View $view
     * @param   bool $selected Mark item as selected
     */
    public function tabbedMenuElement($element, $view, $selected = false)
        {
        $html = "\t";
        $html.= '<li class="tab';
        if ($selected) {
            $html.= ' selected';
        }
        $html.= '"';

        if ($element->id) {
            $html.= ' id="tabElement' . ucfirst($element->id) . '"';
        }

        $html.= ">\n";
        $html.= "\t\t".'<div class="left"></div>'."\n";
        $html.= "\t\t".'<div class="middle">';

        if ($element->link) {
            $html.= '<a href="' . $element->link . '">';
        }

        $html.= $view->escape($element->label);

        if ($element->link) {
            $html.= "</a>";
        }

        $html.= "</div>\n";
        $html.= "\t\t".'<div class="right"></div>'."\n";
        $html.= "\t</li>\n";

        return $html;
    }
}
