<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
* @package Ovoyo_Services
* @subpackage Postcode
*/
class Ovoyo_Services_Postcode_PostcodeAnywhere 
{
    /**
     * @var string The API key that will be used for all requests.
     */
    protected $_apiKey;

////////////////////// CONSTRUCTOR

    /**
     * Contructor
     * 
     * @param string $apiKey
     *   The API key that the class should use for all requests.  You will
     *   need to {@link https://www.postcodeanywhere.co.uk/register/default.aspx set up a PostcodeAnywhere account}
     *   and create a web service key, which is what you will pass into this
     *   function.
     * @param string $apiKeyDeprecated
     *   (deprecated) If present, this will be used instead of $apiKey, in
     *   order to maintain backwards-compatibility with old code.  The old
     *   PostcodeAnywhere API used two parameters (accountCode and licenseCode)
     *   for the connection, but now only one is required.  The original 
     *   licenseCode argument is the same as the apiKey, so external code that
     *   uses the old method should continue to work.
     */
    public function __construct($apiKey, $apiKeyDeprecated=NULL)
    {
        if ($apiKeyDeprecated !== NULL) {
            $this->_apiKey = $apiKeyDeprecated;
        } else {
            $this->_apiKey = $apiKey;
        }
    }

////////////////////// PRIVATE FUNCTIONS FOR API COMMUNICATION
    
    /**
     * _getRequestUrl()
     * Internal helper-function which builds the URL for the specified API 
     * request.
     * 
     * @param string $action
     *   The type of request to perform.  We have defined meangingful
     *   names for all API actions supported by the class.  See the switch
     *   statement for the full list, including the parameters that the
     *   action requests, and a URL for the API documentation.
     * @param array $args
     *   ParameterName => Value pairs for all additional arguments that need
     *   to be passed to the API request.  Parameter names are
     *   case-insensitive.
     */
    private function _getRequestUrl($action, $args = array()) {
        // Set base URL for server.
        $url = 'http://services.postcodeanywhere.co.uk/';

        // Add the appropriate path for the requested action.
        switch ($action) {

            // postcodeToAddressList
            // Get a list of addresses within the specified postcode.
            // Params: 
            //   PostCode: Must be a complete and valid UK post-code.
            //   Building: (optional) A building name/number or company
            //             name to filter the results by.
            // Documentation:
            //   http://www.postcodeanywhere.co.uk/support/webservices/PostcodeAnywhere/Interactive/RetrieveByPostcodeAndBuilding/v1.3/
            case 'postcodeToAddressList':
                $url .= 'PostcodeAnywhere/Interactive/RetrieveByPostcodeAndBuilding/v1.30/xmla.ws';
                break;

            // getAddressRecord
            // Returns the full address record for the given record ID.
            // Params:
            //   Id: The 'id' or 'udprn' of the address you want to look up.
            //   PreferredLanguage: (optional) "English" or "Welsh"
            // Documentation:
            //   http://www.postcodeanywhere.co.uk/support/webservices/PostcodeAnywhere/Interactive/RetrieveById/v1.2/default.aspx
            case 'getAddressRecord':
                $url .= 'PostcodeAnywhere/Interactive/RetrieveById/v1.20/xmla.ws';
                break;

            // geocodeLocation
            // Returns geographic data (OS easting & northing and WGS84
            // latitude & longitude) for the given location.  The location
            // can be a full or partial postcode, a place name, street comma 
            // town, address (comma separated lines) or an 'id' or 'udprn'
            // from another PostcodeAnywhere API result.
            // Params: 
            //   Location: The location to geocode, as described above.
            // Documentation:
            //   http://www.postcodeanywhere.co.uk/support/webservices/Geocoding/UK/Geocode/v2/default.aspx
            case 'geocodeLocation':
                $url .= 'Geocoding/UK/Geocode/v2.00/xmla.ws';
                break;

            // Default case - raise an exception and return.
            default:
                throw new Exception("Invalid PostcodeAnywhere request type: " 
                                   . $action);
                return;
        }
    
        // Add the 'Key' parameter to the array.
        $args['Key'] = $this->_apiKey;

        // Add all request parameters to the URL.
        // We first populate the $args array with the URL-encoded 
        // parameter/value pairs, and then implode this into a query string
        // which is appended to the URL.
        foreach ($args as $parameter => $value) {
            $args[$parameter] = rawurlencode($parameter) . "=" 
                              . rawurlencode($value);
        }
        $url .= "?" . implode("&", $args);
        
        return $url;
    }

    /**
     * _performRequest()
     * Fetch the requested XML file, and return its contents as an array.
     * All PostcodeAnywhere API calls return tabular data, so they can all be 
     * treated in the same way.
     * Each element in the returned array is a row (numerically indexed) and 
     * each row is an array of key => value pairs, where the key is the 
     * attribute (column) name.  The casing of key-names matches the casing 
     * supplied in the XML response from the PostcodeAnywhere API.
     * If an error occurs, Ovoyo_ErrorHandler::addError() is called with the
     * description of the error, and the function returns false.
     * Note that if the response is valid, but contains no rows, then an empty
     * array will be returned.
     *
     * @param string $action @see _getRequestUrl().
     * @param array $args (optional) @see _getRequestUrl().
     * @return mixed Array of response data (possibly empty, if none returned)
     *   or false, if an error occurred.
     */
    private function _performRequest($action, $args = array())
    {
        // Get the request URL for this action.
        // _getRequestUrl() throws an exception if the $action is invalid.  If
        // this happens, we report the error and return false.
        try {
            $url = $this->_getRequestUrl($action, $args);
        } catch (Exception $e) {
            Ovoyo_ErrorHandler::addError($e->getMessage());
            return false;
        }
    
        // Open the XML document using file_get_contents(), via a remote call.
        // If the document could not be opened, report the error and return
        // false.
        $rawData = @file_get_contents($url);
        if ($rawData === false) {
            Ovoyo_ErrorHandler::addError("POSTCODE_ERROR_CANNOT_OPEN_POSTCODEANYWHERE");
            return false;
        }

        // Parse the document into an structured XML object.
        // If parsing fails, report the error and return false.
        $xmlData = @simplexml_load_string($rawData);
        if ($xmlData === false || !isset($xmlData->Columns)) {
            Ovoyo_ErrorHandler::addError("POSTCODE_ERROR_CANNOT_READ_POSTCODEANYWHERE");
            return false;
        }

        // Check whether the request was successful.  If we got an error
        // response, then report it and return false.
        if ($xmlData->Columns['Items'] == 4 
            && $xmlData->Columns->Column['Name'] == 'Error')
        {
            Ovoyo_ErrorHandler::addError($xmlData->Rows->Row['Description']);
            return false;
        }

        // Build the response array.
        $arrResults = array();
        
        $rows = $xmlData->Rows->children();
        if (is_object($rows) && count($rows) > 0) {
            foreach ($rows as $row) {
                $rowItems = array();
        
                foreach($row->attributes() as $attrName => $attrValue) {
                // Note that attribute names are kept in their original
                // mixed-case format.  Attribute values need to be converted
                // to strings, as they are actually SimpleXMLElement objects.
                    $rowItems[$attrName] = strval($attrValue);
                }
        
                $arrResults[] = $rowItems;
            }
        }
        
        // Return the result of the query.
        return $arrResults;
    }

////////////////////// PUBLIC API FUNCTIONS

    /**
     * fetchPostalAddressList()
     * Returns an array of all addresses within the specified postcode,
     * indexed by ID.  This ID can be used to retrieve the full address 
     * details.
     * The address strings are designed for human display, and there is no
     * guarantee that their format won't change in the future.  If you 
     * need to manipulate the address data, then you should not be using 
     * this function.
     * 
     * @param string $postcode Must be a complete and valid postcode
     * @return array If an error occurs the array will be empty.
     */
    public function fetchPostalAddressList($postcode) 
    {
        // Perform the request and retrieve the address data.
        $args = array('PostCode' => $postcode);
        $result = $this->_performRequest("postcodeToAddressList", $args);

        // Build the address list.
        $addressList = array();
        if (is_array($result)) {
            foreach ($result as $row) {
                // The address string is made up of the following fields.
                // There is no point including the Postcode, as they will
                // all be the same.
                // We also omit the County for similar reasons, although there
                // are arguments for including it.
                $address = $this->collapseAddress(
                                                $row['Company'],
                                                $row['Line1'],
                                                $row['Line2'],
                                                $row['Line3'],
                                                $row['Line4'],
                                                $row['Line5'],
                                                $row['PostTown']
                                              );
                $addressList[$row['Udprn']] = $address;
            }
        }

        return $addressList;
    }
    
    /**
     * fetchPostalAddressRecord()
     * Returns the full postal address record matching the specified ID.
     * If the record is not found, or an error occurs, an empty array
     * is returned.
     * 
     * @param int $addressId
     * @return array
     */
    public function fetchPostalAddressRecord($addressId) 
    {
        // Perform the request and retrieve the address data.
        $args = array('Id' => $addressId);
        $result = $this->_performRequest("getAddressRecord", $args);
        
        // If the result is an array, then the address we want is the first
        // element.  If not, an error occurred, so we return an empty
        // array.
        if (is_array($result) && count($result) > 0) {
            $result = $result[0];
        } else {
            $result = array();
        }

        return $result;
    }

    /**
     * fetchPostalAddress()
     * This function was originally created under the old PostcodeAnywhere
     * API, and so has been retained for backwards-compatibility with any
     * projects.
     * It is the same as fetchPostalAddressRecord(), which returns the full 
     * postal address record matching the specified ID, except:
     *   - Only a subset of fields are returned.
     *   - Field names are converted to the old lower-case format.
     *   - Any blank fields are omitted.
     * If the record is not found, or an error occurs, an empty array
     * is returned.
     * 
     * @deprecated Use fetchPostalAddressRecord() instead.
     * @param int $addressId
     * @return array
     */
    public function fetchPostalAddress($addressId)
    {
        $result = $this->fetchPostalAddressRecord($addressId);
        
        // If there are any items in the array, then a full address record
        // was returned.  We convert this to only contain the appropriate
        // fields, using the appropriate names.
        $modifiedResult = array();
        if (count($result) > 0) {
            if ($result['Line1'] !== "") {
                $modifiedResult['line1'] = $result['Line1'];
            }
            if ($result['Line2'] !== "") {
                $modifiedResult['line2'] = $result['Line2'];
            }
            if ($result['Line3'] !== "") {
                $modifiedResult['line3'] = $result['Line3'];
            }
            if ($result['Line4'] !== "") {
                $modifiedResult['line4'] = $result['Line4'];
            }
            if ($result['Line5'] !== "") {
                $modifiedResult['line5'] = $result['Line5'];
            }
            if ($result['PostTown'] !== "") {
                $modifiedResult['post_town'] = $result['PostTown'];
            }
            if ($result['County'] !== "") {
                $modifiedResult['county'] = $result['County'];
            }
            if ($result['Postcode'] !== "") {
                $modifiedResult['postcode'] = $result['Postcode'];
            }
        }
        
        return $modifiedResult;
    }

    /**
     * geocodeLocation()
     * Get geocode data for a location.
     * 
     * @param string $location
     * @return string $output
     */
    public function geocodeLocation($location)
    {
        // Perform the request and retrieve the address data.
        $args = array('Location' => $location);
        $result = $this->_performRequest("geocodeLocation", $args, true);

        // If the request failed or there were no results, return false.
        if (!is_array($result) || count($result) == 0) {
            return false;
        }
        // Otherwise return the array of matches.
        else {
            return $result;
        }

    }
    
////////////////////// PUBLIC UTILITY FUNCTIONS
    
    /**
     * collapseAddress()
     * Collapse the individual address elements into a single string.
     * May be called with any number of arguments.
     * Any non-blank arguments will be joined into a single string, with
     * the components separated by commas.  Blank arguments (empty strings)
     * will be skipped.  Arguments are trim()med before checking.
     *
     * @param string,... Multiple address components.
     * @return string The tidied address string.
     */
    public function collapseAddress() {
        $args = func_get_args();
        $result = "";
        foreach ($args as $arg) {
            $arg = trim($arg);
            if ($arg !== "") {
                if ($result !== "") {
                    $result .= ", ";
                }
                $result .= $arg;
            }
        }
    
        return $result;
    }
    
    /**
     * Checks to see if postcode is valid
     *
     * @param string $postcode
     * @return boolean
     */
    public function isValidUkPostcode($postcode)
    {
        $postcode = str_replace(" ", "", $postcode);
        $postcode = strtoupper($postcode);
    
        if (eregi('^[A-Z]{1,2}[0-9]{1,2}[A-Z]? ?[0-9][A-Z]{2}$', $postcode)
        && (strlen($postcode) < 8) && (strlen($postcode) > 0)
        ) {
            $halve[1] = substr($postcode, -3, 3);
            $position = strpos($postcode, $halve[1]);
            $halve[0] = substr($postcode, 0, $position);
    
            return $halve;
        }
    
        return false;
    }
    
}