<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Zend_Form_Decorator_Label
 */
require_once 'Zend/Form/Decorator/Label.php';

/**
 * Replacement label decorator
 *
 * Single static method to take a Ovoyo_Model instance (or sub class of)
 * and create a Zend_Form instance from it. Based on code found in the following article:
 * http://codecaine.co.za/posts/form-generation-with-zend-form-part-2/
 *
 * @package    Ovoyo_Form
 */
class Ovoyo_Form_Decorator_TdLabel extends Zend_Form_Decorator_Label
{
    /**
     * Render a label
     * 
     * @param  string $content 
     * @return string
     */
    public function render($content)
    {
        $element = $this->getElement();
        $view    = $element->getView();
        if (null === $view) {
            return $content;
        }

        $label     = $this->getLabel();
        $separator = $this->getSeparator();
        $placement = $this->getPlacement();
        $tag       = $this->getTag();
        $id        = $this->getId();
        $class     = $this->getClass();
        $options   = $this->getOptions();

        if (empty($label) && empty($tag)) {
            return $content;
        }

        if ($element instanceof Zend_Form_Element_Submit) {
            $label = '';
        }

        if (null !== $tag) {
            require_once 'Zend/Form/Decorator/HtmlTag.php';
            $decorator = new Zend_Form_Decorator_HtmlTag();
            $decorator->setOptions(array('tag' => $tag, 'class' => $class . ' ' . 'label-' . $element->getName()));
            $label = $decorator->render($label);
        }

        switch ($placement) {
            case self::APPEND:
                return $content . $separator . $label;
            case self::PREPEND:
                return $label . $separator . $content;
        }
    }
}
