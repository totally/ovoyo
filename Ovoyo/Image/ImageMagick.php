<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Image_Abstract
 */
require_once 'Ovoyo/Image/Abstract.php';

/**
 * MagickWand Image class
 *
 * @package    Ovoyo_Image
 */
class Ovoyo_Image_ImageMagick extends Ovoyo_Image_Abstract
{
    public $imagick;
        
    /**
     * Constructor
     * 
     * @return	void
     */
    public function __construct()
    {
        $this->imagick = new Imagick();
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::openFromFile()
     */
    public function openFromFile($inFile)
    {
        $this->imagick->readImage($inFile);
        $this->_originalFilename = $inFile;
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::saveToFile()
     */
    public function saveToFile($outFile) 
    {
        $this->imagick->writeImage($outFile);
        if (!is_file($outFile)) {
            require_once 'Ovoyo/Image/Exception.php';
            throw new Ovoyo_Image_Exception("Failed to create file $outFile");
        }
        
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::save()
     */
    public function save()
    {
        if (!$this->_originalFilename) {
            require_once 'Ovoyo/Image/Exception.php';
            throw new Ovoyo_Image_Exception('Filename not known, use saveToFile() instead');
        }
        
        return $this->saveToFile($this->_originalFilename);
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::getWidth()
     */
    public function getWidth()
    {
        return $this->imagick->getImageWidth();
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::getHeight()
     */
    public function getHeight()
    {
        return $this->imagick->getImageHeight();
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::resize()
     */
    public function resize($width, $height, $crop = true, $outFile = null) 
    {
        // get width and height of image
        $origWidth  = $this->getWidth();
        $origHeight = $this->getHeight();

        // info to pass to resize function
        $resizeFilter = Imagick::FILTER_LANCZOS;
        $resizeBlur   = 1.0;

        // Make sure we have a width and height
        $width  = (!$width) ? $origWidth : $width;
        $height = (!$height) ? $origHeight : $height;

        // get our scale
        $scaleWidth  = $width / $origWidth;
        $scaleHeight = $height / $origHeight;
        $scale = ($scaleWidth <= $scaleHeight) ? $scaleWidth : $scaleHeight;
       
        if ($scale > 1) {
            $resizeBlur = 1.5;
        }
        
        // make sure CMYK images are converted to RGB
        if ($this->imagick->getImageColorspace() == imagick::COLORSPACE_CMYK) {
            $this->imagick->setImageColorspace(imagick::COLORSPACE_SRGB);
        }

        // if scale is > 1 and $this->_setAllowUpscaling is set to false,
        // just output file contents as we can't make images larger
        if (!$this->_allowUpscaling 
            && ($scale > 1 || ($scale == 1 && !$crop))) {
                
            // if cropping, then get offsets and centre align
            if ($crop) {
                $offsetX = ceil(($width - $origWidth) / 2);
                $offsetY = ceil(($height - $origHeight) / 2);

                $newImagick = new Imagick();
                $pixel = new ImagickPixel($this->_defaultBackgroundColour);
                $newImagick->newImage(
                    $width, $height, $pixel
                );
                $newImagick->setImageFormat($this->imagick->getImageFormat());
                $newImagick->compositeImage(
                    $this->imagick, Imagick::COMPOSITE_OVER, $offsetX, $offsetY
                );

                $this->imagick = $newImagick;
            }
            
            // save image to a file
            if ($outFile) {
                $this->saveToFile($outFile);  
            }            
            return true;
        }

        // resize, no cropping
        if (!$crop) {
            // get actual new width and height - constraining proportions
            $width  = floor($origWidth * $scale);
            $height = floor($origHeight * $scale);

            $this->imagick->resizeImage($width, $height, $resizeFilter, 
                                        $resizeBlur);

            $debug.= "$fileName (not cropped):<BR>"
                   . "orig width x orig height: $origWidth x $origHeight<br />"
                   . "width x height: $width x $height<br />"
                   . "<BR>";

        // resize and crop
        } else {
            $scale = ($scaleWidth >= $scaleHeight) ? $scaleWidth : $scaleHeight;
            if (!$this->_allowUpscaling) {
                $scale = ($scale > 1) ? 1 : $scale;
            }

            // get actual new width and height - constraining proportions
            $resizeWidth  = floor($origWidth * $scale);
            $resizeHeight = floor($origHeight * $scale);

            $cropX = 0;
            $cropY = 0;
            $offsetX = 0;
            $offsetY = 0;
            if ($scaleHeight == $scale) {
                $cropX = ceil(($resizeWidth - $width) / 2);
                if ($cropX < 0) {
                    $offsetX = $offsetX * -1;
                    $cropX = 0;
                    $cropY = ceil(($origHeight - $height) / 2);
                }
            } else {
                $cropY = ceil(($resizeHeight - $height) / 2);
                if ($cropY < 0) {
                    $offsetY = $offsetY * -1;
                    $cropY = 0;
                    $cropX = ceil(($origWidth - $width) / 2);
                }
            }

            if ($origWidth < $width) {
                $offsetX = ceil(($width - $origWidth) / 2);
            }
            if ($origHeight < $height) {
                $offsetY = ceil(($height - $origHeight) / 2);
            }

            $newImagick = new Imagick();
            $pixel = new ImagickPixel($this->_defaultBackgroundColour);
            $newImagick->newImage($width, $height, $pixel);
            $newImagick->setImageFormat($this->imagick->getImageFormat());
            $newImagick->setImageColorspace(
                $this->imagick->getImageColorspace()
            );

            // resize & crop image
            if ($scale != 1) {
                $this->imagick->resizeImage($resizeWidth, $resizeHeight, 
                                            $resizeFilter, $resizeBlur);
            }
            $this->imagick->cropImage($resizeWidth, $resizeHeight, $cropX, 
                                      $cropY);
            $newImagick->compositeImage(
                $this->imagick, Imagick::COMPOSITE_OVER,
                $offsetX, $offsetY
            );

            $debug.= "$fileName (cropped):<BR>"
                   . "orig width x orig height: $origWidth x $origHeight<br />"
                   . "width x height: $width x $height<br />"
                   . "resize_width x resize_height: $resizeWidth x $resizeHeight<br />"
                   . "scale: $scale<br />"
                   . "offset_x x offset_y: $offsetX x $offsetY<br />"
                   . "crop_x x crop_y: $cropX x $cropY<br />"
                   . "<BR>";

            $this->imagick = $newImagick;
        }

        //throw new Exception($debug);
        //error_log($debug);

        // save new image
        if ($outFile) {
            $this->saveToFile($outFile);
        }

        return true;        
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::crop()
     */
    public function crop($width, $height, $offsetX = 0, $offsetY = 0, 
                         $outFile = null)
    {
        $this->imagick->cropImage($width, $height, $offsetX, $offsetY);
        if ($outFile) {
            $this->saveToFile($outFile);
        }
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo/Image/Ovoyo_Image_Abstract#rotate($degrees, $outFile)
     */
    public function rotate($degrees, $outFile = null)
    {
        $this->imagick->rotateImage(new ImagickPixel(), $degrees);
        if ($outFile) {
            $this->saveToFile($outFile);
        }
        
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::flipX()
     */
    public function flipX($outFile = null)
    {
        $this->imagick->flipImage();
        if ($outFile) {
            $this->saveToFile($outFile);
        }
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::flipY()
     */
    public function flipY($outFile = null)
    {
        $this->imagick->flopImage();
        if ($outFile) {
            $this->saveToFile($outFile);
        }
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::addWatermark()
     */
    public function addWatermark($image, $waterMarkPosX = 'right', 
                                 $waterMarkPosY = 'bottom', $alpha = 0.5)
    {
        $imageWidth  = $this->getWidth();
        $imageHeight = $this->getHeight();

        if ($image instanceOf Ovoyo_Image_ImageMagick) {
            $waterMark = self::cloneImage($image);
            
        } else {
            // do nothing if watermark file does not exist
            if (!is_file($image)) { 
                return true; 
            }
            
            $waterMark = new self;
            $waterMark->openFromFile($image);
        }

        $waterMark->imagick->evaluateImage(Imagick::EVALUATE_MULTIPLY, $alpha, 
                                           Imagick::CHANNEL_ALPHA);

        $waterMarkWidth  = $waterMark->getWidth();
        $waterMarkHeight = $waterMark->getHeight();

        // if watermark is larger than given image, then scale down
        // @todo

        // get offset for water mark
        $offsetX = 0;
        $offsetY = 0;

        // offset X
        if ($waterMarkPosX == 'right' && $waterMarkWidth < $imageWidth) {
            $offsetX = $imageWidth - $waterMarkWidth;
        } else if ($waterMarkPosX == 'middle'
                   && $imageWidth != $waterMarkWidth){
            $offsetX = ceil(($imageWidth - $waterMarkWidth) / 2);
        }

        // offset Y
        if ($waterMarkPosY == 'bottom' 
            && $waterMarkHeight < $imageHeight) {
            $offsetY = $imageHeight - $waterMarkHeight;
        } else if ($waterMarkPosY == 'middle'
                   && $imageHeight != $waterMarkHeight){
            $offsetY = ceil(($imageHeight - $waterMarkHeight) / 2);
        }

        $this->imagick->compositeImage(
            $waterMark->imagick, Imagick::COMPOSITE_OVER, 
            $offsetX, $offsetY
        );
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::setCanvasSize()
     */
    public function setCanvasSize($width, $height, $offsetX = 0, $offsetY = 0)
    {
        $newImagick = new Imagick();
        $pixel = new ImagickPixel($this->_defaultBackgroundColour);
        $newImagick->newImage($width, $height, $pixel);
        $newImagick->setImageFormat($this->imagick->getImageFormat());
        $newImagick->setImageColorspace(
            $this->imagick->getImageColorspace()
        );
        
        
        $newImagick->compositeImage(
            $this->imagick, Imagick::COMPOSITE_OVER,
            $offsetX, $offsetY
        );
        
        $this->imagick = $newImagick;
    }
    
    /**
     * Clone existing magickwand image
     * 
     * @param   Ovoyo_Image_MagickWand $image Image to clone
     * @return  Ovoyo_Image_MagickWand
     */
    public static function cloneImage($image)
    {
        $clone = new Ovoyo_Image_ImageMagick;
        $clone->_allowUpscaling = $image->_allowUpscaling;
        $clone->_defaultBackgroundColour = $image->_defaultBackgroundColour;
        $clone->imagick = $image->imagick->clone();
        return $clone;
    }
}
