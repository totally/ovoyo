<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Image_Abstract
 */
require_once 'Ovoyo/Image/Abstract.php';

/**
 * MagickWand Image class
 *
 * @package    Ovoyo_Image
 */
class Ovoyo_Image_MagickWand extends Ovoyo_Image_Abstract
{
    public $wand;
    
    private $_filter = MW_LanczosFilter;
    private $_blur   = 1.0;
    
    /**
     * Constructor
     * 
     * @return	void
     */
    public function __construct()
    {
        $this->wand = NewMagickWand();
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::openFromFile()
     */
    public function openFromFile($inFile)
    {
        MagickReadImage($this->wand, $inFile);
        $this->_originalFilename = $inFile;
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::saveToFile()
     */
    public function saveToFile($outFile)
    {
        MagickStripImage($this->wand);
        MagickWriteImage($this->wand, $outFile);
        if (!is_file($outFile)) {
            require_once 'Ovoyo/Image/Exception.php';
            throw new Ovoyo_Image_Exception("Failed to create file $outFile");
        }
        
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::save()
     */
    public function save()
    {
        if (!$this->_originalFilename) {
            require_once 'Ovoyo/Image/Exception.php';
            throw new Ovoyo_Image_Exception('Filename not known, use saveToFile() instead');
        }
        
        return $this->saveToFile($this->_originalFilename);
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::getWidth()
     */
    public function getWidth()
    {
        return MagickGetImageWidth($this->wand);
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::getHeight()
     */
    public function getHeight()
    {
        return MagickGetImageHeight($this->wand);
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::resize()
     */
    public function resize($width, $height, $crop = true, $outFile = null)
    {
        // get width and height of image
        $origWidth  = $this->getWidth();
        $origHeight = $this->getHeight();
        
        if (!$origWidth || !$origHeight) {
            $exception = 'Could not resize and original width or height could '
                       . 'not be determined';
            throw new Exception($exception);
        }

        // info to pass to resize function
        $resizeFilter = $this->_filter;
        $resizeBlur   = $this->_blur;

        // Make sure we have a width and height
        $width  = (!$width) ? $origWidth : $width;
        $height = (!$height) ? $origHeight : $height;

        // get our scale
        $scaleWidth  = $width / $origWidth;
        $scaleHeight = $height / $origHeight;
        $scale = ($scaleWidth <= $scaleHeight) ? $scaleWidth : $scaleHeight;
            
        if ($scale > 1) {
            $resizeBlur = 1.5;
        }
            
        // if scale is > 1 and $this->_setAllowUpscaling is set to false,
        // just output file contents as we can't make images larger
        if (!$this->_allowUpscaling 
            && ($scale > 1 || ($scale == 1 && !$crop))) {
                
            // if cropping, then get offsets and centre align
            if ($crop) {
                $offsetX = ceil(($width - $origWidth) / 2);
                $offsetY = ceil(($height - $origHeight) / 2);

                $newWand = NewMagickWand();
                $pixelWand = NewPixelWand($this->_defaultBackgroundColour);
                MagickNewImage(
                    $newWand, $width, $height, $pixelWand
                );
                MagickSetImageFormat(
                    $newWand, MagickGetImageFormat($this->wand)
                );
                MagickCompositeImage(
                    $newWand, $this->wand, 
                    MW_OverCompositeOp, $offsetX, $offsetY
                );

                $this->wand = $newWand;
            }
            // save image to a file
            if ($outFile) {
                $this->saveToFile($outFile);
            }
            return true;
        }

        // resize, no cropping
        if (!$crop) {
            // get actual new width and height - constraining proportions
            $width  = floor($origWidth * $scale);
            $height = floor($origHeight * $scale);

            MagickResizeImage(
                $this->wand, $width, $height, $resizeFilter, $resizeBlur
            );

            $debug.= "$fileName (not cropped):<BR>"
                   . "orig width x orig height: $origWidth x $origHeight<br />"
                   . "width x height: $width x $height<br />"
                   . "<BR>";

        // resize and crop
        } else {
            $scale = ($scaleWidth >= $scaleHeight) ? $scaleWidth : $scaleHeight;
            if (!$this->_allowUpscaling) {
                $scale = ($scale > 1) ? 1 : $scale;
            }

            // get actual new width and height - constraining proportions
            $resizeWidth  = floor($origWidth * $scale);
            $resizeHeight = floor($origHeight * $scale);

            $cropX = 0;
            $cropY = 0;
            $offsetX = 0;
            $offsetY = 0;
            if ($scaleHeight == $scale) {
                $cropX = ceil(($resizeWidth - $width) / 2);
                if ($cropX < 0) {
                    $offsetX = $offsetX * -1;
                    $cropX = 0;
                    $cropY = ceil(($origHeight - $height) / 2);
                }
            } else {
                $cropY = ceil(($resizeHeight - $height) / 2);
                if ($cropY < 0) {
                    $offsetY = $offsetY * -1;
                    $cropY = 0;
                    $cropX = ceil(($origWidth - $width) / 2);
                }
            }

            if ($origWidth < $width) {
                $offsetX = ceil(($width - $origWidth) / 2);
            }
            if ($origHeight < $height) {
                $offsetY = ceil(($height - $origHeight) / 2);
            }

            $newWand = NewMagickWand();
            $pixelWand = NewPixelWand($this->_defaultBackgroundColour);
            MagickNewImage($newWand, $width, $height, $pixelWand);
            MagickSetImageFormat($newWand, MagickGetImageFormat($this->wand));
            MagickSetImageColorspace(
                $newWand, MagickGetImageColorspace($this->wand)
            );

            // resize & crop image
            if ($scale != 1) {
                MagickResizeImage(
                    $this->wand, $resizeWidth, $resizeHeight, 
                    $resizeFilter, $resizeBlur
                );
            }
            MagickCropImage(
                $this->wand, $resizeWidth, $resizeHeight, $cropX, $cropY
            );
            MagickCompositeImage(
                $newWand, $this->wand, MW_OverCompositeOp, $offsetX, $offsetY
            );

            $debug.= "$fileName (cropped):<BR>"
                   . "orig width x orig height: $origWidth x $origHeight<br />"
                   . "width x height: $width x $height<br />"
                   . "resize_width x resize_height: $resizeWidth x $resizeHeight<br />"
                   . "scale: $scale<br />"
                   . "offset_x x offset_y: $offsetX x $offsetY<br />"
                   . "crop_x x crop_y: $cropX x $cropY<br />"
                   . "<BR>";

            $this->wand = $newWand;
        }

        // throw new Exception($debug);
        //error_log($debug);

        // save new image
        if ($outFile) {
            $this->saveToFile($outFile);
        }

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::crop()
     */
    public function crop($width, $height, $offsetX = 0, $offsetY = 0, $outFile = null)
    {
        MagickCropImage($this->wand, $width, $height, $offsetX, $offsetY);
        if ($outFile) {
            $this->saveToFile($outFile);
        }
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::rotate()
     */
    public function rotate($degrees, $outFile = null)
    {
        MagickRotateImage($this->wand, null, $degrees);
        if ($outFile) {
            $this->saveToFile($outFile);
        }
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::flipX()
     */
    public function flipX($outFile = null)
    {
        MagickFlipImage($this->wand);
        if ($outFile) {
            $this->saveToFile($outFile);
        }
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::flipY()
     */
    public function flipY($outFile = null)
    {
        MagickFlopImage($this->wand);
        if ($outFile) {
            $this->saveToFile($outFile);
        }
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::addWatermark()
     */
    public function addWatermark($image, $waterMarkPosX = 'right', $waterMarkPosY = 'bottom', $alpha = 0.5)
    {
        $imageWidth  = $this->getWidth();
        $imageHeight = $this->getHeight();

        if ($image instanceOf Ovoyo_Image_MagickWand) {
            $waterMark = self::cloneImage($image);
            
        } else {
            // do nothing if watermark file does not exist
            if (!is_file($image)) { return true; }
            
            $waterMark = new self;
            $waterMark->openFromFile($image);
        }

        MagickEvaluateImage($waterMark->wand, MW_MultiplyEvaluateOperator, $alpha, MW_AlphaChannel);

        $waterMarkWidth  = $waterMark->getWidth();
        $waterMarkHeight = $waterMark->getHeight();

        // if watermark is larger than given image, then scale down
        // @todo

        // get offset for water mark
        $offsetX = 0;
        $offsetY = 0;

        // offset X
        if ($waterMarkPosX == 'right' && $waterMarkWidth < $imageWidth) {
            $offsetX = $imageWidth - $waterMarkWidth;
        } else if ($waterMarkPosX == 'middle' && $waterMarkWidth < $imageWidth) {
            $offsetX = ceil(($imageWidth - $waterMarkWidth) / 2);
        }

        // offeset Y
        if ($waterMarkPosY == 'bottom' && $waterMarkHeight < $imageHeight) {
            $offsetY = $imageHeight - $waterMarkHeight;
        } else if ($waterMarkPosY == 'middle' && $waterMarkHeight < $imageHeight) {
            $offsetY = ceil(($imageHeight - $waterMarkHeight) / 2);
        }

        MagickCompositeImage($this->wand, $waterMark->wand, MW_OverCompositeOp, $offsetX, $offsetY);
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Image_Abstract::setCanvasSize()
     */
    public function setCanvasSize($width, $height, $offsetX = 0, $offsetY = 0)
    {
        $newWand = NewMagickWand();
        $pixelWand = NewPixelWand($this->_defaultBackgroundColour);
        MagickNewImage($newWand, $width, $height, $pixelWand);
        MagickSetImageFormat($newWand, MagickGetImageFormat($this->wand));
        MagickSetImageColorspace(
            $newWand, MagickGetImageColorspace($this->wand)
        );

        MagickCompositeImage(
            $newWand, $this->wand, MW_OverCompositeOp, $offsetX, $offsetY
        );
        
        $this->wand = $newWand;
    }
    
    /**
     * Clone existing magickwand image
     *
     * @param   Ovoyo_Image_MagickWand $image Image to clone
     * @return  Ovoyo_Image_MagickWand
     */
    public static function cloneImage($image)
    {
        $clone = new Ovoyo_Image_MagickWand();
        $clone->_allowUpscaling = $image->_allowUpscaling;
        $clone->_defaultBackgroundColour = $image->_defaultBackgroundColour;
        $clone->wand = CloneMagickWand($image->wand);
        return $clone;
    }
    

    /**
     * Set filter
     *
     * For examples of filters and their effects, see http://www.dylanbeattie.net/magick/filters/result.html
     *
     * @param   string $filter
     * @return  void
     */
    public function setFilter($filter)
    {
        $this->_filter = $filter;
    }
    
    /**
     * Set blur
     *
     * For examples of blurring and its affect, see http://www.dylanbeattie.net/magick/filters/result.html
     *
     * @param   string $blur
     * @return  void
     */
    public function setBlur($blur)
    {
        $this->_blur = $blur;
    }
    
}
