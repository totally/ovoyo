<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Authentication_Adapter
 */
require_once 'Ovoyo/Authentication/Adapter.php';

/**
 * Application authentication
 *
 * @package     Ovoyo_Authentication
 * @subpackage  Adapter
 */
class Ovoyo_Authentication_Adapter_Simple extends Ovoyo_Authentication_Adapter
{
    /**
     * Init
     *
     */
    public function init()
    {
        $this->_userClass = 'Ovoyo_Authentication_User';
        $this->_userSessionClass = 'Ovoyo_Authentication_UserSession';
        $this->_idField = 'email';
        $this->_tablePrefix = 'users_';
    }

    /**
     * Authenticate
     *
     * Check we have a valid, logged in, user
     */
    protected function _authenticate()
    {
        $webContainer = Ovoyo_WebContainer::getInstance();
        $user = null;

        // if we have a login request, call login()
        if ($_REQUEST['_login']) {
            $this->_login();
            return;
        }

        // if we have a valid session id, try fetching logged in user
        $sessionId = $_REQUEST[$this->_sessionIdentifier];
        if (!isset($sessionId)) {
            $sessionId = $_COOKIE[$this->_sessionIdentifier];
        }
        if (isset($sessionId)) {
            $dropCookie = false;

            // if session id has come via get or post, need to flag to drop cookie
            if (!$_COOKIE[$this->_sessionIdentifier]) {
                $_COOKIE[$this->_sessionIdentifier] = $sessionId;
                $dropCookie = true;
            }
    
            // fetch user from session id
            $userSession = $this->getUserSession();
            $userSession->fetch($sessionId);

            if (isset($userSession->email)) {
                $user = $this->getUser();
                if ($user->fetch($userSession->email)) {
                    $userSession->refresh();
                }
            }
    
            // drop cookie if required
            if ($dropCookie) {
                $userSession->dropSessionIdCookie();
            }
            
            // destroy session cookie if we don't have a valid user
            if (!isset($user->{$this->_idField})) {
                $secure_cookie = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? true : false;
                setcookie($this->_sessionIdentifier, null, 0, "/", '', $secure_cookie);
                unset($_COOKIE[$this->_sessionIdentifier]);
            }
        }
        
        // if url is an allowed exception - do nothing further
        if ($this->allowedException()) {
            // make user available globally
            if (isset($user)) {
                $webContainer->setUser($user);
            }
            return;
        }
        
        // if we have a logout request, call logout()
        if ($_REQUEST['_logout']) {
            $this->_logout();
            return;
        }

        // if we don't have logged in user, send to login url
        if ($this->_enforcing && !isset($user->{$this->_idField})) {
        
            $this->_redirect($this->_loginUrl);
            return;
        }
    
        // if we don't have logged in user then redirect to login screen
        // or simply return if authentication is not being enforced
        if (!isset($user->{$this->_idField})) {
            if ($this->_enforcing) {
                $this->_redirect($this->_loginUrl);
            }
            return;
        }
        
        // make user instance available globally
        $webContainer->setUser($user);
        
        // finally if logged in and on login page, redirect to login success
        $urlPath = preg_replace("/\?.*$/", "", $this->_loginUrl);
        $urlPath = rtrim($urlPath, "/");
        
        if ($urlPath == Ovoyo_WebContainer::getInstance()->getParam('urlPath')) {
            header("Location: $this->_loginSuccessUrl");
            exit;
        }
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo/Authentication/Ovoyo_Authentication_Adapter#getDefaultControllerClass()
     */
    public static function getDefaultControllerClass()
    {
        return 'Ovoyo_Controller_Authentication_Simple';
    }
}
