<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */


/**
 * @see Zend_Form
 */
require_once 'Zend/Form.php';

/**
 * Extension to Zend_Form to provide an more useful(?) tabular format
 *
 * @package    Ovoyo_Form
 */
class Ovoyo_Form extends Zend_Form
{
    private $_resetZendDecorators = false;
    private $_defaultElementDecorators;
    
    /**
     * Initialize form (used by extending classes)
     *
     * @return void
     */
    public function init()
    {
        $this->setDisableLoadDefaultDecorators(true);

        $this->addElementPrefixPath('Ovoyo_Form_Decorator', 
                                    'Ovoyo/Form/Decorator/', self::DECORATOR);

        $this->addDecorator('FormElements')
             ->addDecorator(array('containingTable' => 'HtmlTag'), 
                            array('tag' => 'table', 'class' => 'ovoyoForm'))
             ->addDecorator('Form');
             
        $this->_defaultElementDecorators = array(
                'ViewHelper',
            'Errors', 
            array('TdElement', 
                  'options' => array('tag' => 'td', 'class' => 'element')
            ),
            array('TdLabel', 
                  'options' => array('tag' => 'td', 'class' => 'label')
            ),
            array('decorator' => array('tr' => 'HtmlTag'), 
                  'options' => array('tag' => 'tr')
            )
        );
    }

    /**
     * Add a button set
     *
     * Just a wrapper for addDisplayGroup
     *
     * @param  array $elements
     * @param  string $name
     * @param  int $order
     * @return Zend_Form
     * @throws Zend_Form_Exception if no valid elements provided
     */
    public function addButtonSet(array $elements, $name, $options = null)
    {
        parent::addDisplayGroup($elements, $name, $options);

        $displayGroup = $this->getDisplayGroup($name);
        $displayGroup->addPrefixPath('Ovoyo_Form_Decorator', 
                                     'Ovoyo/Form/Decorator/');
        $displayGroup->setDecorators(array('TdButtonSet'));
    }

    /**
     * Remove a button set
     *
     * @param  string $name
     * @return void
     */
    public function removeButtonSet($name)
    {
        $displayGroup = $this->getDisplayGroup($name);
        
        $elements = $displayGroup->getElements();
        foreach ($elements AS $element) {
            $this->removeElement($element->getName());
        }

        $this->removeDisplayGroup($name);
    }

    /**
     * Reset decorators with zend default ones
     *
     * @return  void
     */
    public function resetZendDecorators()
    {
        $this->_resetZendDecorators = true;
    }
    
    /**
     * Render form
     *
     * @param  Zend_View_Interface $view
     * @return string
     */
    public function render(Zend_View_Interface $view = null)
    {
        if ($this->_resetZendDecorators) {
        	$removeFormDecorator = false;
            if (!$this->getDecorator('Form')) {
                $removeFormDecorator = true;
            }
            
            $zendForm = new Zend_Form();
            $this->setDecorators($zendForm->getDecorators());
            
            if ($removeFormDecorator) {
                $this->removeDecorator('Form');
            }
            
            // remove decorators on hidden fields
            $elements = $this->getElements('hidden');
            foreach ($elements AS $element) {
                if (!$element instanceof Zend_Form_Element_Hidden) {
                    continue;
                }
                $element->setDecorators(array('ViewHelper'));
            }
            
            return parent::render($view);
        }
            
        $groupsDecorator = array();
        if (count($this->getDisplayGroups())) {
            $groupsDecorator = array('decorator' => array('table' => 'HtmlTag'),
                 'options' => array('tag' => 'table', 'class' => 'ovoyoForm')
            );
            $this->removeDecorator('containingTable');
            $this->setDisplayGroupDecorators(array('FormElements', 
                'Fieldset', 
                array('decorator' => array('tr' => 'HtmlTag'), 
                      'options' => array('tag' => 'tr')
                )
            ));
            $this->_defaultElementDecorators[] = $groupsDecorator;
        }
        
        // use td tags for table elements
        $this->setElementDecorators($this->_defaultElementDecorators);

        // remove decorators on hidden fields
        $elements = $this->getElements('hidden');
        foreach ($elements AS $element) {
            if (!$element instanceof Zend_Form_Element_Hidden) {
                continue; 
            }
            $element->setDecorators(array('ViewHelper'));
        }
        
        // reset file decorators on file elements
        $elements = $this->getElements('file');
        foreach ($elements AS $element) {
            if (!$element instanceof Zend_Form_Element_File) {
                continue; 
            }
            $element->setDecorators(array('File', 
            array('TdElement', 
                  'options' => array('tag' => 'td', 'class' => 'element')
            ),
            array('TdLabel', 
                  'options' => array('tag' => 'td', 'class' => 'label')
            ),
            array('decorator' => array('tr' => 'HtmlTag'), 
                  'options' => array('tag' => 'tr')
            )));
        }

        return parent::render($view);
    }

    /**
     * Sets our default element decorators
     * 
     * @param array $decorators
     * @return void
     */
    public function setDefaultElementDecorators(array $decorators)
    {
        $this->_defaultElementDecorators = $decorators;
    }

    /**
     * Add a default element decorator, if named same as existing will override previous one
     * 
     * @param array $decorators
     * @param int $position will set the position of the decorator
     * @return void
     */
    public function addDefaultElementDecorator(array $decorator, $position = null)
    {
        try {
            if (!$position) {
                $this->_defaultElementDecorators[] = $decorator;
            } else {
                if ($position <= 0) {
                    throw new Exception("Position of the decorator has to be 1 or greater");
                }
                $count = 1;
                $decorators = array();
                if (count($this->_defaultElementDecorators)) {
                    $newDecoratorAdded = false;
                    foreach ($this->_defaultElementDecorators as $defaultDecorator) {
                        if ($count == $position) {
                            $decorators[] = $decorator;
                            $newDecoratorAdded = true;
                        }
                        $decorators[] = $defaultDecorator;
                        $count++; 
                    }
                    if (!$newDecoratorAdded) {
                        $decorators[] = $defaultDecorator; /// add to end
                    }
                }
                $this->_defaultElementDecorators = $decorators;
            }
        } catch (Exception $e) {
            Ovoyo_ErrorHandler::addException($e);
        }
    }

    /**
     * Retrieve plugin loader for given type
     *
     * $type may be one of:
     * - decorator
     * - element
     *
     * If a plugin loader does not exist for the given type, defaults are
     * created.
     *
     * @param   string $type
     * @return  Zend_Loader_PluginLoader_Interface
     */
    public function getPluginLoader($type = null)
    {
        $addOvoyoPrefixPath = false;
        
        // if first time fetching element plugin loader, paths to Ovoyo also
        if ($type == self::ELEMENT && !isset($this->_loaders[$type])) {
            $addOvoyoPrefixPath = true;
        }
        
        $pluginLoader = parent::getPluginLoader($type);
        
        if ($addOvoyoPrefixPath) {
            $pluginLoader->addPrefixPath('Ovoyo_Form_Element', 
                                         'Ovoyo/Form/Element');
        }
        
        return $pluginLoader;
    }

}
