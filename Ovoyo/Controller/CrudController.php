<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Controller
 */
require_once 'Ovoyo/Controller.php';

/**
 * @see Ovoyo_Model
 */
require_once 'Ovoyo/Model.php';

/**
 * Simple abstract controller class to provide CRUD activities
 *
 * @package    Ovoyo_Controller
 */
abstract class Ovoyo_Controller_CrudController extends Ovoyo_Controller
{
    public $baseUrl;

    public $modelClass;
    public $modelOptions = array();
    public $checkExisting = array();

    public $defaultSearchOptions = array();
    public $defaultSearchSort;
    public $defaultSearchDir;
    
    public $searchCols = array();
    public $searchMethod = 'search';
    
    public $errorsOnFetch = true;
    
    public $insertMethod = 'insert';
    public $updateMethod = 'update';
    public $deleteMethod = 'delete';

    public $logicalDeleteField;
    public $logicalDeleteDateField;
    
    public $addButtons = false;

    public $formConfig   = array();
    public $formOptions  = array();

    public $createSuccessUrl;
    public $updateSuccessUrl;
    public $deleteSuccessUrl;
    
    public $createSuccessMessage = 'New item was successfully added';
    public $updateSuccessMessage = 'Item successfully updated';
    public $deleteSuccessMessage = 'Item was successfully deleted';
    
    public $createButtonLabel = 'Create';
    public $updateButtonLabel = 'Update';
    public $deleteButtonLabel = 'Delete';
    
    public $buttonSetLabel = '';
    
    public $returnJson;
    public $jsonData = array('success' => false);
    public $jsonReturnFields = array();
    public $jsonRenderContent = true;

    /**
     * Constructor
     *
     * @param   $request
     * @param   $response
     * @param   $invokeArgs
     */
    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        $this->baseUrl = Ovoyo_WebContainer::getInstance()->getBaseUrl();
        
        parent::__construct($request, $response, $invokeArgs);
        
        $this->createSuccessUrl = ($this->createSuccessUrl) ? $this->createSuccessUrl : $this->baseUrl;
        $this->updateSuccessUrl = ($this->updateSuccessUrl) ? $this->updateSuccessUrl : $this->baseUrl;
        $this->deleteSuccessUrl = ($this->deleteSuccessUrl) ? $this->deleteSuccessUrl : $this->baseUrl;

        // set some default form options if not set
        if (!array_key_exists('method', $this->formConfig)) {
            $this->formConfig['method'] = 'post';
        }
        if (!array_key_exists('action', $this->formConfig)) {
            $this->formConfig['action'] = $this->getRequest()->getPathInfo();
        }

        // some default form options
        $this->formOptions['useTableTags'] = true;
        
        $formAction = $this->getRequest()->getParam('_formAction');
        if ($formAction) {
            $this->getRequest()->setParam($formAction, 1);
        }
        
        if (!is_bool($this->returnJson)) {
            $this->returnJson = ($this->getRequest()->isXmlHttpRequest() && !$this->_hasParam('_noJson')) ? true : false;
        }
    }

    /**
     * Default action is to list/search (read) items
     *
     * @return null
     */
    public function indexAction()
    {
        if (empty($this->modelClass)) { return; }
        
        // set default search options
        $searchOptions = $_REQUEST['searchOptions'];
        if (is_array($this->defaultSearchOptions)) {
            foreach ($this->defaultSearchOptions AS $option => $value) {
                if (isset($searchOptions[$option])) { continue; }
                $searchOptions[$option] = $value;
            }
        }

        $sort = ($_REQUEST['sort']) ? $_REQUEST['sort'] : $this->defaultSearchSort;
        $dir  = ($_REQUEST['dir']) ? $_REQUEST['dir'] : $this->defaultSearchDir;
         
        $order = $sort ? $sort : '';
        $order.= ($sort && $dir) ? ' ' . $dir : '';
        
        // create model instance
        try {
                $model = $this->getModel();

                // do search and pass info to view
                $stmt = $model->search($searchOptions, $order, $this->searchCols);

                $this->view->assign('data', $stmt->fetchAll(Zend_Db::FETCH_OBJ));
        } catch (Exception $e) {
                Ovoyo_ErrorHandler::addException($e);
        }
    }

    /**
     * Add new item
     *
     * @return void
     */
    public function createAction()
    {
        if (empty($this->modelClass)) { return; }
        
        $this->_preCrudAction();
        $this->_preCreateAction();
        
        try {
                // create model instance
                $model = $this->getModel();
                
                // add all primary keys to ignored fields as they should not appear in the form
                $this->formOptions['ignorePrimaryKeys'] = true;
        
                // create form from model and add appropriate elements
                $form = $model->toForm($this->formConfig, $this->formOptions);
                $form->setAttrib('id', $this->_formId('create'));
                $form->setAttrib('name', $this->_formId('create'));
        
                $form->addElement('hidden', '_formSubmit', array('value' => 1));
                $form->addElement('hidden', '_formAction', array('value' => 'create'));
        
                if ($this->addButtons) {
                   $form->addElement('submit', 'create', array('label' => $this->createButtonLabel));
                   $form->addButtonSet(array('create'), 'createButtonSet', array('legend' => $this->buttonSetLabel));
                }
                
                $this->_customiseForm($form);
        
                // update instance with details of form POST
                if ($this->_hasParam('_formSubmit')) {
                    $model->updateVariables($this->_getAllParams());
                    $form->populate($this->_getAllParams());
                }
                
            $this->view->assign('model', $model);
            $this->view->assign('form', $form);
        } catch (Exception $e) {
                Ovoyo_ErrorHandler::addException($e);
            if ($this->returnJson) {
                $this->_returnJson($this->jsonData, $this->jsonRenderContent);
            } else {
                   return;
            }
        }

        // try and add new category
        if ($this->_getParam('create') && $form->isValid($this->_getAllParams())) {
            $doInsert = true;
                       
            $this->_preModelCrud($model);
            $this->_preModelInsert($model);
            
            // check model doesn't already exist
            if (is_array($this->checkExisting)) {
                if ($model->alreadyExists($this->checkExisting['cols'], $this->checkExisting['where'])) {
                    $doInsert = false;

                    if ($this->checkExisting['message']) {
                        Ovoyo_ErrorHandler::addError($this->checkExisting['message']);
                    } else {
                        Ovoyo_ErrorHandler::addError('A similar item already exists');
                    }
                }
            }

            // add new item
            if ($doInsert && $model->{$this->insertMethod}()) {
                if ($this->createSuccessMessage) {
                    Ovoyo_SystemMessage::addMessage($this->createSuccessMessage);
                }
            
                $this->_postModelInsert($model);
                $this->_postModelCrud($model);
            
                $this->jsonData['success'] = true;
                if (is_array($this->jsonReturnFields)) {
                    foreach ($this->jsonReturnFields AS $field) {
                        if (!isset($model->{$field})) { continue; }
                        $this->jsonData[$field] = $model->{$field};
                    }
                }

                if (!$this->returnJson && $this->createSuccessUrl) {
                    $url = $this->_processUrl($this->createSuccessUrl, $model);
                    header("Location: $url");
                    exit;
                }
            }
            
            // add some useful info to jsonData
            $primaryKeys = $model->getPrimaryKeys();
            foreach ($primaryKeys AS $primaryKey) {
                $this->jsonData[$primaryKey] = $model->{$primaryKey};
            }
        }
        
        $this->_postCreateAction();
        
        if ($this->returnJson) {
            $this->_returnJson($this->jsonData, $this->jsonRenderContent);
        }
    }
    
    /**
     * Create XHR function alias
     *
     * Calls $this->update()
     *
     * @return  void
     */
    public function xhrCreateAction()
    {
        $this->_setLayout('output');
        $this->createAction();
    }

    /**
     * Read action
     *
     * Alias for indexAction()
     *
     * @return void
     */
    public function readAction()
    {
        $this->indexAction();
    }
    
    /**
     * Read XHR function alias
     *
     * Calls $this->update()
     *
     * @return  void
     */
    public function xhrReadAction()
    {
        $this->_setLayout('output');
        $this->readAction();
    }

    /**
     * Update action
     *
     * @return void
     */
    public function updateAction()
    {
        // do a delete
        if ($this->_getParam('delete')) {
            $this->deleteAction();
            if ($this->returnJson) { return; }
        } else {
            $this->_preCrudAction();
        }

        $this->_preUpdateAction();
        
        try {
            if (!$model = $this->getModel()) {
                throw new Exception('Could not create model');
            }
            
            // add some useful info to jsonData
            $primaryKeys = $model->getPrimaryKeys();
            foreach ($primaryKeys AS $primaryKey) {
                $this->jsonData[$primaryKey] = $model->{$primaryKey};
            }
            
            // create form from model and add appropriate elements
            $form = $model->toForm($this->formConfig, $this->formOptions);
            $form->setAttrib('id', $this->_formId('update'));
            $form->setAttrib('name', $this->_formId('update'));
            $form->addElement('hidden', '_formAction');
    
            if ($this->addButtons) {
               $form->addElement('submit', 'update', array('label' => $this->updateButtonLabel));
               $form->addElement('submit', 'delete', array('label' => $this->deleteButtonLabel,
                                                           'class' => 'secondary',
                                                           'onClick' => 'return doConfirm(\'Are you sure you want to delete this item?\');'));
               $form->addButtonSet(array('update', 'delete'), 'updateButtonSet', array('legend' => $this->buttonSetLabel));
            }
            
            $this->view->assign('model', $model);
            
            $this->_customiseForm($form);
            
            $form->populate($model->toArray());
            
            $this->view->assign('form', $form);
        } catch (Exception $e) {
            Ovoyo_ErrorHandler::addException($e);
            if ($this->returnJson) {
                $this->_returnJson($this->jsonData, $this->jsonRenderContent);
            }
            return;
        }

        // update form
        if ($this->_getParam('update')) {
            $model->updateVariables($this->_getAllParams());
            $form->populate($this->_getAllParams());
        }

        // do an update
        if ($this->_getParam('update') && $form->isValid($this->_getAllParams())) {
            $doUpdate = true;

            $this->_preModelCrud($model);
            $this->_preModelUpdate($model);
            
            // check model doesn't already exist
            if (is_array($this->checkExisting)) {
                if ($model->alreadyExists($this->checkExisting['cols'], $this->checkExisting['where'])) {
                    $doUpdate = false;

                    if ($this->checkExisting['message']) {
                        Ovoyo_ErrorHandler::addError($this->checkExisting['message']);
                    } else {
                        Ovoyo_ErrorHandler::addError('A similar item already exists');
                    }
                }
            }
            
            if ($doUpdate && $model->{$this->updateMethod}()) {
                if ($this->updateSuccessMessage) {
                    Ovoyo_SystemMessage::addMessage($this->updateSuccessMessage);
                }
                
                $this->_postModelUpdate($model);
                $this->_postModelCrud($model);
                
                $this->jsonData['success'] = true;
                if (is_array($this->jsonReturnFields)) {
                    foreach ($this->jsonReturnFields AS $field) {
                        if (!isset($model->{$field})) { continue; }
                        $this->jsonData[$field] = $model->{$field};
                    }
                }
                
                if (!$this->returnJson && $this->updateSuccessUrl) {
                    $url = $this->_processUrl($this->updateSuccessUrl, $model);
                    header("Location: $url");
                    exit;
                }
            }
        }
        
        $this->_postUpdateAction();
        
        if ($this->returnJson) {
            $this->_returnJson($this->jsonData, $this->jsonRenderContent);
        }
    }
    
    /**
     * Update XHR function alias
     *
     * Calls $this->update()
     *
     * @return  void
     */
    public function xhrUpdateAction()
    {
        $this->_setLayout('output');
        $this->updateAction();
    }

    /**
     * Edit action
     *
     * Alias for updateAction()
     *
     * @return void
     */
    public function editAction()
    {
        $this->updateAction();
    
    }
    
    /**
     * Edit XHR function alias
     *
     * Calls $this->edit()
     *
     * @return  void
     */
    public function xhrEditAction()
    {
        $this->_setLayout('output');
        $this->editAction();
    }

    /**
     * View action
     *
     * Alias for updateAction()
     *
     * @return void
     */
    public function viewAction()
    {
        $this->updateAction();
    }

    /**
     * Delete action
     *
     * Alias for editAction()
     *
     * @return void
     */
    public function deleteAction()
    {
        $this->_preCrudAction();
        $this->_preDeleteAction();
        
        // create model instance
        $model = Ovoyo_Model::factory($this->modelClass, $this->modelOptions);
        
        // fetch model
        if (!$model = $this->getModel()) {
            throw new Exception('Could not create model');
        }
        
        // add some useful info to jsonData
        $primaryKeys = $model->getPrimaryKeys();
        foreach ($primaryKeys AS $primaryKey) {
            $this->jsonData[$primaryKey] = $model->{$primaryKey};
        }
        
        $this->_preModelCrud($model);
        $this->_preModelDelete($model);

        // this really shouldn't be used any more as the same cab be achieved
        // using _preModelDelete() or _preDelete() in the model class
        // itself
        if (isset($this->logicalDeleteField)) {
            $model->{$this->logicalDeleteField} = 1;
        }
        if (isset($this->logicalDeleteDateField)) {
            $model->{$this->logicalDeleteDateField} = new Zend_Date();
        }
        
        if ($model->{$this->deleteMethod}()) {
            if ($this->deleteSuccessMessage) {
                Ovoyo_SystemMessage::addMessage($this->deleteSuccessMessage);
            }
            
            $this->_postModelDelete($model);
            $this->_postModelCrud($model);
        
            $this->jsonData['success'] = true;
            
            if (!$this->returnJson && $this->deleteSuccessUrl) {
                $url = $this->_processUrl($this->deleteSuccessUrl, $model);
                header("Location: $url");
                exit;
            }
        }

        $this->_postDeleteAction();
        
        if ($this->returnJson) {
            $this->_returnJson($this->jsonData, $this->jsonRenderContent);
        }
    }
    
    /**
     * Delete XHR function alias
     *
     * Calls $this->delete()
     *
     * @return  void
     */
    public function xhrDeleteAction()
    {
        $this->_setLayout('output');
        $this->setView('messages', 'default');
        $this->deleteAction();
    }

    /**
     * Json search
     *
     * Alias for indexAction()
     *
     * @return void
     */
    public function jsonSearchAction()
    {
        $this->_setLayout('output');
        $view = $this->view;
        
        if (empty($this->modelClass)) {
            $view->setViewContent('No model defined');
        }
        
        // set default search options
        $searchOptions = $this->_getParam('searchOptions');
        if (is_array($this->defaultSearchOptions)) {
            foreach ($this->defaultSearchOptions AS $option => $value) {
                if (isset($searchOptions[$option])) { continue; }
                $searchOptions[$option] = $value;
            }
        }
        
        // work out page number
        $pageNum = 1;
        
        // ignore limiting params if _noLimit param passed
        if (!$this->_getParam('_noLimit')) {
            $limit = $this->_getParam('limit');
            if ($this->_getParam('pageNum')) {
                $pageNum = $this->_getParam('pageNum');
            } else if ($this->_getParam('start') && $limit) {
                $pageNum = floor($this->_getParam('start') / $limit) + 1;
            }
        }

        // json sorters
        if (preg_match('/\{/', $this->_getParam('sort'))) {
            $sorters = Zend_Json::decode($this->_getParam('sort'));
            $order = array();
            foreach ($sorters as $sorter) {
                $order[] = $sorter['property'] . ' ' . $sorter['direction'];
            }
            
        } else { // plain string
            $order = ($this->_getParam('sort')) ? $this->_getParam('sort')
                                                : $this->defaultSearchSort;
            if ($this->_getParam('sort') && $this->_getParam('dir')) {
                $order.= ' ' . $this->_getParam('dir');
            } else if ($this->defaultSearchDir) {
                $order.= ' ' . $this->defaultSearchDir;
            }
            
            $order = ($order == ' ') ? '' : $order;
        }
        
        try {
            // create model instance
            $model = $this->getModel();

            $cols = $this->searchCols;
            $this->_preJsonSearch($searchOptions, $order, $cols);
            
            $stmt = $model->{$this->searchMethod}($searchOptions, $order, $this->searchCols);
            $data = $stmt->fetchAll(Zend_Db::FETCH_OBJ);
            
            $this->_postJsonSearch($data);
    
            $view->setViewContent($view->jsonResultSet($data, 'items', $pageNum, $limit));
        } catch (Exception $e) {
            $view->setViewContent($e->getMessage());
        }
    }

    
    /**
     * Fetch model related to controller
     *
     * @return  Ovoyo_Model|false
     */
    public function getModel()
    {
        $modelOptions = $this->modelOptions;
        
        // this really shouldn't be used any more - the deleteMethod should
        // be set via modelOptions array or in the model init() method itself
        if (isset($this->logicalDeleteField)) {
            $modelOptions['deleteMethod'] = Ovoyo_Model::MODEL_DELETE_METHOD_LOGICAL;
        }
        
            $model = Ovoyo_Model::factory($this->modelClass, $modelOptions);
            
            // fetch model if we have values for all primary keys
            $primaryKeys = $model->getPrimaryKeys();
            $attemptFetch = true;
            $fetchBy = array();
            foreach ($primaryKeys AS $primaryKey) {
                if (!$this->_getParam($primaryKey)) {
                    $attemptFetch = false;
                    break;
                }
                $fetchBy[$primaryKey] = $this->_getParam($primaryKey);
            }
        
        if ($attemptFetch && !$model->fetchBy($fetchBy) && $this->errorsOnFetch) {
            Ovoyo_ErrorHandler::addError("Item not found");
            return $model;
        }
        
        return $model;
    }
    
    /**
     * Process a url spec
     *
     * @param   string $urlSpec
     * @param   obj $model
     * @return  string
     */
    private function _processUrl($urlSpec, $model)
    {
        if (empty($this->modelClass)) { return $urlSpec; }
        
        // if nothing to replace, just return url
        if (!preg_match('/:/', $urlSpec)) { return $urlSpec; }
        
        $matches      = array();
        $patterns     = array();
        $replacements = array();
        
        $tableInfo = $model->tableInfo();
        $modelVars = $tableInfo['cols'];
        rsort($modelVars);
                
        foreach ($modelVars AS $var) {
            if (is_array($model->{$var}) || is_object($model->{$var})) { continue; }
            
            $patterns[]     = "/:" . $var ."/";
            $replacements[] = $model->{$var};
        }
        
        $url = (count($patterns) > 0) ? preg_replace($patterns, $replacements, $urlSpec) : $urlSpec;
        
        return $url;
    }
    
    /**
     * Get id to assign to form
     *
     * Id is generated from the model class name
     *
     * @param   string $prefix Optional prefix
     * @return  string
     */
    protected function _formId($prefix = '')
    {
        if (empty($this->modelClass)) { return 'Form'; }
        
        $formId = strrev(preg_replace("/_.*/", '', strrev($this->modelClass)));
        return $prefix . ucfirst($formId) . 'Form';
    }
    
    /**
     * Optional method that can be overriden to provide customisation to form object
     *
     * @param   $form Ovoyo_Form
     * @return  void
     */
    protected function _customiseForm($form) {}

    /**
     * Actions to be peform before createAction
     *
     * Utility class which should be overidden by sub classes
     *
     * @return  void
     */
    protected function _preCreateAction()
    {}
    
    /**
     * Actions to be peform after createAction
     *
     * Utility class which should be overidden by sub classes
     *
     * @return  void
     */
    protected function _postCreateAction()
    {}
    
    /**
     * Actions to be peform before updateAction
     *
     * Utility class which should be overidden by sub classes
     *
     * @return  void
     */
    protected function _preUpdateAction()
    {}
    
    /**
     * Actions to be peform after updateAction
     *
     * Utility class which should be overidden by sub classes
     *
     * @return  void
     */
    protected function _postUpdateAction()
    {}
    
    /**
     * Actions to be peform before deleteAction
     *
     * Utility class which should be overidden by sub classes
     *
     * @return  void
     */
    protected function _preDeleteAction()
    {}
    
    /**
     * Actions to be peform after deleteAction
     *
     * Utility class which should be overidden by sub classes
     *
     * @return  void
     */
    protected function _postDeleteAction()
    {}
    
    /**
     * Actions to be peform before createAction, updateAction and deleteAction
     *
     * Utility class which should be overidden by sub classes
     *
     * @return  void
     */
    protected function _preCrudAction()
    {}
    
    /**
     * Actions to perform before $model->{insertMethod}() is called
     *
     * Utility class which should be overidden by sub classes
     *
     * @param   Ovoyo_Model $model
     * @return  void
     */
    protected function _preModelInsert($model)
    {}
    
    /**
     * Actions to perform after $model->{insertMethod}() is called
     *
     * Utility class which should be overidden by sub classes
     *
     * @param   Ovoyo_Model $model
     * @return  void
     */
    protected function _postModelInsert($model)
    {}
    
    /**
     * Actions to perform before $model->{updateMethod}() is called
     *
     * Utility class which should be overidden by sub classes
     *
     * @param   Ovoyo_Model $model
     * @return  void
     */
    protected function _preModelUpdate($model)
    {}
    
    /**
     * Actions to perform after $model->{updateMethod}() is called
     *
     * Utility class which should be overidden by sub classes
     *
     * @param   Ovoyo_Model $model
     * @return  void
     */
    protected function _postModelUpdate($model)
    {}
    
    /**
     * Actions to perform before $model->{deleteMethod}() is called
     *
     * Utility class which should be overidden by sub classes
     *
     * @param   Ovoyo_Model $model
     * @return  void
     */
    protected function _preModelDelete($model)
    {}
    
    /**
     * Actions to perform after $model->{deleteMethod}() is called
     *
     * Utility class which should be overidden by sub classes
     *
     * @param   Ovoyo_Model $model
     * @return  void
     */
    protected function _postModelDelete($model)
    {}
    
    /**
     * Actions to perform before $model->{insertMethod}(), $model->{updateMethod}() or $model->{deleteMethod}() is called
     *
     * Utility class which should be overidden by sub classes
     *
     * @return  void
     */
    protected function _preModelCrud($model)
    {}
    
    /**
     * Actions to perform after $model->{insertMethod}(), $model->{updateMethod}() or $model->{deleteMethod}() is called
     *
     * Utility class which should be overidden by sub classes
     *
     * @return  void
     */
    protected function _postModelCrud($model)
    {}

    /**
     * Actions to perform prior to search method being called from within
     * the jsonSearchAction()
     *
     * @param array $searchOptions
     * @param string $order
     * @param array $cols
     * @return void
     */
    protected function _preJsonSearch(&$searchOptions, &$order, &$cols)
    {}
    

    /**
     * Actions to perform after search method has been called from within
     * the jsonSearchAction()
     *
     * @param array $data
     * @return void
     */
    protected function _postJsonSearch(&$data)
    {}
}
