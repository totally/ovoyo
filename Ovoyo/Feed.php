<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Feed
 *
 * A class for performing basic feed manipulation
 *
 * @package    Ovoyo_Feed
 */
class Ovoyo_Feed
{
    /**
     * Factory for Ovoyo_Feed classes
     *
     * @param   string $type Type of feed class to create
     * @return  obj An object which is a subclass of Ovoyo_Feed_Abstract
     * @throws  Exception
     */
    public static final function factory($type = 'Rss')
    {     
        $className = 'Ovoyo_Feed_' . $type;
        
        // if class already exists, simply return new instance
        if (class_exists($className)) {
            $model = new $className();
            return $model;
        }

        // if class physically exists but hasn't been loaded, then load it
        require_once 'Ovoyo/ClassLoader.php';
        if (Ovoyo_ClassLoader::exists($className)) {
            $model = new $className();
            return $model;
        }
        
        // class does not exist, so throw error
        require_once 'Ovoyo/Feed/Exception.php';
        throw new Ovoyo_Feed_Exception("Unable to create feed class of type '$type'");
    }

}
