<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */


/**
 * @see Zenk_View_Helper_FormElement
 */
require_once 'Zend/View/Helper/FormElement.php';

/**
 * Display a nicely layed out set of checboxes
 *
 * @package    Ovoyo_View
 */
class Ovoyo_View_Helper_FormTimeSelect extends Zend_View_Helper_FormElement
{
    /**
     * Constructor
     *
     * Possible options are:
     *
     * - 'name' string name to use for file input
     * - 'options' array holding additional shared attributes for image and file input:
     *
     * @param   string $name Name of file input
     * @param   array $attribs
     * @param   array $options Optional array holding options for 'assigned' and 'unassigned' selects
     * @return  void
     */
    public function formTimeSelect($name, $value = null, $attribs = array(), $options = array())
        {
            $info = $this->_getInfo($name, $value, $attribs, $options);
        extract($info); // name, value, attribs, options, listsep, disable
        
            $view = new Ovoyo_View;
            
            if ($value) {
               list($hourValue, $minuteValue) = explode(':', $value);
            }
            
            $hourInterval = ($attribs['hourInterval']) ? $attribs['hourInterval'] : 1;
            $minuteInterval = ($attribs['minuteInterval']) ? $attribs['minuteInterval'] : 1;
            
        if ($attribs['use12HourFormat']) {
            $hours = range(0, 12, $hourInterval);
        } else {
            $hours = range(0, 23, $hourInterval);
        }
        
        $minutes = range(0, 59, $minuteInterval);
        
        if (isset($attribs['hoursFrom']) && isset($attribs['hoursTo'])) {
            $hours = range($attribs['hoursFrom'], $attribs['hoursTo'], $hourInterval);
        }
        
        if (isset($attribs['minutesFrom']) && isset($attribs['minutesTo'])) {
            $minutes = range($attribs['minutesFrom'], $attribs['minutesTo'], $minuteInterval);
        }
        
        $html = "";
        
        $html .= '<select'
                . ' name="' . $this->view->escape($name) . '-hours"'
                . ' id="' . $this->view->escape($id) . '-hours"'
                . $disabled
                . $this->_htmlAttribs(array('class' => $attribs['class']))
                . ' >';
        
        if ($attribs['includeBlank'] || $attribs['includeBlankHour']) {
            $html .= '<option value=""></option>';
        }
        
        foreach ($hours as $hour) {
            $html .= '<option value="' . str_pad($hour, 2, 0, STR_PAD_LEFT) . '"';
            $html .= ($hourValue == $hour) ? ' selected="selected"' : '';
            $html .= '>';
            $html .= str_pad($hour, 2, 0, STR_PAD_LEFT);
            $html .= '</option>';
        }
        
        $html .= '</select> : ';
        
        
        $html .= '<select'
                . ' name="' . $this->view->escape($name) . '-minutes"'
                . ' id="' . $this->view->escape($id) . '-minutes"'
                . $disabled
                . $this->_htmlAttribs(array('class' => $attribs['class']))
                . ' >';
        
        if ($attribs['includeBlank'] || $attribs['includeBlankMinute']) {
            $html .= '<option value=""></option>';
        }
        
        foreach ($minutes as $minute) {
            $html .= '<option value="' . str_pad($minute, 2, 0, STR_PAD_LEFT) . '"';
            $html .= ($minuteValue == $minute) ? ' selected="selected"' : '';
            $html .= '>';
            $html .= str_pad($minute, 2, 0, STR_PAD_LEFT);
            $html .= '</option>';
        }
        
        $html .= '</select>';
        
        return $html;
    }
}