<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Services_PaymentProvider_Abstract
 */
require_once 'Ovoyo/Services/PaymentProvider/Abstract.php';

/**
 * CharityClear class
 *
 * @package    Ovoyo_Services_PaymentProvider
 */
class Ovoyo_Services_PaymentProvider_CharityClear extends Ovoyo_Services_PaymentProvider_Abstract
{
    // Mandatory request fields
    const CC_FIELD_MERCHANT_ID              = 'VPMerchantID';
    const CC_FIELD_MERCHANT_PASSWORD        = 'VPMerchantPassword';
    const CC_FIELD_AMOUNT                   = 'VPAmount';
    const CC_FIELD_COUNTRY_CODE             = 'VPCountryCode';
    const CC_FIELD_CURRENCY_CODE            = 'VPCurrencyCode';
    const CC_FIELD_TRANSACTION_UNQUE        = 'VPTransactionUnique';
    const CC_FIELD_ORDER_DESC               = 'VPOrderDesc';
    const CC_FIELD_CALLBACK_URL             = 'VPCallBack';

    // Optional request fields
    const CC_FIELD_VERIFY_URL               = 'VPVerifyURL';

    // Address fields
    const CC_FIELD_BILLING_ADDRESS_NUMBER   = 'VPBillingHouseNumber';
    const CC_FIELD_BILLING_ADDRESS_STREET   = 'VPBillingStreet';
    const CC_FIELD_BILLING_ADDRESS_CITY     = 'VPBillingCity';
    const CC_FIELD_BILLING_ADDRESS_STATE    = 'VPBillingState';
    const CC_FIELD_BILLING_ADDRESS_POSTCODE = 'VPBillingPostCode';
    const CC_FIELD_BILLING_EMAIL            = 'VPBillingEmail';
    const CC_FIELD_BILLING_TELEPHONE        = 'VPBillingPhoneNumber';

    const CC_PAYMENT_URL                    = 'https://gateway.cardstream.com/charityclear.asp';

    // Response fields
    const CC_RESPONSE_FIELD_CODE              = 'VPResponseCode';
    const CC_RESPONSE_FIELD_MESSAGE           = 'VPMessage';
    const CC_RESPONSE_FIELD_AMOUNT            = 'VPAmountReceived';
    const CC_RESPONSE_FIELD_TRANSACTION_UNQUE = 'VPTransactionUnique';
    const CC_RESPONSE_FIELD_CROSS_REFERENCE   = 'VPCrossReference';

    // Payment statuses
    const CC_TRANSACTION_SUCCESS            = '00';
    const CC_CARD_REFERRED                  = '02';
    const CC_CARD_DECLINED                  = '05';
    const CC_CARD_ERROR                     = '30';

    protected $_merchantId;
    protected $_merchantPassword;

    /**
     * Array containing the fields required before being sent to Epdq
     * @var array
     */
    public $requiredFields = array(
        self::CC_FIELD_MERCHANT_ID         => "Merchant ID is missing",
        self::CC_FIELD_MERCHANT_PASSWORD   => "Merchant password is missing",
        self::CC_FIELD_AMOUNT              => "Amount is missing",
        self::CC_FIELD_COUNTRY_CODE        => "Country code is missing",
        self::CC_FIELD_CURRENCY_CODE  => "Currency code is missing",
        self::CC_FIELD_TRANSACTION_UNQUE   => "Country code is missing",
        self::CC_FIELD_ORDER_DESC          => "Order description is missing",
        self::CC_FIELD_CALLBACK_URL        => "Callback url missing",
    );

    /**
     * Array containing the optional fields to send to Epdq
     * @var array
     */
    public $optionalFields = array(
         self::CC_FIELD_VERIFY_URL,
         self::CC_FIELD_BILLING_ADDRESS_NUMBER,
         self::CC_FIELD_BILLING_ADDRESS_STREET,
         self::CC_FIELD_BILLING_ADDRESS_CITY,
         self::CC_FIELD_BILLING_ADDRESS_STATE,
         self::CC_FIELD_BILLING_ADDRESS_POSTCODE,
         self::CC_FIELD_BILLING_EMAIL,
         self::CC_FIELD_BILLING_TELEPHONE,
    );

    /**
     * The value of the fields to be sent to chairty clear
     * @var array
     */
    public $requestFields = array();

    /**
     * Array containing the value of the response fields
     * @var array
     */
    public $responseFields = array();

    /**
     * Errors
     * @var array
     */
    public $errors = array();

    /**
     * @var int
     */
    public $paymentId;

    /**
     * @var array
     */
    public $paymentDetails;

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::_init()
     */
    protected function _init()
    {
        if ($this->_useTestPayment) {
            $this->_merchantId = $this->_testMerchantId;
            $this->_merchantPassword = $this->_testMerchantPassword;
        } else {
            $this->_merchantId = $this->_liveMerchantId;
            $this->_merchantPassword = $this->_liveMerchantPassword;
        }
    }

    /**
     * Populate the fields to be sent to Epdq
     * @param array $vars
     */
    protected function _setRequestFields(array $vars)
    {
        foreach ($this->requiredFields as $field => $errorMsg) {
            if (!isset($vars[$field])) {
                $this->errors[] = $errorMsg;
            } else {
                $this->requestFields[$field] = $vars[$field];
                unset($vars[$field]);
            }
        }

        foreach ($this->optionalFields as $field) {
            if (isset($vars[$field]) && strlen($vars[$field]) != 0) {
                $this->requestFields[$field] = $vars[$field];
                unset($vars[$field]);
            }
        }
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_Abstract::sendPayment()
     */
    public function sendPayment(array $vars)
    {
        try {

            $controller = $vars['controller'];
            if($controller) {
                $renderer = $controller->getHelper('viewRenderer');
                $renderer->setNoRender(true);
            }

            // create payment in payment table
            $paymentId = $vars['paymentId'];
            if (!$paymentId) {
                if (!$paymentId = $this->createPayment($vars['amount'], $vars['summary'], $vars)) {
                    return false;
                }
            }

            $vars[self::CC_FIELD_TRANSACTION_UNQUE] = $paymentId;

            $this->_setRequestFields($vars);

            $script = '<script type="text/javascript">'."\n";
            $script .= 'document.getElementById("charityClearPaymentForm").submit();'."\n";
            $script .= '</script>'."\n";

            $form = $this->_createPaymentForm();

            if (!$form) {
                return false;
            }

            if ($controller) {
                $view = $controller->view;
                $controller->getResponse()->setBody($form->render($view) . $script);
            } else {
                echo $form->render() . $script;
                exit;
            }

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Sets up a payment, ready to send to Epdq
     *
     * @access public
     * @return mixed string The HTML form
     */
    protected function _createPaymentForm()
    {
        if ($this->_hasErrors()) {
            foreach($this->errors as $errorMsg){
                Ovoyo_ErrorHandler::addError($errorMsg);
            }
            return false;
        }

        $form = new Ovoyo_Form();

        $form->setAttrib('id', 'charityClearPaymentForm')
             ->setAttrib('name', 'charityClearPaymentForm')
             ->setAttrib('action', self::CC_PAYMENT_URL)
             ->setMethod('post');

        foreach ($this->requestFields as $fieldName => $value) {
            $form->addElement('hidden', $fieldName,
                array(
                    'value' => $value,
                    'required' => true
                )
            );
        }

        return $form;
    }

    /**
     * Has error method
     * @return true if there is no errors
     *         false otherwise
     */
    protected function _hasErrors()
    {
        return !empty($this->errors);
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_Abstract::validateCallback()
     */
    public function validateCallback($data = array())
    {
        $valid = false;

        if (array_key_exists(self::CC_RESPONSE_FIELD_CODE, $data)) {
            $valid = true;
        }

        return $valid;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_Abstract::handleCallback()
     */
    public function handleCallback(array $data)
    {
        // fetch the payment
        $payment = Ovoyo_Services_PaymentProvider::getPayment(
            array('connName' => $this->_connName)
        );
        if (!$payment->fetch($data[self::CC_RESPONSE_FIELD_TRANSACTION_UNQUE])) {
            return false;
        }

        $this->paymentId = $payment->paymentId;

        // update payment details
        $payment->status = $data[self::CC_RESPONSE_FIELD_CODE];
        $payment->reference = $data[self::CC_RESPONSE_FIELD_CROSS_REFERENCE];
        $payment->callbackData = $data;
        $payment->paid = ($data[self::CC_RESPONSE_FIELD_CODE] == self::CC_TRANSACTION_SUCCESS) ? 1 : 0;

        if ($payment->paid) {
            $payment->paidDate = new Zend_Date;
        }

        if (!$payment->update()) {
            return false;
        }

        $this->amount = $payment->amount;
        $this->paymentDetails = $payment->details;
        $this->paymentDetails['paymentId'] = $payment->paymentId;

        return $payment->paid;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_Abstract::completePayment()
     */
    public function completePayment()
    {
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_Abstract::processPaymentInfo()
     */
    public function processPaymentInfo(array $data)
    {
        $paid = $this->handleCallback($data);

        if ($paid) {
            $result = array();
            $result['paymentId'] = $this->paymentId;
        } else {
            $result = false;
        }

        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_Abstract::postConvertToOrder()
     */
    public function postConvertToOrder($config)
    {
        if ($config['paymentCompleteUrl'] && !headers_sent()) {
            $redirectUrl = $config['host'] . $config['paymentCompleteUrl'] . '/';
            if ($this->paymentId) {
                $redirectUrl.= '?paymentId=' . $this->paymentId;
            }

            echo   '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
            <html>
                <head>
                    <title>Redirecting...</title>
                    <meta http-equiv="REFRESH" content="0;url=' . $redirectUrl . '">
                </head>
                <body>
                    <p style="font-family: Arial">
                        You are now being redirected back to ' . $config['host'] . '<br /><br />
                        If the page has not refreshed within 5 seconds, please <a href="' . $redirectLink . '">click here</a>
                    </p>
                </body>
            </html>';
        }

        exit;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_Abstract::prepareData()
     */
    public function prepareData(array $data)
    {
        $amount = sprintf("%.2f", $data['amount']);
        $amount = preg_replace('/\./', '', $amount); // remove decimal place

        $currency = (is_numeric($data['currency'])) ? $data['currency'] : '826';
        $country = (is_numeric($data['country'])) ? $data['country'] : '826';
        $address1 = explode(' ', $data['address1'], 2); // separate number from street

        $preparedData = array(
            self::CC_FIELD_MERCHANT_ID                => $this->_merchantId,
            self::CC_FIELD_MERCHANT_PASSWORD          => $this->_merchantPassword,
            self::CC_FIELD_AMOUNT                     => $amount,
            self::CC_FIELD_COUNTRY_CODE               => $country,
            self::CC_FIELD_CURRENCY_CODE              => $currency,
            self::CC_FIELD_ORDER_DESC                 => $data['description'],
            self::CC_FIELD_CALLBACK_URL               => $this->_callbackUrl,
            self::CC_FIELD_VERIFY_URL                 => $this->_verifyUrl,
            self::CC_FIELD_BILLING_ADDRESS_NUMBER     => $address1[0],
            self::CC_FIELD_BILLING_ADDRESS_STREET     => $address1[1],
            self::CC_FIELD_BILLING_ADDRESS_CITY       => $data['townCity'],
            self::CC_FIELD_BILLING_ADDRESS_STATE      => $data['state'],
            self::CC_FIELD_BILLING_ADDRESS_POSTCODE   => $data['postcode'],
            self::CC_FIELD_BILLING_EMAIL              => $data['email'],
            self::CC_FIELD_BILLING_TELEPHONE          => $data['telephone'],
        );

        return $preparedData;
    }
}