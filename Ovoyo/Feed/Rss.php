<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Feed_Abstract
 */
require_once 'Ovoyo/Feed/Abstract.php';
require_once 'Zend/Feed.php';

/**
 * Rss Feed class
 *
 * @package    Ovoyo_Feed
 */
class Ovoyo_Feed_Rss extends Ovoyo_Feed_Abstract
{

    /**
     * Displays the XML feed and generates correct headers
     * 
     * @param   string $file Feed XML
     * @access public
     * @return  null
     * @throws  Ovoyo_Feed_Exception
     */
    public function displayFeed($data, $metaData)
    {

        $this->formatData($data, $metaData);
        $this->convertDataToFeed($data);

        assert($this->feed instanceof Zend_Feed_Abstract);

        $this->feed->send();

        exit;

    }

    /**
     *
     * Formats database model input into valid Zend_Feed Array
     *
     * @param array $data Database result arrays
     * @param array $metaData Feed metadata
     * @access public
     * @return null
     * @throws Ovoyo_Feed_Exception
     */
    public function formatData($data, $metaData)
    {

        $entries = array();

        foreach ($data as $item) {

            if (is_array($item)) {
                $content = eregi_replace("\n", " <br /> ", $item['content']);
    
                $entries[] = array('title'        => $item['title'],
                                   'link'         => $item['link'],
                                   'description'  => $content,
                                   'guid'         => '',
                                   'content'      => $content,
                                   'lastUpdate'   => $item['dateUpdated']);

            } else {
                $content = eregi_replace("\n", " <br /> ", $item->content);
    
                $entries[] = array('title'        => $item->title,
                                   'link'         => $item->link,
                                   'description'  => $content,
                                   'guid'         => '',
                                   'content'      => $content,
                                   'lastUpdate'   => $item->dateUpdated);
            }
        }

        $this->dataArray = $metaData;
        $this->dataArray['entries'] = $entries;

    }
    
    /**
     *
     * Converts arrya data to a Zend RSS Feed
     *
     * @access public
     * @return void
     */
    public function convertDataToFeed()
    {

        $this->feed = Zend_Feed::importArray($this->dataArray, 'rss');
     
    }

}
