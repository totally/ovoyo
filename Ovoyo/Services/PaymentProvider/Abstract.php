<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * PaymentProvider
 *
 * A class for performing payments to 3rd party prividers
 *
 * @package    Ovoyo_Services_PaymentProvider
 */
abstract class Ovoyo_Services_PaymentProvider_Abstract
{
    protected $_useTestPayment;
    protected $_connName;

    /**
     * Constructor
     *
     * @param array $options
     * @return void
     */
    public function __construct($options = array())
    {
        if (is_array($options) || $options instanceof Zend_Config) {
            foreach ($options AS $option => $value) {
                $this->{"_$option"} = ($value) ? $value : null;
            }
        }
        $this->_init();
    }

    /**
     * Create the payment
     *
     * @param array $details
     * @return boolean|integer
     */
    public function createPayment($amount, $summary, array $details)
    {
        $payment = Ovoyo_Services_PaymentProvider::getPayment(
            array('connName' => $this->_connName)
        );
        if (!$payment->create($amount, $summary, $details)) {
            return false;
        }

        return $payment->paymentId;
    }
    /**
     * Init method
     * @return void
     */
    abstract protected function _init();
    /**
     * send a payment off to a provider
     *
     * @param array $vars The payment info
     * @param object instanceof Ovoyo_Controller
     * @return bool, true on success false on failure
     * @throws Ovoyo_Services_PaymentProvider_Exception
     */
    abstract public function sendPayment(array $vars);

    /**
     * handle a callback from payment provider
     *
     * @param $data array of callback data
     * @return bool, true on success false on failure
     * @throws Ovoyo_Services_PaymentProvider_Exception
     */
    abstract public function handleCallback(array $data);

    /**
     * Complete the payment
     * @return void
     */
    abstract public function completePayment();

    /**
     * Used in Pelorous_Payment class to process the
     * payment callback data from A to Z (i.e this method could call
     * handleCallback() and completePayment()) and to provide a uniform
     * interface
     *
     * @param $data array of callback data
     * @return array containing usefull payment info on success, false on failure
     */
    abstract public function processPaymentInfo(array $data);

    /**
     * Used in Pelorous_payment class and called after a payment has been
     * converted to an order.
     * @param $config array of config data to use
     * @return void
     */
    abstract public function postConvertToOrder($config);

    /**
     * Prepare the data to prepare
     * @param the data to prepare
     * @return array the data ready to be sent
     */
    abstract public function prepareData(array $data);

    /**
     * validateCallback
     *
     * @param array data
     * @return boolean checks to see if callback belongs to this payment provider
     */
    abstract public function validateCallback($data = array());

}