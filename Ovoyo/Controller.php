<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Controller_Helper_ViewRenderer
 */
require_once 'Ovoyo/Controller/Helper/ViewRenderer.php';

/**
 * Default controller class
 *
 * @package    Ovoyo_Controller
 */
class Ovoyo_Controller extends Zend_Controller_Action
{
    /*
    public $webContainer;

    protected $_viewPath;
    protected $_view;
    protected $_viewInstance;
    protected $_viewData = array();

    protected $_actionParameters = array();
*/
    protected $_tableFormDecorators;
    protected $_tableElementDecorators;
    protected $_tableButtonDecorators;

    
    /**
     * See Zend_Controller_Action for details
     *
     * @param   $request
     * @param   $response
     * @param   $invokeArgs
     * @return  void
     */
    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        $viewRenderer = new Ovoyo_Controller_Helper_ViewRenderer();
        $viewRenderer->setViewSuffix('php');
                     
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
                
        parent::__construct($request, $response, $invokeArgs);
        
        // unfortunately this is a bit of hack to make it easier to render form elements in a table format
        $this->_tableFormDecorators    = array('FormElements', array('HtmlTag', array('tag' => 'table')), 'Form');
        $this->_tableElementDecorators = array('ViewHelper','Errors',
                                         array('decorator' => array('td' => 'HtmlTag'), 'options' => array('tag' => 'td', 'class' => 'element')),
                                         array('TdLabel', 'options' => array('tag' => 'td', 'class' => 'label')),
                                         array('decorator' => array('tr' => 'HtmlTag'), 'options' => array('tag' => 'tr')),);

        $this->_tableButtonDecorators  = array(array('decorator' => 'ViewHelper', 'options' => array('helper' => 'formSubmit')),
                                               array('TdSubmit'),);
    }
      
    /**
     * Added default error action
     *
     * @return  void
     */
    public function errorAction()
    {
        $this->setView('error');
        
        $errors = $this->_getParam('error_handler');
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                Ovoyo_ErrorHandler::addException($errors->exception);
                $this->getResponse()->setRawHeader('HTTP/1.1 404 Not Found');
                $this->setView('404');
                
                if (Ovoyo_WebContainer::getConfig('application')->enableFirebugLogger) {
                    $logger = Zend_Registry::get('firebugLogger');
                    $logger->err('Page not found ' . $this->webContainer->getUrlPath());
                }
                break;
            default:
                if (Ovoyo_WebContainer::getConfig('application')->enableFirebugLogger) {
                    $logger = Zend_Registry::get('firebugLogger');
                    $logger->err($errors->exception);
                }
                Ovoyo_ErrorHandler::addException($errors->exception);
                break;
        }
        
    
        // Clear previous content
        $this->getResponse()->clearBody();
    }
    
    /**
     * Set view to use
     *
     * @param   string $view
     * @param   string $moduleName Optional module name
     * @return  void
     */
    public function setView($view, $moduleName = null)
    {
        $this->_helper->viewRenderer($view);
        
        if ($moduleName) {
            $request = $this->getRequest();
            $request->setModuleName($moduleName);
            
            $this->view->setScriptPath(Ovoyo_WebContainer::getInstance()->getModuleDirectory() . '/views');
        }
    }
    
    /**
     * Set template
     *
     * @param string $template
     * @deprecated Use _setLayout() instead
     */
    protected function _setTemplate($template)
    {
        $this->_setLayout($template);
    }
    
    /**
     * Get current template
     *
     * @return  string
     * @deprecated Use _getLayout() instead
     */
    protected function _getTemplate()
    {
        return $this->_getLayout();
    }
    
    /**
     * Set layout
     *
     * @param string $layout
     */
    protected function _setLayout($layout)
    {
        $this->view->setLayout($layout);
    }
    
    /**
     * Get current layout
     *
     * @return  string
     */
    protected function _getLayout()
    {
        return $this->view->getLayout();
    }
    
    /**
     * Make a piece of data accessible to view
     *
     * @param   string $name
     * @param   mixed $value
     * @deprecated  deprecated since version xx - please use $this->view->assign()
     */
    protected function _addViewData($name, $value)
    {
        $this->view->assign($name, $value);
    }
    
    /**
     * Get view data
     *
     * @param   mixed $name
     * @deprecated  deprecated since version xx - please use $this->view->$name
     */
    protected function _getViewData($name)
    {
        return $this->view->{$name};
    }
    
    /**
     * Add content directly to view
     *
     * @param   string $content
     * @return        void
     */
    protected function _addViewContent($content)
    {
        $this->view->appendViewContent($content);
    }
    
    /**
     * Set view content
     *
     * @param   string $content
     * @return        void
     */
    protected function _setViewContent($content)
    {
        $this->view->setViewContent($content);
    }

    /**
     * Get view content
     *
     * @return  string
     */
    protected function _getViewContent()
    {
        return $this->view->getViewContent();
    }
    
    /**
     * Render view and return content for post processing
     *
     * @return  string
     */
    protected function _getRenderedViewContent()
    {
        $this->_helper->viewRenderer->postDispatch();
        return $this->getResponse()->getBody();
    }
    
    /**
     * Short hand method to return Json encoded data
     *
     * @param   array $data Data to return
     * @param   boolean $includeViewContent Option to render view and include content in data array
     * @return  void
     */
    protected function _returnJson($data, $includeViewContent = false)
    {
        require_once 'Zend/Json.php';
        
        if ($includeViewContent) {
            $data['content'] = $this->_getRenderedViewContent();
        }
        
        $response = Zend_Json::encode($data);
        
        // if any $_FILES data was included, then we need to wrap the json
        // within a text area as the original request must have been via
        // an HTTP POST
        if (isset($_FILES) && count($_FILES) > 0) {
            $response = '<textarea>' . $response . '</textarea>';
        } else {
            header("Content-Type: application/json");
        }
        
        $this->getResponse()->setBody($response);
        $this->_noViewRender();
    }
    
    /**
     * Short hand method for setting view not to render
     */
    protected function _noViewRender()
    {
        $this->_helper->viewRenderer->setNoRender(true);
    }

    /**
     * Check to see if there is a valid view script
     *
     * @return  boolean
     */
    protected function _haveValidViewScript()
    {
        $haveViewScript = false;
        $viewScript = $this->getViewScript();
        $scriptPaths = $this->view->getScriptPaths();
        foreach ($scriptPaths AS $path) {
            if (is_readable($path . $viewScript)) {
                $haveViewScript = true;
                break;
            }
        }
        
        return $haveViewScript;
    }
    
    /**
     * Simple magic __get call to provide access to $this->webContainer for backwards compatibility
     *
     * @param   string $variable
     * @return  mixed
     */
    function __get($variable)
    {
        if ($variable == 'webContainer') {
            return Ovoyo_WebContainer::getInstance();
        } else {
            return $this->{$variable};
        }
    }
}
