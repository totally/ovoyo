<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Image_Abstract
 */
require_once 'Ovoyo/Image/Abstract.php';

/**
 * MagickWand Image class
 *
 * @package    Ovoyo_Image
 */
class Ovoyo_Image_Gd extends Ovoyo_Image_Abstract
{
    
    /** 
     * Resize and image, with option to save to new file
     *
     * @param   int $width Desired width
     * @param   int $height Desired height
     * @param   boolean $crop Optional flag to specify if image should be cropped - 1/0
     * @param   string $outfile Destination filename
     * @return  void
     */
    public function resize($width, $height, $crop = true, $outfile = null) 
    {
     
    }
    
    /**
     * Save to file
     * 
     * @param   string $outfile Destination filename
     * @return  void
     * @throws  Ovoyo_Image_Exception
     */
    public function save($outfile) 
    {
    }
}
