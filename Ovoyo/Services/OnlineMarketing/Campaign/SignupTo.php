<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Email marketing campaign class
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage Campaign
 */
class Ovoyo_Services_OnlineMarketing_Campaign_SignupTo
      extends Ovoyo_Services_OnlineMarketing_Campaign_Abstract
{
    public $sendTime;

    /**
     * Constructor
     *
     * @param array $config
     */
    public function __construct($config)
    {
        foreach ($config AS $option => $value) {
            $this->{$option} = $value;
        }

        $this->setApiHandler($config);
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::create()
     */
    public function create($name, $headers, $content)
    {
        $apiHandler = $this->getApiHandler();

        // required params
        $params = array(
            'name' => $name,
            'from_name' => $headers['fromName'],
            'reply_to' => $headers['replyToAddress'],
            'subject' => $headers['subject'],
            'url' => $content['htmlUrl'],
            'personalised' => 1,
            'type' => 'advanced'
        );

        // optional params
        if (isset($headers['fromAddress'])) {
            $params['from_address'] = $headers['fromAddress'];
        }

        if (isset($content['textUrl'])) {
            $params['plain_text'] = $content['textUrl'];
        }

        $response = $apiHandler->callApi('email.php', $params);
        if (!is_numeric($response)) {
            $exception = 'Unable to create new campaign - response from server: '
                       . $response;
            throw new Exception($exception);
        }

        $this->campaignId = $response;

        return $this->campaignId;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::update()
     */
    public function update($name, $headers, $content)
    {
        $apiHandler = $this->getApiHandler();

        // required params
        $params = array(
            'campaign_id' => $this->campaignId,
            'name' => $name,
            'from_name' => $headers['fromName'],
            'reply_to' => $headers['replyToAddress'],
            'subject' => $headers['subject'],
            'url' => $content['htmlUrl'],
        );

        // optional params
        if (isset($headers['fromAddress'])) {
            $params['from_address'] = $headers['fromAddress'];
        }

        if (isset($content['textUrl'])) {
            $params['plain_text'] = $content['textUrl'];
        }

        $response = $apiHandler->callApi('email.php', $params);
        if (!is_numeric($response)) {
            $exception = 'Unable to update campaign - response from server: '
                       . $response;
            throw new Exception($exception);
        }

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::delete()
     */
    public function delete()
    {
        $apiHandler = $this->getApiHandler();

        $params = array('campaign_id' => $this->campaignId);

        $response = $apiHandler->callApi('delete.php', $params);
        if (!is_numeric($response)) {
            // error was caused because campaign does not exist
            if (preg_match('/reference 335002/', $response)) {
               // this is ok - do not throw an exception
            } else {
                $exception = 'Unable to delete campaign - response from '
                           . 'server: ' . $response;
                throw new Exception($exception);
            }
        }

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::send()
     */
    public function send($lists)
    {
        $apiHandler = $this->getApiHandler();
        $sendTime = ($this->sendTime) ? $this->sendTime
                                      : date('Y-m-d h:i');

        $params = array(
            'campaign_id' => $this->campaignId,
            'send_time' => $sendTime,
            'google_analytics' => 1
        );

        if (!is_array($lists) || count($lists) == 0) {
            $exception = 'Invalid list selected';
            throw new Exception($exception);

        } elseif (count($lists) == 1) {
            $params['list_id'] = current($lists);

        } else { // multiple lists, so build a search first
            $searchParams = array(
                'mode' => 'add',
                'name' => 'campaign_' . date('Ymd_his'),
                'list_id' => implode(",", $lists),
            );

            $response = $apiHandler->callApi('search.php', $searchParams);

            if (!is_numeric($response)) {
                $exception = 'Unable to send - response from server: '
                           . $response;
                throw new Exception($exception);
            }

            // add the search id to params
            $params['search_id'] = $response;
        }

        $response = $apiHandler->callApi('send.php', $params);
        if (!is_numeric($response)) {
            $exception = 'Unable to send - response from server: '
                       . $response;
            throw new Exception($exception);
        }

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::sendPreview()
     */
    public function sendPreview($sendTo)
    {
        $apiHandler = $this->getApiHandler();

        $params = array(
            'campaign_id' => $this->campaignId,
            'email' => $sendTo
        );

        $response = $apiHandler->callApi('preview.php', $params);
        if (!is_numeric($response)) {
            $exception = 'Unable to send preview - response from server: '
                       . $response;
            throw new Exception($exception);
        }

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::setApiHandler()
     */
    public function setApiHandler($config = null)
    {
        $this->_apiHandler = new Ovoyo_Services_OnlineMarketing_ApiHandler_SignupTo($config);
    }
}
