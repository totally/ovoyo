<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Zend_View
 */
require_once 'Zend/View.php';

/**
 * View class
 *
 * @package    Ovoyo_View
 */
class Ovoyo_View extends Zend_View
{
    protected $_defaultLayout = 'default';
    protected $_layout;
    protected $_viewContent;

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->addHelperPath('Ovoyo/View/Helper', 'Ovoyo_View_Helper');
        $this->addHelperPath('Ovoyo/View/Helper/ExtJs', 'Ovoyo_View_Helper_ExtJs');
    }

    /**
     * Render page
     *
     * @param   string $script Name of view script to include
     * @return  string
     * @throws  Exception
     */
    public function render($script)
    {
        // apply view if one has been supplied and we don't already have view content
        if ($script && !$this->_viewContent) {
            $this->_viewContent = parent::render($script);
        }
        
        // get appropriate layout
        $layout = $this->getLayout();
        
        if (!$layout) {
            return $this->_viewContent;
        } else if (!is_file($layout)) {
            return 'Could not find layout file ' . $layout;
        }  
           
        // start output buffering
        ob_start();
        
        include $layout;

        // end output buffering
        return ob_get_clean();
    }

    /**
     * Set view content
     *
     * If view content is set manually via this method, no attempt will be made to include
     * any provided view file - only the provided view content will be used
     *
     * @param string $viewContent
     * @return void
     */
    public final function setViewContent($viewContent)
    {
        $this->_viewContent = $viewContent;
    }
    
    /**
     * Get view content
     *
     * If view content has been set manually, it can be retrieved via this method
     *
     * @return  string
     */
    public final function getViewContent()
    {
        return $this->_viewContent;
    }

    /**
     * Append view content
     *
     * Append additional view content
     *
     * @param   string $viewContent
     * @return  void
     */
    public final function appendViewContent($viewContent)
    {
        $this->_viewContent.= $viewContent;
    }

    /**
     * Set template to use
     *
     * @param   string $template Template to use
     * @return  void
     * @deprecated Use _setLayout() instead
     */
    public final function setTemplate($template = null)
    {
        $this->setLayout($template);
    }

    /**
     * Set layout to use
     *
     * @param string $layout Layout to use
     * @return void
     */
    public final function setLayout($layout = null)
    {
        $this->_layout = $layout;
    }

    /**
     * Set default to use
     *
     * @param string $layout Layout to use
     * @return void
     */
    public final function setDefaultLayout($layout = null)
    {
        $this->_defaultLayout = $layout;
    }

    /**
     * Get include path for layout
     *
     * If a layout has been specified and exists, use that.
     * If layout has not been specified, use default
     *
     * @return string Include path to appropriate layout
     */
    public function getLayout()
    {
        $webContainer = Ovoyo_WebContainer::getInstance();
        
        // do we have default layout override
        $defaultLayout = $webContainer->getParam('defaultLayout');
        if (empty($defaultLayout)) {
            $defaultLayout = $this->_defaultLayout;
        }
        
        $layout     = ($this->_layout) ? $this->_layout : $defaultLayout;
        $layoutPath = $webContainer->getParam('layoutsDirectory');

        // if a layout has been specified, check it exists
        if ($layout) {
            // check _layout has php extension
            if (!preg_match("/\.php$/", $layout)) {
                $layout.= '.php';
            }

            // check _layout has leading forward slash
            if (!preg_match("/^\//", $layout)) {
                $layout = '/' . $layout;
            }

            if (!is_file($layoutPath . $layout)) { return ''; }
        }

        return $layoutPath . $layout;
    }
    
    /**
     * Clear all assigned variables
     *
     * Clears all variables assigned to Zend_View either via {@link assign()} or
     * property overloading ({@link __set()}).  This override also clears down the
     * _layout and _viewContent vars
     *
     * @return void
     */
    public function clearVars()
    {
        parent::clearVars();
        $this->_layout    = null;
        $this->_viewContent = null;
    }
    
    /**
     * Given a base path, sets the script, helper, and filter paths relative to it
     *
     * Assumes a directory structure of:
     * <code>
     * basePath/
     *     view_scripts*.php
     *     helpers/
     *     filters/
     * </code>
     *
     * @param  string $path
     * @param  string $prefix Prefix to use for helper and filter paths
     * @return Zend_View_Abstract
     */
    public function setBasePath($path, $classPrefix = 'Zend_View')
    {
        $path        = rtrim($path, '/');
        $path        = rtrim($path, '\\');
        $path       .= DIRECTORY_SEPARATOR;
        $classPrefix = rtrim($classPrefix, '_') . '_';
        $this->setScriptPath($path);
        $this->setHelperPath($path . 'helpers', $classPrefix . 'Helper');
        $this->setFilterPath($path . 'filters', $classPrefix . 'Filter');
        return $this;
    }

    /**
     * Given a base path, add script, helper, and filter paths relative to it
     *
     * Assumes a directory structure of:
     * <code>
     * basePath/
     *     view_scripts*.php
     *     helpers/
     *     filters/
     * </code>
     *
     * @param  string $path
     * @param  string $prefix Prefix to use for helper and filter paths
     * @return Zend_View_Abstract
     */
    public function addBasePath($path, $classPrefix = 'Zend_View')
    {
        $path        = rtrim($path, '/');
        $path        = rtrim($path, '\\');
        $path       .= DIRECTORY_SEPARATOR;
        $classPrefix = rtrim($classPrefix, '_') . '_';
        $this->addScriptPath($path);
        $this->addHelperPath($path . 'helpers', $classPrefix . 'Helper');
        $this->addFilterPath($path . 'filters', $classPrefix . 'Filter');
        return $this;
    }
    
    /**
     * Simple magic __get call to provide access to $this->webContainer for backwards compatibility
     *
     * @param   string $variable
     * @return  mixed
     */
    function __get($variable)
    {
        if ($variable == 'webContainer') {
            return Ovoyo_WebContainer::getInstance();
        } else {
            return $this->{$variable};
        }
    }
}
