<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * PaymentProvider
 *
 * A class for performing basic image manipulation
 *
 * @package    Ovoyo_Services_PaymentProvider
 */
class Ovoyo_Services_PaymentProvider
{
    protected static $_paymentClass = 'Ovoyo_Payment';
    
    /**
     * Factory for Ovoyo_Image classes
     *
     * @param   string $type Type of image class to create
     * @return  obj An object which is a subclass of Ovoyo_Image_Abstract
     * @throws  Exception
     */
    public static final function factory($type = "RBSWorldPay", $options = array())
    {
        $className = 'Ovoyo_Services_PaymentProvider_' . $type;
        
        // if class already exists, simply return new instance
        if (class_exists($className)) {
            $model = new $className($options);
            return $model;
        }

        // if class physically exists but hasn't been loaded, then load it
        require_once 'Ovoyo/ClassLoader.php';
        
        if (Ovoyo_ClassLoader::exists($className)) {
            $model = new $className($options);
            return $model;
        }
        
        // class does not exist, so throw error
        require_once 'Ovoyo/Services/PaymentProvider/Exception.php';
        throw new Ovoyo_Services_PaymentProvider_Exception("Unable to create PaymentProvider class of type '$type'");
    }

    /**
     * Set payment class payment providers should use when processing payments
     *
     * @param string $paymentClass
     * @return void
     * @throws new Exception
     */
    public static function setPaymentClass($paymentClass)
    {
        if (!Ovoyo_ClassLoader::exists($paymentClass)) {
            $exception = 'Payment class "' . $paymentClass . '" not '
                       . 'found';
            throw new Exception($exception);
        }
        
        self::$_paymentClass = $paymentClass;
    }
    
    /**
     * Get payment class payment providers should use when processing payments
     *
     * @return string
     */
    public static function getPaymentClass()
    {
        return self::$_paymentClass;
    }
    
    /**
     * Get instance of payment class payment providers should use when
     * processing payments
     *
     * @param array $config
     * @return Ovoyo_Payment
     */
    public static function getPayment($config)
    {
        return new self::$_paymentClass($config);
    }
}
