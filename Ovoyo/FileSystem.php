<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Class to process file information
 *
 * @package    Ovoyo_File
 */
class Ovoyo_FileSystem
{
    
    /**
     * This function takes a file size in bytes and returns a formatted string 
     * for display output
     *
     * @param   int $bytes Size of the file in bytes
     * @return  string
     */
    public static function convertFileSize($bytes)
    {
        $units = array('bytes', 'kb', 'mb', 'gb');
        
        $formatted = $bytes . ' ' . $units[0];
        
        for ($i = 0; $i < count($units); $i++) {
            if (($bytes / pow(1024, $i)) >= 1) { 
                $formatted = round($bytes / pow(1024, $i), 2) . ' ' . $units[$i];
            }
        }
        
        return $formatted;
    }
    
     /**
     * Checks a directory to see if it exists and is writeable
     *
     * @return  boolean
     */
    public static function checkDirectory($path) 
    {

        if (!is_dir($path)) {
            `mkdir -p $path`;   
            chmod($path, 0777);           
            if (!is_dir($path)) {
                throw new Exception("Upload directory ($path) could not created");
                return false;
            }
        } else {
            chmod($path, 0777);
        }
        
        return true;
        
    }
    
    /**
     * Strips out any windows filename stuff (spaces / weird chars / etc)
     *
     * @return  string
     */
    public static function convertToValidName($fileName) 
    {

        $allowedChars = "/[^a-z0-9\\-\\_\\.]/i";
        
        $cleanFileName = preg_replace($allowedChars, "_", strtolower($fileName));
        
        return $cleanFileName;
        
    }
}
