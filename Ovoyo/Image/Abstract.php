<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Image
 *
 * A class for performing basic image manipulation
 *
 * @package    Ovoyo_Image
 */
abstract class Ovoyo_Image_Abstract
{
    protected $_originalFilename;
    protected $_allowUpscaling = false;
    protected $_defaultBackgroundColour = 'transparent';
    
    
    /**
     * Open image from a file
     *
     * @param   string $inFile Input filename
     * @return  boolean
     * @throws  Ovoyo_Image_Exception
     */
    abstract public function openFromFile($inFile);
     
    /**
     * Save to file
     *
     * @param   string $outFile Destination filename
     * @return  boolean
     * @throws  Ovoyo_Image_Exception
     */
    abstract public function saveToFile($outFile);
    
    /**
     * Save image
     *
     * Image will be saved over original filename
     *
     * @return  boolean
     * @throws  Ovoyo_Image_Exception
     */
    abstract public function save();

    /**
     * Get image width
     *
     * @return int
     */
    abstract public function getWidth();
    
    /**
     * Get image height
     *
     * @return int
     */
    abstract public function getHeight();
    
    /**
     * Resize an image, with option to save to new file
     *
     * @param   int $width Desired width
     * @param   int $height Desired height
     * @param   boolean $crop Optional flag to specify if image should be cropped - 1/0
     * @param   string $outFile If supplied, resized file will be saved to this filename
     * @return  boolean
     * @throws  Ovoyo_Image_Exception
     */
    abstract public function resize($width, $height, $crop = true, $outFile = null);
    
    /**
     * Crop an image, with option to save to new file
     *
     * @param   int $width Width of cropped area
     * @param   int $height Height of cropped area
     * @param   int $offsetX Offset from left where cropping should start
     * @param   int $offsetY Offset from top where cropping should start
     * @param   string $outFile If supplied, resized file will be saved to this filename
     * @return  boolean
     * @throws  Ovoyo_Image_Exception
     */
    abstract public function crop($width, $height, $offsetX = 0, $offsetY = 0, $outFile = null);

    /**
     * Rotate an image
     *
     * @param   int $degrees
     * @param   string $outFile If supplied, resized file will be saved to this filename
     * @return  boolean
     */
    abstract public function rotate($degrees, $outFile = null);

    /**
     * Flip image along its X axis
     *
     * @param   string $outFile If supplied, resized file will be saved to this filename
     * @return  boolean
     */
    abstract public function flipX($outFile = null);
    
    /**
     * Flip image along its Y axis
     *
     * @param   string $outFile If supplied, resized file will be saved to this filename
     * @return  boolean
     */
    abstract public function flipY($outFile = null);
    
    /**
     * Add watermark to a file
     *
     * @param   string|Ovoyo_Image $waterMarkImage Either the full path to watermark image file, or an existing Ovoyo_Imaged instance to use as watermark
     * @param   string $waterMarkPosX One of 'left', 'center' or 'right'
     * @param   string $waterMarkPosY One of 'top', 'middle', 'bottom'
     * @param   float $alpha Alpha value for setting transparency of watermark
     * @return  boolean
     */
    abstract public function addWatermark($image, $waterMarkPosX = 'right', $waterMarkPosY = 'bottom', $alhpa = 0.5);

    /**
     * Set canvas size
     * 
     * @param	string $width
     * @param	string $height
     * @param	int $offsetX
     * @param	int $offsetY
     * @return	void
     */
    abstract public function setCanvasSize($width, $height, $offsetX = 0, $offsetY = 0);
    
    /**
     * Set whether or not an image can be resized to larger than its 
     * original dimensions when using the resize method
     * 
     * @param   boolean $allowUpscaling
     * @return  void
     */
    public function setAllowUpscaling($allowUpscaling)
    {
        $this->_allowUpscaling = $allowUpscaling;
    }

    /**
     * Set default background colour 
     * 
     * @param	string $colour
     * @return	void
     */
    public function setDefaultBackgroundColour($colour)
    {
        $this->_defaultBackgroundColour = $colour;
    }
    
    /**
     * Get default background colour 
     * 
     * @return	string
     */
    public function getDefaultBackgroundColour()
    {
        return $this->_defaultBackgroundColour;
    }
}
