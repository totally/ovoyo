<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Controller
 */
require_once 'Ovoyo/Controller.php';


/**
 * @see Ovoyo_Form
 */
require_once 'Ovoyo/Form.php';


/**
 * Controller to take care of security - logging users in and out
 *
 * @package     Ovoyo_Controller
 * @subpackage  Authentication
 */
class Ovoyo_Controller_Authentication_SimpleController extends Ovoyo_Controller
{
    /**
     * Login action
     */
    public function loginAction()
    {  
        $form = new Ovoyo_Form;
        $form->setAttrib('id', 'login');

        $form->addElement('text', 'email', array('label' => 'Email:'));
        $form->addElement('password', 'password', array('label' => 'Password:'));
        $form->addElement('submit', '_login', array('label' => 'Login'));
        
        // if no view script, then set view content as rendered form
        if (!$this->_haveValidViewScript()) {
            $this->_setViewContent($form->render());
            
        // assign form to view
        } else {
            $this->view->assign('form', $form);
        }
        
    }

    /**
     * Change password
     */
    public function changePasswordAction()
    {
        // deal with request
        if ($this->_getParam('changePassword')) {
            try {
                $user = Ovoyo_WebContainer::getInstance()->getUser();
                
                $success = $user->changePassword(
                    $this->_getParam('oldPassword'), 
                    $this->_getParam('newPassword'), 
                    $this->_getParam('newPasswordConfirmation')
                ); 
                if ($success) {
                    Ovoyo_SystemMessage::addMessage(
                        'Your password was updated successfully'
                    );
                }

            } catch (Exception $e) {
                Ovoyo_ErrorHandler::addException($e);
            }
        }

        $form = new Ovoyo_Form;
        $form->setAttrib('id', 'changePasswordForm');

        // create form instance
        $form->addElement('password', 'oldPassword', array('label' => 'Old Password:'));
        $form->addElement('password', 'newPassword', array('label' => 'New Password:'));
        $form->addElement('password', 'newPasswordConfirmation', array('label' => 'Confirm New Password:'));
        $form->addElement('submit', 'changePassword', array('label' => 'Submit'));

        $form->addButtonSet(array('changePassword'), 'Actions');
    
        // if no view script, then set view content as rendered form
        if (!$this->_haveValidViewScript()) {
            $viewContent = OVoyo_ErrorHandler::getErrorsString()
                         . $form->render();
            
            $this->_setViewContent($viewContent);
            
        // assign form to view
        } else {
            $this->view->assign('form', $form);
        }
    }
}
