<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Services_PaymentProvider_
 */
require_once 'Ovoyo/Services/PaymentProvider/Abstract.php';

/**
 * RBSWorldPay class
 *
 * @package    Ovoyo_Services_PaymentProvider
 */
class Ovoyo_Services_PaymentProvider_CommittedGiving extends Ovoyo_Services_PaymentProvider_Abstract
{
    const CG_URL = 'https://www.committedgiving.uk.net/';
    const CG_PAYMENT_URN = '/public/pay.aspx';

    const CG_STATUS_SUCCESS = 'successful';
    const CG_STATUS_DECLINED = 'declined';
    const CG_STATUS_FAILED = 'failed';

    protected $_paymentUrl;

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_::_init()
     */
    protected function _init()
    {
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_::sendPayment()
     */
    public function sendPayment(array $vars, $controller = null)
    {
        if ($controller) {
            $renderer = $controller->getHelper('viewRenderer');
            $renderer->setNoRender(true);
        }

        $script = '<script type="text/javascript">'."\n"
                . 'document.getElementById("committedGivingPaymentForm").submit();'
                ."\n" . '</script>'."\n";

        $form = $this->_createPaymentForm($vars);

        if ($controller) {
            $view = $controller->view;
            $controller->getResponse()->setBody($form->render($view) . $script);
        } else {
            echo $form->render() . $script;
            exit;
        }
    }

    /**
     * Creates a Zend_Form from supplied variables to submit to Committed
     * Giving
     *
     * @param array $vars
     */
    protected function _createPaymentForm($vars)
    {
        $form = new Zend_Form;
        $form->setAction($this->_paymentUrl);
        $form->setMethod('POST');
        $form->setAttrib('id', 'committedGivingPaymentForm');
        $form->setAttrib('style', 'display: none;');

        foreach ($vars AS $name => $value) {
            $form->addElement('hidden', $name, array('value' => $value));
        }

        $form->addElement('submit', '_submitPayment');

        return $form;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_::handleCallback()
     */
    public function handleCallback(array $data)
    {
        $paymentId = $data['zPaymentID'];

        // fetch the payment
        $payment = Ovoyo_Services_PaymentProvider::getPayment(
                array('connName' => $this->_connName)
        );

        if (!$paymentId || !$payment->fetch($paymentId)) {
            return false;
        }

        $this->paymentId = $payment->paymentId;

        $transStatus = strtolower($data['zTransStatus']);

        // update payment details
        $payment->status = $transStatus;
        $payment->reference = $data['zTransRef'];
        $payment->callbackData = $data;
        $payment->paid = $transStatus == self::CG_STATUS_SUCCESS ? 1 : 0;
        $payment->paidDate = new Zend_Date($data['zTransDate']);

        if (!$payment->update()) {
            return false;
        }

        $this->reference = $payment->reference;
        $this->amount = $payment->amount;
        $this->details = $payment->details;
        $this->paymentId = $payment->paymentId;

        return $payment->paid;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_::completePayment()
     */
    public function completePayment()
    {
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_::processPaymentInfo()
     */
    public function processPaymentInfo(array $data)
    {
        $paid = $this->handleCallback($data);

        if ($paid) {
            $result = array();
            $result['paymentId'] = $this->paymentId;
        } else {
            $result = false;
        }

        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_::postConvertToOrder()
     */
    public function postConvertToOrder($config)
    {
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_::validateCallback()
     */
    public function validateCallback($data = array())
    {
        return array_key_exists('zTransStatus', $data);
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_::prepareData()
     */
    public function prepareData(array $data)
    {
        $this->_paymentUrl = self::CG_URL
                           . $data['config']['xClientURI']
                           . self::CG_PAYMENT_URN;

        $preparedData = array(
            'xPaymentId'   => $data['paymentId'],
            'xFirstName'   => $data['firstName'],
            'xLastName'    => $data['lastName'],
            'xTotal'       => $data['amount'],
            'xAddress1'    => $data['address1'],
            'xAddress2'    => $data['address2'],
            'xAddress3'    => $data['address3'],
            'xTownCity'    => $data['townCity'],
            'xPostcode'    => $data['postcode'],
            'xCountry'     => $data['country'],
            'xCountry'     => $data['country'],
            'xEmail'       => $data['email'],
            'xTelephone'   => $data['telephone'],
            'xDescription' => $data['description'],
            'xClientID'    => $data['config']['xClientID'],
            'xCurrency'    => $this->_currency,
            'xCallbackURL' => $this->_callbackUrl,
            'xTestMode'    => $this->_useTestPayment
        );

        return $preparedData;
    }

}