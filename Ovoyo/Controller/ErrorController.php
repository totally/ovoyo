<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Controller
 */
require_once 'Ovoyo/Controller.php';


/**
 * Standard Error Controller
 *
 * @package    Ovoyo_Controller
 */
class Ovoyo_Controller_ErrorController extends Ovoyo_Controller
{
    /**
     * Pre-dispatch
     *
     * @return  void
     */
    public function preDispatch()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_setTemplate('output');
        }
    }
    
    public function indexAction()
    {
        $this->errorAction();
    }
    
    public function errorAction()
    {
        $this->setView('error');
        
        $errors = $this->_getParam('error_handler');
        
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                Ovoyo_ErrorHandler::addException($errors->exception);
                $this->getResponse()->setRawHeader('HTTP/1.1 404 Not Found');
                $this->setView('404');
                
                if (Ovoyo_WebContainer::getConfig('application')->enableFirebugLogger) {
                    $logger = Zend_Registry::get('firebugLogger');
                    $logger->err('Page not found ' . $this->webContainer->getUrlPath());
                }
                break;
            default:
                if (Ovoyo_WebContainer::getConfig('application')->enableFirebugLogger) {
                    $logger = Zend_Registry::get('firebugLogger');
                    $logger->err($errors->exception);
                }
                Ovoyo_ErrorHandler::addException($errors->exception);
                break;
        }
        
    
        // Clear previous content
        $this->getResponse()->clearBody();
    }
}
