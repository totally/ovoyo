<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Model
 */
require_once 'Ovoyo/Model.php';

/**
 * Application config options
 *
 * @package    Ovoyo_Application
 */
class Ovoyo_Application_Config extends Ovoyo_Model
{
    public $name;
    public $value;
    
    /**
     * Init
     *
     * @return  void
     */
    public function init()
    {
        $this->requiredFields = array('name', 'value');

    }

    /**
     * Method for checking item validation
     *
     * @return  boolean
     */
    public function isValid()
    {
        $valid = parent::isValid();
        
        if ($this->alreadyExists('name')) {
            Ovoyo_ErrorHandler::addError('An option already exists with the same name');
            $valid = false;
        }
        
        if (!empty($this->name)) {
            require_once 'Zend/Validate/Regex.php';
            $validator = new Zend_Validate_Regex("/^([A-Za-z]|[0-9]|.)+$/");
            if (!$validator->isValid($this->name)) {
                Ovoyo_ErrorHandler::addError('Option names can only contain letters, numbers and full stops');
                $valid = false;
            }
        }
        
        return $valid;
    }
    
    /**
     * Get options
     *
     * If $returnFlatArray is set to true, instead of breaking options
     * down into a Zend_Config object, this will just return a single dimension
     * array of key/value pairs
     *
     * @param   boolean $returnFlatArray
     * @return  Zend_Config|array
     */
    public function getOptions($returnFlatArray = false)
    {
        try {
                   
            $result = $this->search(null, 'name ASC');
            $rows   = $result->fetchAll(Zend_Db::FETCH_OBJ);
            
            $options = array();
            foreach ($rows AS $row) {
                if ($returnFlatArray) {
                    $options[$row->name] = $row->value;
                    continue;
                }
                
                // if we are here, create an array to be converted into a Zend_Config object
                $parts = array_reverse(explode('.', $row->name));
                
                $i = 1;
                $option = array();
                foreach ($parts AS $part) {
                    if ($i == 1) {
                        $option = array($part => $row->value);
                        $i++;
                    } else {
                        $option = array($part => $option);
                    }
                }
                $options = array_merge_recursive($options, $option);
            }
            
        } catch (Exception $e) {
            throw $e;
        }
        
        $options = ($returnFlatArray) ? $options : new Zend_Config($options);
        
        return $options;
    }
}
