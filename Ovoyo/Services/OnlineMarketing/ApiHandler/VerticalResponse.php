<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Vertical Response API handler
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage ApiHandler
 */
class Ovoyo_Services_OnlineMarketing_ApiHandler_VerticalResponse
    extends Ovoyo_Services_OnlineMarketing_ApiHandler
{
    protected $_sessionId;
    protected $_username;
    protected $_password;
    
    protected $_client;
    
    protected $_sessionDuration = 5;
    protected $_wsdl = "https://api.verticalresponse.com/wsdl/1.0/VRAPI.wsdl";
    
    /**
     * Constructor
     *
     * @param   array $config Config options
     * @return  void
     */
    public function __construct($config)
    {
        // check for required options
        if (!isset($config['vrUsername'])) {
            throw new Exception('username is a required config option');
        }
        if (!isset($config['vrPassword'])) {
            throw new Exception('username is a required config option');
        }
        
        $this->_username = $config['vrUsername'];
        $this->_password = $config['vrPassword'];
    }
    
    /**
     * Make a call to api
     *
     * @param   string $method The API method to call
     * @param   array $params Optional params to add as querystring to API url
     * @return  string Response from API call
     * @throws  Exception
     */
    public function callApi($method, $params = null)
    {
        $this->_client = new Zend_Soap_Client($this->_wsdl);
        $this->_client->setSoapVersion(SOAP_1_1);
        
        if (!$this->_sessionId) {
            $this->_login();
        }
        
        if (!is_string($method)) {
            throw new Exception('Method must be supplied as a string');
        }
        
        $params['session_id'] = $this->_sessionId;
        
        return $this->_client->{$method}($params);
    }
    
    protected function _login()
    {
        $this->_sessionId = $this->_client->login(array(
            'username' => $this->_username,
            'password' => $this->_password,
            'session_duration_minutes' => $this->_sessionDuration
        ));
        
        if (!$this->_sessionId) {
            throw new Exception('Failed to login to Vertical Response');
        }
    }
}