<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */


/**
 * @see Ovoyo_Menu_Element
 */
require_once 'Ovoyo/Menu/Element.php';

/**
 * Generic class to menu style navigation
 *
 * @package    Ovoyo_Menu
 */
class Ovoyo_Menu
{
    protected $_viewHelper = 'menu';

    public $id = '';
    public $title;
    public $class;

    public $elements = array();
    
    /**
     * Constructor
     *
     * @param string $id Id/name of menu
     * @param array $config
     */
    public function __construct($id, $config = array())
    {
        $this->id = $id;
        foreach ($config AS $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Add sub menu
     *
     * @param Ovoyo_Menu $subMenu
     * @return void
     */
    public function addSubMenu($subMenu)
    {
        $this->elements[] = $subMenu;
    }

    /**
     * Add element to menu
     *
     * @param string $label Element label
     * @param array $config Element config
     * @return void
     */
    public function addElement($label = '', $config = array())
    {
        if ($label) {
            $config['label'] = $label;
            $this->elements[] = new Ovoyo_Menu_Element($config);
        } else {
            // item is a separator
            $this->elements[] = null;
        }
    }

    /**
     * Check to see if any of a menu's elements is currently selected
     *
     * A menu element is considered selected if it's link or any of its
     * submenu items' links match the current url path
     *
     * @return boolean
     */
    public function selected()
    {
        list($selectedId, $selectedElement) = $this->getSelected();

        return ($selectedId) ? true : false;
    }

    /**
     * Get selected menu element
     *
     * If a selected element is found, the id of the element is returned as
     * well as the Ovoyo_Menu_Element instance
     *
     * @param string $currentMatch
     * @param int $currentMatchScore
     * @return null|array
     */
    public function getSelected($currentMatch = null, $currentMatchScore = 0)
    {
        global $level;
        
        $selected = false;
        $selectedId = null;
        $currentMatchLink = (is_null($currentMatch)) ? '' : $currentMatch->link;

        if (is_array($this->elements)) {
            foreach ($this->elements AS $id => $element) {
                if ($element instanceof Ovoyo_Menu_Element) {
                    if ($score = $element->selected($currentMatchLink, $currentMatchScore)) {
                        $selected  = true;
                        $selectedId = $id;
                        $currentMatch = $element;
                        $currentMatchLink = $element->link;
                        $currentMatchScore = $score;
                    }
                } else if ($element instanceof Ovoyo_Menu) {
                    list($matched, $matchedId, $matchedElement, $score) = $element->getSelected($currentMatch, $currentMatchScore);
                    if (!is_null($matchedId)) {
                        $selected = true;
                        $selectedId = $id;
                        $currentMatch = $matchedElement;
                        $currentMatchLink = $currentMatch->link;
                        $currentMatchScore = $score;
                    }
                }
            }
        }
        
        return array($selected, $selectedId, $currentMatch, $currentMatchScore);
    }
    
    /**
     * Get menu as an array
     *
     * @param boolean $checkSelected
     * @return string
     */
    public function toArray($checkSelected = true)
    {
        $selected = false;
        $selectedId = null;
    
        // only work out selected if allowed to - this is to prevent matches
        // with a higher score being selected when they shouldn't be
        if ($checkSelected) {
            list($selected, $selectedId, $selectedElement, $score) = $this->getSelected();
        } else {
            $checkSelected = false;
        }
        
        $menu = array(
            'id' => $this->id,
            'title' => $this->title,
            'class' => $menu->class,
            'selected' => $selected
        );
    
        $elements = array();
        if (is_array($this->elements)) {
            foreach ($this->elements AS $id => $element) {
                $selected = (!is_null($selectedId) && $id == $selectedId) ? true : false;
                if ($element instanceof Ovoyo_Menu_Element) {
                    $item = $element->toArray();
                    $item['selected'] = $selected;
                    $elements[] = $item;
                } else if ($element instanceof Ovoyo_Menu) {
                    $item = $element->toArray($checkSelected);
                    $item['selected'] = $selected;
                    $elements[] = $item;
                } else {
                    $elements[] = array();
                }
            }
        }
        
        $menu['elements'] = $elements;
    
        return $menu;
    }

    /**
     * Render menu
     *
     * @param Ovoyo_View $view
     * @param boolean $checkSelected Flag to prevent menu item trying to find selected item
     * @returnstring
     */
    public function render($view, $checkSelected = true)
    {
        return $view->{$this->_viewHelper}($this, $view, $checkSelected);
    }

    /**
     * Set view helper
     *
     * @param   string $viewHelper
     * @return  void
     */
    public function setViewHelper($viewHelper)
    {
        $this->_viewHelper = $viewHelper;
    }
}
