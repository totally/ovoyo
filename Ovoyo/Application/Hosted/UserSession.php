<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Application_UserSession
 */
require_once 'Ovoyo/Application/UserSession.php';

/**
 * User session class
 *
 * @package     Ovoyo_Application
 * @subpackage  Hosted
 */
class Ovoyo_Application_Hosted_UserSession extends Ovoyo_Application_UserSession
{
    public $workspaceId;

}
