<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Email marketing campaign class
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage Campaign
 */
abstract class Ovoyo_Services_OnlineMarketing_Campaign_Abstract
{
    public $campaignId;
    
    protected $_apiHandler;
    
    
    /**
     * Create campaign
     *
     * @param   string $name Name of campaign
     * @param   array $headers
     * @param   array $content
     * @return  boolean
     */
    abstract public function create($name, $headers, $content);
    
    /**
     * Update campaign
     *
     * @param   string $name Name of campaign
     * @param   array $headers
     * @param   array $content
     * @return  boolean
     */
    abstract public function update($name, $headers, $content);
    
    /**
     * Send campaign
     * 
     * @param   array  $lists Lists to send to
     * @return  boolean
     */
    abstract public function send($lists);
    
    /**
     * Send preview
     * 
     * @param   string $sendTo
     * @return  boolean
     */
    abstract public function sendPreview($sendTo);

    /**
     * Delete campaign
     * 
     * @return  boolean
     */
    abstract public function delete();

    /**
     * Set api handler
     * 
     * @param   array $config
     * @return  void
     */
    abstract public function setApiHandler($config = null);
    
    /**
     * Get api handler
     * 
     * @return  Ovoyo_Services_OnlineMarketing_ApiHandler
     */
    public function getApiHandler()
    {
        if (!isset($this->_apiHandler)) {
            throw new Exeption('API handler has not been defined');
        }
        return $this->_apiHandler;
    }
}
