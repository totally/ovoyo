<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Class which will output a menu
 *
 * @package    Ovoyo_View
 */
class Ovoyo_View_Helper_FormButtonSet
{
    private $_secondarySeparator = '|';
    private $_buttonHelpers = array('button' => 'formButton', 'submit' => 'formSubmit', 'reset' => 'fromReset');
    
    
    /**
     * Constructor
     *
     * @param   Ovoyo_View $view
     * @param   array $button Array holding button details
     * @param   string $name Element name
     * @param   array $options
     * @return  void
     */
    public function formButtonSet(Ovoyo_View $view, array $buttons, $name, $options = null) {
            // separate off primary and seconday buttons
            $primary   = array();
        $secondary = array();
        foreach ($buttons AS $id => $button) {
            $type = $button['type'];
            if ($type != 'button' && $type != 'submit' && $type != 'reset') { continue; }

            $button['options']['class'] = ($button['options']['class']) ? $button['options']['class'] : 'primary';
            if ($button['options']['class'] == 'primary') {
                $primary[$id] = $button;
            } else {
                $secondary[$id] = $button;
            }
        }
        
        $label = (!empty($options['label'])) ? $label . ' &rsaquo;&rsaquo;' : '&nbsp;';
        
        $html.= '<table class="ovoyoButtonSet">' . "\n"
              . "\t" .'<tr class="buttonSet">' . "\n"
              . "\t\t" . '<td class="label">' . $label . '</td>' . "\n"
              . "\t\t" . '<td class="element">' . "\n";
              
        // display primary elements
        foreach ($primary AS $id => $button) {
            $helper = $this->_buttonHelpers[$button['type']];
            
            $html.= "\t\t\t"
                  . $view->{$helper}($id, $button['options']['label'], $button['options'])
                  . "\n";
        }
        
            // display secondary elements
        foreach ($secondary AS $id => $button) {
            $helper = $this->_buttonHelpers[$button['type']];
            
            $html.= "\t\t\t"
                  . $this->_secondarySeparator . ' '
                  . $view->{$helper}($id, $button['options']['label'], $button['options'])
                  . "\n";
        }

        $html.= "\t\t</td>\n"
              . "\t</tr>\n"
              . "</table>\n";

        return $html;
    }
}
