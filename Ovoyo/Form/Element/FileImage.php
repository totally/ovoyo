<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

require_once 'Zend/Form/Element.php';

/**
 * Assignable selects element
 *
 * @package    Ovoyo_Form
 */
class Ovoyo_Form_Element_FileImage extends Zend_Form_Element
{
    /**
     * Use formHidden view helper by default
     * @var string
     */
    public $helper = 'formFileImage';

}
