<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Zend_Form_Decorator_Abstract
 */
require_once 'Zend/Form/Decorator/Abstract.php';

/**
 * Replacement label decorator
 *
 * Single static method to take a Ovoyo_Model instance (or sub class of)
 * and create a Zend_Form instance from it. Based on code found in the following article:
 * http://codecaine.co.za/posts/form-generation-with-zend-form-part-2/
 *
 * @package    Ovoyo_Form
 */
class Ovoyo_Form_Decorator_TdSubmit extends Zend_Form_Decorator_Abstract
{
    /**
     * Render a label
     * 
     * @param  string $content 
     * @return string
     */
    public function render($content)
    {
        $element = $this->getElement();
        $view    = $element->getView();
        if (null === $view) {
            return $content;
        }

        $separator = $this->getSeparator();
    #    $placement = $this->getPlacement();
        $options   = $this->getOptions();

        $submit = '<tr><td class="label">'
                . '<td' . $class . '>' . $view->formSubmit($element->getName(), $element->getLabel(), $options) . '</td></tr>';

        return $submit;
    }
}
