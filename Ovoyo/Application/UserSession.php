<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Model
 */
require_once 'Ovoyo/Model.php';

/**
 * User session class
 *
 * @package    Ovoyo_Application
 */
class Ovoyo_Application_UserSession extends Ovoyo_Model
    implements Ovoyo_Authentication_Interface_UserSession
{
    protected $_expirationSeconds;
    
    /**
     * Start new session for user
     *
     * @param   ing $userId
     * @param   string $sessionIdentifier Session identifier
     * @return  boolean
     */
    public function startSession($userId, $sessionIdentifier)
    {
        $this->beginTransaction();
        try {
            $this->userId     = $userId;
            $this->sessionId  = $this->_createSessionId();
            $this->lastAccess = new Zend_Date;
            if (!$this->insert()) {
                $this->rollback();
                return false;
            }

            // clear out any sessions for user over 6 hours old
            $where = "userId = " . $this->userId
                   . " AND lastAccess < (NOW() - INTERVAL 21600 SECOND)";
    
            $this->delete($where);
            
            $this->dropSessionIdCookie($sessionIdentifier);
            
            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }

        return true;
    }
    
    /**
     * Refresh session
     *
     * @return  boolean
     */
    public function refresh()
    {
        $now = new Zend_Date;
        
        if (is_numeric($this->_expirationSeconds)
            && $this->_expirationSeconds > 0
        ) {
            $expires = $this->lastAccess;
            $expires->addSecond($this->_expirationSeconds);
            if ($now->isLater($expires)) {
                return false;
            }
        }
        
        $this->lastAccess = $now;
        return $this->update();
    }
    
    /**
     * Create a random session ID string
     *
     * @return string a random session ID string 64 characters in length
     */
    protected final function _createSessionId()
    {
        $sessionId = '';

        srand((double) microtime() * 1000000);

        for ($n = 0; $n < 64; $n++) {
            $varchar = rand(1,3);
            switch ($varchar) {
                case 1: $sessionId.= chr(rand(97,122));
                break;
                case 2: $sessionId.= chr(rand(48,57));
                break;
                case 3: $sessionId.= chr(rand(65,90));
                break;
            }
        }

        return $sessionId;
    }
    
    /**
     * Drop cookie containing a session ID
     *
     * @param   string $sessionIdentifier Session identifier
     * @return  void
     */
    public function dropSessionIdCookie($sessionIdentifier)
    {
        $secure_cookie = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? true : false;
        setcookie($sessionIdentifier, $this->sessionId, 0, "/", '', $secure_cookie);
    }
    
    /**
     * Delete session
     *
     * @param   string $sessionIdentifier Session identifier
     * @return  void
     */
    public function end($sessionIdentifier)
    {
        if (!$this->delete()) {
            return false;
        }

        $secure_cookie = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? true : false;
        setcookie($sessionIdentifier, null, 0, "/", '', $secure_cookie);
        unset($_COOKIE[$sessionIdentifier]);
        
        return true;
    }
}
