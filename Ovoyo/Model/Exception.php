<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Exception
 */
require_once 'Ovoyo/Exception.php';

/**
 * Ovoyo Exception
 *
 * @package    Ovoyo_Model
 */
class Ovoyo_Model_Exception extends Ovoyo_Exception
{}
