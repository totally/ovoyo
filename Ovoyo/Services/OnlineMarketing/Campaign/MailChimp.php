<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Email marketing campaign class
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage Campaign
 */
class Ovoyo_Services_OnlineMarketing_Campaign_MailChimp
      extends Ovoyo_Services_OnlineMarketing_Campaign_Abstract
{
    public $sendTime;

    /**
     * Constructor
     *
     * @param array $config
     */
    public function __construct($config)
    {
        foreach ($config AS $option => $value) {
            $this->{$option} = $value;
        }

        $this->setApiHandler($config);
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::create()
     */
    public function create($list, $headers, $content)
    {
        $apiHandler = $this->getApiHandler();

        // required params
        $params = array(
            'options' => array(
                'list_id' => current($list),
                'from_name' => $headers['fromName'],
                'from_email' => $headers['fromAddress'],
                'subject' => $headers['subject'],
                'title' => $headers['name'],
                'generate_text' => true
            ),
            'content' => array(
                'url' => $content['htmlUrl']
            ),
            'method' => 'campaignCreate',
            'type' => 'regular'
        );

        $response = $apiHandler->callApi($params);
        
        if (!$response = @unserialize($response)) {
            $msg = "Unexpected response from Mailchimp";
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }
        
        if (is_array($response)) {
            $msg = "Error " . $response['code'] . " " . $response['error'];
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }
        
        // the string campaignId is returned
        $this->campaignId = $response;

        return $this->campaignId;
    }

    /**
     * Mailchimp will only accept updates for individual values so a number
     * of API calls are made here to update the campaign
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::update()
     */
    public function update($name, $headers, $content)
    {
        $apiHandler = $this->getApiHandler();

        // required params
        $params = array(
            'cid' => $this->campaignId,
            'method' => 'campaignUpdate'
        );

        // Update list param
        $list = $headers['list'];
        $params['name'] = 'list_id';
        $params['value'] = current($list);

        $response = $apiHandler->callApi($params);
        
        if (!$response = @unserialize($response)) {
            $msg = "Unexpected response from Mailchimp";
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }
        
        if ($response['error']) {
            $msg = "Error " . $response['code'] . " " . $response['error'];
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }

        // Update title param
        $params['name'] = 'title';
        $params['value'] = $name;

        $response = $apiHandler->callApi($params);
        
        if (!$response = @unserialize($response)) {
            $msg = "Unexpected response from Mailchimp";
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }
        
        if ($response['error']) {
            $msg = "Error " . $response['code'] . " " . $response['error'];
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }

        // Update subject param
        $params['name'] = 'subject';
        $params['value'] = $headers['subject'];

        $response = $apiHandler->callApi($params);
        
        if (!$response = @unserialize($response)) {
            $msg = "Unexpected response from Mailchimp";
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }
        
        if ($response['error']) {
            $msg = "Error " . $response['code'] . " " . $response['error'];
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }

        // Update from_name param
        $params['name'] = 'from_name';
        $params['value'] = $headers['fromName'];

        $response = $apiHandler->callApi($params);
        
        if (!$response = @unserialize($response)) {
            $msg = "Unexpected response from Mailchimp";
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }
        
        if ($response['error']) {
            $msg = "Error " . $response['code'] . " " . $response['error'];
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }

        // Update from_email param
        $params['name'] = 'from_email';
        $params['value'] = $headers['fromAddress'];

        $response = $apiHandler->callApi($params);
        
        if (!$response = @unserialize($response)) {
            $msg = "Unexpected response from Mailchimp";
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }
        
        if ($response['error']) {
            $msg = "Error " . $response['code'] . " " . $response['error'];
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::delete()
     */
    public function delete()
    {
        $apiHandler = $this->getApiHandler();

        $params = array(
            'cid' => $this->campaignId,
            'method' => 'campaignDelete'
        );

        $response = $apiHandler->callApi($params);

        return $response;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::send()
     */
    public function send($lists)
    {
        $apiHandler = $this->getApiHandler();

        if ($this->sendTime) {
            $params = array(
                'cid' => $this->campaignId,
                'method' => 'campaignSchedule',
                'schedule_time' => $this->sendTime
            );
        } else {
            $params = array(
                'cid' => $this->campaignId,
                'method' => 'campaignSendNow'
            );
        }

        $response = $apiHandler->callApi($params);
        
        if (!$response = @unserialize($response)) {
            $msg = "Unexpected response from Mailchimp";
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }
        
        if ($response['error']) {
            $msg = "Error " . $response['code'] . " " . $response['error'];
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }
        
        return $response;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::sendPreview()
     */
    public function sendPreview($sendTo)
    {
        $apiHandler = $this->getApiHandler();

        $emails = explode(',', str_replace(" ", "", $sendTo));
        
        $params = array(
            'method' => 'campaignSendTest',
            'cid' => $this->campaignId,
            'test_emails' => $emails
        );

        $response = $apiHandler->callApi($params);
        
        if (!$response = @unserialize($response)) {
            $msg = "Unexpected response from Mailchimp";
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }
        
        if ($response['error']) {
            $msg = "Error " . $response['code'] . " " . $response['error'];
            Ovoyo_ErrorHandler::addError($msg);
            return false;
        }
        
        return $response;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_Campaign_Abstract::setApiHandler()
     */
    public function setApiHandler($config = null)
    {
        $this->_apiHandler = new Ovoyo_Services_OnlineMarketing_ApiHandler_MailChimp($config);
    }
}
