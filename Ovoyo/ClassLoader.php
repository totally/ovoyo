<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Class loader
 *
 * @package    Ovoyo_ClassLoader
 */
class Ovoyo_ClassLoader
{
    private static $_instance;
    private $_exists = array();

    /**
     * Constructor
     *
     */
    private function __construct()
        {
        }

    /**
     * Get web container instance
     *
     * @return obj Ovoyo_WebContainer instance
     */
    private static function _getInstance()
    {
        if (self::$_instance === null) self::$_instance = new self;
        return self::$_instance;
    }

    /**
     * Load a class
     *
     * @param   string $className Full name of class
     * @param   string|array $includePaths List of include paths
     * @param   bool $ignoreDefaultIncludePath Flag to specify if default include path specified by ini_get('include_path') should be ignored
     * @return  boolean
     * @throws  Ovoyo_ClassLoader_Exception
     */
    public static function load($className, $includePaths = '', $ignoreDefaultIncludePath = false)
    {
        // do nothing if class already exists
        #echo $className."<BR>";
        if (@class_exists($className)) { return true; }

        $classInclude = preg_replace('/_/', DIRECTORY_SEPARATOR, $className) . '.php';

        // build include paths
        $paths = array();
        if (!$ignoreDefaultIncludePath) {
            $paths = explode(PATH_SEPARATOR, ini_get('include_path'));
            
        }

        if (!empty($includePaths)) {
            if (is_string($includePaths)) {
                // if single include path without "/" prefix, append to all known existing paths
                if (!preg_match('/' . PATH_SEPARATOR . '/', $includePaths) && !preg_match('/^([A-Z]:|\/)/i', $includePaths)) {

                    $newPaths = '';
                    foreach ($paths AS $path) {
                        $newPaths.= $path . DIRECTORY_SEPARATOR . $includePaths . PATH_SEPARATOR;
                    }
                    $paths = array_merge($paths, explode(PATH_SEPARATOR, $newPaths));

                // otherwise just append to existing paths
                } else {
                    $paths = array_merge($paths, explode(PATH_SEPARATOR, $includePaths));
                }
            } else if (is_array($includePaths)) {
                $paths = array_merge($paths, $includePaths);
            }
        }
            
        // try and include class
        foreach ($paths AS $path) {
            #echo $path . DIRECTORY_SEPARATOR . $classInclude . "<BR>\n";
            if (is_file($path . DIRECTORY_SEPARATOR . $classInclude) && preg_match('/^[A-Z]/', $className)) {
                include_once $path . DIRECTORY_SEPARATOR . $classInclude;
                return true;

/*
            } else {
                include_once $classInclude;
                if (class_exists($className)) {
                    return;
                }
*/
            }
        }

        #require_once 'Ovoyo/ClassLoader/Exception.php';
        #throw new Ovoyo_ClassLoader_Exception("$classInclude not found - include path(" . implode(":", $paths) . ")");
        return false;
    }

    /**
     * Check to see if a class exists
     *
     * @param   string $className Full name of class
     * @param   string|array $includePaths List of include paths
     * @param   bool $ignoreDefaultIncludePath Flag to specify if default include path specified by ini_get('include_path') should be ignored
     * @return  bool
     */
    public static function exists($className, $includePaths = '', $ignoreDefaultIncludePath = false)
    {
        $instance = self::_getInstance();
        
        // check cache array first
        if (isset($instance->_exists[$className]) && is_bool($instance->_exists[$className])) {
            return $instance->_exists[$className];
        }

        // try loading to class as test for existence, ignoring any exceptions
        try {
            $exists = self::load($className, $includePaths, $ignoreDefaultIncludePath);
            $instance->_exists[$className] = $exists;

        } catch (Exception $e) {
            $instance->_exists[$className] = false;
            return false;
        }
        
        return $exists;
    }
}
