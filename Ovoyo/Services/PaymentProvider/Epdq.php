<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Services_PaymentProvider_Abstract
 */
require_once 'Ovoyo/Services/PaymentProvider/Abstract.php';

/**
 * Epdq class
 *
 * @package    Ovoyo_Services_PaymentProvider
 * @TODO remove comments/var dump
 */
class Ovoyo_Services_PaymentProvider_Epdq extends Ovoyo_Services_PaymentProvider_Abstract
{

    const EPDQ_URL = 'https://secure2.epdq.co.uk/cgi-bin/CcxBarclaysEpdq.e';

    // Public constants for common numeric values.
    const EPDQ_COUNTRY_UK      = '826';
    const EPDQ_CURRENCY_GBP    = '826';

    // Fields that can be sent to Epdq
    // Mandatory request fields
    const EPDQ_FIELD_EPDQ_DATA                  = 'epdqdata';
    const EPDQ_FIELD_MERCHANT_DISPLAY_NAME      = 'merchantdisplayname';
    const EPDQ_FIELD_RETURN_URL                 = 'returnurl';

    // Optional request fields
    const EPDQ_FIELD_BILLING_ADDRESS_1          = 'baddr1';
    const EPDQ_FIELD_BILLING_ADDRESS_2          = 'baddr2';
    const EPDQ_FIELD_BILLING_ADDRESS_3          = 'baddr3';
    const EPDQ_FIELD_BILLING_CITY               = 'bcity';
    const EPDQ_FIELD_BILLING_STATE              = 'bstate';
    const EPDQ_FIELD_BILLING_COUNTY_PROVINCE    = 'bcountyprovince';
    const EPDQ_FIELD_BILLING_POSTAL_CODE        = 'bpostalcode';
    const EPDQ_FIELD_BILLING_COUNTRY            = 'bcountry';
    const EPDQ_FIELD_BILLING_TELEPHONE_NUMBER   = 'btelephonenumber';
    const EPDQ_FIELD_EMAIL                      = 'email';
    const EPDQ_FIELD_SHIPPING_ADDRESS_1         = 'saddr1';
    const EPDQ_FIELD_SHIPPING_ADDRESS_2         = 'saddr2';
    const EPDQ_FIELD_SHIPPING_ADDRESS_3         = 'saddr3';
    const EPDQ_FIELD_SHIPPING_CITY              = 'scity';
    const EPDQ_FIELD_SHIPPING_STATE             = 'sstate';
    const EPDQ_FIELD_SHIPPING_COUNTY_PROVINCE   = 'scountyprovince';
    const EPDQ_FIELD_SHIPPING_POSTAL_CODE       = 'spostalcode';
    const EPDQ_FIELD_SHIPPING_COUNTRY           = 'scountry';
    const EPDQ_FIELD_SHIPPING_TELEPHONE_NUMBER  = 'stelephonenumber';
    const EPDQ_FIELD_COLLECT_DELIVERY_ADDRESS   = 'collectdeliveryaddress';


    // Used to fetch the epdq encrypted data
    const EPDQ_ENCRYPTION_TOOL_URL              = 'https://secure2.epdq.co.uk/cgi-bin/CcxBarclaysEpdqEncTool.e';
    const EPDQ_ENCRYPTION_FIELD_CLIENT_ID       = 'clientid';
    const EPDQ_ENCRYPTION_FIELD_PASSWORD        = 'password';
    const EPDQ_ENCRYPTION_FIELD_CHARGE_TYPE     = 'chargetype';
    const EPDQ_ENCRYPTION_FIELD_TOTAL           = 'total';
    const EPDQ_ENCRYPTION_FIELD_CURRENCY_CODE   = 'currencycode';
    const EPDQ_ENCRYPTION_FIELD_ORDER_ID        = 'oid';


    // response fields
    const EPDQ_RESPONSE_TRANSACTION_STATUS      = 'transactionstatus';
    const EPDQ_RESPONSE_ORDER_ID                = 'oid';
    const EPDQ_RESPONSE_TOTAL                   = 'total';
    const EPDQ_RESPONSE_CLIENT_ID               = 'clientid';
    const EPDQ_RESPONSE_CHARGE_TYPE             = 'chargetype';
    const EPDQ_RESPONSE_DATE_TIME               = 'datetime';
    const EPDQ_RESPONSE_ECI_STATUS              = 'ecistatus';
    const EPDQ_RESPONSE_CARD_PREFIX             = 'cardprefix';

    const EPDQ_TRANSACTION_SUCCESS              = 'Success';

    /**
     * The epdq encrypted data retrieved from epdq encryption tool
     * @var string
     */
    protected $_epdqData;

    /**
     * The value of fields to be sent to the epdq encryption tool
     * @var array
     */
    public $epdqEncryptionRequestParams = array();

    /**
     * The name of fields to be sent to the epdq encryption tool
     * @var array
     */
    public $epdqEncryptionRequiredParams = array(
        self::EPDQ_ENCRYPTION_FIELD_ORDER_ID => 'Order Id missing',
        self::EPDQ_ENCRYPTION_FIELD_CLIENT_ID => 'Client Id missing',
        self::EPDQ_ENCRYPTION_FIELD_PASSWORD => 'Password missing',
        self::EPDQ_ENCRYPTION_FIELD_CHARGE_TYPE => 'Charge type missing',
        self::EPDQ_ENCRYPTION_FIELD_TOTAL => 'Total amount missing',
        self::EPDQ_ENCRYPTION_FIELD_CURRENCY_CODE => 'Currency code missing'
    );

    /**
     * The value of the fields to be sent to epdq
     * @var array
     */
    public $requestFields = array();

    /**
     * Array containing the fields required before being sent to Epdq
     * @var array
     */
    public $requiredFields = array(
        self::EPDQ_FIELD_EPDQ_DATA => "EPDQ data missing",
        self::EPDQ_FIELD_MERCHANT_DISPLAY_NAME => "Merchant name missing",
        self::EPDQ_FIELD_RETURN_URL => "Return url missing"
    );

    /**
     * Array containing the optional fields to send to Epdq
     * @var array
     */
    public $optionalFields = array(
         self::EPDQ_FIELD_BILLING_ADDRESS_1,
         self::EPDQ_FIELD_BILLING_ADDRESS_2,
         self::EPDQ_FIELD_BILLING_ADDRESS_3,
         self::EPDQ_FIELD_BILLING_CITY,
         self::EPDQ_FIELD_BILLING_STATE,
         self::EPDQ_FIELD_BILLING_COUNTY_PROVINCE,
         self::EPDQ_FIELD_BILLING_POSTAL_CODE,
         self::EPDQ_FIELD_BILLING_COUNTRY,
         self::EPDQ_FIELD_BILLING_TELEPHONE_NUMBER,
         self::EPDQ_FIELD_EMAIL,
         self::EPDQ_FIELD_SHIPPING_ADDRESS_1,
         self::EPDQ_FIELD_SHIPPING_ADDRESS_2,
         self::EPDQ_FIELD_SHIPPING_ADDRESS_3,
         self::EPDQ_FIELD_SHIPPING_CITY,
         self::EPDQ_FIELD_SHIPPING_STATE,
         self::EPDQ_FIELD_SHIPPING_COUNTY_PROVINCE,
         self::EPDQ_FIELD_SHIPPING_POSTAL_CODE,
         self::EPDQ_FIELD_SHIPPING_COUNTRY,
         self::EPDQ_FIELD_SHIPPING_TELEPHONE_NUMBER,
         self::EPDQ_FIELD_COLLECT_DELIVERY_ADDRESS
    );

    /**
     * Array containing the value of the response fields
     * @var array
     */
    public $responseFields = array();

    /**
     * Errors
     * @var array
     */
    public $errors = array();


    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::_init()
     */
    protected function _init()
    {

    }
    /**
     * Populate the fields to be sent to Epdq
     * @param array $vars
     */
    protected function _setRequestFields(array $vars)
    {
        foreach($this->requiredFields as $field => $errorMsg) {
            if(!isset($vars[$field])) {
                $this->errors[] = $errorMsg;
            }else {
                $this->requestFields[$field] = $vars[$field];
                unset($vars[$field]);
            }
        }
        foreach($this->optionalFields as $field) {
            if(isset($vars[$field]) && strlen($vars[$field])!=0) {
                $this->requestFields[$field] = $vars[$field];
                unset($vars[$field]);
            }
        }
    }

    /**
     * Populate the params to get the epdq data encryption field
     * @param array $vars
     */
    protected function _setEncryptionRequestParams(array $vars)
    {
        foreach($this->epdqEncryptionRequiredParams as $param => $errorMsg) {
            if(!isset($vars[$param])) {
                $this->errors[] = $errorMsg;
            }else {
                $this->epdqEncryptionRequestParams[$param] = $vars[$param];
                unset($vars[$param]);
            }
        }
    }

    /**
     * Has error method
     * @return true if there is no errors
     *         false otherwise
     */
    protected function _hasErrors()
    {
        return !empty($this->errors);
    }


    /**
     * Perform a cURL operation to fetch the epdq data encrypted field
     * @return void
     *
     */
    protected function _fetchEpdqEncryptedData()
    {
        if($this->_hasErrors()) {
            return false;
        }
        $url = self::EPDQ_ENCRYPTION_TOOL_URL;
        $params = '';
        foreach($this->epdqEncryptionRequestParams as $param => $value) {
            $params .= $param . '=' . $value . '&';
        }
        $params = rtrim($params, '&');
        $user_agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $result = curl_exec($ch);
        curl_close($ch);
        if (preg_match('/"([A-Za-z0-9]+)"/', $result, $matches)) {
            $this->_epdqData = $matches[1];
        }else {
            $this->errors[] = 'Error while processing the epdq data';
            return false;
        }
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract#sendPayment()
     */
    public function sendPayment(array $vars)
    {
        $controller = $vars['controller'];
        if($controller) {
            $renderer = $controller->getHelper('viewRenderer');
            $renderer->setNoRender(true);
        }
        $this->_setEncryptionRequestParams($vars);

        if(!$this->_fetchEpdqEncryptedData()) {
            return false;
        }
        $vars[self::EPDQ_FIELD_EPDQ_DATA] = $this->_epdqData;
        $this->_setRequestFields($vars);

        $script = '<script type="text/javascript">'."\n";
        $script .= 'document.getElementById("epdqPaymentForm").submit();'."\n";
        $script .= '</script>'."\n";

        $form = $this->_createPaymentForm();
        if(!$form) {
            return false;
        }
        if($controller) {
            $view = $controller->view;
            $controller->getResponse()->setBody($form->render($view) . $script);
        }else {
            echo $form->render() . $script;
            exit;
        }
        return true;
    }


    /**
     * Sets up a payment, ready to send to Epdq
     *
     * @access public
     * @return mixed string The HTML form
     */
    protected function _createPaymentForm()
    {
        if ($this->_hasErrors()) {
            return false;
        }

        $form = new Ovoyo_Form();

        $form->setAttrib('id', 'epdqPaymentForm')
             ->setAttrib('name', 'epdqPaymentForm')
             ->setAttrib('action', self::EPDQ_URL)
             ->setMethod('post');

        foreach($this->requestFields as $fieldName => $value) {
            $form->addElement('hidden', $fieldName,
                array('value' => $value,
                      'required' => true
                )
            );
        }
        return $form;
    }


    /**
     * Get a response field value
     *
     * @param string field name
     * @return string value
     */
    protected function _getResponseField($field)
    {
        return $this->response_fields[$field];
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_Abstract::validateCallback()
     */
    public function validateCallback($data = array())
    {
        $valid = false;
        if (array_key_exists(self::EPDQ_RESPONSE_TRANSACTION_STATUS, $data)) {
            $valid = true;
        }

        return $valid;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract#handleCallback()
     */
    public function handleCallback(array $data)
    {
        $this->response_fields = $data;
        // fetch the payment
        $payment = Ovoyo_Services_PaymentProvider::getPayment(
            array('connName' => $this->_connName)
        );
        if (!$payment->fetch(
                $this->_getResponseField(self::EPDQ_RESPONSE_ORDER_ID)
            )) {
            return false;
        }
        $this->paymentId = $payment->paymentId;
        // update payment details
        $payment->status = $this->_getResponseField(
            self::EPDQ_RESPONSE_TRANSACTION_STATUS
        );
        $payment->reference = $payment->paymentId;
        $payment->callbackData = $data;
        $payment->paid = ($payment->status == self::EPDQ_TRANSACTION_SUCCESS)
                       ? 1 : 0;
        if($payment->paid == 1) {
            $payment->paidDate = new Zend_Date;
        }
        if (!$payment->update()) {
            return false;
        }

        return $payment->paid;
    }
    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract#completePayment()
     */
    public function completePayment()
    {

    }



    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::processPaymentInfo()
     */
    public function processPaymentInfo(array $data)
    {
        $paid = $this->handleCallback($data);
        if($paid) {
            $result = array();
            $result['paymentId'] = $this->paymentId;
        } else {
            $result = false;
        }
        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::postConvertToOrder()
     */
    public function postConvertToOrder($config)
    {

    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::prepareData()
     */
    public function prepareData(array $data)
    {
        $preparedData = array(
            self::EPDQ_ENCRYPTION_FIELD_ORDER_ID        => $data['paymentId'],
            self::EPDQ_ENCRYPTION_FIELD_CLIENT_ID       => $data['config']['clientId'],
            self::EPDQ_ENCRYPTION_FIELD_PASSWORD        => $data['config']['password'],
            self::EPDQ_ENCRYPTION_FIELD_CHARGE_TYPE     => 'Auth',
            self::EPDQ_ENCRYPTION_FIELD_TOTAL           => $data['amount'],
            self::EPDQ_ENCRYPTION_FIELD_CURRENCY_CODE   => self::EPDQ_CURRENCY_GBP,
            self::EPDQ_FIELD_MERCHANT_DISPLAY_NAME      => $data['config']['merchantName'],
            self::EPDQ_FIELD_RETURN_URL                 => $data['callbackUrl'],
            self::EPDQ_FIELD_BILLING_ADDRESS_1          => $data['address1'],
            self::EPDQ_FIELD_BILLING_ADDRESS_2          => $data['address2'],
            self::EPDQ_FIELD_BILLING_ADDRESS_3          => $data['address3'],
            self::EPDQ_FIELD_BILLING_CITY               => $data['townCity'],
            self::EPDQ_FIELD_BILLING_POSTAL_CODE        => $data['postcode'],
            self::EPDQ_FIELD_BILLING_COUNTRY            => $data['country'],
            self::EPDQ_FIELD_BILLING_TELEPHONE_NUMBER   => $data['telephone'],
            self::EPDQ_FIELD_EMAIL                      => $data['email'],
            self::EPDQ_FIELD_SHIPPING_ADDRESS_1         => $data['address1'],
            self::EPDQ_FIELD_SHIPPING_ADDRESS_2         => $data['address2'],
            self::EPDQ_FIELD_SHIPPING_ADDRESS_3         => $data['address3'],
            self::EPDQ_FIELD_SHIPPING_CITY              => $data['townCity'],
            self::EPDQ_FIELD_SHIPPING_POSTAL_CODE       => $data['postcode'],
            self::EPDQ_FIELD_SHIPPING_COUNTRY           => $data['country'],
            self::EPDQ_FIELD_SHIPPING_TELEPHONE_NUMBER  => $data['telephone'],
            self::EPDQ_FIELD_COLLECT_DELIVERY_ADDRESS   => '0'
        );
        return $preparedData;
    }
}
