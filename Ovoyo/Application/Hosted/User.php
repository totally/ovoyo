<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Application_User
 */
require_once 'Ovoyo/Application/User.php';

/**
 * @see Ovoyo_ErrorHandler
 */
require_once 'Ovoyo/ErrorHandler.php';

/**
 * User class
 *
 * @package     Ovoyo_Application
 * @subpackage  Hosted
 */
class Ovoyo_Application_Hosted_User extends Ovoyo_Application_User
{   
    
    /**
     * Init - setup model options
     *
     * @return  void
     */
    public function init()
    {
        parent::init();
        
        $joins = array(
            'userWorkspace' => array(
                'type' => 'left',
                'alias' => 'uw',
                'on' => 'uw.userId=u.userId',
                'cols' => array()
            )
        );
        $this->_setSearchJoins($joins);
        
        // define relationships
        $this->addRelationship('workspaces', array(
            'relation'  => 'Ovoyo_Application_Hosted_Workspace',
            'relatedBy' => array(
                'userWorkspaces' => 'Ovoyo_Application_UserWorkspace'
            ),
            'options'   => $this->_modelOptions(),
            'cascade'   => array('update' => false, 'delete' => true)));
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Ovoyo_Model#_searchGroup($select, $group)
     */
    protected function _searchGroup($select, $group)
    {
        $select->group('u.userId');
        parent::_searchGroup($select, $group);
    }

    /**
     * Get list of workspaces user has access to
     *
     * @return  array List of workspaces
     * @throw   Exception
     */
    public function allowedWorkspaces()
    {
        $workspaces = array();

        $select = $this->_db->select();

        if (!$this->superuser) {
            $select->from(array('uw' => 'userWorkspace'), array('workspaceId'))
                   ->join(array('w' => 'workspace'), 'w.workspaceId = uw.workspaceId', array('workspaceName'))
                   ->where('uw.userId = ?', $this->userId);
        } else {
            $select->from(array('w' => 'workspace'), array('workspaceId', 'workspaceName'));
        }
        $select->order('w.workspaceName ASC');

        // fetch data
        $rows = $this->_db->fetchAll($select);

        foreach ($rows AS $row) {
            $workspaces[$row['workspaceId']] = $row['workspaceName'];
        }

        return $workspaces;
    }
    
    /**
     * Get workspaces associated with user
     *
     * Results returned as an array, with an option to fetch workspaces NOT assigned to user instead
     *
     * @param   boolean $getUnassigned
     * @return  array
     * @throws  Exception
     */
    public function getWorkspaces($getUnassigned = false)
    {
        $workspaces  = array();

        try {
            $select = $this->_db->select();

            // get unassigned workspaces
            if ($getUnassigned) {
                $select->from(array('w' => $this->_fullTableName('workspace')))
                       ->joinLeft(array('uw' => $this->_fullTableName('userWorkspace')), 'uw.workspaceId=w.workspaceId AND uw.userId = ' . $this->userId, array())
                       ->where('uw.userId IS NULL');

            // get assigned workspaces
            } else {
                $select->from(array('uw' => $this->_fullTableName('userWorkspace')))
                       ->join(array('w' => $this->_fullTableName('workspace')), 'w.workspaceId=uw.workspaceId', 'workspaceName')
                       ->where('uw.userId = ?', $this->userId);
            }

            $select->order('w.workspaceName');

            $rows = $this->_db->fetchAll($select, array(), Zend_Db::FETCH_OBJ);
            foreach ($rows AS $row) {
                $workspaces[$row->workspaceId] = $row->workspaceName;
            }
        } catch (Exception $e) {
            throw $e;
        }

        return $workspaces;
    }

    /**
     * Set the list of workspaces a user has access to
     *
     * @param   array $workspaces List of workspace ids that user has access to
     * @return  bool
     * @throws  Exception
     */
    public function setAssignedWorkspaces(array $workspaces)
    {
        // if user is a superuser, then just return true
        if ($this->superuser) { 
            return true; 
        }
        
        // put workspace ids in more useful array - where key matches value, 
        // for easy lookup
        $newArray = array();
        foreach ($workspaces AS $workspaceId) {
            $newArray[$workspaceId] = $workspaceId;
        }
        $workspaces = $newArray;
        
        $this->beginTransaction();
        try {
            // fetch list of currently assigned workspace
            $currentWorkspaces = $this->getWorkspaces();
            
            // deal with workspaces that were assigned but are not any longer
            $workspace = Ovoyo_Model::factory(
                'Ovoyo_Application_Hosted_Workspace', $this->_modelOptions()
            );
            foreach ($currentWorkspaces AS $workspaceId => $name) {
                if ($workspaces[$workspaceId]) { 
                    continue; 
                }
                
                $workspace->fetch($workspaceId);
                $workspace->unassignUser($this->userId);
            }
            
            // now add any new workspaces that were not previously assigned
            foreach ($workspaces AS $workspaceId) {
                if ($currentWorkspaces[$workspaceId]) { 
                    continue; 
                }
                
                $workspace->fetch($workspaceId);
                $workspace->assignUser($this->userId);
            }
            
            // re-fetch workspace to get latest info
            $this->fetch($this->userId);
            
            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }
         
        return true;
    }

    /**
     * Check whether a user exists within a workspace
     *
     * @return  boolean
     */
    public function existsInWorkspace($workspaceId)
    {
        $workspaces = $this->allowedWorkspaces();
        return (array_key_exists($workspaceId, $workspaces));
    }
    
    /**
     * Search users in a workspace
     *
     * @param   array $options
     * @param   string $order
     * @return  Zend_Db_Statment
     */
    public function workspaceUserSearch($options = array(), $order = '')
    {
        // build query
        $select = $this->_db->select();

        $select->from(array('u' => $this->_fullTableName('user')))
               ->join(array('uw' => $this->_fullTableName('userWorkspace')), 'uw.userId = u.userId', array('workspaceId'))
               ->where('u.deleted = 0');

        // limit by user id
        if ($options['userId']) {
            $select->where('u.userId = ?', $options['userId']);
        }
        
        // limit by superuser
        if (isset($options['superuser'])) {
            $select->where('u.superuser = ?', $options['superuser']);
        }
        
        // limit by workspace
        if ($options['workspaceId']) {
            $select->where('uw.workspaceId = ?', $options['workspaceId']);
        }

        // search string
        if ($options['searchString']) {
            $searchString = $options['searchString'].'%';
            $select->where('u.email LIKE ? OR u.firstName LIKE ? OR u.lastName LIKE ?', $searchString, $searchString, $searchString);
        }
        
        // grouping
        $select->group('u.userId');
        
        // add ordering
        if ($order != '') {
            $select->order($order);
        } else {
            $select->order('email DESC');
        }

        // return statement
        return $this->_db->query($select);
        
    }
    
    /**
     * Delete user from a workspace
     *
     * @return  boolean
     * @throws  Exception
     */
    public function deleteFromWorkspace()
    {
        try {
            // get workspace
            $workspace = $this->webContainer->getWorkspace();
            if (!$workspace->workspaceId) {
                Ovoyo_ErrorHandler::addError('Workspace not found');
                return false;
            }
            
            $this->beginTransaction();
            
            // remove from workspace
            $workspace->unassignUser($this->userId);
            
            // delete user permanently if not assigned to other workspaces
            $workspaces = $this->getWorkspaces();
            if (count($workspaces) == 0) {
                $this->delete(array('userId' => $this->userId), 'deleted', 'deletedDate');
            }
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }
        
        $this->commit();
        return true;
    }
    
    /**
     * Invite user to join a workspace
     *
     * @param   string $additionalMessage Additional message to add to invite
     * @param   string $confirmationUrl Optional override of confirmation url
     * @return  boolean
     * @throws  Exception
     */
    public function inviteToWorkspace($additionalMessage = '', $confirmationUrl = '')
    {
        try {
            $workspace = $this->webContainer->getWorkspace();
            if (!$workspace->workspaceId) {
                Ovoyo_ErrorHandler::addError('Workspace not found');
                return false;
            }
            
            // create an invite key
            $inviteKey = $this->_createWorkspaceInviteKey();
            
            $this->beginTransaction();
            
            // get workspace invite object
            $workspaceInvite = Ovoyo_Model::factory('Ovoyo_Application_Hosted_WorkspaceInvite', $this->_modelOptions());

            // delete any old invites for this user and workspace
            $deleteBy = array('workspaceId' => $workspace->workspaceId,
                              'userId'      => $this->userId);
            $workspaceInvite->deleteBy($deleteBy);
            
            // save new invite, which expires in 48 hrs
            $workspaceInvite->workspaceId = $workspace->workspaceId;
            $workspaceInvite->userId      = $this->userId;
            $workspaceInvite->inviteKey   = $inviteKey;
            $workspaceInvite->expires     = date('Y-m-d H:i:s', strtotime("now + 48 hours"));
            $workspaceInvite->insert();
            
            // get confirmation url
            $host = ($_SERVER['HTTPS'] == "on") ? "https://" : "http://";
            $host.= $_SERVER['HTTP_HOST'];
            $confirmationUrl = ($confirmationUrl) ? $confirmationUrl : '/account/accept_invite/';
            $confirmationUrl = $host . $confirmationUrl;
            
            $config = $this->webContainer->getConfig('application');
            
            // subject
            $subject = $config->applicationName . ' - workspace invite';
            
            // message body
            $message = 'Hi ' . $this->firstName . "\n\n"
                     . 'You have been invited to join the ' . $workspace->workspaceName . ', ' . $config->applicationName . ' workspace.' . "\n\n";
                     
            $message.= ($additionalMessage) ? "$additionalMessage\n\n" : '';
            
            $message.= "To join this workspace, please click on the \"Accept Invite\" below.\n\n"
                     . '<a href="' . $confirmationUrl . '?inviteKey=' . $inviteKey . '">Accept Invite</a>' . "\n\n"
                     . 'If you can\'t click on the link above, please cut and paste the full "confirmation link" below '
                     . "into your browser and enter your \"invite key\" when prompted.\n\n"
                     . 'Confirmation Link: ' . $confirmationUrl . "\n"
                     . 'Invite key: ' . $inviteKey . "\n\n";
            
            // send invite
            require_once 'Zend/Mail.php';
            
            $mail = new Zend_Mail();
            $mail->setFrom($config->email->fromEmail, $config->email->fromName);
            $mail->addTo($this->email);
            $mail->setSubject($subject);
            $mail->setBodyHtml(nl2br($message));
            $mail->setBodyText(strip_tags($message));
            $mail->send();
            
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }
        
        $this->commit();
        return true;
    }

    /**
     * Accept workspace invite
     *
     * @param   string $inviteKey
     * @return  boolean
     * @throw   Exception
     */
    public function acceptWorkspaceInvite($inviteKey)
    {
        // fetch invite and workspace
        try {
            $workspaceInviteModelOptions = $this->_modelOptions();
            $workspaceInviteModelOptions['deleteMethod'] = self::MODEL_DELETE_METHOD_PHYSICAL;
            
            $workspaceInvite = Ovoyo_Model::factory(
                'Ovoyo_Application_Hosted_WorkspaceInvite', 
                $workspaceInviteModelOptions
            );
            
            if (!$workspaceInvite->fetchBy(array('inviteKey' => $inviteKey))) {
                Ovoyo_ErrorHandler::addError('Supplied invite key is invalid, please try again');
                return false;
            }
            
            // check that the invite hasn't expired
            if ($workspaceInvite->expires
                && $workspaceInvite->expires != '0000-00-00 00:00:00') {
                $now = new Zend_Date;
                $expires = new Zend_Date($workspaceInvite->expires);
                
                if ($now->isLater($expires)) {
                    Ovoyo_ErrorHandler::addError('Supplied invite key has expired');
                    return false;
                }
            }
            
            // fetch workspace
            $workspace = Ovoyo_Model::factory(
                'Ovoyo_Application_Hosted_Workspace', 
                $this->_modelOptions()
            );
            
            if (!$workspace->fetch($workspaceInvite->workspaceId)) {
                 return false; 
            }
            
        } catch (Exception $e) {
            throw $e;
        }

        // assign user to workspace and delete invite
        $this->beginTransaction();
        try {
            $workspace->assignUser($workspaceInvite->userId);
            $workspaceInvite->delete();
            
            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }
        
        return true;
    }
    
    /**
     * Create workspace invite key
     *
     * @return string a random key, 10 charactes in length
     */
    private final function _createWorkspaceInviteKey()
    {
        $inviteKey = '';

        srand((double) microtime() * 1000000);

        for ($n = 0; $n < 10; $n++) {
            $varchar = rand(1,3);
            switch ($varchar) {
                case 1: $inviteKey.= chr(rand(97,122));
                        break;
                case 2: $inviteKey.= chr(rand(48,57));
                        break;
                case 3: $inviteKey.= chr(rand(65,90));
                        break;
            }
        }

        return $inviteKey;
    }
    
    /**
     * Set the list of roles a user is assigned to
     *
     * @param   array $roles List of role ids that user is assigned to
     * @param   int $workspaceId
     * @return  boolean
     * @throws  Exception
     */
    public function setRoles(array $roles, $workspaceId)
    {
        // check for user id
        if (!$this->userId) {
            Ovoyo_ErrorHandler::addError('No user specified');
            return true; 
        }
        
        // check we have a workspace id
        if (!$workspaceId) {
            Ovoyo_ErrorHandler::addError('No workspace specified');
            return false;
        }
    
        // if user is a superuser, then just return true
        if ($this->superuser) { 
            return true; 
        }
        
        $this->beginTransaction();
        try {
            // remove existing roles
            $options = $this->_modelOptions();
            $options['deleteMethod'] = self::MODEL_DELETE_METHOD_PHYSICAL;
            $userRole = Ovoyo_Model::factory(
                'Ovoyo_Application_Hosted_UserRole', $options
            );
            
            $deleteBy = array(
                'userId' => $this->userId,
                'workspaceId' => $workspaceId
            );
            if (!$userRole->deleteBy($deleteBy)) {
                $this->rollback();
                return false;
            }
            
            // insert new roles
            $userRole->userId = $this->userId;
            $userRole->workspaceId = $workspaceId;
            foreach ($roles AS $role) {
                $userRole->role = $role;
                if (!$userRole->insert()) {
                    $this->rollback();
                    return false;
                }
            }
            
            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }
         
        return true;
    }
    
    /**
     * Get roles user is assigned to
     *
     * @return  array
     */
    public function getRoles()
    {
        if (isset($this->_roles)) {
            return $this->_roles;
        }
        
        $this->_roles = array();
        
        $workspaceId = Ovoyo_WebContainer::getInstance()->getWorkspaceId();
        if (!$workspaceId) {
            return $this->_roles;
        }
        
        // get available roles
        $acl = Ovoyo_Application_Security::getAcl();
        $availableRoles = array_flip($acl->getRoles());
        
        $userRole = Ovoyo_Model::factory(
            'Ovoyo_Application_Hosted_UserRole', $this->_modelOptions()
        );
        $searchFilter = array(
            'userId' => $this->userId,
            'workspaceId' => $workspaceId
        );
        
        $results = $userRole->search($searchFilter)->fetchAll(
            Zend_Db::FETCH_OBJ
        );
        
        foreach ($results AS $row) {
            if (!array_key_exists($row->role, $availableRoles)) {
                continue;
            }
            $this->_roles[$row->role] = $row->role;
        }

        return $this->_roles;
    } 
}
