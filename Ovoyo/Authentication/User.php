<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_ErrorHandler
 */
require_once 'Ovoyo/ErrorHandler.php';

/**
 * @see Ovoyo_Authentication_Interface_User
 */
require_once 'Ovoyo/Authentication/Interface/User.php';

/**
 * User class
 *
 * @package    Ovoyo_Authentication
 */
class Ovoyo_Authentication_User
    implements Ovoyo_Authentication_Interface_User
{
    public $email;
    public $password;
    public $firstName;
    public $lastName;
    public $superuser;
    
    public $roles;
    
    protected $_hashType;
    protected $_userSession;
    protected $_hasRoleCache = array();
    
    /**
     * Set hash algorithm to use when encrypting/decrypting passwords
     *
     * @param   string $hashType
     * @return  void
     */
    public function setHashType($hashType)
    {
        $this->_hashType = $hashType;
    }
    
    /**
     * Set user session instance
     *
     * @param   Ovoyo_Authentication_UserSession $userSession
     * @return  void
     */
    public function setUserSession($userSession)
    {
        $this->_userSession = $userSession;
    }
    
    /**
     * Fetch user
     *
     * @param   string $email
     * @return  boolean
     */
    public function fetch($email)
    {
        if (!$user = Ovoyo_Application_Security::getUser($email)) {
            return false;
        }
        
        $this->email     = $user['email'];
        $this->password  = $user['password'];
        $this->firstName = $user['firstname'];
        $this->lastName  = $user['lastname'];
        $this->superuser = ($user['superuser']) ? 1 : 0;
        $this->roles = (is_array($user['roles'])) ? $user['roles']
                                                  : array();
                                                  
        return true;
    }

    /**
     * Get user's full name
     *
     * @return
     */
    public function name()
    {
        $name = '';
        $name = $this->firstName;
        if ($this->lastName) {
            $name.= ($name) ? " $this->lastName" : $this->lastName;
        }
        return $name;
    }
    
    /**
     * Perform login
     *
     * @param   string $email Email
     * @param   string $password Password
     * @param   string $sessionIdentifier Session identifier
     * @return  boolean
     */
    public function login($email, $password, $sessionIdentifier)
    {
        
        // error check
        if (!$email || !$password) {
            $error = 'You must provide both email address and password';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }
        
        // user not found
        if (!$user = Ovoyo_Application_Security::getUser($email)) {
            $error = 'You provided an incorrect email address or password';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }
        
        // validate password
        if (isset($this->_hashType)) {
            $password = hash($this->_hashType, $password);
        }
        if ($password !== $user['password']) {
            $error = 'You provided an incorrect email address or password';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }

        // create new session for user
        return $this->_userSession->startSession($email, $sessionIdentifier);
    }

    /**
     * Perform logout
     *
     * @param   string $sessionIdentifier Session identifier
     * @return  boolean
     */
    public function logout($sessionIdentifier)
    {
        if (!$this->_userSession->end($sessionIdentifier)) {
            return false;
        }

        return true;
    }
    
    /**
     * Check to see if a user has a specific role
     *
     * @param string $role
     * @return boolean
     */
    public function hasRole($role)
    {
        if ($this->superuser) {
            return true;
        }
        
        if (array_key_exists($role, $this->_hasRoleCache)) {
            return $this->_hasRoleCache[$role];
        }
        
        // first check that role is valid within the ACL
        $acl = Ovoyo_Application_Security::getAcl();
        if (!$acl->hasRole($role)) {
            $this->_hasRoleCache[$role] = false;
            return false;
        }
        
        // now check role has been assigned to user
        $hasRole = false;
        foreach ($this->roles AS $userRole) {
            if ($userRole == $role) {
                $hasRole = true;
                break;
            }
            
            if ($acl->inheritsRole($userRole, $role)) {
                $hasRole = true;
                break;
            }
        }
        
        $this->_hasRoleCache[$role] = $hasRole;
        return $hasRole;
    }
    
    /**
     * Pre login actions
     *
     * @return void
     */
    public function preLogin()
    {}
    
    /**
     * Post login actions
     *
     * @return void
     */
    public function postLogin()
    {}
    
    /**
     * Pre logout actions
     *
     * @return void
     */
    public function preLogout()
    {}
    
    /**
     * Post logout actions
     *
     * @return void
     */
    public function postLogout()
    {}
}
