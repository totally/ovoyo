<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Simple error handler
 *
 * Implements the Singleton pattern as their should only ever
 * be a single error handler instance
 *
 * @package    Ovoyo_ErrorHandler
 */
class Ovoyo_ErrorHandler
{
    // object instance
    private static $_instance;

    private $_errors = array();
    private $_errorFields = array();
    private $_debug  = false;
    private $_removeDuplicates  = true;

    /**
     * Constructor
     *
     */
    public function __construct()
        {
        }

    /**
     * Get error handler instance
     *
     * @return  obj Ovoyo_WebContainer instance
     */
    private static function _getInstance()
    {
        if (self::$_instance === null) self::$_instance = new self;
        return self::$_instance;
    }

    /**
     * Get error handler instance
     *
     * @return  obj Ovoyo_WebContainer instance
     */
    public static function getInstance()
    {
        return self::_getInstance();
    }

    /**
     * Add error
     *
     * @param   string $message Error message
     * @param   string $debugInfo Additional debug information
     * @param   array $backtrace Backtrace info
     * @return  void
     */
    public static function addError($message, $debugInfo = '', $backtrace = array())
    {
        $instance = self::_getInstance();
        $instance->_errors[] = array('message'   => $message,
                                     'debugInfo' => $debugInfo,
                                     'backtrace' => $backtrace);
    }
    
    // add error fields
    public static function addErrorFields($field)
    {
        $instance = self::_getInstance();
        $instance->_errorFields[$field] = $field;
    }

    /**
     * Add exception
     *
     * @param   obj $exception Exception to add
     * @return  void
     */
    public static function addException($exception)
    {
        Ovoyo_ErrorHandler::addError(
            $exception->getMessage(),
            $exception->getTraceAsString(),
            $exception->getTrace()
        );
    }

    /**
     * Check if any errors have been recorded
     *
     * @return  bool
     */
    public static function anyErrors()
    {
        $instance = self::_getInstance();
        return (count($instance->_errors) > 0) ? true : false;
    }
    
    /**
     * In here for legacy purposes when using old error handler
     *
     * @deprecated
     * @return  function
     */
    public static function any()
    {
        return self::anyErrors();
    }

    /**
     * Reset error handler
     *
     * @return  void
     */
    public static function reset()
    {
        $instance = self::_getInstance();
        $instance->_errors = array();
    }

    /**
     * Switch on debug mode
     *
     * @return  void
     */
    public static function debugOn()
    {
        $instance = self::_getInstance();
        $instance->_debug = true;
    }

    /**
     * Switch off debug mode
     *
     * @return  void
     */
    public static function debugOff()
    {
        $instance = self::_getInstance();
        $instance->_debug = false;
    }
    
    /**
     * Set value of remove duplicates flag
     *
     * @param boolean $removeDuplicates
     * @return void
     */
    public static function setRemoveDuplicates($removeDuplicates)
    {
        $instance = self::_getInstance();
        $instance->_removeDuplicates = $removeDuplicates;
    }
    
    /**
     * Remove duplicate errors
     *
     * @return void
     */
    private static function _removeDuplicates()
    {
        $instance = self::_getInstance();
        $current = $instance->_errors;
        
        self::reset();
        
        $new = array();
        foreach ($current AS $error) {
            $key = Zend_Json::encode($error);
            if (array_key_exists($key, $new)) {
                continue;
            }
            
            $new[$key] = $error;
            
            self::addError(
                $error['message'], $error['debugInfo'], $error['backtrace']
            );
        }
    }

    /**
     * Display errors
     *
     * If $reset is set to true, errors will be reset after being displayed
     * If $suppressHtml is set to true errors will be returned in text format
     *
     * @param   boolean $reset Reset errors
     * @param   boolean $suppressHtml Suppress HTML
     * @return  void
     */
    public static function displayErrors($reset = true, $suppressHtml = false)
    {
        echo Ovoyo_ErrorHandler::getErrorsString($reset, $suppressHtml);
    }
    
    /**
     * Get errors string
     *
     * If $reset is set to true, errors will be reset after being displayed
     * If $suppressHtml is set to true errors will be returned in text format
     *
     * @param   boolean $reset Reset errors
     * @param   boolean $suppressHtml Suppress HTML
     * @return  void
     */
    public static function getErrorsString($reset = true, $suppressHtml = false)
    {
        $instance = self::_getInstance();
        if ($instance->_removeDuplicates) {
            self::_removeDuplicates();
        }

        $errorString = '<ul class="errors">';
        foreach ($instance->_errors AS $error) {
            $errorString.= '<li>';
            $errorString.= ($suppressHtml) ? $error['message']
                                           : nl2br($error['message']);

            if ($instance->_debug && $error['debugInfo']) {
                $errorString.= ' (';
                $errorString.= ($suppressHtml) ? $error['debugInfo']
                                               : nl2br($error['debugInfo']);
                $errorString.= ')';
            }
            
            $errorString.= '</li>';

            // always add a \n here in case HTML is suppressed
            $errorString.= "\n";
        }
        $errorString.= '</ul>';

        $errorString = ($suppressHtml) ? strip_tags($errorString)
                                       : $errorString;
                        
        if ($reset) {
            self::reset();
        }
        
        return $errorString;
    }
    
    /**
     * Get errors
     *
     * @return  array
     */
    public static function getErrors()
    {
        $instance = self::_getInstance();
        if ($instance->_removeDuplicates) {
            self::_removeDuplicates();
        }
        
        return $instance->_errors;
    }
    
    /**
     * Return an array of errors
     *
     * @return  array
     */
    public static function getErrorFields()
    {
        $instance = self::_getInstance();
        
        return $instance->_errorFields;
    }

    /**
     * Apply some magic function calls
     *
     * This only really here so error handler is backwards compatible with
     * some old Totally code
     *
     * @param string $method Name of method
     * @param array $arguments List of arguments to pass to method
     * @return result of function call
     * @throws Exception
     */
    public function __call($method, $arguments)
    {
        try {
            // old add method
            if ($method == 'add') {
                return call_user_func_array(array(self, 'addError'), $arguments);

            // old load errors method
            } else if ($method == 'load_errors') {
                return;
            }

            // else throw error as method does not exist
            throw new Exception("method $method not found");
        } catch (Exception $e) {
            throw $e;
        }
    }
}
