<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */


/**
 * @see Zenk_View_Helper_FormElement
 */
require_once 'Zend/View/Helper/FormElement.php';

/**
 * Display a nicely layed out set of checboxes
 *
 * @package    Ovoyo_View
 */
class Ovoyo_View_Helper_FormFileImage extends Zend_View_Helper_FormElement
{
    /**
     * Constructor
     *
     * Possible options are:
     *
     * - 'name' string name to use for file input
     * - 'options' array holding additional shared attributes for image and file input:
     *
     * @param   string $name Name of file input
     * @param   array $options Optional array holding options for 'assigned' and 'unassigned' selects
     * @return  void
     */
    public function formFileImage($name, $value = null, array $options)
        {
            $view = new Ovoyo_View;

        // set up default values
        $imgId = ($options['imgId']) ? $options['imgId'] : $name . 'Img' ;
        $imgConatiner = ($options['imgConatiner']) ? $options['imgConatiner'] : $name . 'TblContainer' ;
        
        $html = '<table id="' . $imgConatiner . '" cellpadding="0" cellspacing="0" style="width: auto;' . (($options['hideImage']) ? 'display: none' : '') . '">
                         <tr>';
        if (!$options['noImage']) {
            $html .= '<td>
                          <img id="' . $imgId . '" src="' . $options['imgSrc'] . '" class="' . $options['imgClass'] . '" />
                      </td>';
        }
           
            
        if ($options['fileName']) {
            $html .= '<td style="padding-right: 5px;">';
            $html .= ($options['fileSrc']) ? '<a href="' . $options['fileSrc'] . '" target="_blank">' : '';
            $html .= $options['fileName'];
            $html .= ($options['fileSrc']) ? '</a>' : '';
            $html .= '</td>';
        }
            
        if ($options['includeDelete']) {
            // set up default values
            $deleteId = ($options['deleteId']) ? $options['deleteId'] : $name . 'Delete';
            $deleteClass = ($options['deleteClass']) ? $options['deleteClass'] : '';
            $deleteText = ($options['deleteText']) ? $options['deleteText'] : 'Delete Image';
            
            $html .= '<td valign="bottom">
                          <a href="javascript:void(0);" title="' . $deleteText . '" id="' . $deleteId . '" class="' . $deleteClass . '">';
            if ($options['deleteIconSrc']) {
                $html .= '<img src="' . $options['deleteIconSrc'] . '" border="0" />';                        
            } else {
                $html .= $deleteText;
            }
            
            $html .= '    </a>
                      </td>';
        }
        
        $html .= '
             </tr>
        </table>';
        
        $html .= '<input id="' . $name . '" type="file" name="' . $name . '" class="' . $options['class'] . '"';
        if ($options['disabled']) {
            $html .= ' disabled="disabled"';    
        }
        
        $html .= ' />';
        return $html;
    }
}