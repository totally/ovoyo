<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Ovoyo Profiler class
 *
 * Some simple profiling functions, for debugging purposes.  Very basic at the
 * moment, and all output is generated in-line.
 * Note that there will not be any real impact on speed if you leave profiling
 * statements in live code.  Providing that Ovoyo_Profiler::start() is not 
 * called, any calls to output() will do nothing, and the amount of overhead
 * that they generate will be negligible (it takes something like 1 million
 * output() statements before you see 0.01ms added to the processing time). 
 *  
 * Usage:
 * 
 * // Start profiler.  All times logged will be relative to this point:
 *     Ovoyo_Profiler::start()
 * 
 * // Use output() to print profiling statements.  Your message will be
 * // printed, along with a time-stamp.  They are output using
 * // print(), but wrapped in <pre> tags to improve readability.
 *     Ovoyo_Profiler::output("Entering slow for loop.");
 *     foreach (...) {
 * 
 *     }
 *     Ovoyo_Profiler::output("End of for loop.");
 * 
 * // You can leave profiling statements in code, and just suppress their
 * // output, by stopping the profiler.  It can be started again by calling
 * // start() - this won't reset the timer.
 *     Ovoyo_Profiler::stop();
 *     Ovoyo_Profiler::output("This won't be output.");
 *     Ovoyo_Profiler::start();    // Timer won't be reset.
 *
 * // You can reset the timer at any point, by calling reset().
 *     Ovoyo_Profiler::reset();
 * 
 * 
 * @package    Ovoyo_Profiler
 */
class Ovoyo_Profiler
{
    protected static $startTime;
    protected static $isRunning = false;
    
    /**
    * Get current time stamp.  We use this function as a wrapper,
    * so it is easy to modify in the future (e.g. to get user time
    * instead of system time, or to perform early rounding).
    *
    * @return float
    */
    protected static function _generateTimeStamp()
    {
        return microtime(true);
    }

    /**
    * Starts the profiler.
    * Must be called prior to any output() calls, otherwise the output
    * will be silently discarded.
    * Will set the timer if this is the first time it is called, but will
    * not reset the timer on subsequent calls.
    *
    * @return void
    */
    public static function start()
    {
        if (!self::$isRunning) {
            self::$isRunning = true;
            if (self::$startTime === NULL) {
                self::$startTime = self::_generateTimeStamp();
                self::output("STARTED " . __CLASS__);
            } else {
                self::output("RESTARTED " . __CLASS__);
            }
        }
    }

    /**
     * Stops the profiler.
     * Places the profiler into a 'stopped' state, where all output() is
     * discarded.  It may be restarted by calling start().
     *
     * @return void
     */
    public static function stop()
    {
        self::output("STOPPED " . __CLASS__);
        self::$isRunning = false;
    }
    
    /**
     * Resets the timer.  Profiler does not need to be running.
     *
     * @return void
     */
    public static function reset()
    {
        self::output("TIMER RESET " . __CLASS__);
        self::$startTime = self::_generateTimeStamp();
    }
    
    /**
    * Prints the specified message to the PHP output stream,
    * along with the current profile time (i.e. the length of time
    * since reset() was last called).
    * If profiler is not currently running, function does nothing.
    *
    * @param string $msg
    * @return void
    */
    public static function output($msg)
    {
        if (self::$isRunning) {
            $now = self::_generateTimeStamp();
            $runTime = round($now - self::$startTime, 4);
    
            print("<pre>");
            print("[" . sprintf("%1.4f", $runTime) . "] " . $msg . "\n");
            print("</pre>");
        }
    }

}