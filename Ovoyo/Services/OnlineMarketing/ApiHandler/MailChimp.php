<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * MailChimp API handler
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage ApiHandler
 */
class Ovoyo_Services_OnlineMarketing_ApiHandler_MailChimp
    extends Ovoyo_Services_OnlineMarketing_ApiHandler
{
    public $datacenterPrefix;
    public $apiKey;
    public $apiUrl;

    protected $_baseApiParams;


    /**
     * Constructor
     *
     * @param   array $config Config options
     * @return  void
     */
    public function __construct($config)
    {
        // check for required options
        if (!isset($config['apiKey'])) {
            throw new Exception('apiKey is a required config option');
        }

        $apiSplit = explode('-', $config['apiKey']);

        $this->apiKey = $config['apiKey'];
        $this->datacenterPrefix = $apiSplit[1] ? $apiSplit[1] : 'uk1';
        $this->apiUrl = 'http://' . $this->datacenterPrefix
                      . '.api.mailchimp.com/1.3/';

        $this->_baseApiParams = array(
            'apikey' => $this->apiKey,
            'output' => 'php'
        );
    }

    /**
     * Make a call to api
     *
     * @param   array $params Optional params to add as querystring to API url
     * @return  string Response from API call
     * @throws  Exception
     */
    public function callApi($params = null)
    {
        $response = null;

        $apiUrl = $this->apiUrl;

        // add base params
        if (is_array($this->_baseApiParams)) {
            foreach ($this->_baseApiParams AS $param => $value) {
                $params[$param] = $value;
            }
        }

        $url = $apiUrl .'?'. http_build_query($params);

        try {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

            $response = curl_exec($ch);

            curl_close($ch);

            if (Ovoyo_WebContainer::getConfig('application')->enableFirebugLogger) {
                $logger = Zend_Registry::get('firebugLogger');
                $message = "Making SignupTo API call:\n"
                         . ' - url: ' . $apiUrl . "\n"
                         . ' - response: ' . $response ;
                $logger->log($message, Zend_Log::INFO);
            }

        } catch (Exception $e) {
            throw $e;
        }

        return $response;
    }
}