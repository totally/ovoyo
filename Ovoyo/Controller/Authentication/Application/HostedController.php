<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Controller_Authentication_SimpleController
 */
require_once 'Ovoyo/Controller/Authentication/SimpleController.php';

/**
 * @see Ovoyo_Form
 */
require_once 'Ovoyo/Form.php';

/**
 * Controller to take care of security - logging users in and out
 *
 * @package     Ovoyo_Controller
 * @subpackage  Authentication
 */
class Ovoyo_Controller_Authentication_Application_HostedController extends Ovoyo_Controller_Authentication_SimpleController
{
    /**
     * Login action
     */
    public function loginAction()
    {
        // see if we have a user already
        $user = Ovoyo_WebContainer::getInstance()->getUser();

        $form = new Ovoyo_Form;
        $form->setAttrib('id', 'login');

        // decide if we are displaying login or workspace select
        $displayWorkspaceSelect = false;
        if (!$user->userId) {
            $form->addElement('text', 'email', array('label' => 'Email:'));
            $form->addElement('password', 'password', array('label' => 'Password:'));
            $form->addElement('submit', '_login', array('label' => 'Login'));

        } else {
            $this->selectWorkspaceAction();
            return;
        }

        $this->view->assign('form', $form);
    }
    
    /**
     * Select workspace action
     *
     * @return  void
     */
    public function selectWorkspaceAction()
    {
        $user = Ovoyo_WebContainer::getInstance()->getUser();
        if (!$user->userId) {
            $this->loginAction();
            return;
        }
        
        $workspaces = $user->allowedWorkspaces();

        $form = new Ovoyo_Form;
        $form->setAttrib('id', 'login');
        $form->addElement('select', 'workspaceId', array('label' => 'Select Workspace:'));
        $workspaceElement = $form->getElement('workspaceId');
        $workspaceElement->addMultiOptions($workspaces);
        $form->addElement('submit', '_loginSelectWorkspace', array('label' => 'Select'));

        $this->view->assign('form', $form);
    }
}
