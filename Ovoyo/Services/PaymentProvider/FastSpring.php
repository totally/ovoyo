<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Services_PaymentProvider_Abstract
 */
require_once 'Ovoyo/Services/PaymentProvider/Abstract.php';

/**
 * SagePay class
 *
 * @package    Ovoyo_Services_PaymentProvider
 */
class Ovoyo_Services_PaymentProvider_FastSpring extends Ovoyo_Services_PaymentProvider_Abstract
{
    const FS_ORDER_URL = 'http://sites.fastspring.com/:storeName/api/order';
    const FS_API_URL = 'https://api.fastspring.com';

    const FS_TRANS_SUCCESS = 'OK';
    const FS_TRANS_CANCELLED = 'ABORT';
    const FS_TRANS_FAILED = 'NOTAUTHED';

    const FS_TYPE_PAYMENT = 'PAYMENT';
    
    // Field names
    const FS_FIELD_REFERRER = 'referrer';
    const FS_FIELD_OPERATION = 'operation';
    const FS_FIELD_DESTINATION = 'destination';
    const FS_FIELD_MODE = 'destination';
    const FS_FIELD_CURRENCY = 'currency';
    const FS_FIELD_AMOUNT = 'amount';
    const FS_FIELD_TAGS = 'tags';
    
    // Contact field names
    const FS_FIELD_CONTACT_FIRST_NAME = 'contact_fname';
    const FS_FIELD_CONTACT_LAST_NAME = 'contact_lname';
    const FS_FIELD_CONTACT_COMPANY = 'contact_company';
    const FS_FIELD_CONTACT_EMAIL_ADDRESS = 'contact_email';
    const FS_FIELD_CONTACT_TELEPHONE = 'contact_phone';

    // Product field name
    const FS_FIELD_PRODUCT_QUANTITY = '_quantity';
    const FS_FIELD_PRODUCT_PATH = '_path';
    
    // Custom field names
    const FS_FIELD_RETURN_URL = 'return_url';

    // Field values
    const FS_FIELD_VALUE_CURRENCY_GBP = 'GBP';
    const FS_FIELD_VALUE_CURRENCY_USD = 'USD';
    const FS_FIELD_VALUE_CURRENCY_EUR = 'EUR';
    
    const FS_FIELD_VALUE_OPERATION_CREATE = 'create';
    const FS_FIELD_VALUE_OPERATION_UPDATE = 'update';
    
    const FS_FIELD_VALUE_DESTINATION_CONTENTS = 'contents';
    const FS_FIELD_VALUE_DESTINATION_CHECKOUT = 'checkout';
    
    const FS_FIELD_VALUE_MODE_TEST = 'test';

    // status fields
    const FS_ORDER_STATUS_OPEN = 'open';
    const FS_ORDER_STATUS_REQUEST = 'request';
    const FS_ORDER_STATUS_REQUESTED = 'requested ';
    const FS_ORDER_STATUS_ACCEPTANCE = 'acceptance';
    const FS_ORDER_STATUS_ACCEPTED = 'accepted';
    const FS_ORDER_STATUS_FULFILLMENT = 'fulfillment';
    const FS_ORDER_STATUS_FULFILLED = 'fulfilled';
    const FS_ORDER_STATUS_COMPLETED = 'completed';
    const FS_ORDER_STATUS_COMPLETION = 'completion';
    const FS_ORDER_STATUS_CANCELLED = 'canceled';
    const FS_ORDER_STATUS_FAILED = 'failed';
    
    private $_unsuccessfullPaymentStatuses = array(
        self::FS_ORDER_STATUS_CANCELLED,
        self::FS_ORDER_STATUS_FAILED
    );

    /**
     * Store name
     * @var string
     */
    protected $_storeName = '';
    
    /**
     * Payment url
     * @var string
     */
    protected $_paymentUrl = '';
    
    /**
     * Payment url
     * @var string
     */
    protected $_paymentType = self::FS_TYPE_PAYMENT;

    /**
     * API Username
     * @var string
     */
    protected $_apiUsername = '';

    /**
     * API Password
     * @var string
     */
    protected $_apiPassword = '';

    /**
     * the currency
     * @var string
     */
    protected $_currency = self::FS_FIELD_VALUE_CURRENCY_GBP;

    /**
     * operation
     * @var string
     */
    protected $_operation = self::FS_FIELD_VALUE_OPERATION_CREATE;
    
    /**
     * destination
     * @var string
     */
    protected $_destination = self::FS_FIELD_VALUE_DESTINATION_CONTENTS;
    
    /**
     * test mode
     * @var string
     */
    protected $_testMode = '';
    
    /**
     * server imp
     * @var string
     */
    protected $_serverImp = '';
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::_init()
     */
    protected function _init()
    {
        if ($this->_storeName) {
            if($this->_paymentType == 'API') {
                $this->_paymentUrl = self::FS_API_URL . '/' . $this->_storeName;
            } else {
                $this->_paymentUrl = str_replace(":storeName", $this->_storeName, self::FS_ORDER_URL);
            }
        }
        
        
        if($this->_useTestPayment) {
            $this->_testMode = true;
        }
    }

    /**
     * Sets up a payment, ready to send to Protx
     *
     * @access public
     * @param string form id
     * @param string text to appear on submit button if javascript is not enabled
     * @return mixed string The HTML form
     */
    protected function _createPaymentForm($vars)
    {
        $form = new Ovoyo_Form();
        $form->setAttrib('id', 'fastSpringForm')
             ->setAttrib('name', 'fastSpringForm')
             ->setAttrib('action', $this->_paymentUrl)
             ->setMethod('post');

        $form->addElement('hidden', self::FS_FIELD_OPERATION,
                          array('value' => $this->_operation,
                                'required' => true
                          ));

        $form->addElement('hidden', self::FS_FIELD_DESTINATION,
                          array('value' => $this->_destination,
                                'required' => true
                          ));
        
        // Add param if is test mode
        if ($this->_testMode) {
            $form->addElement('hidden', self::FS_FIELD_MODE,
                array('value' => self::FS_FIELD_VALUE_MODE_TEST,
                      'required' => true
            ));
        }
        

        foreach ($vars as $key => $value) {
            if ($key==self::FS_FIELD_AMOUNT) {
                continue;
            } else {
                $form->addElement('hidden', $key,
                array('value' => $value
                ));
            }
        }
        
        // Set value for tags
        $tagValue = "";
        
        //handle amounts and currencies here
        if ($vars[self::FS_FIELD_CURRENCY] == self::FS_FIELD_VALUE_CURRENCY_USD) {
            $tagValue.= "total_usd=" . $vars[self::FS_FIELD_AMOUNT];
        } else {
            $lcCurrency = strtolower($vars[self::FS_FIELD_CURRENCY]);
            $tagValue.= "total_usd=101,total_" . $lcCurrency . "=" . $vars[self::FS_FIELD_AMOUNT];
        }
        
        if ($vars[self::FS_FIELD_RETURN_URL]) {
            $tagValue.= ($tagValue) ? ',' : '';
            $tagValue.= 'return_url=' . $vars[self::FS_FIELD_RETURN_URL];
        }
        
        if (is_array($vars[self::FS_FIELD_TAGS]) && count($vars[self::FS_FIELD_TAGS])>1) {
            $count = 0;
            $tagValue.= ($tagValue) ? ',' : '';
            foreach($vars[self::FS_FIELD_TAGS] as $key => $value) {
                $tagValue.= $key . '=' . $value;
                $count++;
                $tagValue.= ($count != count($vars[self::FS_FIELD_TAGS])) ? : '';
            }
        }
        
        // Add tags field to form
        $form->addElement('hidden', 'tags',
            array('value' => $tagValue
        ));
        
        return $form;
    }

    /**
     * send a payment off to a provider
     *
     * @param array $vars The payment info
     * @param object instanceof Ovoyo_Controller
     * @return void Callback is provided through sagepay
     */
    public function sendPayment(array $vars, $controller = null)
    {
        if($controller) {
            $renderer = $controller->getHelper('viewRenderer');
            $renderer->setNoRender(true);
        }

        //$this->_setRequestFields($vars);

        $script = '<script type="text/javascript">'."\n";
        $script .= 'document.getElementById("fastSpringForm").submit();'."\n";
        $script .= '</script>'."\n";

        $form = $this->_createPaymentForm($vars);
        if(!$form) {
            return false;
        }
        if($controller) {
            $view = $controller->view;
            $controller->getResponse()->setBody($form->render($view) . $script);
        } else {
            echo $form->render() . $script;
            exit;
        }
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_Abstract::validateCallback()
     */
    public function validateCallback($data = array())
    {
        $valid = false;
    
        if (array_key_exists('transStatus', $data)) {
            $valid = true;
        }
    
        return $valid;
    }

    /**
     * handle a callback from payment provider
     *
     * @param array $data callback data
     * @return bool
     * @throws Ovoyo_Services_PaymentProvider_Exception
     */
    public function handleCallback(array $data)
    {
        list($serverImp, $timeStamp, $paymentId) = explode("-", $data['OrderReferrer']);

        // fetch the payment
        $payment = Ovoyo_Services_PaymentProvider::getPayment(
            array('connName' => $this->_connName)
        );
        if (!$paymentId || !$payment->fetch($paymentId)) {
            return false;
        }
        
        if ($this->_serverImp && $serverImp != $this->_serverImp) {
            return false;
        }
        
        $this->paymentId = $payment->paymentId;

        // update payment details
        $payment->status = $data['OrderStatus'];
        $payment->reference = $data['OrderReference'];
        $payment->callbackData = $data;
        $payment->paid = (!in_array($data['OrderStatus'], $this->_unsuccessfullPaymentStatuses)) ? 1 : 0;
        $payment->paidDate = new Zend_Date;

        if (!$payment->update()) {
            return false;
        }

        $this->reference = $payment->reference;
        $this->subscriptionReference = $payment->callbackData['SubscriptionReference'];
        $this->amount = $payment->amount;
        $this->details = $payment->details;
        $this->paymentId = $payment->paymentId;

        return $payment->paid;

    }

    /**
     * Complete the payment
     * @return void
     */
    public function completePayment()
    {

    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::processPaymentInfo()
     */
    public function processPaymentInfo(array $data)
    {
        $callbackData = $this->handleCallback($data);
        $payment = Ovoyo_Services_PaymentProvider::getPayment(
            array('connName' => $this->_connName)
        );
        if ($payment->fetch($callbackData['paymentId'])) {
            $payment->complete($callbackData['success'],
                $callbackData['transactionId'], $callbackData['status']
            );
            $result = array();
            $result['paymentId'] = $callbackData['paymentId'];
        } else {
            $result = false;
        }
        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::postConvertToOrder()
     */
    public function postConvertToOrder($config)
    {
        if($config['paymentCompleteUrl'] && !headers_sent()) {
            header('Location: ' . $config['paymentCompleteUrl'] . '/');
            exit;
        }
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::prepareData()
     */
    public function prepareData(array $data)
    {
        $contactNumber = ($data['telephone']) ? $data['telephone'] : $data['mobile'];
        
        // building the unique string to be passed as the internal trans id
        $referrer = $this->_serverImp . '-' . time() . '-' . $data['paymentId'];

        $preparedData = array(
            self::FS_FIELD_REFERRER                   => $referrer,
            self::FS_FIELD_RETURN_URL                 => $data['returnUrl'],
            // Fast string only accepts amounts in pence!
            self::FS_FIELD_AMOUNT                     => $data['amount'] * 100,
            self::FS_FIELD_CURRENCY                   => $this->_currency,
            self::FS_FIELD_CONTACT_FIRST_NAME         => $data['firstName'],
            self::FS_FIELD_CONTACT_LAST_NAME          => $data['lastName'],
            self::FS_FIELD_CONTACT_EMAIL_ADDRESS      => $data['email'],
            self::FS_FIELD_CONTACT_COMPANY            => $data['company'],
            self::FS_FIELD_CONTACT_TELEPHONE          => $contactNumber,
        );
        
        // Deal with products - may need linking in with shopping basket at a later point
        if (is_array($data['products']) && count($data['products'])) {
            $count = 0;
            foreach ($data['products'] as $product) {
                $count++;
                $preparedData['product_' . $count . self::FS_FIELD_PRODUCT_PATH] = '/' . $product['name'];
                $preparedData['product_' . $count . self::FS_FIELD_PRODUCT_QUANTITY] = $product['quantity'];
            }
        } elseif ($data['product']) {
            $preparedData['product_1' . self::FS_FIELD_PRODUCT_PATH] = '/' . $data['product']['name'];
            $preparedData['product_1' . self::FS_FIELD_PRODUCT_QUANTITY] = $data['product']['quantity'];
        }
        
        return $preparedData;
    }

}