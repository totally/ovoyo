<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Shell functions
 *
 * @package Ovoyo_Utilities
 */
class Ovoyo_Utilities_Shell
{

    /**
     * Apply an sql file to a workspace database
     *
     * @param   string $sqlFile Path to sql file
     * @param   Ovoyo_Db $dbConfig
     * @param   string $context (optional) for error-reporting purposes.
     * @return  boolean
     */
    public static function applySqlFile($sqlFile, $dbConfig, $context = "")
    {
        if (!is_readable($sqlFile)) {
            $error = 'SQL file "' . $sqlFile . '" could not be applied to '
                   . 'workspace because it is not readable';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }

        $rawDbConnection = self::getRawDbConnectionString($dbConfig);

        // Set the path as part of the command, so that the mysql binary
        // can be found on systems where shell_exec() doesn't result in an
        // appropriate path being set (this seems to always be the case on
        // Windows, but may also occur on Unix systems in some cases).
        if (strtolower(substr(PHP_OS, 0, 3)) == "win") {
            $command = "set PATH=" . $_ENV['PATH'] . " & ";
        } else {
            $command = "";
            
            // DISABLED: Currently untested on Unix, and we don't want things
            //           to break!  Re-enable once properly tested.
            //           Note that escapeshellarg() may or may not be required.
//            $command = "export PATH=" . escapeshellarg($_ENV['PATH']) . "; ";
        }
        
        $command .= $rawDbConnection . ' < ' . escapeshellarg($sqlFile);
        
        // Redirect stderr to stdout so that any failures are properly caught.
        // Works on Windows and Unix systems.
        $command .= " 2>&1";
        
        // If there were any errors, report the failure.
        if ($output = shell_exec($command)) {
            if ($context) {
                    $context = ' to ' . $context;
            }
            $error = 'A problem occurred applying SQL file "' . $sqlFile . '"'
                   . $context . ': ' . $output;
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }

        return true;
    }

    /**
     * Get raw database connection string, for use on the command-line
     * (via shell_exec()).
     *
     * @param   Ovoyo_Db $dbConfig
     * @param   boolean $excludeDb
     * @return  string
     */
    public static function getRawDbConnectionString($dbConfig,
                                                    $excludeDb = false)
    {
        // build mysql connection string for executing raw sql
        $mysql = 'mysql --host=' . escapeshellarg($dbConfig->params->host)
               . ' --user=' . escapeshellarg($dbConfig->params->username)
               . ' --password=' . escapeshellarg($dbConfig->params->password);

        if (!$excludeDb) {
            $mysql.= ' --database=' . escapeshellarg($dbConfig->params->dbname);
        }

        return $mysql;
    }

}