<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Application module manager
 *
 * @package    Ovoyo_Application
 */
class Ovoyo_Application_ModuleManager 
{
    // object instance
    protected static $instance;
    
    protected $_enabled = false;
    protected $_modules = array();
    protected $_moduleConfigs = array();
    
    const DEFAULT_MODULE = 'default';
    
    /**
     * Get instance
     *
     * @return obj Ovoyo_WebContainer instance
     */
    public static function getInstance()
    {
        if (self::$instance === null) self::$instance = new self;
        return self::$instance;
    }
    
    /**
     * Initialise module manager
     * 
     * Involves scanning directories within the modules directory and adding
     * any valid modules to the list of controller directories
     *
     * @return  void
     */
    public static function initialise()
    {
        $manager = self::getInstance();

        $webContainer = Ovoyo_WebContainer::getInstance();
        
        $modulesDirectory = $webContainer->getParam('modulesDirectory');
        if (is_dir($modulesDirectory)) {
            $manager->_enabled = true;
            $contents = scandir($modulesDirectory);
            foreach ($contents AS $item) {
                if (preg_match('/^\./', $item)) { 
                    continue; 
                }
                
                $itemPath = $modulesDirectory . DIRECTORY_SEPARATOR . $item;
                if (!is_dir($itemPath)) { 
                    continue; 
                }
                
                $webContainer->addControllerDirectory(
                    $itemPath . DIRECTORY_SEPARATOR . 'controllers', $item
                );
                $manager->_modules[$item] = $itemPath;
            }
        }
    }
    
    /**
     * Get list of registered modules
     * 
     * @return  array
     */
    public static function getModules()
    {
        $manager = self::getInstance();
        return $manager->_modules;
    }
    
    /**
     * Get module config
     * 
     * @param   string $module
     * @param   Zend_Config
     */
    public static function getModuleConfig($module)
    {
        $manager = self::getInstance();
        if (!$manager->_enabled) {
            return array();
        }
        
        if (!isset($manager->_moduleConfigs[$module])) {
            $modulePath = $manager->_modules[$module];
            $configPath = $modulePath . DIRECTORY_SEPARATOR 
                       . 'config' . DIRECTORY_SEPARATOR;
                       
            $configXml = $configPath . 'config.xml';
            $configIni = $configPath . 'config.ini';
                       
            // xml file
            if (is_readable($configXml)) {
                $manager->_moduleConfigs[$module] = new Zend_Config_Xml(
                    $configXml
                );
                
            // ini file
            } else if (is_readable($configIni)) {
                $manager->_moduleConfigs[$module] = new Zend_Config_Ini(
                    $configIni
                );
                
            // no config found
            } else {
                $manager->_moduleConfigs[$module] = new Zend_Config(array());
            }
        }
        
        return $manager->_moduleConfigs[$module];
    }
    
    /**
     * Check to see if application uses modules or not
     *
     * @return  boolean
     */
    public static function enabled()
    {
        $manager = self::getInstance();
        return $manager->_enabled;
    }

}