<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Zend_Config
 */
require_once 'Zend/Config.php';

/**
 * @see Zend_Config_Ini
 */
require_once 'Zend/Config/Ini.php';

/**
 * @see Zend_Cache
 */
require_once 'Zend/Cache.php';

/**
 * @see Zend_Controller_Front
 */
require_once 'Zend/Controller/Front.php';

/**
 * @see Zend_Controller_Router_Rewrite
 */
require_once 'Zend/Controller/Router/Rewrite.php';

/**
 * @see Ovoyo_DbConnectionManager
 */
require_once 'Ovoyo/DbConnectionManager.php';

/**
 * @see Ovoyo_Application_Security
 */
require_once 'Ovoyo/Application/Security.php';

/**
 * @see Ovoyo_ErrorHandler
 */
require_once 'Ovoyo/ErrorHandler.php';

/**
 * @see Ovoyo_ClassLoader
 */
require_once 'Ovoyo/ClassLoader.php';

/**
 * Web container class
 *
 * The web container handles all requests and acts as the glue between all
 * components in our MVC model.  Elements handled by the container include:
 *
 * - environment setup
 * - holding config information
 * - managing DB connections (pooling at some stage?)
 *
 * The container implements the Singleton pattern as their should only ever
 * be a single web container instance
 *
 * @package
 */
class Ovoyo_WebContainer extends Zend_Controller_Front
{
    public $configs = array();
    public $dbConnectionManager;

    public $cacheDir;
    public $cache;

    protected $_configsDir;
    protected $_modules = array();
    protected $_moduleConfigs = array();


    /**
     * Constructor
     *
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
        $this->dbConnectionManager = Ovoyo_DbConnectionManager::getInstance();
        $this->_configsDir = APPLICATION_ROOT . DIRECTORY_SEPARATOR . 'config';
    }

    /**
     * Get web container instance
     *
     * @return Ovoyo_WebContainer $_instance
     */
    public static function getInstance()
    {
        if (self::$_instance === null) self::$_instance = new self;
        return self::$_instance;
    }

    /**
     * Dispatch an HTTP request to a controller/action.
     *
     * See Zend_Controller_Front for more info
     *
     * @param   Zend_Controller_Request_Abstract|null $request
     * @param   Zend_Controller_Response_Abstract|null $response
     * @return  void|Zend_Controller_Response_Abstract Returns response object if returnResponse() is true
     */
    public function dispatch(Zend_Controller_Request_Abstract $request = null, Zend_Controller_Response_Abstract $response = null)
    {
        // setup cache
        $cacheDir = APPLICATION_ROOT . DIRECTORY_SEPARATOR . 'cache';
        if (is_dir($cacheDir)) {
            $this->cacheDir = $cacheDir;

            // try and setup a Zend_Cache instance
            $cacheDir = $this->cacheDir . DIRECTORY_SEPARATOR . 'zend';
            if (!is_dir($cacheDir)) {
                @mkdir($cacheDir);
            }

            if (is_dir($cacheDir)) {
                $frontendOptions = array('automatic_serialization' => true);
                $backendOptions  = array('cache_dir' => $cacheDir);

                $this->cache = Zend_Cache::factory(
                    'Core', 'File', $frontendOptions, $backendOptions
                );
            }
        }

        try {

            // load configuration options
            $this->loadConfigs();

            // switch on error handler debugging
            if ($this->configs['application']->debug->errors) {
                Ovoyo_ErrorHandler::debugOn();
            }

            // load routes
            $this->_loadRoutes();

            // initialise database connections
            $this->_initialiseDbConnections();

            // initialise modules
            $moduleManager = $this->getModuleManager();
            call_user_func($moduleManager . "::initialise");

            // initialise security
            Ovoyo_Application_Security::initialise();
        } catch (Exception $e) {
            die($e->getMessage());
        }

        /**
         * Instantiate default request object (HTTP version) if none provided
         * (copied from Zend_Controller_Front
         *
         * Need to do this here so we can access the request object during
         * authentication
         */
        if (null !== $request) {
            $this->setRequest($request);
        } elseif ((null === $request) && (null === ($request = $this->getRequest()))) {
            require_once 'Zend/Controller/Request/Http.php';
            $request = new Zend_Controller_Request_Http();
            $this->setRequest($request);
        }

        /**
         * Instantiate default response object (HTTP version) if none provided
         */
        if (null !== $response) {
            $this->setResponse($response);
        } elseif ((null === $this->_response) && (null === ($this->_response = $this->getResponse()))) {
            require_once 'Zend/Controller/Response/Http.php';
            $response = new Zend_Controller_Response_Http();
            $this->setResponse($response);
        }

        $urlPath = $request->getPathInfo();
        $urlPath = rtrim($urlPath, "/");
        $this->setParam('urlPath', $urlPath);
        $this->setParam('explodedUrlPath', explode('/', $urlPath));

        // authenticate user (will only be performed if required)
        try {
            Ovoyo_Application_Security::applyAuthentication();
        } catch (Exception $e) {
            die($e->getMessage());
        }

        // add firebug logger to registry if required
        if ($this->getConfig('application')->enableFirebugLogger) {
            require_once 'Zend/Log.php';
            require_once 'Zend/Log/Writer/Firebug.php';

            $logger = new Zend_Log();
            $writer = new Zend_Log_Writer_Firebug();
            $logger->addWriter($writer);
            Zend_Registry::set('firebugLogger', $logger);
        }

        // add chrome logger to registry if required
        if ($this->getConfig('application')->enableChromeLogger) {
            try {
                require_once 'Ovoyo/Utilities/External/PhpConsole.php';
                PhpConsole::start();
            } catch (Exception $e) {}
        }

        try {
            parent::dispatch($request, $response);
        } catch (Exception $e) {
            if ($this->getParam('env') != 'live') {
                Ovoyo_ErrorHandler::addException($e);
            } else {
                Ovoyo_ErrorHandler::addError('An error occurred, please try again');
            }

            echo Ovoyo_ErrorHandler::getErrorsString();
        }
    }

    /**
     * Load configuration options
     *
     * @return null
     */
    public function loadConfigs()
    {
        // path to config files
        $configsDir = $this->_configsDir;

        if (!is_dir($configsDir)) { return; }

        // get list of files in our config directory
        $files = scandir($configsDir);

        try {
            // load each ini file
            foreach ($files AS $file) {
                if (!preg_match("/.ini$/", $file) || preg_match("/\.local\.ini$/", $file)) {
                    continue;
                }

                // get name of our config
                $configName = preg_replace("/.ini$/", '', $file);
                $configPath = $configsDir . DIRECTORY_SEPARATOR . $file;

                $config = new Zend_Config_Ini(
                    $configPath,
                    $this->getParam('env'),
                    array('allowModifications'=>true)
                );

                // merge in local config
                $localConfigFile = str_replace('.ini', '.local.ini', $configPath);
                if (is_file($localConfigFile)) {
                    $localConfig = new Zend_Config_Ini(
                        $localConfigFile,
                        $this->getParam('env')
                    );
                    $config->merge($localConfig);
                    unset($localConfig);
                }

                $config->setReadOnly();

                $this->configs[$configName] = $config;

                /*$xmlConfigFile = preg_replace("/.ini$/", '.xml', $configPath);
                if (!is_file($xmlConfigFile)) {
                    $toConvert = new Zend_Config_Ini($configPath, null, array('skipExtends' => true, 'allowModifications' => true));
                    $writer = new Zend_Config_Writer_Xml(array('config' => $toConvert, 'filename' => $xmlConfigFile));
                    $writer->write();
                }*/
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


    /**
     * Initialise database connections
     *
     * @return null
     */
    private function _initialiseDbConnections()
    {
        // loop through our configs and add any database connections
        foreach ($this->configs AS $configName => $config) {
            if (!isset($config->database->adapter)) {
                continue;
            }

            $this->dbConnectionManager->addConnection($config->database);
        }
    }

    /**
     * Get module manager
     *
     * @param   boolean $getInstance
     * @return
     */
    public static function getModuleManager($getInstance = false)
    {
        if (isset($this) && $this instanceOf Ovoyo_WebContainer) {
            $webContainer = $this;
        } else {
            $webContainer = self::getInstance();
        }

        if (!$moduleManager = $webContainer->getParam('moduleManager')) {
            $moduleManager = 'Ovoyo_Application_ModuleManager';
            $webContainer->setParam(
                'moduleManager', 'Ovoyo_Application_ModuleManager'
            );
        }

        Ovoyo_ClassLoader::load($moduleManager);

        if ($getInstance) {
            return new $moduleManager;
        } else {
            return $moduleManager;
        }
    }

    /**
     * Return list of modules and their directory location
     *
     * @return  array
     */
    public static function getModules()
    {
        $moduleManager = Ovoyo_WebContainer::getModuleManager();

        return call_user_func($moduleManager . "::getModules");
    }

    /**
     * Load routes
     *
     * @return null
     */
    private function _loadRoutes()
    {
        $router = new Zend_Controller_Router_Rewrite();

        // loop through our configs and add any routing info
        foreach ($this->configs AS $configName => $config) {
            if (!$config->routes) {
                continue;
            }
            $router->addConfig($config, 'routes');
        }

        $this->setRouter($router);
    }

    /**
     * Get a database connection
     *
     * This method provides a short hand way of accessing db connections
     *
     * @param   string $connName Name of connection to get
     * @return  obj Zend_Db adapter if connection exists
     * @throw   Exception
     */
    public static function &getDbConnection($connName = 'default')
    {
        // leave this in for backwards compatibility
        if (isset($this) && $this instanceOf Ovoyo_WebContainer) {
            return $this->dbConnectionManager->getConnection($connName);
        }

        return self::getInstance()->dbConnectionManager->getConnection($connName);
    }

    /**
     * Add a database connection
     *
     * This method provides a short hand way of adding a new db connection
     *
     * @param   obj $config Zend_Config instance holding db connection info
     * @param   boolean $replaceExisting
     * @return  obj Zend_Db adapter if connection exists
     * @throw   Exception
     */
    public static function addDbConnection($config, $replaceExisting = false)
    {
        // leave this in for backwards compatibility
        if (isset($this) && $this instanceOf Ovoyo_WebContainer) {
            return $this->dbConnectionManager->addConnection($config, $replaceExisting);
        }

        return self::getInstance()->dbConnectionManager->addConnection($config, $replaceExisting);
    }

    /**
     * Get config data
     *
     * @param   string $name Config name
     * @return  Zend_Config
     */
    public static function getConfig($name)
    {
        // leave this in for backwards compatibility
        if (isset($this) && $this instanceOf Ovoyo_WebContainer) {
            return $this->configs[$name];
        }

        return self::getInstance()->configs[$name];
    }

    /**
     * Get module config
     *
     * @param   string $module
     * @return  Zend_Config
     */
    public static function getModuleConfig($module = null)
    {
        $moduleManager = Ovoyo_WebContainer::getModuleManager();

        return call_user_func($moduleManager . "::getModuleConfig", $module);
    }

    /**
     * Set config dir
     *
     * @param   string $dir
     * @return  void
     */
    public static function setConfigDir($dir)
    {
        $webContainer = self::getInstance();
        $webContainer->_configsDir = $dir;
    }

    /**
     * Get config dir
     *
     * @return  string
     */
    public static function getConfigDir()
    {
        $webContainer = self::getInstance();
        return $webContainer->_configsDir;
    }

    /**
     * Apply some magic function calls
     *
     * @param   string $method Name of method
     * @param   array $arguments List of arguments to pass to method
     * @return  result of function call
     * @throws  Exception
     */
    public function __call($method, $arguments)
    {
        try {
            // getxxxx()
            if (substr($method, 0, 3) == 'get' && count($arguments) == 0) {
                $var = substr($method, 3);
                $var = substr_replace($var, strtolower(substr($var, 0, 1)), 0, 1);

                if ($this instanceOf Ovoyo_WebContainer) {
                    return $this->getParam($var);
                }

                return self::getInstance()->getParam($var);

                // setxxxx()
            } else if (substr($method, 0, 3) == 'set' && count($arguments) == 1) {
                $var = substr($method, 3);
                $var = substr_replace($var, strtolower(substr($var, 0, 1)), 0, 1);
                if ($this instanceOf Ovoyo_WebContainer) {
                    return $this->setParam($var, $arguments[0]);
                }

                return self::getInstance()->setParam($var, $arguments[0]);

            } else {
                throw new Exception("method $method() not found");
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}

