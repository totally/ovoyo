<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Zend_Controller_Dispatcher_Standard
 */
require_once 'Zend/Controller/Dispatcher/Standard.php';

/**
 * Extension of Zend_Controller_Dispatcher_Standard
 *
 * @package    Ovoyo_Controller
 */
class Ovoyo_Controller_Dispatcher extends Zend_Controller_Dispatcher_Standard
{
    protected $_allowZendControllerSelection = false;
    protected $_controllerClassName = '';
    protected $_controllerUrlPath = '';

    /**
     * Get controller class name
     *
     * Try request first; if not found, try pulling from request parameter;
     * if still not found, fallback to default
     *
     * @param Zend_Controller_Request_Abstract $request
     * @return string|false Returns class name on success
     */
    public function getControllerClass(Zend_Controller_Request_Abstract $request)
    {
        $this->_controllerClassName = '';
        
        $webContainer   = Ovoyo_WebContainer::getInstance();
        $haveController = false;
        $controller     = array();
                               
        /*$controllerDirectories = $this->getControllerDirectory();
        if (!is_array($controllerDirectories)) {
            $controllerDirectories = array($controllerDirectories);
        }*/
        
        $moduleName = $request->getModuleName();
        $moduleDir  = $webContainer->getParam('modulesDirectory');
        $pathInfo   = $request->getPathInfo();
        
        $validModule = $this->isValidModule($moduleName);

        // add special case here module assets
        if ($validModule && $moduleName != $this->_defaultModule) {
            if ($assetsDir = $webContainer->getParam('modulesAssetsDirectory')) {
                $testFile = $moduleDir . preg_replace("/^\/{1,}$moduleName/", "/$moduleName/$assetsDir", $pathInfo);
                if (is_file($testFile)) {
                    require_once 'Ovoyo/MimeType.php';
                    if ($content_type = Ovoyo_MimeType::getMimeType($testFile)) {
                        header("Content-type: $content_type");
                    }
                    readfile($testFile);
                    exit;
                }
            }
        }
        
        $controllerDirs = $this->getControllerDirectory();
        if ($validModule) {
            $this->_curModule    = $moduleName;
            $this->_curDirectory = $controllerDirs[$moduleName];
        } elseif ($this->isValidModule($this->_defaultModule)) {
            $request->setModuleName($this->_defaultModule);
            $this->_curModule    = $this->_defaultModule;
            $this->_curDirectory = $controllerDirs[$this->_defaultModule];
        } else {
            require_once 'Zend/Controller/Exception.php';
            throw new Zend_Controller_Exception('No default module defined for this application');
        }
        
        $controllerDir = $this->_curDirectory;
        $this->_controllerUrlPath = '';
        
        
        // if module name does not exist in path info, then assume we are a here via a forward or some other means
        if ($moduleName && $moduleName != $this->_defaultModule && !stristr($pathInfo, $moduleName)) {
            $this->_allowZendControllerSelection = true;
        }
        
        if (!$this->_allowZendControllerSelection) {
            $pathParts = $this->_formatPathInfo($pathInfo, $moduleName);
            $numParts  = count($pathParts);
            
            $urlPath = $webContainer->getParam('urlPath');
    
            // deal with case when baseUrl is not ''
            $baseUrl = $webContainer->getBaseUrl();
            if ($baseUrl != '' && stristr($urlPath, $baseUrl)) {
                $workingDir = $pathParts;
                $explodedBaseUrl = explode('/', $baseUrl);
                $workingDir = array_slice($workingDir, count($explodedBaseUrl) - 1);
                $workingDir[0] = '';
            }
            
            // check out router to see if we have any matches
            $route = $webContainer->getRouter()->getCurrentRoute();
    
            if (!$haveController && $route) {
                $defaults  = $route->getDefaults();
                $controllerName = (preg_match("/Controller$/", $defaults['controller'])) ? $defaults['controller'] : $defaults['controller'] . 'Controller';
                if (Ovoyo_ClassLoader::exists($controllerName, $controllerDir)) {
                    $haveController = true;
                }
            }
            
            // if no route was found, attempt to find controller based on url
            if (!$haveController) {
                $controllerName = implode('_', $pathParts) . 'Controller';
                $controllerName = preg_replace("/^_/", '', $controllerName);
                array_pop($pathParts);
                for ($i = $numParts - 1; $i >= 0; $i--) {
                   
                    // check for indexController first
                    $indexController = preg_replace('/Controller$/', '_IndexController', $controllerName);
                    #echo " - testing: $indexController, $controllerDir<BR>";
                    if (Ovoyo_ClassLoader::exists($indexController, $controllerDir, true)) {
                        $controllerName = $indexController;
                        $haveController = true;
                        break;
                    }
                    
                    #echo " - testing: $controllerName, $controllerDir<BR>";
                    if (Ovoyo_ClassLoader::exists($controllerName, $controllerDir, true)) {
                        $haveController = true;
                        break;
                    }
                    
                    #echo " - have controller: $haveController<BR><BR>";
    
                    $controllerName = implode('_', $pathParts) . 'Controller';
                    $controllerName = preg_replace("/^_/", '', $controllerName);
                    array_pop($pathParts);
                }
                
                // build controller url path so we have a base for working
                // out the action method
                if (!$haveController) {
                    $this->_controllerUrlPath = '';
                } else {
                    $explodedPath = $webContainer->getParam('explodedUrlPath');
                    $controllerPath = array();
                    for ($j = 0; $j <= $i + 1; $j++) {
                        $controllerPath[] = $explodedPath[$j];
                    }
                    $this->_controllerUrlPath = implode($controllerPath, '/');
                }
            }
            
            // if still no controller found, look for default controller
            #if (!$haveController && ($numParts == 0 || ($numParts == 1 && $moduleName != $this->_defaultModule))) {
            if (!$haveController && ($numParts == 0 || $numParts == 1)) {
                $defaultController = $this->getDefaultControllerClass($webContainer->getRequest());
       
                if (Ovoyo_ClassLoader::exists($defaultController, $controllerDir)) {
                    $controllerName = $defaultController;
                    $haveController = true;
                }
            }
            
            // but request reckons it has one, try that
            if (!$haveController) {
                $controllerName = $request->getControllerName();
                if (Ovoyo_ClassLoader::exists($controllerName, $controllerDir)) {
                    $webContainer->setModuleDirectory($controllerDir);
                    $haveController = true;
                }
            }
        }
        
        //$this->getParam('useDefaultControllerAlways'
        
        if ($haveController) {
            $request->setControllerName($controllerName);
        } else if ($this->_allowZendControllerSelection) {
            $controllerName = ucfirst($request->getControllerName());
            if (!preg_match("/Controller$/", $controllerName)) {
                $controllerName.= 'Controller';
            }
            
        } else {
            $controllerName = '';
        }
        
        $this->_controllerClassName = $controllerName;
        
        return $this->_controllerClassName;
    }
    
    /**
     * Formatted pathInfo from request, and return as exploded array
     *
     * Mainly for use in working out class and variables names from a url path. Takes a directory path
     * and returns an array with the consituent elements all being camelCased
     *
     * @param   string $pathInfo
     * @param   string $moduleName
     * @return  array
     */
    protected function _formatPathInfo($pathInfo, $moduleName = null)
    {
        $formatted = array();
        
        $moduleName = ($moduleName) ? $moduleName : $this->_defaultModule;
        $explodedPath = explode('/', $pathInfo);
        foreach ($explodedPath AS $key => $pathItem) {
            if ($key == 1
                && $pathItem == $moduleName
                && $moduleName != $this->_defaultModule) {
                continue;
            }
            $pathItem = preg_replace("/-/", '_', $pathItem);
            $pathItem = preg_replace("/\W/", '', $pathItem);
            $pathItem = strtolower($pathItem);
            $pieces   = explode('_', $pathItem);

            $formattedPathItem = '';
            foreach ($pieces AS $piece) {
                if (!$piece) { continue; }
                $formattedPathItem.= ucfirst($piece);
            }

            if ($formattedPathItem == '') { continue; }
            $formatted[] = $formattedPathItem;
        }

        return $formatted;
    }
    

    /**
     * Dispatch to a controller/action
     *
     * See Zend_Controller_Dispatcher_Standard for more info.  The only reason for this override
     * is so we can apply a slightly different paradigm for action selection, everything else is
     * the same
     *
     * @param   Zend_Controller_Request_Abstract $request
     * @param   Zend_Controller_Response_Abstract $response
     * @return  boolean
     * @throws  Zend_Controller_Dispatcher_Exception
     */
    public function dispatch(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response)
    {
        $this->setResponse($response);

        // get controller class name
        if (!$this->isDispatchable($request)) {
            $controller = $request->getControllerName();

            if (!$this->getParam('useDefaultControllerAlways') && !empty($controller)) {
                require_once 'Zend/Controller/Dispatcher/Exception.php';
                throw new Zend_Controller_Dispatcher_Exception('Invalid controller specified (' . $controller . ')');
            }

            $className = $this->getDefaultControllerClass($request);
        } else {
            // have to call getControllerClass() here since the call to
            // isDispatchable may have resulted in an error and require the
            // ErrorController to be applied
            $className = $this->getControllerClass($request);
            
            if (!$className) {
                $className = $this->getControllerClass($request);
            }
            if (!$className) {
                $className = $this->getDefaultControllerClass($request);
            }
        }

        /**
         * Load the controller class file
         */
        //$className = $this->loadClass($className);
        $webContainer  = Ovoyo_WebContainer::getInstance();
        Ovoyo_ClassLoader::load($className, $this->_curDirectory);

        /**
         * Instantiate controller with request, response, and invocation
         * arguments; throw exception if it's not an action controller
         */
        $controller = new $className($request, $this->getResponse(), $this->getParams());
        if (!$controller instanceof Zend_Controller_Action) {
            require_once 'Zend/Controller/Dispatcher/Exception.php';
            throw new Zend_Controller_Dispatcher_Exception("Controller '$className' is not an instance of Zend_Controller_Action");
        }

        /**
         * Retrieve the action name
         */
        $action = $this->getActionMethod($request, $controller);

        #echo "Controller: $className <br>\n Action: $action <br>\n"; #exit;
        
        /**
         * Dispatch the method call
         */
        $request->setDispatched(true);

        // by default, buffer output
        $disableOb = $this->getParam('disableOutputBuffering');
        $obLevel   = ob_get_level();
        if (empty($disableOb)) {
            ob_start();
        }

        try {
            $controller->dispatch($action);
        } catch (Exception $e) {
            // Clean output buffer on error
            $curObLevel = ob_get_level();
            if ($curObLevel > $obLevel) {
                do {
                    ob_get_clean();
                    $curObLevel = ob_get_level();
                } while ($curObLevel > $obLevel);
            }
            throw $e;
        }

        if (empty($disableOb)) {
            $content = ob_get_clean();
            $response->appendBody($content);
        }

        // Destroy the page controller instance and reflection objects
        $controller = null;
    }
    
    /**
     * Returns TRUE if the Zend_Controller_Request_Abstract object can be
     * dispatched to a controller.
     *
     * @param   Zend_Controller_Request_Abstract $request
     * @return  boolean
     */
    public function isDispatchable(Zend_Controller_Request_Abstract $request)
    {
        $className = $this->_controllerClassName;
        
        if (!$className) {
            $className = $this->getControllerClass($request);
            if ($className != $this->_controllerClassName) {
                $this->_controllerClassName = $className;
            }
            if (!$className) { return false; }
        }
        
        if (class_exists($className, false)) {
            return true;
        }

        $fileSpec    = $this->classToFilename($className);
        $dispatchDir = $this->getDispatchDirectory();
        $test        = $dispatchDir . DIRECTORY_SEPARATOR . $fileSpec;

        return Zend_Loader::isReadable($test);
    }
    
    /**
     * Determine the action name
     *
     * First attempt to retrieve from request; then from request params
     * using action key; default to default action
     *
     * Returns formatted action name
     *
     * @param   Zend_Controller_Request_Abstract $request
     * @param   Zend_Controller_Action  $controller
     * @return  string
     */
    public function getActionMethod(Zend_Controller_Request_Abstract $request, Zend_Controller_Action $controller)
    {
        if ($controller instanceof Ovoyo_Controller_ErrorController) {
            $action = 'error';
        } else if ($this->_allowZendControllerSelection) {
            $action = $request->getActionName();
        } else {
            // check out router to see if we have any matches
            $webContainer  = Ovoyo_WebContainer::getInstance();
            $route = $webContainer->getRouter()->getCurrentRoute();
            
            if ($route) {
                $defaults = $route->getDefaults();
                // only get action if not 'index' - Zend sets "index" as the default if no
                // action was stated in the route definition, however this is probably not what we want
                if (isset($defaults['action']) && $defaults['action'] != 'index') {
                    $action = $defaults['action'];
                }
            }
        }

        $haveAction = (empty($action)) ? false : true;
        
        if (!$haveAction) {
            $moduleName = $request->getModuleName();
            $defaultController = $this->getDefaultControllerClass($request);
        
            $pathInfo = preg_replace(
                '/^' . addcslashes($this->_controllerUrlPath, '/') . '/',
                '',
                $request->getPathInfo()
            );
            
            $pathParts = $this->_formatPathInfo($pathInfo, $moduleName);
            $numParts  = count($pathParts);

            if ($numParts > 0) {
                for ($i = $numParts - 1; $i >= 0; $i--) {
                    $action = $pathParts[$i];
                    $firstChar = substr($action, 0, 1);
                    $action    = substr_replace($action, strtolower($firstChar), 0, 1);
    
                    if (method_exists($controller, $action . "Action")) {
                        $haveAction = true;
                        break;
                    }
                }
                
                // for modules, check for default action
                if (!$haveAction && $moduleName != $this->_defaultModule) {
                    $action = $this->getDefaultAction();
                    if (method_exists($controller, $action . "Action")) {
                        $haveAction = true;
                    }
                }
                
                // check for default action
                if (!$haveAction && $this->getParam('useDefaultActionAlways')) {
                    $action = $this->getDefaultAction();
                    if (method_exists($controller, $action . "Action")) {
                        $haveAction = true;
                    }
                }
                
            } else {
                $action = $this->getDefaultAction();
                if (method_exists($controller, $action . "Action")) {
                    $haveAction = true;
                }
            }
            
            
            $request->setActionName($action);
        }

        // after first call to getActionMetho zend is allowed to specify controller
        $this->_allowZendControllerSelection = true;
        
        return $action . "Action";
    }
    
    
}
