<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */


/**
 * Authentication adapter
 *
 * Abstract class to be extended by any adapter
 *
 * @package    Ovoyo_Authentication
 */
abstract class Ovoyo_Authentication_Adapter
{
    protected $_passwordHashType;
    protected $_loginUrl;
    protected $_loginFailureUrl;
    protected $_loginSuccessUrl;
    protected $_logoutUrl;
    protected $_hashType;
    protected $_sessionLength     = 1800;
    protected $_sessionIdentifier = 'SID';
    protected $_enforcing         = true;
    protected $_exceptions        = array();
    protected $_allowedIps;
    protected $_connName;
    protected $_tablePrefix;
    protected $_expirationSeconds;
    
    protected $_userClass;
    protected $_userSessionClass;

    protected $_idField;
    protected $_deletedField;

    public $_user;
    public $_userSession;

    /**
     * Constructor
     *
     * @param   Zend_Config|array $defaults Default options
     * @param   Zend_Config|array $options Authentication options
     */
    public function __construct($defaults = null, $options = null)
    {
        $classVars = get_object_vars($this);
        
        // set defaults
        if ($defaults instanceof Zend_Config || is_array($defaults)) {
            foreach ($defaults AS $item => $value) {
                if (array_key_exists("_$item", $classVars)) {
                    $this->{"_$item"} = $value;
                }
            }
        }

        // set options
        if ($options instanceof Zend_Config || is_array($options)) {
            foreach ($options AS $item => $value) {
                if (array_key_exists("_$item", $classVars)) {
                    $this->{"_$item"} = $value;
                }
            }
        }
        
        // must have a login url
        if (!isset($this->_loginUrl)) {
            $exception = 'Login url must be defined';
            throw new Exception($exception);
        }
        
        if (!isset($this->_loginSuccessUrl)) {
            $this->_loginSuccessUrl = '/';
        }
        
        if (!isset($this->_loginFailureUrl)) {
            $this->_loginFailureUrl = $this->_loginUrl;
        }
        
        if (!isset($this->_logoutUrl)) {
            $this->_logoutUrl = '/';
        }
        
        if (!is_bool($this->_enforcing)) {
            if ($this->_enforcing === 'false') {
                $this->_enforcing = false;
            } else {
                $this->_enforcing = true;
            }
        }
        
        // add login and login failure urls to exceptions list
        $this->_exceptions[] = $this->_loginUrl;
        $this->_exceptions[] = $this->_loginFailureUrl;
        
        $this->request = Ovoyo_WebContainer::getInstance()->getRequest();

        $this->init();
    }

    /**
     * Init method
     *
     * Sub classes should extend this method rather than have a constructor
     */
    public function init()
    {
    }

    /**
     * Authenticate
     *
     * Check we have a valid, logged in, user
     *
     * @return void
     */
    public final function authenticate()
    {
        $this->_validateIp();
        
        $this->preAuthentication();
        
        $this->_authenticate();

        $this->postAuthentication();
    }
    
    /**
     * Authenticate
     *
     * Check we have a valid, logged in, user
     *
     * @return void
     */
    abstract protected function _authenticate();
    
    /**
     * Check for valid ip address
     *
     * If ip address is not valid, we exit immediately
     *
     * @return  void
     */
    protected final function _validateIp()
    {
        if (isset($this->_allowedIps)) {
            $allowedIps = preg_replace(
                '/[\s|\r|\n]/', '', $this->_allowedIps
            );
            $allowedIps = explode(',', $allowedIps);

            $ipMatchFound = false;
            foreach ($allowedIps AS $allowedIp) {
                $allowedIp = str_replace('.', '\.', $allowedIp);
                $allowedIp = str_replace('*', '.*', $allowedIp);
                $allowedIp = str_replace('%', '.*', $allowedIp);
                if (preg_match('/' . $allowedIp . '/', $_SERVER["REMOTE_ADDR"])) {
                    $ipMatchFound = true;
                    break;
                }
            }

            if (!$ipMatchFound) {
                header('HTTP/1.0 403 Forbidden');
                exit;
            }
        }
    }
    
    /**
     * Pre authentication actions
     *
     * @return  void
     */
    public function preAuthentication()
    {}
    
    /**
     * Post authentication actions
     *
     * @return  void
     */
    public function postAuthentication()
    {}

    /**
     * Check if current url is an allowed exception
     *
     * @return  boolean
     */
    protected function allowedException()
    {
        $urlPath = Ovoyo_WebContainer::getInstance()->getParam('urlPath');
        foreach ($this->_exceptions AS $exception) {
            if (!$exception) {
                continue;
            }
            $exception = preg_replace("/\//", "\/", $exception);
            if (preg_match("/$exception/", $urlPath)) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * Get name of default controller class
     *
     * @return  string
     */
    abstract public static function getDefaultControllerClass();
    
    /**
     * Login
     *
     * Attempt to log a user in - upon successful login, will redirect user to $_loginSuccessUrl,
     * upon failuer will redirect user to $_loginFailureUrl
     *
     * return void
     */
    protected function _login()
    {
        $user = $this->getUser();
        
        $request = Ovoyo_WebContainer::getInstance()->getRequest();
        
        $user->preLogin();
        
        $loggedIn = $user->login(
            $request->getParam('email'),
            $request->getParam('password'),
            $this->_sessionIdentifier
        );
        
        if ($loggedIn) {
            $user->postLogin();
            
            if (!$successUrl = $request->getParam('_loginSuccessUrl')) {
                $successUrl = $this->_loginSuccessUrl . '?loginSuccess=1';
            }
            
            header("Location: $successUrl");
            exit;
        }
    }

    /**
     * Logout
     *
     * Attempt to log a user out - if successful will redirect user to $_logoutUrl
     *
     * return void
     */
    protected function _logout()
    {
        try {
            $user = $this->getUser();
            if (isset($user->{$this->_idField})) {
                $user->preLogout();
                
                $sessionId = $_REQUEST[$this->_sessionIdentifier];
                $logoutSuccess = $user->logout($this->_sessionIdentifier);
                
                $user->postLogout();
            } else {
                // if session cookie still exists we need to destroy it since
                // it must not longer be valid
                if (isset($_COOKIE[$this->_sessionIdentifier])) {
                    $secure_cookie = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? true : false;
                    setcookie($this->_sessionIdentifier, null, 0, "/", '', $secure_cookie);
                    unset($_COOKIE[$this->_sessionIdentifier]);
                }
                
                $logoutSuccess = true;
            }

            
            if ($logoutSuccess) {
                $url = $this->_logoutUrl
                     . '?logoutSuccess=1';
                header("Location: $url");
                exit;
            }
                
        } catch (Exception $e) {
            Ovoyo_ErrorHandler::addException($e);
        }
    }

    /**
     * Get user instance
     *
     * @return  Object Instance of class specified by $this->_userClass
     */
    public final function getUser()
    {
        if (!isset($this->_user)) {
            
            /**
             * @see Ovoyo_Authentication_Interface_User
             */
            require_once 'Ovoyo/Authentication/Interface/User.php';

            /**
             * @see Ovoyo_ClassLoader
             */
            require_once 'Ovoyo/ClassLoader.php';
    
            Ovoyo_ClassLoader::load($this->_userClass);
    
            $modelOptions = $this->_authModelOptions();
    
            $this->_user = new $this->_userClass($modelOptions);
            
            // must implement Ovoyo_Authentication_User_Interface
            if (!$this->_user instanceof Ovoyo_Authentication_Interface_User) {
                $exception = 'User class must implement '
                           . 'Ovoyo_Authentication_Interface_User';
                throw new Exception($exception);
            }
            $this->_user->setHashType($this->_hashType);
            $this->_user->setUserSession($this->getUserSession());
        }
        
        return $this->_user;
    }

    /**
     * Get user session instance
     *
     * @return  Object New instance of class specified by $this->_userSessionClass
     */
    public final function getUserSession()
    {
        if (!isset($this->_userSession)) {
            /**
             * @see Ovoyo_Authentication_Interface_User
             */
            require_once 'Ovoyo/Authentication/Interface/UserSession.php';
            
            $authModelOptions = $this->_authModelOptions();
            $authModelOptions['expirationSeconds'] = $this->_expirationSeconds;
            
            $this->_userSession = Ovoyo_Model::factory(
                $this->_userSessionClass, $authModelOptions
            );
            
            // must implement Ovoyo_Authentication_User_Interface
            if (!$this->_userSession instanceof Ovoyo_Authentication_Interface_UserSession) {
                $exception = 'User session class must implement '
                           . 'Ovoyo_Authentication_Interface_UserSession';
                throw new Exception($exception);
            }
            
        }
        
        return $this->_userSession;
    }
    
    /**
     * Get session identifier
     *
     * @return string
     */
    public final function getSessionIdentifier()
    {
        return $this->_sessionIdentifier;
    }
    
    /**
     * Get hash type
     *
     * @return string
     */
    public final function getHashType()
    {
        return $this->_hashType;
    }

    /**
     * Perform redirection
     *
     * @param   string $url Url to redirect to
     * @param   int $forceRedirect Force redirect even if already at given location
     * @return  void
     */
    protected function _redirect($url, $forceRedirect = null)
    {
        $urlPath = preg_replace("/\?.*$/", "", $url);
        $urlPath = rtrim($urlPath, "/");

        if ($forceRedirect
            || $urlPath != Ovoyo_WebContainer::getInstance()->getParam('urlPath')) {
            header("Location: $url");
            exit;
        }
    }

    /**
     * Get options related to the user model
     *
     * @return  array
     */
    protected function _authModelOptions()
    {
        return array('connName' => $this->_connName, 'tablePrefix' => $this->_tablePrefix);
    }
}
