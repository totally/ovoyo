<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
* @see Ovoyo Model
*/
require_once 'Ovoyo/Model.php';

/**
 * Payment class
 *
 * Used for storing payment info until payment has been processed by an
 * appropriate provider
 *
 * @package    Ovoyo_Payment
 */
class Ovoyo_Payment extends Ovoyo_Model
{
    public $paymentId;
    public $amount;
    public $summary;
    public $details;
    public $paid = 0;
    public $paidDate;
    public $reference;
    public $status;
    public $callbackData;
    
    protected static $_tablePrefixDefault = 'payment_';
    
    /**
     * Init - setup model options
     *
     * @return void
     */
    public function init()
    {
        $this->_tablePrefix = self::$_tablePrefixDefault;
    }
    
    /**
     * Create a new payment
     *
     * @param   float $amount
     * @param   string $summary
     * @param   array $details
     * @return  boolean
     * @throws  Exception
     */
    public function create($amount, $summary, array $details)
    {
        if (!($amount >= 0)) {
            throw new Exception('Can not create a payment with value less than 0.00');
        }
        
        if (!$summary) {
            throw new Exception('Can not create a payment without summary text');
        }
        
        $this->amount = $amount;
        $this->summary = $summary;
        $this->details = $details;
        $this->createdDate = new Zend_Date;
        
        return $this->insert();
    }
    
    /**
     * Complete a payment
     *
     * @param   boolean $paid
     * @param   string $reference
     * @param   string $status
     * @param   array $callbackData
     */
    public function complete($paid, $reference, $status = '', $callbackData = '')
    {
        $this->paid = ($paid) ? 1 : 0;
        $this->reference = $reference;
        $this->status = $status;
        $this->callbackData = $callbackData;
        $this->paidDate = new Zend_Date;
        
        return $this->update();
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo/Ovoyo_Model#preSave()
     */
    public function preSave()
    {
        $this->details = (is_array($this->details)) ? serialize($this->details) : '';
        $this->callbackData = (is_array($this->callbackData)) ? serialize($this->callbackData) : '';
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo/Ovoyo_Model#postSave()
     */
    public function postSave()
    {
        $this->details = ($this->details) ? unserialize($this->details) : '';
        $this->callbackData =  ($this->callbackData) ? unserialize($this->callbackData) : '';
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo/Ovoyo_Model#postFetch()
     */
    public function postFetch()
    {
        $this->details = ($this->details) ? unserialize($this->details) : '';
        $this->callbackData = ($this->callbackData) ? unserialize($this->callbackData) : '';
    }

    /**
     * Set table prefix that should be used by payment class
     *
     * This is need to override the default "payment_" prefix
     *
     * @param string $tablePrefix;
     * @return void
     */
    public static function setTablePrefix($tablePrefix)
    {
        self::$_tablePrefixDefault = $tablePrefix;
    }
}
