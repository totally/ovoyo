<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Zend_Db_Table_Abstract
 */
require_once 'Zend/Db/Table/Abstract.php';

/**
 * Ovoyo Db class 
 *
 * This is only required since the Zend_Db_Table class is abstract and can not be instantiated
 *
 * @package    Ovoyo_Db
 */
class Ovoyo_Db_Table extends Zend_Db_Table_Abstract
{
}
