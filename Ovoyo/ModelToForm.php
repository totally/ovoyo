<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Form
 */
require_once 'Ovoyo/Form.php';

/**
 * @see Zend_Config
 */
require_once 'Zend/Config.php';

/**
 * Model to form class
 *
 * Single static method to take a Ovoyo_Model instance (or sub class of)
 * and create a Zend_Form instance from it. Based on code found in the following article:
 * http://codecaine.co.za/posts/form-generation-with-zend-form-part-2/
 *
 * @package    Ovoyo_ModelToForm
 */
class Ovoyo_ModelToForm
{
    /**
     * Create form from model
     *
     * @param   Ovoyo_Model $model An instance of an Ovoyo_Model class or sub-class
     * @param   array $formConfig
     * @param   array $formOptions
     * @return  Zend_Form
     */
    public static function create($model, $formConfig = array(), $formOptions = array())
        {
        // build items to "ignore" array
        $ignore = array();
        if (is_array($formOptions['ignore'])) {
            foreach ($formOptions['ignore'] AS $item) {
                $ignore[$item] = $item;
            }
        }

        $info     = $model->tableInfo();
        $metadata = $model->tableMetadata(true);
        
        $passwordFields = array();
        
        // all primary keys should be hidden - regardless of type of data
        if (!$formOptions['showPrimaryKeys']) {
            foreach ($info['primary'] AS $primaryKey) {
                if (array_key_exists($primaryKey, $ignore)) { continue; }
    
                $formOptions['elements'][$primaryKey]['hidden'] = true;
            }
        }
        
        // option to ignore primary keys completely
            if ($formOptions['ignorePrimaryKeys']) {
            foreach ($info['primary'] AS $primaryKey) {
                $ignore[$primaryKey] = $primaryKey;
            }
        }

        // work out type for non-primary key table elements
        foreach ($metadata as $columnName => $columnDetails)
        {
            $element = null;
            
            // ignore specific fields
            if (array_key_exists($columnName, $ignore)) { continue; }
            
            // if item has been set to hidden, deal with now
            if ($formOptions['elements'][$columnName]['hidden']) {
                $element = array('hidden', array('value' => $model->$columnName));
                $formConfig['elements'][$columnName] = $element;
                continue;
            }

            $type       = null;
            $validators = array();
            $options    = array();
            $extra      = array();
            $required   = ($columnDetails['NULLABLE'] || !is_null($columnDetails['DEFAULT'])) ? false : true;
            $required   = false;

            // apply extra info already provided
            if (is_array($formOptions['elements'][$columnName]['extra'])) {
                $extra = $formOptions['elements'][$columnName]['extra'];
            }

            $dataType = ($formOptions['elements'][$columnName]['dataType']) ? $formOptions['elements'][$columnName]['dataType'] : $columnDetails['DATA_TYPE'];

            if ($formOptions['elements'][$columnName]['label']) {

                $label = $formOptions['elements'][$columnName]['label'];
            } else {
                $dbConfig = $model->_db->getConfig();
                if ($dbConfig['nameStyle'] == 'underscore') {
                    $label = ucfirst(strtolower(preg_replace("/_/", " ", $columnName)));
                } else {
                    $label = ucfirst(strtolower(preg_replace("/([a-z])([A-Z])/", "$1 $2", $columnName)));
                }
                
                if ($formOptions['labelAppend']) {
                    $label.= $formOptions['labelAppend'];
                } else if (!$formOptions['noLabelAppend']) {
                    $label.= ':';
                }
            }

            // enum cols should be dropdowns
            if (preg_match('/^enum/i', $dataType)) {
                preg_match_all('/\'(.*?)\'/', $dataType, $matches);

                $options = array();
                foreach ($matches[1] as $match) {
                    $options[$match] = $match;
                }

                $type    = 'select';
                $options = $options;
            }

            // replace any bracketed types like float, dec, etc
            $dataType = preg_replace('/\(.*/', '', $dataType);

            // other data types
            switch ($dataType) {
                case 'varchar':
                case 'char':
                case 'password':
                    $length = $columnDetails['LENGTH'];
                    $type = ($dataType == 'password' && !$formOptions['showPasswords']) ? 'password' : 'text';
                    $validators[] = array('StringLength', false, array(0, $length));
                    $extra['maxlength'] = $length;
                    break;

                case 'text':
                case 'mediumtext':
                case 'longtext':
                    $type = 'textarea';
                    $extra['rows'] = ($extra['rows']) ? $extra['rows'] : 5;
                    $extra['cols'] = ($extra['cols']) ? $extra['cols'] : 30;
                    break;

                case 'int':
                case 'bigint':
                case 'mediumint':
                case 'smallint':
                case 'tinyint':
                    $type = 'text';
                    $validators[] = array('int', false);
                    break;

                case 'float':
                case 'decimal':
                case 'double':
                    $type = 'text';
                    $validators[] = array('float', false);
                    break;

                case 'date':
                case 'datetime':
                    $type = 'text';
                    break;

                case 'bit':
                case 'bool':
                case 'boolean':
                    $type = 'checkbox';
                    $options['value'] = 1;
                    break;
                    
                case 'radioBit':
                    $type = 'radio';
                    $options['1'] = 'Yes';
                    $options['0'] = 'No';
                    break;

                default:
                    if (preg_match('/^enum/', $dataType )) {
                        ;
                    }
                    break;
            }

            if (is_null($element)) {
                // if column name is "password" then, change to a password field
                if ($columnName == 'password' && !$formOptions['elements'][$columnName]['dataType']) {
                    if (!$formOptions['showPasswords']) {
                        $type = 'password';
                        $passwordFields[$columnName] = $columnName;
                    }
                }

                $validateOptions = array('label'    => $label,
                                         'required' => $required);

                if (!$formConfig['ignoreValidators']) {
                    $validateOptions['validators']  = $validators;
                }
                
                $element = array($type, $validateOptions);

                if (count($options) > 0) {
                    $element[1]['multiOptions'] = $options;
                }
                foreach ($extra AS $key => $val) {
                    $element[1][$key] = $val;
                }

                $formConfig['elements'][$columnName] = $element;
            }
        }
        
        // build form
        $form = new Ovoyo_Form($formConfig);
        $form->populate($model->toArray());

        if ($formOptions['noFormTags']) {
            $form->removeDecorator('Form');
        }
        
        // reset zend decorators
        if ($formConfig['resetZendDecorators']) {
            $form->resetZendDecorators();
        }
        
        // render password fields
        if ($formOptions['renderPasswords']) {
            foreach ($passwordFields AS $field) {
                $form->{$field}->setRenderPassword(true);
            }
        }

        return $form;
        }
}
