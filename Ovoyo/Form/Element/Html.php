<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */
/**
 * @see  Zend_Form_Element_Xhtml
 */
require_once 'Zend/Form/Element/Xhtml.php';

/**
 * Simple way to inject HTML directly into a form.
 * 
 * Usage: $form->addElement('html', $name, array('value' => $htmlToDisplay));
 * 
 * Works for Ovoyo_Form objects without requiring any further configuration.
 * To use with Zend_Form objects, you will need to add the necessary Ovoyo 
 * directory to the path, (using addPrefixPath()) so that the element can be
 * found.
 * 
 * @package    Ovoyo_Form
 */
class Ovoyo_Form_Element_Html extends Zend_Form_Element_Xhtml
{
    /**
     * @var string Name of the form view helper to use for rendering.
     */
    public $helper = 'formHtml';

    /**
     * isValid()
     * Always return true, as element does not have any editable sections.
     * If we didn't return true, then the 'value' property disappears if
     * there is a validation error.
     */
    public function isValid($value, $context = NULL) {
        return true;
    }
}
