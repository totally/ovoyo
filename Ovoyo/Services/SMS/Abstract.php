<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * SMS
 * A class for integrating SMS functionality from 3rd party prividers
 * 
 * @package	Ovoyo_Services
 * @subpackage	SMS
 */
abstract class Ovoyo_Services_SMS_Abstract
{
    protected $_localCountryCode = '44';
        
    /**
     * Creates a new SMS to be sent
     *
     * @param	int $to The recipient's number in full international format (no '+' or '00')
     * @param	string $text The message body text
     * @param	string $from Optional SMS sender which gets displayed on the recipient's handset.
     * @return	boolean
     */
    abstract public function send($to, $text, $from = null);
      
        
    /**
     * Format a mobile number to standard international format
     *
     * @param   string $no The unformatted mobile number
     * @return  string The formatted mobile number
     */
    public function formatMobile($no) 
    {
        if (empty($no)) {
            return $no;
        }
        
        $no = preg_replace('/\D/', '', $no);
        
        $char1      = $no[0];
        $char2      = $no[1];
        $chars1and2 = $char1.$char2;
        
        // International number
        if ($char1 == '+' 
            || $chars1and2 == '00' 
            || ($char1 > 0 && strlen($no) > 11)) { 
                
            switch ($char1) {
                case '+':
                    return substr($no, 1);
                    break;
                
                case '0':
                    return substr($no, 2);
                    break;
                
                default:
                    return $no;
                    break;
            }
            
        // UK Number
        } else { 
            if ($char1 == '0') {
                $no = substr($no, 1);
            }
            
            return $this->_localCountryCode . $no;
        }
    }
        
    /**
     * Set the local country code
     *
     * @param  int $code
     * @return void
     */
    public function setLocalCountryCode($code)
    {
        $this->_localCountryCode . $code;
    }
}
