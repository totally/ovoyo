<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Services_PaymentProvider_Abstract
 */
require_once 'Ovoyo/Services/PaymentProvider/Abstract.php';

/**
 * RBSWorldPay class
 *
 * @package    Ovoyo_Services_PaymentProvider
 */
class Ovoyo_Services_PaymentProvider_RbsWorldPay extends Ovoyo_Services_PaymentProvider_Abstract
{
    protected $_cartId;
    protected $_installationId;
    protected $_merchantId;
    protected $_testMode = 0; //default  to alive transaction
    protected $_currency = 'GBP';
    protected $_callbackUrl;

    public $currency;
    public $dataToSend = array();
    public $reference;
    public $status;
    public $paymentDetails = array(); // populated after callback

    private $requiredFields = array( '_cartId'         => "Cart ID is Required",
                                     '_installationId' => "Installation ID is Required" );

    const WP_TEST_MODE_SEND_SUCCESS = 100;
    const WP_TEST_MODE_SEND_FAIL = 101;
    const WP_TEST_MODE_LIVE = 0;

    const WP_ERROR_INSTALLATION_ID_NOT_NUMERIC = 'Installation ID must be supplied and must be numeric';

    const WP_PAYMENT_URL = 'https://secure.worldpay.com/wcc/purchase';
    const WP_TEST_PAYMENT_URL = 'https://secure-test.worldpay.com/wcc/purchase';

    const WP_TRANSACTION_SUCCESS = 'Y';
    const WP_TRANSACTION_CANCELLED = 'C';
    const WP_TRANSACTION_FAILED = 'N';

    const WP_TRANSACTION_AUTH_MODE_FULL = 'A';
    const WP_TRANSACTION_AUTH_MODE_PRE_AUTH = 'E';
    const WP_TRANSACTION_AUTH_MODE_POST_AUTH = 'O'; // Can only be this in the response

    const WP_CALLER_IP_VALIDATION = '.worldpay.com';
    const WP_RBS_CALLER_IP_VALIDATION = '.outbound.wp3.rbsworldpay.com';

    const WP_FUTUREPAY_STATUS_CHANGE_MERCH_CANCELLED = 'Merchant Cancelled';
    const WP_FUTUREPAY_STATUS_CHANGE_CUST_CANCELLED = 'Customer Cancelled';

    const WP_FUTUREPAY_TIME_UNIT_DAY = 1;
    const WP_FUTUREPAY_TIME_UNIT_WEEK = 2;
    const WP_FUTUREPAY_TIME_UNIT_MONTH = 3;
    const WP_FUTUREPAY_TIME_UNIT_YEAR = 4;

    const WP_FUTUREPAY_TYPE_REGULAR = 'regular';
    const WP_FUTUREPAY_TYPE_LIMITED = 'limited';

    const WP_HTTP_INTERFACE = 'https://secure.worldpay.com/wcc/iadmin';
    const WP_HTTP_HOST = 'secure.worldpay.com';
    const WP_HTTP_PORT = 80;
    const WP_HTTP_TIMEOUT = 30;
    const WP_HTTP_METHOD = 'GET';
    const WP_HTTP_VERSION = '1.1';

    const WP_CALLBACK_PREFIX = 'M_';

    const WP_CALLBACK_PAYMENT_ID = 'M_paymentId';


    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::_init()
     */
    protected function _init()
    {
        if($this->_useTestPayment) {
            $this->_testMode = self::WP_TEST_MODE_SEND_SUCCESS;
        } else {
            $this->_testMode = self::WP_TEST_MODE_LIVE;
        }

        if ($this->_showCurrency) {
        	$this->_hideCurrency = false;
        } else {
        	$this->_hideCurrency = true;
        }
    }
    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract#sendPayment()
     */
    public function sendPayment(array $vars)
    {
        try {
            /*$this->_cartId = $vars['cartId'];
            $this->_callbackUrl = $vars['callbackUrl'];*/

            if (!$this->isValid()) {
                return false;
            }

            // create payment in payment table
            $paymentId = $vars['paymentId'];
            if(!$paymentId) {
                if (!$paymentId = $this->createPayment($vars['amount'], $vars['summary'], $vars)) {
                    return false;
                }
            }
            
            if ($this->_removeCallbackHttp) {
                $this->_callbackUrl = str_replace('http://', '', $this->_callbackUrl);
            }
            
            if (isset($_SERVER['HTTPS'])) {
                $this->_callbackUrl = str_replace('http://', 'https://', $this->_callbackUrl);
            }
            
            $description = ($vars['desc']) ? $vars['desc'] : $vars['summary'];

            $url = ($this->_testMode == self::WP_TEST_MODE_SEND_SUCCESS || $this->_testMode == self::WP_TEST_MODE_SEND_FAIL) ? self::WP_TEST_PAYMENT_URL : self::WP_PAYMENT_URL;
            $url.= '?instId=' . urlencode($this->_installationId);
            $url.= ($this->_merchantId) ? '&accId1=' . urlencode($this->_merchantId) : '';
            $url.= '&testMode=' . urlencode($this->_testMode);
            $url.= '&cartId=' . urlencode($this->_cartId);
            $url.= '&currency=' . urlencode($this->_currency);
            $url.= '&hideCurrency=' . urlencode($this->_hideCurrency);
            $name = $vars['title'] ? $vars['title'] . ' ' : '';
            $name.= $vars['firstName'] . ' ' . $vars['lastName'];
            $url.= '&name=' . urlencode($name);
            $url.= '&amount=' . urlencode($vars['amount']);
            $address = $vars['address1'] . "\n" . $vars['address2'];
            $address.= $vars['address3'] ? "\n" . $vars['address3'] : '';
            $address.=  "\n" . $vars['town'] . "\n" . $vars['county'];
            $url.= '&address=' . urlencode($address);
            $url.= '&postcode=' . urlencode($vars['postcode']);
            $url.= '&country=' . urlencode($vars['country']);
            $url.= '&tel=' . urlencode($vars['telephone']);
            $url.= '&fax=' . urlencode($vars['fax']);
            $url.= '&email=' . urlencode($vars['email']);
            $url.= '&desc=' . urlencode($description);
            
            if (is_array($vars['futurepay'])) {
                $url .= '&' . http_build_query($vars['futurepay']);
            }
            
            // Dynamic URL, must be configured in WP back end: <wpdisplay item=MC_callback-ppe empty="http://www.myserver.com/callback.cgi">
            $url.= '&' . self::WP_CALLBACK_PREFIX . 'callbackurl=' . urlencode($this->_callbackUrl);

            // Data we want back
            $url.= '&' . self::WP_CALLBACK_PREFIX . 'paymentId=' . urlencode($paymentId);
            $url.= '&' . self::WP_CALLBACK_PREFIX . 'email=' . urlencode($vars['email']);

            header("Location: $url");
            exit;

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Validates a payment
     *
     * @access public
     * @return boolean TRUE if the payment is valid, FALSE if it isn't
     */
    public function isValid()
    {
        $valid = true;
        if (is_array($this->requiredFields)) {
            foreach($this->requiredFields as $var => $errorCode) {
                if (!$this->{$var}) {
                    Ovoyo_ErrorHandler::addError($errorCode);
                    $valid = false;
                }
            }
        }

        if (!is_numeric($this->_installationId)) {
            Ovoyo_ErrorHandler::addError(self::WP_ERROR_INSTALLATION_ID_NOT_NUMERIC);
            $valid = false;
        }

        return $valid;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_PaymentProvider_Abstract::validateCallback()
     */
    public function validateCallback($data = array())
    {
        $valid = false;

        if (array_key_exists('transStatus', $data)) {
            $valid = true;
        }

        return $valid;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract#handleCallback()
     */
    public function handleCallback(array $data)
    {
        // fetch the payment
        $payment = Ovoyo_Services_PaymentProvider::getPayment(
            array('connName' => $this->_connName)
        );
        if (!$payment->fetch($data['M_paymentId'])) {
            return false;
        }
        $this->paymentId = $payment->paymentId;

        // update payment details
        $payment->status = $data['transStatus'];
        $payment->reference = $data['transId'];
        $payment->callbackData = $data;
        $payment->paid = ($data['transStatus'] == self::WP_TRANSACTION_SUCCESS) ? 1 : 0;
        $payment->paidDate = new Zend_Date;

        if (!$payment->update()) {
            return false;
        }

        $this->amount = $payment->amount;
        $this->paymentDetails = $payment->details;
        $this->paymentDetails['paymentId'] = $payment->paymentId;

        return $payment->paid;
    }

    public function completePayment()
    {
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::processPaymentInfo()
     */
    public function processPaymentInfo(array $data)
    {
        $paid = $this->handleCallback($data);
        if($paid) {
            $result = array();
            $result['paymentId'] = $this->paymentId;
        } else {
            $result = false;
        }
        return $result;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::postConvertToOrder()
     */
    public function postConvertToOrder($config)
    {
        if($config['paymentCompleteUrl'] && !headers_sent()) {
            $redirectUrl = $config['host'] . $config['paymentCompleteUrl'] . '/';
            if ($this->paymentId) {
                $redirectUrl.= '?paymentId=' . $this->paymentId;
            }
            echo   '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
            <html>
                <head>
                    <title>Redirecting...</title>
                    <meta http-equiv="REFRESH" content="0;url=' . $redirectUrl . '">
                </head>
                <body>
                    <p style="font-family: Arial">
                        You are now being redirected back to ' . $config['host'] . '<br /><br />
                        If the page has not refreshed within 5 seconds, please <a href="' . $redirectLink . '">click here</a>
                    </p>
                </body>
            </html>';
        }
        exit;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Services/PaymentProvider/Ovoyo_Services_PaymentProvider_Abstract::prepareData()
     */
    public function prepareData(array $data)
    {
        $preparedData = array(
            'paymentId' => $data['paymentId'],
            'firstName' => $data['firstName'],
            'lastName'  => $data['lastName'],
            'amount'    => $data['amount'],
            'address1'  => $data['address1'],
            'address2'  => $data['address2'],
            'address3'  => $data['address3'],
            'town'      => $data['townCity'],
            'postcode'  => $data['postcode'],
            'country'   => $data['country'],
            'email'     => $data['email'],
            'telephone' => $data['telephone'],
            'desc'      => $data['description'],
            'futurepay' => $data['futurepay'],
        );
        return $preparedData;
    }
}