<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Authentication_Adapter
 */
require_once 'Ovoyo/Authentication/Adapter.php';

/**
 * Override adapter
 *
 * Adapter to use for overriding existing authenticaion realms
 *
 * @package     Ovoyo_Authentication
 * @subpackage  Adapter
 */
class Ovoyo_Authentication_Adapter_Override extends Ovoyo_Authentication_Adapter
{
    /**
     * Authenticate
     *
     * @return  void
     */
    protected function _authenticate()
    {}
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo/Authentication/Ovoyo_Authentication_Adapter#getDefaultControllerClass()
     */
    public static function getDefaultControllerClass()
    {}
}
