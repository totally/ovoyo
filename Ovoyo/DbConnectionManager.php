<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_DB
 */
require_once 'Ovoyo/Db.php';

/**
 * @see Zend_Db_Table
 */
require_once 'Zend/Db/Table.php';

/**
 * DB Connection Manager
 *
 * Used to manage one or more database connections.  Tries to minimise connections
 * to different dbs by making sure only once connection is made per db host
 *
 * The conenction manager the Singleton pattern as their should only ever
 * be a single db connection manager instance
 *
 * @package    Ovoyo_DbConnectionManger
 */
class Ovoyo_DbConnectionManager
{
    // object instance
    private static $instance;

    private $_connections       = array();
    private $_connectionsByHost = array();
    private $_connected         = array();

    /**
     * Constructor
     *
     */
    public function __construct()
        {
        }

    /**
     * Get web container instance
     *
     * @return obj Ovoyo_WebContainer instance
     */
    public static function getInstance()
    {
        if (self::$instance === null) self::$instance = new self;
        return self::$instance;
    }

    /**
     * Add a connection
     *
     * @param   obj $dbConfig Zend_Config
     * @param   boolean $replaceExisting
     * @return  boolean
     * @throws  Exception
     */
    function addConnection($dbConfig, $replaceExisting = false)
    {
        // host
        $host = $dbConfig->params->host;

        // connection name
        $name = ($dbConfig->connName) ? $dbConfig->connName : 'default';

        // can not add new connection with same name
        if (isset($this->_connections[$name]) && !$replaceExisting) {
            return true;
        }

        // store connection by name
        $this->_connections[$name]['config'] = $dbConfig;

        try {
            // deal with legacy PEAR connections
            if ($dbConfig->usePear) {
                require_once 'pear/DB.php';

                $dsn = $dbConfig->adapter.'://'
                     . $dbConfig->params->username . ':' . $dbConfig->params->password . '@'
                     . $dbConfig->params->host . '/'. $dbConfig->params->dbname;

                $dbh = DB::connect($dsn);
                if (DB::isError($dbh)) {
                    throw new Exception("Could not connect using PEAR ($dsn)");
                }

                if ($dbConfig->adapter == 'mysql' || $dbConfig->adapter == 'mysqli') {
                    Ovoyo_Db::restoreCharacterSet($dbh, true);
                }

                $this->_connections[$name]['db'] =& $dbh;
                $this->_connected[$name] = true;

            // connect using Ovoyo DB handler
            } else {
/*
                // if we already have a connection for this host, assign it to our connections array
                if (isset($this->_connectionsByHost[$host])) {
                    $this->_connections[$name]['db'] =& $this->_connectionsByHost[$host];

                // otherwise create new connection
                } else {
                    $db = new Ovoyo_Db($dbConfig);
                    $this->_connections[$name]['db'] =& $db;
                    $this->_connectionsByHost[$host] =& $db;
                }
*/
                $db = new Ovoyo_Db($dbConfig);
                $this->_connections[$name]['db'] = $db;
                $this->_connected[$name] = false;

                if (Ovoyo_WebContainer::getConfig('application')->enableFirebugDbProfiler) {
                    $db->enableFirebugProfiler();
                }
            }
        } catch (Exception $e) {
            throw $e;
        }

        return true;
    }
    
    /**
     * Add an alias to an existing connection
     *
     * @param   string $connName Name of connection to create alias for
     * @param   string $alias
     * @return  void
     * @throws  Exception
     */
    public function addConnectionAlias($connName = 'default', $alias)
    {
        try {
            $conn = $this->getConnection($connName);
            if (!$conn) {
                throw new Exception('Can not create db connection alias for non-existent connection');
            }
            
            $this->_connections[$alias]['config'] =& $this->_connections[$connName]['config'];
            $this->_connections[$alias]['db']=& $this->_connections[$connName]['db'];
            $this->_connected[$alias] =& $this->_connections[$connName];
            
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Get a connection
     *
     * @param   string $connName Name of connection to get
     * @return  Zend_Db Zend_Db adapter if connection exists
     * @throws  Exception
     */
    public function &getConnection($connName = 'default')
    {
        // make sure we have a connection name
        $connName = ($connName) ? $connName  : 'default';

        try {
            // get stored connection
            $db = $this->_connections[$connName]['db'];

            if ($db instanceOf Ovoyo_Db) {                
                $dbConfig = $this->getDbConfig($connName);
                $host = $dbConfig->params->host;
                
                // if not already connected, call getConnected()
                if (!$this->_connected[$connName]) {
                    $db->getConnection();
                    $this->_connected[$connName] = true;
                    $this->_connectionsByHost[$host] = $connName;
                    
                    $adapter = $db->getAdapter();
                    
                    // for mysql, set character set of client to match that of server
                    if ($adapter instanceof Zend_Db_Adapter_Mysqli || $adapter instanceof Zend_Db_Adapter_Pdo_Mysql) {
                        Ovoyo_Db::restoreCharacterSet($db);
                    }
                }
                
                // make sure we use connection as default table adapter
                Zend_Db_Table::setDefaultAdapter($db->getAdapter());

/*
                // select correct schema
                $sql = $db->quoteInto('use ?', $this->_connections[$connName]['config']->params->dbname);
                $db->query($sql);
*/


            // deal with legacy PEAR connections
            } else if (!stristr(get_class($db), 'DB_')) {
                throw new Exception("failed to get connection ($connName)");
            }

        } catch (Zend_Exception $e) {
            throw $e;
        }

        return $db;
    }

    /**
     * Get config info for a connection
     *
     * @param string $connName Name of connection to get
     * @return obj Zend_Config Zend config object
     */
    public function getDbConfig($connName = 'default')
    {
        // make sure we have a connection name
        $connName = ($connName) ? $connName  : 'default';

        return $this->_connections[$connName]['config'];
    }

    /**
     * Test a config to see if it will produce a valid connection
     *
     * @param   Zend_Config $dbConfig
     * @return  boolean
     * @throws  Exception
     */
    public function testConfig(Zend_Config $dbConfig)
    {
        try {
            $db = Zend_Db::factory($dbConfig);
            $db->getConnection();
        } catch (Exception $e) {
            throw $e;
        }
        
        return true;
    }
}
