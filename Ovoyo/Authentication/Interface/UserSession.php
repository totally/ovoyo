<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * User interface
 *
 * @package    Ovoyo_Authentication
 * @subpackage  Interface
 */
interface Ovoyo_Authentication_Interface_UserSession
{
    /**
     * Start new session for user
     *
     * @param   string|int $userId User identifier
     * @param   string $sessionIdentifier Session identifier
     * @return  boolean
     */
    public function startSession($userId, $sessionIdentifier);
    
    /**
     * Refresh session
     *
     * @return  boolean|void
     */
    public function refresh();
    
    /**
     * Drop cookie containing a session ID
     *
     * @param   string $sessionIdentifier Session identifier
     * @return  void
     */
    public function dropSessionIdCookie($sessionIdentifier);
    
    
    /**
     * Delete session
     *
     * @param   string $sessionIdentifier Session identifier
     * @return  void
     */
    public function end($sessionIdentifier);
}