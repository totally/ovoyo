<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_ErrorHandler
 */
require_once 'Ovoyo/ErrorHandler.php';

/**
 * @see Ovoyo_Model
 */
require_once 'Ovoyo/Model.php';

/**
 * @see Zend_Validate_Hostname
 */
require_once 'Zend/Validate/Hostname.php';

/**
 * User class
 *
 * @package    Ovoyo_Application
 */
class Ovoyo_Application_User extends Ovoyo_Model
    implements Ovoyo_Authentication_Interface_User
{
    public $userId;
    public $email;
    public $password;
    public $firstName;
    public $lastName;
    public $notes;
    public $active;

    protected $_passwordLength = 8;

    protected $_hashType;
    protected $_preHashPassword;
    protected $_oldPassword;
    protected $_userSession;
    protected $_roles;
    protected $_hasRoleCache = array();

    /**
     * Internal options array
     */
    public $emailValidationOptions = array(
        'mx'       => false,
        'deep'     => false,
        'domain'   => false, // true will enable domain validation
        'allow'    => Zend_Validate_Hostname::ALLOW_DNS,
        'hostname' => null
    );

    /**
     * Set hash algorithm to use when encrypting/decrypting passwords
     *
     * @param   string $hashType
     * @return  void
     */
    public function setHashType($hashType)
    {
        $this->_hashType = $hashType;
    }

    /**
     * Set user session instance
     *
     * @param   Ovoyo_Authentication_UserSession $userSession
     * @return  void
     */
    public function setUserSession($userSession)
    {
        $this->_userSession = $userSession;
    }

    /**
     * Change password
     *
     * @param   string $oldPassword Old password
     * @param   string $newPassword New Password
     * @param   string $passwordConfirmation New password confirmation
     * @return  boolean
     * @throws  Exception if any db errors occur
     */
    public function changePassword($oldPassword, $newPassword, $passwordConfirmation)
    {
        $errors = false;

        // validate credentials
        if (isset($this->_hashType)) {
            $oldPassword = hash($this->_hashType, $oldPassword);
        }

        // check old password is correct
        if ($oldPassword != $this->password) {
            $error = 'Your old password was incorrect';
            $this->invalidFields['oldPass'] = true;
            Ovoyo_ErrorHandler::addError($error);
            $errors = true;
        }

        // check password confirmation is correct
        if ($newPassword != $passwordConfirmation) {
            $this->invalidFields['newPass'] = true;
            $this->invalidFields['confirmPass'] = true;
            $error = 'Your password confirmation was incorrect';
            Ovoyo_ErrorHandler::addError($error);
            $errors = true;
        }

        // check new password is valid
        if (!$this->_validatePassword($newPassword)) {
            $this->invalidFields['newPass'] = true;
            $this->invalidFields['confirmPass'] = true;
            $errors = true;
        }

        // apply hash to new password
        if (isset($this->_hashType)) {
            $newPassword = hash($this->_hashType, $newPassword);
        }

        // check new password and old password are not the same
        if ($this->password == $newPassword) {
            $this->invalidFields['oldPass'] = true;
            $this->invalidFields['newPass'] = true;
            $this->invalidFields['confirmPass'] = true;
            $error = 'New password and old password are the same';
            Ovoyo_ErrorHandler::addError($error);
            $errors = true;
        }

        if ($errors) {
            return false;
        }

        // change password
        try {
            $this->password = $newPassword;
            if (!$this->update()) {
                return false;
            }

        } catch (Exception $e) {
            $this->password = $oldPassword;
            throw $e;
        }

        return true;
    }

    /**
     * Generate a random passwword
     *
     * @return  string
     */
    public function generatePassword()
    {
        $password = '';

        srand((double) microtime() * 1000000);

        for ($n=0; $n < $this->_passwordLength; $n++) {
            $varchar = rand(1,3);
            switch ($varchar) {
                case 1: $password.= chr(rand(97,122));
                        break;
                case 2: $password.= chr(rand(48,57));
                        break;
                case 3: $password.= chr(rand(65,90));
                        break;
            }
        }

        return $password;
    }

    /**
     * Reset password
     *
     * By default, a call to resetPassword() will send an email to the user containing
     * their new login details.  It is possible to add a custom message to this
     * email, as well as configure who the email is sent from.  Config options can
     * be passed as an array or Zend_Config object.  Possible config options are:
     *
     *  - 'customMessage' - an custom message to appear within the email
     *  - 'fromEmail' - the email address the messages will be sent from
     *  - 'fromName' - the email from name
     *  - 'applicationName' - the name of your application.  If not set will use the current server host
     *
     * @param   boolean $sendEmail Option to send password to user, defaults to true
     * @param   array|Zend_Config $emailConfig
     * @return  boolean
     */
    public function resetPassword($sendEmail = true, $emailConfig = null)
    {
        $this->beginTransaction();

        try {

            $this->password = $this->generatePassword();

            // apply hash to password
            $this->_preHashPassword = $this->password;
            if (isset($this->_hashType)) {
                $this->password = hash($this->_hashType, $this->password);
            }

            if (!$this->update()) {
                $this->rollback();
                return false;
            }

            if ($sendEmail) {
                if ($emailConfig instanceOf Zend_Config) {
                    $emailConfig = $emailConfig->toArray();
                } else if (!is_array($emailConfig)) {
                    $emailConfig = array();
                }

                // set defaults
                $customMessage = (isset($emailConfig['customMessage'])) ? $emailConfig['customMessage'] . "\n\n" : '';
                $fromEmail = (isset($emailConfig['fromEmail'])) ? $emailConfig['fromEmail'] : null;
                $fromName = (isset($emailConfig['fromName'])) ? $emailConfig['fromName'] : null;

                $currentHost = ($_SERVER['HTTPS'] == "on") ? "https://" : "http://";
                $currentHost.= $_SERVER['HTTP_HOST'] . '/';

                $host = (isset($emailConfig['applicationHost'])) ? $emailConfig['applicationHost'] : $currentHost;
                $applicationName = (isset($emailConfig['applicationName'])) ? $emailConfig['applicationName'] : $host;

                // subject
                $subject = 'Your password has been reset';

                // message body
                $message = 'Hi ' . $this->firstName . "\n\n"
                         . 'Your password for <b><a href="' . $host . '">' . $applicationName . '</a></b> has been reset.  Your new details are below' . "\n\n"
                         . $customMessage
                         . 'New Password: <b>' . $this->_preHashPassword . "</b>\n\n"
                         . 'Make sure to keep these details safe and delete this email as soon as possible.';

                // send invite
                require_once 'Zend/Mail.php';

                $mail = new Zend_Mail();
                $mail->setFrom($fromEmail, $fromName);
                $mail->addTo($this->email);
                $mail->setSubject($subject);
                $mail->setBodyHtml(nl2br($message));
                $mail->setBodyText(strip_tags($message));
                if (!$mail->send()) {
                    $this->rollback();
                    return false;
                }
            }

            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }

        return true;

    }

    /**
     * Peform validation
     *
     * @return  boolean
     */
    public function isValid()
    {
        $valid = parent::isValid();

        // load validators
        Zend_Loader::loadClass('Zend_Validate_StringLength');
        Zend_Loader::loadClass('Zend_Validate_Alnum');
        Zend_Loader::loadClass('Zend_Validate_EmailAddress');

        $emailAddress = new Zend_Validate_EmailAddress($this->emailValidationOptions ?: array());

        if (!$emailAddress->isValid($this->email)) {
            Ovoyo_ErrorHandler::addError("Supplied email address is invalid");
            $this->invalidFields['email'] = true;
            $valid = false;
        }

        // only validate password on insert - changePassword() handles updates
        if (!$this->userId) {
            $password = ($this->_preHashPassword) ? $this->_preHashPassword : $this->password;
            if (!$this->_validatePassword($password)) {
                $this->invalidFields['password'] = true;
                $valid = false;
            }
        }

        return $valid;
    }

    /**
     * Validate password
     *
     * @param string $password
     * @return boolean
     */
    protected function _validatePassword($password)
    {
        $valid = true;

        // password length
        $passwordLength = new Zend_Validate_StringLength(4, 16);
        if (!$passwordLength->isValid($password)) {
            $error = 'Password must be between 4 and 16 characters long';
            Ovoyo_ErrorHandler::addError($error);
            $valid = false;
        }

        // password characters
        $passwordAlnum = new Zend_Validate_Alnum();
        if (!$passwordAlnum->isValid($password)) {
            $error = 'Password can only contain letters and numbers';
            Ovoyo_ErrorHandler::addError($error);
            $valid = false;
        }

        return $valid;
    }

    /**
     * Set the list of roles a user is assigned to
     *
     * @param   array $roles List of role ids that user is assigned to
     * @return  boolean
     * @throws  Exception
     */
    public function setRoles(array $roles)
    {
            // check for user id
        if (!$this->userId) {
            Ovoyo_ErrorHandler::addError('No user specified');
            return true;
        }

        // if user is a superuser, then just return true
        if ($this->superuser) {
            return true;
        }

        $this->beginTransaction();
        try {
            // remove existing roles
            $userRole = Ovoyo_Model::factory(
                'Ovoyo_Application_UserRole', $this->_modelOptions()
            );
            $deleteBy = array('userId' => $this->userId);
            if (!$userRole->deleteBy($deleteBy)) {
                $this->rollback();
                return false;
            }

            // insert new roles
            $userRole->userId = $this->userId;
            foreach ($roles AS $role) {
                $userRole->role = $role;
                if (!$userRole->insert()) {
                    $this->rollback();
                    return false;
                }
            }

            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }

        return true;
    }

    /**
     * Get roles user is assigned to
     *
     * @return  array
     */
    public function getRoles()
    {
        if (isset($this->_roles)) {
            return $this->_roles;
        }

        $this->_roles = array();

        // get available roles
        $acl = Ovoyo_Application_Security::getAcl();
        $availableRoles = array_flip($acl->getRoles());

        $userRole = Ovoyo_Model::factory(
            'Ovoyo_Application_UserRole', $this->_modelOptions()
        );
        $searchFilter = array('userId' => $this->userId);

        $results = $userRole->search($searchFilter)->fetchAll(
            Zend_Db::FETCH_OBJ
        );

        foreach ($results AS $row) {
            if (!$availableRoles[$row->role]) {
                continue;
            }
            $this->_roles[$row->role] = $row->role;
        }

        return $this->_roles;
    }

    /**
     * Check to see if user has a particular role
     *
     * @param string $role
     * @return boolean
     */
    public function hasRole($role)
    {
        if ($this->superuser) {
            return true;
        }

        if (isset($this->_hasRoleCache[$role])) {
            return $this->_hasRoleCache[$role];
        }

        // first check that role is valid within the ACL
        $acl = Ovoyo_Application_Security::getAcl();
        if (!$acl->hasRole($role)) {
            $this->_hasRoleCache[$role] = false;
            return false;
        }

        // now check role has been assigned to user
        $hasRole = false;
        $userRoles = $this->getRoles();
        foreach ($userRoles AS $userRole) {
            if ($userRole == $role) {
                $hasRole = true;
                break;
            } else if ($acl->inheritsRole($userRole, $role)) {
                $hasRole = true;
                break;
            }
        }

        $this->_hasRoleCache[$role] = $hasRole;
        return $hasRole;
    }

    /**
     * Set length of auto-generated passwords
     *
     * @param   int $length
     * @return  void
     */
    public function setPasswordLength($length)
    {
        $this->_passwordLength = $length;
    }

    /**
     * Filter search
     *
     * @param   Zend_Db_Select $select
     * @param   array|string $filterOptions Search options - can be an array of key/values pairs or a string
     * @return  void
     */
    protected function _searchFilter($select, $filterOptions)
    {
        // where clause has been supplied as array - expecting key => value pairs
        if (is_array($filterOptions)) {
            foreach ($filterOptions AS $option => $value) {
                if (!is_int($option) && $option == 'searchString') {
                    $value.= '%';
                    $where = 'u.email LIKE ? '
                           . 'OR u.firstName LIKE ? '
                           . 'OR u.lastName LIKE ?';
                    $select->where($where, $value, $value, $value);

                } else if (is_numeric($option)) {
                    $select->where($value);
                } else {
                    $select->where($option . ' = ?', $value);
                }
            }

        // where clause has been supplied as a string
        } else if (is_string($filterOptions)) {
            $select->where($filterOptions);

        }
    }

    /**
     * Order search
     *
     * @param   Zend_Db_Select $select
     * @param   array|string $order Order options as a string or key/value pairs
     * @return  void
     */
    protected function _searchOrder($select, $order = null)
    {
        if (is_array($order)) {
            foreach ($order AS $orderString) {
                $select->order($orderString);
            }

        } else if ($order != '') {
            $select->order($order);

        } else {
            $select->order('email DESC');
        }
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Ovoyo_Model#preInsert()
     */
    public function preInsert()
    {
        $tableInfo = $this->tableInfo();
        if (isset($tableInfo['metadata']['createdDate'])) {
            $this->createdDate = Zend_Date::now();
        }

        // apply hash to password
        $this->_preHashPassword = $this->password;
        if (isset($this->_hashType)) {
            $this->password = hash($this->_hashType, $this->password);
        }

        return true;
    }

    /**
     * Perform login
     *
     * @param   string $email Email
     * @param   string $password Password
     * @param   string $sessionIdentifier Session identifier
     * @param   boolean $ignoreHash
     * @return  boolean
     */
    public function login($email, $password, $sessionIdentifier, $ignoreHash = false)
    {
        // error check
        if (!$email || !$password) {
            $error = 'You must provide both email address and password';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }

        // validate credentials
        if (isset($this->_hashType) && !$ignoreHash) {
            $password = hash($this->_hashType, $password);
        }

        $fetchBy = array('email' => $email, 'password' => $password);

        // add deleted flag if required
        $tableInfo = $this->tableInfo();
        if (isset($tableInfo['metadata']['deleted'])) {
            $fetchBy['deleted'] = 0;
        }

        if (!$this->fetchBy($fetchBy)) {
            $error = 'You provided an incorrect email address and password';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }

        // check user's account is still active
        if (!$this->active) {
            $error = 'Your account is no longer active, please speak to '
                   . 'your system administrator';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }

        $this->beginTransaction();
        try {
            // create new session for user
            $sessionStarted = $this->_userSession->startSession(
                $this->userId, $sessionIdentifier
            );

            if (!$sessionStarted) {
                $this->rollback();
                return false;
            }

            // update last login date/time if exists
            if (isset($tableInfo['metadata']['lastLogin'])) {
                $this->lastLogin = Zend_Date::now();

                $colInfo = $tableInfo['metadata']['lastLogin'];
                if ($colInfo['DATA_TYPE'] == 'date') {
                    $colValue = $this->lastLogin->get('yyyyMMdd');
                } else {
                    $colValue = $this->lastLogin->get('yyyyMMddHHmmss');

                }

                $data = array('lastLogin' => $colValue);
                $where = 'userId=' . $this->userId;
                $this->_dbTable->update($data, $where);
            }

            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }

        return true;
    }

    /**
     * Perform logout
     *
     * @param   string $sessionIdentifier Session identifier
     * @return  boolean
     */
    public function logout($sessionIdentifier)
    {
        return $this->_userSession->end($sessionIdentifier);
    }

    /**
     * Get user's full name
     *
     * @return
     */
    public function name()
    {
        $name = '';
        $name = $this->firstName;
        if ($this->lastName) {
            $name.= ($name) ? " $this->lastName" : $this->lastName;
        }
        return $name;
    }

    /**
     * Pre login actions
     *
     * @return void
     */
    public function preLogin()
    {}

    /**
     * Post login actions
     *
     * @return void
     */
    public function postLogin()
    {}

    /**
     * Pre logout actions
     *
     * @return void
     */
    public function preLogout()
    {}

    /**
     * Post logout actions
     *
     * @return void
     */
    public function postLogout()
    {}
}
