<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * System message
 *
 * A class for handling simple info messages e.g. success messages, warning messages etc.
 * Error messages should be handled via Ovoyo_ErrorHandler class
 * Implements the Singleton pattern as their should only ever
 * be a single system message instance
 *
 * @package    Ovoyo_SystemMessage
 */
class Ovoyo_SystemMessage
{
    private static $_instance;
    private static $_disableSessionStore = false;

    private $_messages = array();
    private $_nonPersistent = array();
    private $_session;

    const DEFAULT_TYPE = 'success';

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        /**
         * @see Zend_Session_session
         */
        require_once 'Zend/Session/Namespace.php';

        // create session name space and fetch an existing messages
        $this->_session = new Zend_Session_Namespace('Ovoyo_SystemMessage');
        if (isset($this->_session->messages)) {
            $this->_messages = $this->_session->messages;
        }
    }
    
    /**
     * Disable storing of messages in the session
     *
     * @return void
     */
    public static function disableSessionStore()
    {
        self::reset();
        self::$_disableSessionStore = true;
    }

    /**
     * Get system message instance
     *
     * @return  obj Ovoyo_WebContainer instance
     */
    private static function _getInstance()
    {
        if (self::$_instance === null) self::$_instance = new self;
        return self::$_instance;
    }

    /**
     * Add system message
     *
     * The $persistent flag is use to specify if message should be saved to
     * session and remain persistent across page loads
     *
     * @param string $message message
     * @param string $type Type of message
     * @param boolean $persistent
     * @return void
     */
    public static function addMessage($message, $type = '', $persistent = true)
    {
        $type = ($type) ? $type : self::DEFAULT_TYPE;

        $instance = self::_getInstance();
        $instance->_messages[$type][] = $message;
        
        if (!$persistent) {
            $instance->_nonPersistent[$type][$message] = 1;
        }
        $instance->_copyToSession();
    }

    /**
     * Check if any messages have been recorded
     *
     * @return  bool
     */
    public static function anyMessages()
    {
        $instance = self::_getInstance();
        return (count($instance->_messages) > 0) ? true : false;
    }

    /**
     * Reset messages
     *
     * @param   string $type Type of messages to reset
     * @return  void
     */
    public static function reset($type = '')
    {
        $instance = self::_getInstance();
        
        if ($type) {
            $instance->_messages[$type] = array();
        } else {
            $instance->_messages = array();
        }
        
        $instance->_copyToSession();
    }

    /**
     * Display messages
     *
     * @param   string $type Type of messages to display
     * @param   bool $reset Flag to specify if messages should be reset after being displayed
     * @param   bool $suppressHtml Flag to specify if HTML should be supressed, messages will be returned in text format
     * @return  void
     */
    public static function displayMessages($type = '', $reset = true, $suppressHtml = false)
    {
        echo Ovoyo_SystemMessage::getMessageString($type, $reset, $suppressHtml);
    }

    /**
     * Get message string
     *
     * @param   string $type Type of messages to display
     * @param   bool $reset Flag to specify if messages should be reset after being displayed
     * @param   bool $suppressHtml Flag to specify if HTML should be supressed, messages will be returned in text format
     * @return  void
     */
    public static function getMessageString($type = '', $reset = true, $suppressHtml = false)
    {
        $instance = self::_getInstance();

        // if no type has been supplied, display all
        if ($type) {
            $messages[$type] = $instance->_messages[$type];
        } else {
            $messages = $instance->_messages;
        }

        $messageString = '';
        foreach ($messages AS $type => $typeMessages) {
            if (count($typeMessages) == 0) {
                continue;
            }
            $messageString.= '<ul class="' . $type . '">';
            foreach ($typeMessages AS $message) {
                $messageString.= '<li>';
                $messageString.= $message;
                $messageString.= '</li>';

                // always add a \n here in case HTML is suppressed
                $messageString.= "\n";
            }
            $messageString.= '</ul>';

            if ($reset) {
                self::reset($type);
            }
        }
        
        $messageString = ($suppressHtml) ? strip_tags($messageString) : $messageString;
        return $messageString;
    }
    
    /**
     * Copy messages from internal array to namespace session
     *
     * @return  void
     */
    private function _copyToSession()
    {
        if (self::$_disableSessionStore) {
            return;
        }
        
        // for some reason, we need to copy the messages array in order to store it in our session
        $messages = array();
        foreach ($this->_messages AS $type => $type_messages) {
            foreach ($type_messages AS $message) {
                // don't copy non-persistent messages
                if ($this->_nonPersistent[$type][$message]) {
                    continue;
                }
                $messages[$type][] = $message;
            }
        }
        
        $this->_session->messages = $messages;
    }
}
