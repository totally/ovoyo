<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Zend_Config_Xml
 */
require_once 'Zend/Config/Xml.php';

/**
 * @see Zend_Acl
 */
require_once 'Zend/Acl.php';

/**
 * @see Ovoyo_Model
 */
require_once 'Ovoyo/Model.php';

/**
 * Application security
 *
 * @package    Ovoyo_Application
 */
class Ovoyo_Application_Security
{
    // object instance
    private static $instance;
    
    private static $_adapter;
    
    private $_configs = array();
    
    private $_acl;
    private $_lastProcessedLog = array();
    private $_routes = array();
    private $_authentication = array();
    private $_users = array();
    
    private $_config = array();
    
    protected $_defaultAdapter = 'Ovoyo_Authentication_Adapter_Simple';
    protected $_defaultLoginUrl = '/login/';
    
    const DEFAULT_MODULE = 'default';
    const REGISTRY_CACHE_ID = 'ovoyo_app_acl';

    /**
     * Constructor
     *
     * @return  void
     */
    public function __construct()
    {
        $cache = Ovoyo_WebContainer::getInstance()->cache;
        
        if (!isset($cache)) {
            return;
        }
        
        // get acl from cache
        if ($registry = $cache->load(self::REGISTRY_CACHE_ID)) {
            $this->_acl = $registry['acl'];
            $this->_lastProcessedLog = $registry['lastProcessedLog'];
            $this->_authentication = $registry['authentication'];
            $this->_routes = $registry['routes'];
            $this->_users = $registry['users'];
        }
        
        if (!isset($this->_acl)) {
            $this->_acl = new Zend_Acl;
        }
    }
    
    /**
     * Get instance
     *
     * @return obj Ovoyo_WebContainer instance
     */
    public static function getInstance()
    {
        if (self::$instance === null) self::$instance = new self;
        return self::$instance;
    }
    
    /**
     * Update registry and store to cache
     *
     * @return  void
     */
    private function _updateRegistry()
    {
        $cache = Ovoyo_WebContainer::getInstance()->cache;
        if (!isset($cache)) {
            return;
        }
        
        $registry = array(
            'acl' => $this->_acl,
            'lastProcessedLog' => $this->_lastProcessedLog,
            'routes' => $this->_routes,
            'authentication' => $this->_authentication,
            'users' => $this->_users
        );
        
        $cache->save($registry, self::REGISTRY_CACHE_ID);
    }
    
    /**
     * Force rebuild of security registry
     *
     * @return  void
     */
    public static function rebuildRegistry()
    {
        // rebuild is forced by clearing the registry cache
        $cache = Ovoyo_WebContainer::getInstance()->cache;
        if (!isset($cache)) {
            return;
        }
        
        $cache->save(null, self::REGISTRY_CACHE_ID);
    }
    
    /**
     * Initialise security
     *
     * Involves processsing any security.xml files found under primary config
     * directory, and also under any module specific config directories
     *
     * @return  void
     */
    public static function initialise()
    {
        $security = self::getInstance();
        
        // build list of config files to process
        $security->_configs = array();
            
        // path to primary security.xml
        $securityXml = Ovoyo_WebContainer::getConfigDir()
                     . DIRECTORY_SEPARATOR . 'security.xml';
        
        if (is_file($securityXml)) {
            $module = self::DEFAULT_MODULE;
            $security->_configs[$module] = $securityXml;
        }
        
        // add module specific security.xml files
        $modules = Ovoyo_WebContainer::getModules();
        
        foreach ($modules AS $module => $directory) {
            $securityXml = $directory . DIRECTORY_SEPARATOR
                         . 'config' . DIRECTORY_SEPARATOR . 'security.xml';
            
            if (is_readable($securityXml)) {
                $security->_configs[$module] = $securityXml;
            }
        }
        
        // loop over each config - if file is new or has changed we need to
        // re-process all configs
        $processConfigs = false;
        foreach ($security->_configs AS $module => $securityXml) {
            // file is new
            if (!$lastProcessed = $security->_lastProcessedLog[$module]) {
                $processConfigs = true;
                break;
                
            // check to see if file has changed
            } else {
                $stat = stat($securityXml);
                if ($lastProcessed < $stat['mtime']) {
                    $processConfigs = true;
                    break;
                }
            }
        }
        
        // process configs
        //$processConfigs = true;
        if ($processConfigs) {
            $security->_config = array(
                'routes' => array(),
                'authentication' => array(),
                'users' => array(),
                'routes' => array()
            );
            
            // load each config file - data is collated into $security->_config
            // array
            foreach ($security->_configs AS $module => $securityXml) {
                $security->_loadConfigFile($securityXml, $module);
            }
            
            // build acl
            $security->_buildAcl();
            
            // assign updated routes, authentication and user data
            $security->_routes = $security->_config['routes'];
            $security->_authentication = $security->_config['authentication'];
            $security->_users = $security->_config['users'];
            
            // set last process log times
            foreach ($modules AS $module => $directory) {
                $security->_lastProcessedLog[$module] = time();
            }
            
            // save updated details to registry
            $security->_updateRegistry();
        }
        
        // apply any routes
        if (is_array($security->_routes)) {
            $router = Ovoyo_WebContainer::getInstance()->getRouter();
            foreach ($security->_routes AS $url => $route) {
                $router->addRoute(
                    $url,
                    new Zend_Controller_Router_Route(
                        $url,
                        $route['defaults']
                    )
                );
            }
        }
    }
    
    /**
     * Load security config from an xml file
     *
     * @param   string $xmlFile Path to xml file containing security info
     * @param   string $module
     * @return  void
     */
    private function _loadConfigFile($xmlFile, $module)
    {
        $config = new Zend_Config_Xml($xmlFile);
        $config = $config->toArray();
        
        // process authentication rules
        $this->_processAuthentication($config['authentication']);
                
        // process roles
        $this->_processRoles($config['roles'], $module);
        
        // process users
        $this->_processUsers($config['users']);
        
        // process users
        $this->_processRoutes($config['routes']);
    }
    
    /**
     * Process authentication rules
     *
     * @param   array $authentication
     * @return  void
     */
    private function _processAuthentication($authentication)
    {
        $config =& $this->_config['authentication'];
        
        // hash alogrithm used for password encyrption
        if (!isset($config['hashType'])) {
            $hashType = $authentication['hashType'];
            $config['hashType'] = $hashType;
        }
         
        // deal with defaults
        if (!isset($config['defaults'])) {
            $config['defaults'] = array();
        }
        
        // apply new auth defaults to config
        if (is_array($authentication['defaults'])) {
            foreach ($authentication['defaults'] AS $key => $value) {
                // don't allow overrides of default values
                if (!isset($config['defaults'][$key])) {
                    $config['defaults'][$key] = $value;
                }
            }
        }
        
        // deal with realms
        $realms = array();
        if (is_array($authentication['realms']['realm'])) {
            $realms = $this->_zendConfigMultiItemArrayTidy(
                $authentication['realms']['realm']
            );
        }
        
        $adapterDefaultControllers = array();
        foreach ($realms AS $realm) {
            if (!$url = $realm['url']) {
                continue;
            }
            
            // get adapter
            if (isset($realm['adapter'])) {
                $adapter = $realm['adapter'];
            } else if (isset($config['defaults']['adapter'])) {
                $adapter = $config['defaults']['adapter'];
                $realm['adapter'] = $adapter;
            } else {
                $adapter = $this->_defaultAdapter;
                $realm['adapter'] = $adapter;
            }
            
            // put exceptions into correct format
            if (isset($realm['exceptions']) 
                && array_key_exists('exception', $realm['exceptions'])) {
                $realm['exceptions'] = $this->_zendConfigMultiItemArrayTidy(
                    $realm['exceptions']['exception']
                );
            }
            
            if (isset($realm['loginUrl'])) {
                $loginUrl = $realm['loginUrl'];
            } else if (isset($config['defaults']['loginUrl'])) {
                $loginUrl = $config['defaults']['loginUrl'];
                $realm['loginUrl'] = $loginUrl;
            } else {
                $loginUrl = $this->_defaultLoginUrl;
                $realm['loginUrl'] = $loginUrl;
            }
            
            $config['realms'][$url] = $realm;
            
            
            // add "login" routes
            require_once 'Ovoyo/Authentication.php';
            $adapterInstance = Ovoyo_Authentication::factory(
                $adapter, $authentication['defaults'], $realm
            );
            
            if (!$adapterInstance instanceof Ovoyo_Authentication_Adapter_Override) {
                
                // get default controller for adapter
                if (!array_key_exists($adapter, $adapterDefaultControllers)) {
                    Ovoyo_ClassLoader::load($adapter);
                    $defaultController = call_user_func(
                        $adapter . "::getDefaultControllerClass"
                    );
                    $adapterDefaultControllers[$adapter] = $defaultController;
                }
                
                $defaultController = $adapterDefaultControllers[$adapter];
                    
                $routeDefaults = array();
                if (isset($realm['loginController'])) {
                    // deal with two formats for legacy purposes
                    if (is_array($realm['loginController'])
                        && array_key_exists('controller', $realm['loginController'])) {
                        $routeDefaults = $realm['loginController'];
                        
                    // this is the preferred and expected format
                    } else {
                        $routeDefaults = array(
                            'controller' => $realm['loginController']
                        );
                    }
                    
                } else {
                    Ovoyo_ClassLoader::load($adapter);
                    $controller = call_user_func(
                        $adapter . "::getDefaultControllerClass"
                    );
                    $routeDefaults = array(
                        'controller' => $controller
                    );
                }
                    
                $routeUrl = $loginUrl;
                if (preg_match('/\/$/', $routeUrl)) {
                    $routeUrl.= '*';
                }
                $route = array(
                    'url' => $routeUrl,
                    'defaults' => $routeDefaults
                );
                
                // add new route if it doesn't exist yet
                if (!array_key_exists($loginUrl, $this->_config['routes'])) {
                    $this->_config['routes'][$loginUrl] = $route;
                    
                // update existing route if controller is not the default
                } else if ($routeDefaults['controller'] != $defaultController) {
                    $this->_config['routes'][$loginUrl] = $route;
                }
            }
        }
    }
    
    /**
     * Process roles in config file
     *
     * @param   array $roles
     * @param   string $module
     * @return  void
     * @throws  Exception
     */
    private function _processRoles($roles, $module)
    {
        $roles = $this->_zendConfigMultiItemArrayTidy($roles['role']);
        
        // process each role
        foreach ($roles AS $role) {
            if (is_null($role)) {
                continue;
            }
            $roleName = $role['name'];
            
            // put parents list into an array
            if ($parents = $role['parents']) {
                $parents = explode(",", $parents);
                foreach ($parents AS $key => $parent) {
                    $parents[$key] = trim($parent);
                }
            } else {
                $parents = array();
            }
            
            // role already exists in our config
            if (isset($this->_config['roles'][$roleName])) {
            
                // update existing values
                $roleConfig = $this->_config['roles'][$roleName];
                $roleConfig['parents'] = $parents;
                
            // role is new
            } else {
                $roleConfig = array(
                    'role' => $roleName,
                    'parents' => $parents
                );
            }
            
            
            $this->_config['roles'][$roleName] = $roleConfig;
        }
    }
    
    /**
     * Process users in config file
     *
     * @param   array $users
     * @return  void
     * @throws  Exception
     */
    private function _processUsers($users)
    {
        $users = $this->_zendConfigMultiItemArrayTidy($users['user']);

        // process each user
        foreach ($users AS $user) {
            if (!$email = $user['email']) {
                continue;
            }
            
            
            // put roles in useful format
            $roles = $this->_zendConfigMultiItemArrayTidy($user['roles']['role']);
            
            // is user exists, just update roles
            if (isset($this->_config['users'][$email])) {
                foreach ($roles AS $role) {
                    $this->_config['users'][$email]['roles'][$role] = $role;
                }
                
            // add user
            } else {
                $this->_config['users'][$email] = $user;
                
                // put roles in more useful format
                $this->_config['users'][$email]['roles'] = array();
                foreach ($roles AS $role) {
                   $this->_config['users'][$email]['roles'][$role] = $role;
                }
            }
        }
    }
    
    /**
     * Process routes
     *
     * @param   array $users
     * @return  void
     */
    private function _processRoutes($routes)
    {
        if (!is_array($routes) || count($routes) == 0) {
            return;
        }
    
        $router = Ovoyo_WebContainer::getInstance()->getRouter();
        foreach ($routes AS $key => $route) {
            $type = ($route['type']) ? $route['type']
            : 'Zend_Controller_Router_Route';
            $router->addRoute(
            $key,
            new $type(
            $route['route'],
            $route['defaults']
            )
            );
        }
    }
    
    /**
     * Build acl
     *
     * @return  void
     */
    private function _buildAcl()
    {
        $this->_acl = new Zend_Acl;
        
        // add all modules as resources
        $this->_acl->addResource(
            new Zend_Acl_Resource(self::DEFAULT_MODULE)
        );
        $modules = Ovoyo_WebContainer::getModules();
        foreach ($modules AS $module => $directory) {
            $this->_acl->addResource(
                new Zend_Acl_Resource($module)
            );
        }
        
        // add roles
        if (is_array($this->_config['roles'])) {
            foreach ($this->_config['roles'] AS $role) {
                $this->_acl->addRole(
                    new Zend_Acl_Role($role['role']), $role['parents']
                );
            }
        }
    }
    
    /**
     * Apply authentication
     *
     * @return  void
     */
    public static function applyAuthentication()
    {
        $webContainer = Ovoyo_WebContainer::getInstance();
        
        $security = self::getInstance();
        $authentication = $security->_authentication;
        
        if (!is_array($authentication['realms'])) {
            return;
        }
            
        $match = null;
        
        // check to see if we have a trailing slash on end of url
        if (!preg_match('/\/$/', $webContainer->getParam('urlPath'))) {
            $urlPath = $webContainer->getParam('urlPath') . '/';
        }
        
        foreach ($authentication['realms'] AS $realm) {
            // if url matches request url, try and authenticate
            $pattern = preg_replace("/\//", "\/", $realm['url']);
            $pattern = preg_replace("/\*/", ".*", $pattern);
            
            if (preg_match("/$pattern/", $urlPath)) {
                $score = strlen($realm['url']);
                if (!isset($match['score']) || $score > $match['score']) {
                    $match['score'] = $score;
                    $match['realm'] = $realm;
                }
            }
        }
        
        if (isset($match['realm'])) {
            require_once 'Ovoyo/Authentication.php';
            
            $realm = $match['realm'];
            $realm['hashType'] = $authentication['hashType'];
            $auth = Ovoyo_Authentication::factory(
                $realm['adapter'], $authentication['defaults'], $realm
            );
            
            self::$_adapter = $auth;
            $auth->authenticate();

            // override controller path
            if ($realm['controllerPath']) {
                if (preg_match("/^\//", $realm['controllerPath'])) {
                    $webContainer->setControllerDirectory(
                        $realm['controllerPath']
                    );
                } else {
                    $webContainer->setControllerDirectory(
                        APPLICATION_ROOT . '/' . $realm['controllerPath']
                    );
                }
            }

            return;
        }
        
    }
    
    /**
     * Get authentication adapter
     *
     * @return Ovoyo_Authentication_Adapter
     */
    public static function getAuthenticationAdapter()
    {
        return self::$_adapter;
    }
    
    /**
     * Get acl
     *
     * @return  Zend_Acl
     */
    public static function getAcl()
    {
        $security = self::getInstance();
        return $security->_acl;
    }
    
    /**
     * Get users
     *
     * Will only return users specified in config files
     *
     * @return  array
     */
    public static function getUsers()
    {
        $security = self::getInstance();
        return $security->_users;
    }

    /**
     * Get user
     *
     * @param   string $email
     * @return  array|false
     */
    public static function getUser($email)
    {
        $security = self::getInstance();
        foreach ($security->_users AS $user) {
            if ($user['email'] == $email) {
                return $user;
            }
        }
        
        return false;
    }
    
    /**
     * Fix issue with Zend_Config toArray() method for items which can have
     * one or more child elements of the same type
     *
     * @param   array $array
     * @return  array
     */
    private function _zendConfigMultiItemArrayTidy($array)
    {
        if (!is_array($array)) {
            return array($array);
        }
        
        $key = key($array);
        if ($key !== 0) {
            $array = array($array);
        }
        
        return $array;
    }
}
