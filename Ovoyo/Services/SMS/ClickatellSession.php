<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

require_once 'Ovoyo/Model.php';
require_once 'Ovoyo/Services/SMS/Clickatell.php';

/**
 * Clickatell_SMS_Session class
 *
 * @package	Ovoyo_Services
 * @subpackage	SMS
 */
class Ovoyo_Services_SMS_ClickatellSession extends Ovoyo_Model
{
    public $sessionId;

    private $apiId;
    private $username;
    private $password;
    private $httpClient;

    /**
     * Constructor
     *
     *
     * @param	string $apiId
     * @param	string $username
     * @param	string $password
     * @return	void
     */
    public function setParams($apiId, $username, $password)
    {
        $this->apiId = $apiId;
        $this->username = $username;
        $this->password = $password;

        $httpConfig = array('timeout' => Ovoyo_Services_SMS_Clickatell::SMS_HTTP_TIMEOUT, 'keepalive' => true);
        $this->httpClient = new Zend_Http_Client(Ovoyo_Services_SMS_Clickatell::CLICKATELL_HTTP_SEND_INTERFACE, $httpConfig);
    }

    /**
     * Fetches a specific from the database and populates this class instance
     * properties. Refreshes session if still valid.
     *
     * @param	int $sessionId The session ID to fetch
     * @return	boolean
     */
    public function fetch($sessionId)
    {
        if (parent::fetch($sessionId)){
            $this->refreshSession();
            return true;
        }
        return false;
    }

    /**
     * Fetches the latest session from the database and initialises the
     * class parameters
     *
     * @return	boolean
     */
    public function fetchLatestSession()
    {
        $sql = 'SELECT sessionId'
             . '  FROM clickatellSession'
             . ' ORDER BY createdDate DESC';

        $stmt = $this->_db->query($sql);

        if ($sessionId = $stmt->fetchColumn()){
            return $this->fetch($sessionId);
        }
    }

    /**
     * Checks if there is also a valid session available. If not, creates
     * a new session
     *
     * Returns Session ID if successful, FALSE if failure
     *
     * @return int|false
     */
    public function create($no_fetch = false)
    {
        if (!$no_fetch && $this->fetchLatestSession()) {
            return $this->sessionId;
        }

        $parameters = array();
        $parameters['api_id'] = $this->apiId;
        $parameters['user'] = $this->username;
        $parameters['password'] = $this->password;

        $response = $this->send_HTTP_request(
            Ovoyo_Services_SMS_Clickatell::CLICKATELL_HTTP_SESSION_INTERFACE,
            $parameters
        );

        if (substr($response, 0, 2) != 'OK') {
            Ovoyo_ErrorHandler::addError($response);
            return false;
        }

        $this->sessionId = substr($response, 4);

        $this->_db->query(
        	'REPLACE INTO clickatellSession VALUES(?, NOW())',
            array($this->sessionId)
        );

        return $this->sessionId;
    }

    /**
     * Refreshes the current session instance
     *
     * @return	boolean
     */
    public function refreshSession()
    {
        if (!$this->sessionId) {
            return false;
        }

        $parameters['sessionId'] = $this->sessionId;
        $response = $this->send_HTTP_request(
            Ovoyo_Services_SMS_Clickatell::CLICKATELL_HTTP_PING_INTERFACE,
            $parameters
        );

        if (substr($response, 0, 2) != 'OK') {
            return false;
        }

        $this->update();

        return true;
    }

    /**
     * Sends an HTTP request to the specific URL, and attaches any parameters
     *
     * Returns the string of the returned value of the HTTP request, or
     * false if there was an error
     *
     * @param	array Associative array of parameters to pass
     * @return	string|false
     */
    public function send_HTTP_request($url, &$parameters)
    {
        $client = $this->httpClient;
        $client->resetParameters();

        $client->setUri($url);
        $client->setParameterGet($parameters);

        $response = $client->request();

        if ($response->isError()) {
            return false;
        }

        return $response->getBody();
    }
}