<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Email marketing campaign class
 *
 * @package    Ovoyo_Services_OnlineMarketing
 */
class Ovoyo_Services_OnlineMarketing_Campaign
{
    /**
     * Factory for Ovoyo_Services_OnlineMarketing_Campaign classes
     *
     * @param   string $provider Provider of marketing services
     * @param   array $config
     * @return  Ovoyo_Services_OnlineMarketing_Campaign_Abstract 
     * @throws  Exception
     */
    public static final function factory($provider, $config = null)
    {     
        $className = 'Ovoyo_Services_OnlineMarketing_Campaign_' . $provider;
        
        // if class already exists, simply return new instance
        if (class_exists($className)) {
            $model = new $className($config);
            return $model;
        }

        // if class physically exists but hasn't been loaded, then load it
        require_once 'Ovoyo/ClassLoader.php';
        if (Ovoyo_ClassLoader::exists($className)) {
            $model = new $className($config);
            return $model;
        }
        
        // class does not exist, so throw error
        throw new Exception("Unable to campaign class for provider '$provider'");
    }
}
