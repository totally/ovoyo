<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Mollom adapter
 *
 * Allows to insert captchas driven by Mollom service
 *
 * @see http://mollom.com/api/
 *
 * @category   Zend
 * @package    Zend_Captcha
 * @subpackage Adapter
 */
class Ovoyo_Services_Spam_Mollom_Captcha extends Zend_Captcha_Base
{
    /**
     * Constructor
     *
     * @param  array|Zend_Config $options
     * @return void
     */
    public function __construct($options = null)
    {
        $this->setService(new Zend_Service_ReCaptcha());
        parent::__construct($options);
    }

    /**
     * Set service object
     *
     * @param  Zend_Service_ReCaptcha $service
     * @return Zend_Captcha_ReCaptcha
     */
    public function setService($service)
    {
        $this->_service = $service;
        return $this;
    }

    /**
     * Retrieve ReCaptcha service object
     *
     * @return Zend_Service_ReCaptcha
     */
    public function getService()
    {
        return $this->_service;
    }
    
    /**
     * Generate captcha
     *
     * @see Zend_Form_Captcha_Adapter::generate()
     * @return string
     */
    public function generate()
    {
        return "";
    }

    /**
     * Validate captcha
     *
     * @see    Zend_Validate_Interface::isValid()
     * @param  mixed $value
     * @return boolean
     */
    public function isValid($value, $context = null)
    {

        return true;
    }

    /**
     * Render captcha
     *
     * @param  Zend_View_Interface $view
     * @param  mixed $element
     * @return string
     */
    public function render(Zend_View_Interface $view = null, $element = null)
    {
        return $this->getService()->getCaptchaHTML();
    }
}
