<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Email marketing list class
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage List
 */
class Ovoyo_Services_OnlineMarketing_List_VerticalResponse
      extends Ovoyo_Services_OnlineMarketing_List_Abstract
{
    /**
     * Contruct method
     * @param array $config
     */
    public function __construct($config)
    {
        foreach ($config AS $option => $value) {
            $this->{$option} = $value;
        }

        $this->setApiHandler($config);
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::create()
     */
    public function create($name)
    {
        $apiHandler = $this->getApiHandler();
        
        $params = array(
            'name' => $name,
            'type' => 'email',
            'custom_field_names' => array()
        );
        
        return $apiHandler->callApi('createList', $params);
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::delete()
     */
    public function delete()
    {
        // not implemented
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::getList()
     */
    public function getList()
    {
        // get subscriber count
        $apiHandler = $this->getApiHandler();
        
        $params = array(
            'list_id' => $this->listId
        );

        // using enumerateLists method here instead of getListMembers as the 
        // latter returns all user data i.e. too much detail
        $vrList = $apiHandler->callApi('enumerateLists', $params);
        $vrList = current($vrList);

        $list = array();

        $list['name'] = $vrList->name;
        $list['numSubscribers'] = $vrList->size;

        return $list;
    }

    /**
     * Get available lists
     *
     * @return  array
     */
    public function getLists()
    {
        $apiHandler = $this->getApiHandler();

        $params = array(
            'list_id' => $this->listId,
            'max_records' => 9999999
        );

        $members = $apiHandler->callApi('getListMembers', $params);

        return $members;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::addSubscriber()
     */
    public function addSubscriber($data)
    {
        $apiHandler = $this->getApiHandler();

        $params = array(
            'list_member' => array(
                'list_id' => $this->listId,
                'member_data' => array(
                    array(
                        'name' => 'email_address',
                        'value' => $data['email']
                    )
                )
            ),
            'validate_postal_address' => false
        );

        $members = $apiHandler->callApi('addListMember', $params);

        return $response;
    }

    /**
     * Batch version of addSubscriber
     */
    public function addSubscribers($data)
    {
        $apiHandler = $this->getApiHandler();
        
        // we fake a file upload here to use the appendFile API method
        
        $string = '';
        
        foreach ($data AS $key => $value) {
            $string.= implode(',', $value) . PHP_EOL;
        }

        $params = array(
            'list_id'    => $this->listId,
            'file'       => array(
                'filename'  => 'tempCsv',
                'delimiter' => 'csv',
                'contents'  => base64_encode($string),
            ),
            'fields' => array(
                'email_address',
                'first_name',
                'last_name',
                'title',
                'company_name',
                'address_1',
                'address_2',
                'city',
                'state',
                'postalcode',
                'country',
                'work_phone',
                'home_phone',
                'mobile_phone',
                'fax',
                'marital_status',
                'gender'
            ),
        );

        $response = $apiHandler->callApi('appendFileToList', $params);
        
        return $response;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::getSubscribers()
     */
    public function getSubscribers()
    {
        $apiHandler = $this->getApiHandler();

        $params = array(
            'method' => 'listMembers',
            'id' => $this->listId
        );

        $response = $apiHandler->callApi($params);

        return $response;
    }

    /**
     * Import data into list
     *
     * If list of fields are not provided, data is assumed to only contain
     * email addresses
     *
     * @param   array $data Data to import
     * @param   array $fields List of fields to be imported
     * @return  int Import id | false
     */
    public function import(array $data, $fields = null)
    {
        if (count($data) == 0) {
            Ovoyo_ErrorHandler::addError('List does not contain any data');
            return false;
        }

        if (!is_array($fields)) {
            $fields = array('email');
            if (!$data[0]['email']) {
                $newData = array();
                foreach ($data AS $email) {
                    $newData[] = array('email' => $email);
                }
                $data = $newData;
            }
        }
        
        if (count($fields) > 1) {
            $this->addSubscribers($data);
        } else {
            foreach ($data AS $subscriber) {
                $this->addSubscriber($subscriber);
            }
        }

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::hasImportCompleted()
     */
    public function hasImportCompleted($importId = null)
    {
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::postImport()
     */
    public function postImport($importId = null)
    {
        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::setApiHandler()
     */
    public function setApiHandler($config = null)
    {
        $this->_apiHandler = new Ovoyo_Services_OnlineMarketing_ApiHandler_VerticalResponse($config);
    }
}