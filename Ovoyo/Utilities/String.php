<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * String functions
 *
 * @package Ovoyo_Utilities
 */
class Ovoyo_Utilities_String
{
    /**
     * Create random string
     *
     * @param string $length
     * @return string
     */
    public final static function random($length = 8)
    {
        $string = '';

        srand((double) microtime() * 1000000);

        for ($n = 0; $n < $length; $n++) {
            $varchar = rand(1,3);
            switch ($varchar) {
                case 1: $string.= chr(rand(97,122));
                break;
                case 2: $string.= chr(rand(48,57));
                break;
                case 3: $string.= chr(rand(65,90));
                break;
            }
        }

        return $string;
    }
}