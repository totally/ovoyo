<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Email marketing list class
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage List
 */
class Ovoyo_Services_OnlineMarketing_List_SignupTo
      extends Ovoyo_Services_OnlineMarketing_List_Abstract
{
    public $ftpServer;
    public $ftpUsername;
    public $ftpPassword;
    public $ftpLocalDir;
    public $ftpRemoteDir;
    
    protected $_importMethod;
    
    const IMPORT_TYPE_FTP = 'ftp';
    const IMPORT_TYPE_SCP = 'scp';
    
    const IMPORT_MAX_RECORDS_PER_FILE = 3000;
    
   
    /**
     * Contruct method
     * @param array $config
     */
    public function __construct($config)
    {
        foreach ($config AS $option => $value) {
            $this->{$option} = $value;
        }
        
        $this->ftpLocalDir = $this->ftpLocalDir . DIRECTORY_SEPARATOR
                           . $this->listId;

        $this->ftpRemoteDir = $this->ftpRemoteDir . $this->listId . DIRECTORY_SEPARATOR;
        
        $this->setApiHandler($config);
    }
        
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::create()
     */
    public function create($name, $categoryId = 0)
    {
        $apiHandler = $this->getApiHandler();
        
        $params = array(
            'mode' => 'add',
            'name' => $name
        );
        
        if ($categoryId > 0) {
            $params['category'] = $categoryId;
        }

        $response = $apiHandler->callApi('list.php', $params);
        if (!is_numeric($response)) {
            $exception = 'Unable to create new list - response from server: '
                       . $response;
            throw new Exception($exception);
        }
        
        $this->listId = $response;
        
        return $this->listId;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::delete()
     */
    public function delete()
    {
        $apiHandler = $this->getApiHandler();
        
        $params = array(
            'mode' => 'remove',
            'list_id' => $this->listId
        );

        $response = $apiHandler->callApi('list.php', $params);
        if (!is_numeric($response)) {
            $exception = 'Unable to delete list - response from server: '
                       . $response;
            throw new Exception($exception);
        }
        
        return true;
    }
    
        /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::getList()
     */
    public function getList()
    {
        // fetch all lists
        if (!$lists = $this->getLists()) {
            return false;
        }
        
        $list = array();
        if (!$list = $lists[$this->listId]) {
            return false;
        }
        
        // get subscriber count
        $apiHandler = $this->getApiHandler();
        $params = array(
            'mode' => 'count',
            'list_id' => $this->listId,
        );

        $response = $apiHandler->callApi('list.php', $params);
        if (!is_numeric($response)) {
            return false;
        }
        
        $list['numSubscribers'] = $response;
        
        return $list;
    }
    
        /**
     * Get available lists
     *
     * @return  array
     */
    public function getLists()
    {
        $apiHandler = $this->getApiHandler();
        
        $params = array(
            'mode' => 'view_list_names',
        );

        $response = $apiHandler->callApi('list.php', $params);
        
        if (!@unserialize($response)) {
            return false;
        }
        
        $listItems = unserialize($response);
        
        $lists = array();
        foreach ($listItems as $listItem) {
            $lists[$listItem['id']] = $listItem;
        }
        
        return $lists;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::addSubscriber()
     */
    public function addSubscriber($data)
    {
        if (!is_array($data) || !$data['email']) {
            Ovoyo_ErrorHandler::addError('Subscriber\'s email is required');
            return false;
        }
        
        $params = array(
            'pid' => $this->listId,
            'mode' => 'add',
            'email' => $data['email']
        );
        
        $apiHandler = $this->getApiHandler();
        $response = $apiHandler->callApi('capture.php', $params);
        return $response;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::addSubscriber()
     */
    public function removeSubscriber($data)
    {
        if (!is_array($data) || !$data['email']) {
            Ovoyo_ErrorHandler::addError('Subscriber\'s email is required');
            return false;
        }
        
        $params = array(
            'pid' => $this->listId,
            'mode' => 'remove',
            'email' => $data['email']
        );
        
        $apiHandler = $this->getApiHandler();
        $response = $apiHandler->callApi('capture.php', $params);
        
        return $response;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::getSubscribers()
     */
    public function getSubscribers()
    {
        $apiHandler = $this->getApiHandler();
        
        $params = array(
            'pid' => $this->listId,
        );

        $response = $apiHandler->callApi('review.php', $params);
        
        return $response;
    }
    
        /**
     * Import data into list
     *
     * If list of fields are not provided, data is assumed to only contain
     * email addresses
     *
     * @param   array $data Data to import
     * @param   array $fields List of fields to be imported
     * @return  int Import id | false
     */
    public function import(array $data, $fields = null)
    {
        if (count($data) == 0) {
            Ovoyo_ErrorHandler::addError('List does not contain any data');
            return false;
        }
        
        if (!is_array($fields)) {
            $fields = array('email');
            if (!$data[0]['email']) {
                $newData = array();
                foreach ($data AS $email) {
                    $newData[] = array('email' => $email);
                }
                $data = $newData;
            }
        }
        
        $importMethod = $this->_getImportMethod();

        switch ($importMethod) {
            case self::IMPORT_TYPE_FTP:
                return $this->_importViaFtp($data, $fields);
                
            case self::IMPORT_TYPE_SCP:
                return $this->_importViaScp($data, $fields);
        }
        
        return false;
    }
    
        /**
     * Get import type
     *
     * @return  string
     */
    protected function _getImportMethod()
    {
        if (!isset($this->_importMethod)) {
            $this->_setImportMethod(self::IMPORT_TYPE_FTP);
        }
        
        return $this->_importMethod;
    }
    
    /**
     * Set import type
     *
     * @param   string $importMethod
     * @return  void
     */
    protected function _setImportMethod($importMethod)
    {
        $this->_importMethod = $importMethod;
    }
    
    /**
     * Import data into list via FTP
     *
     * @param   array $data Data to import
     * @param   array $fields List of fields to be imported
     * @return  array Import id(s)
     * @throws  Exception
     */
    protected function _importViaFtp(array $data, $fields)
    {
        // check local ftp dir exists
        if (!is_dir($this->ftpLocalDir)) {
            // try creating
            @mkdir($this->ftpLocalDir);
            @chmod($this->ftpLocalDir, 0755);
            
            if (!is_dir($this->ftpLocalDir)) {
                $exception = 'Local FTP directory "' . $this->ftpLocalDir . '" '
                           . 'does not exist';
                throw new Exception($exception);
            }
        }
        
        // create .csv files for upload - need to split into files containing
        // no more than self::IMPORT_MAX_RECORDS_PER_FILE entries
        $files = array();
        $recordCount = 0;
        
        foreach ($data AS $record) {
            if ($recordCount % self::IMPORT_MAX_RECORDS_PER_FILE == 0) {
                if ($recordCount > 0) {
                    $files[] = $csv;
                }
                $csv = implode($fields, ',');
            }

            $csv .= "\n";
            
            $i = 0;
            foreach ($fields AS $field) {
                $csv .= ($i > 0) ? ',' : '';
                $csv .= '"' . $record[$field] . '"';
                $i++;
            }
            
            $recordCount++;
        }
        $files[] = $csv;
        
        $importIds = array();
        foreach ($files AS $key => $csv) {
            // create filename
            $filename = $key . '.csv';
                      
            // full file path to file
            $filePath = $this->ftpLocalDir . DIRECTORY_SEPARATOR
                      . $filename;
                      
            if (!file_put_contents($filePath, $csv)) {
                $exception = 'Could not create .csv file "' . $filePath . '" '
                           . 'for import';
                throw new Exception($exception);
            }

            // ftp url to pass to singup.to
            $ftpUrl = $this->ftpServer . $this->ftpRemoteDir . $filename;
            
            // make api call to signup.to
            $apiHandler = $this->getApiHandler();
            
            $params = array(
                'list_id' => $this->listId,
                'url' => $ftpUrl,
                'username' => $this->ftpUsername,
                'password' => $this->ftpPassword,
                'overwrite' => 'remove'
            );
            
            $response = $apiHandler->callApi('import.php', $params);
            if (!is_numeric($response)) {
                @unlink($filePath);
                $exception = 'List import failed with response: ' . $response;
                throw new Exception($exception);
            }
            
            $importIds[$key] = $response;
        }
        
        return $importIds;
    }
    
    /**
     * Import data into list via SCP
     *
     * @param   array $data Data to import
     * @param   array $fields List of fields to be imported
     * @return  int|false
     * @throws  Exception
     */
    protected function _importViaScp(array $data, $fields)
    {
        // @todo
        return false;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::hasImportCompleted()
     */
    public function hasImportCompleted($importId = null)
    {
        $apiHandler = $this->getApiHandler();
        
        $params = array('import_id' => $importId);

        $response = $apiHandler->callApi('import_complete.php', $params);

        if (!is_numeric($response)) {
            $exception = 'Unable to check list import status - response from '
                       . 'server: ' . $response;
            throw new Exception($exception);
        }
        
        return ($response == $importId);
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::postImport()
     */
    public function postImport($importId = null)
    {
        if (@is_dir($this->ftpLocalDir)) {
            $handle = opendir($this->ftpLocalDir);
            while (false !== ($file = readdir($handle))) {
                @unlink($this->ftpLocalDir . DIRECTORY_SEPARATOR . $file);
            }
            
            if (!@rmdir($this->ftpLocalDir)) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * (non-PHPdoc)
     * @see Ovoyo_Services_OnlineMarketing_List_Abstract::setApiHandler()
     */
    public function setApiHandler($config = null)
    {
        $this->_apiHandler = new Ovoyo_Services_OnlineMarketing_ApiHandler_SignupTo($config);
    }
}
