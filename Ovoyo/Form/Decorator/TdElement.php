<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Zend_Form_Decorator_Label
 */
require_once 'Zend/Form/Decorator/HtmlTag.php';

/**
 * Replacement label decorator
 *
 * Single static method to take a Ovoyo_Model instance (or sub class of)
 * and create a Zend_Form instance from it. Based on code found in the following article:
 * http://codecaine.co.za/posts/form-generation-with-zend-form-part-2/
 *
 * @package    Ovoyo_Form
 */
class Ovoyo_Form_Decorator_TdElement extends Zend_Form_Decorator_HtmlTag
{
    /**
     * Get the formatted open tag
     *
     * @param  string $tag
     * @param  array $attribs
     * @return string
     */
    protected function _getOpenTag($tag, array $attribs = null)
    {
        $element = $this->getElement();
        $attribs['class'] = $attribs['class'] . ' element-' . $element->getName();
        $html = '<' . $tag;
        if (null !== $attribs) {
            $html .= $this->_htmlAttribs($attribs);
        }
        $html .= '>';
        return $html;
    }
}
