<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Class to process file information
 *
 * For more details see http://code.google.com/apis/maps/documentation/geocoding/index.html
 *
 * @package Ovoyo_Services
 */
class Ovoyo_Services_Google_GeoCoding
{
    public $apiKey;
    
    const API_URL = 'http://maps.google.com/maps/geo';
    
    const STATUS_G_GEO_SUCCESS = 200;
    const STATUS_G_GEO_SERVER_ERROR = 500;
    const STATUS_G_GEO_MISSING_QUERY = 601;
    const STATUS_G_GEO_UNKNOWN_ADDRESS = 602;
    const STATUS_G_GEO_UNAVAILABLE_ADDRESS = 603;
    const STATUS_G_GEO_BAD_KEY = 610;
    const STATUS_G_GEO_TOO_MANY_QUERIES = 620;
    
    /**
     * Contstructor
     *
     * @param  string $apiKey
     * @return void
     */
    public function __construct($apiKey) {
        
        $this->apiKey = $apiKey;
    }

    /**
    * Get latitude and longitude for an address
    *
    * @param    string $address Address to geo details for
    * @param    string $sensor Indicates whether or not the geocoding request comes from a device with a location sensor - defaults to false
    * @return   array
    * @throws   Exception
    */
    public function getLatLong($address, $sensor = false)
    {
        $sensor = ($sensor) ? 'true' : 'false';
        
        $request = self::API_URL
                 . '?q=' . urlencode($address)
                 . '&key=' . $this->apiKey
                 . '&sensor=' . $sensor
                 . '&output=json'
                 . '&oe=utf8';

        // set time out of 1 second for request
        $streamContext = stream_context_create(array('http' => array('timeout' => 1)));
        
        $result = file_get_contents($request, 0, $streamContext);
        $result = Zend_Json::decode($result);
        
        $status = $result['Status']['code'];
        if ($status != self::STATUS_G_GEO_SUCCESS) {
            $errorMessage = 'An error occurred while trying to fetch geo location of address';
            
            switch ($status) {
                case self::STATUS_G_GEO_SERVER_ERROR:
                    $errorMessage.= ', reason for the failure is unknown';
                    break;

                case self::STATUS_G_GEO_MISSING_QUERY:
                    $errorMessage.= ', supplied address is empty';
                    break;
                    
                case self::STATUS_G_GEO_UNKNOWN_ADDRESS:
                    $errorMessage.= ', no location could be found for the address supplied';
                    break;
                    
                case self::STATUS_G_GEO_UNAVAILABLE_ADDRESS:
                    $errorMessage.= ', details cannot be returned due to legal or contractual reasons';
                    break;
                    
                case self::STATUS_G_GEO_BAD_KEY:
                    $errorMessage.= ', incorrect api key provided';
                    break;
                    
                case self::STATUS_G_GEO_TOO_MANY_QUERIES:
                    $errorMessage.= '. The given api key has gone over the requests limit in the 24 hour period or has submitted too many requests in too short a period of time';
                    break;
            }
            
            throw new Exception($errorMessage);
        }
        
        // work out best result
        $bestResult = array();
        foreach ($result['Placemark'] AS $place) {
            if ($place['AddressDetails']['Accuracy'] > $bestResult['AddressDetails']['Accuracy']) {
                $bestResult = $place;
            }
        }
        
        return $bestResult;
    }
}
