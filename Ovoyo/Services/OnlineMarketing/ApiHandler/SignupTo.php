<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * Signupt API handler
 *
 * @package    Ovoyo_Services_OnlineMarketing
 * @subpackage ApiHandler
 */
class Ovoyo_Services_OnlineMarketing_ApiHandler_SignupTo
      extends Ovoyo_Services_OnlineMarketing_ApiHandler
{
    public $apiCid;
    public $apiHash;
    
    protected $_baseApiParams;
    
    const API_URL = 'https://api.sign-up.to/';
    
    /**
     * Constructor
     * 
     * @param   array $config Config options
     * @return  void
     */
    public function __construct($config)
    {
        // check for required options
        if (!isset($config['apiCid'])) {
            throw new Exception('apiCid is a required config option');
        }
        if (!isset($config['apiHash'])) {
            throw new Exception('apiHash is a required config option');
        }
        
        $this->apiCid  = $config['apiCid'];
        $this->apiHash = $config['apiHash'];
        
        $this->_baseApiParams = array(
            'cid' => $this->apiCid,
            'hash' => $this->apiHash
        );
    }
    
    /**
     * Make a call to api
     *
     * @param   string $url Url to call
     * @param   array $params Optional params to add as querystring to API url
     * @return  string Response from API call
     * @throws  Exception
     */
    public function callApi($url, $params = null)
    {
        $response = null;
        
        $apiUrl = self::API_URL . $url;
        
        // add base params
        if (is_array($this->_baseApiParams)) {
            foreach ($this->_baseApiParams AS $param => $value) {
                $params[$param] = $value;
            }   
        }
        
        // build querystring
        if (is_array($params)) {
            $paramPrefix = preg_match('/\?/', $url) ? '&' : '?';
            foreach ($params AS $param => $value) {
                $apiUrl.= $paramPrefix . $param . '=';
                $apiUrl.= urlencode($value);
                
                $paramPrefix = '&';
            } 
        }
        
        try {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

            $response = curl_exec($ch);

            curl_close($ch);
            
            if (Ovoyo_WebContainer::getConfig('application')->enableFirebugLogger) {
                $logger = Zend_Registry::get('firebugLogger');
                $message = "Making SignupTo API call:\n"
                         . ' - url: ' . $apiUrl . "\n"
                         . ' - response: ' . $response ;
                $logger->log($message, Zend_Log::INFO);
            }
            
        } catch (Exception $e) {
            throw $e;
        }
          
        return $response;
    }
}
