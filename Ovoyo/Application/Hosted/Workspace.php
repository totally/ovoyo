<?php
/**
 * Ovoyo - Web Application Framework
 *
 * @link https://www.assembla.com/spaces/ovoyo/ for the Assembla source repository
 * @copyright Copyright (c) 2010-2013 Totally Communications Ltd. (http://www.totallycommunications.com)
 * @license http://www.totallycommunications.com/license/bsd.txt New BSD License
 */

/**
 * @see Ovoyo_Model
 */
require_once 'Ovoyo/Model.php';

/**
 * Application workspace
 *
 * @package     Ovoyo_Application
 * @subpackage  Hosted
 */
class Ovoyo_Application_Hosted_Workspace extends Ovoyo_Model
{
    public $workspaceId;
    public $workspaceName;
    public $description;
    public $dbName;

    public $workspaceModules;
    public $modules;

    public $dbNameCreatePrefix = '';

    protected $_dbConfig;
    protected $_workspaceDb;

    protected $_enabledModules;

    /**
     * Init
     *
     * @return  void
     */
    public function init()
    {
        $this->requiredFields = array('workspaceName');
    }

    /**
     * Register application workspace
     *
     * Adds db connection to connection manager and makes workspace globally available via web container.
     * Also removes any modules that user does not have access to
     *
     * @return  bool
     * @throws  Exception
     */
    public function register()
    {
        $webContainer = Ovoyo_WebContainer::getInstance();

        $webContainer->addDbConnection($this->getDbConfig());
        $webContainer->setWorkspace($this);
        $webContainer->setWorkspaceId($this->workspaceId);

        return true;
    }

    /**
     * Get db connection name
     *
     * @return  string
     */
    public function getConnName()
    {
        $connName = '';

        // if db config has been defined, try getting connName from that
        if (isset($this->_dbConfig->connName)) {
            $connName = $this->_dbConfig->connName;

        // otherwise construct a unique connection name from host and db name
        } else {
            $connName = $this->host . ':' . $this->dbName;
        }

        return $connName;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Ovoyo_Model#postInsert()
     */
    public function postInsert()
    {
        // name of database to create
        $dbName = $this->dbNameCreatePrefix
                . 'workspace_' . $this->workspaceId;

        // update workspace with new dbName
        $data = array('dbName' => $dbName);
        $where = 'workspaceId=' . $this->workspaceId;
        if (!$this->_dbTable->update($data, $where)) {
            $exception = 'Could not update workspace with database name';
            throw Exception($e);
        }
        $this->dbName = $dbName;

        // this will commit the previous transaction, so need to handle
        // previous query rollbacks manually if an error occurs
        $this->_createDatabase($dbName);

        try {
            // install default modules to workspace
            $modules = Ovoyo_WebContainer::getModules();

            foreach ($modules AS $module => $directory) {
                $config = Ovoyo_WebContainer::getModuleConfig($module);
                if (!$config->installation->installedByDefault) {
                    continue;
                }

                $this->moduleInstall($module);
            }
        } catch (Exception $e) {
            $this->delete();
            $this->_dropDatabase($dbName);
            return false;
        }

        return true;
    }

    /**
     * (non-PHPdoc)
     * @see Ovoyo/Ovoyo_Model#postDelete()
     */
    public function postDelete()
    {
        $deleteWhere = array('workspaceId' => $this->workspaceId);

        // delete links between users and workspaces
        $userWorkspace = Ovoyo_Model::factory('Ovoyo_Application_Hosted_UserWorkspace', $this->_modelOptions());
        $userWorkspace->delete($deleteWhere);

        // delete workspace invites
        $workspaceInvite = Ovoyo_Model::factory('Ovoyo_Application_Hosted_WorkspaceInvite', $this->_modelOptions());
        $workspaceInvite->delete($deleteWhere);

        // delete workspace modules
        $workspaceModule = Ovoyo_Model::factory('Ovoyo_Application_Hosted_WorkspaceModule', $this->_modelOptions());
        $workspaceModule->delete($deleteWhere);

        return true;
    }

    /**
     * Check if workspace is valid
     *
     * @return  boolean
     */
    public function isValid()
    {
        $valid = parent::isValid();

        // check no other entry with same name
        if ($this->workspaceName && $this->alreadyExists('workspaceName')) {
            $error = 'An workspace with the same name already exists';
            Ovoyo_ErrorHandler::addError($error);
            $valid = false;
        }

        // test to see if connection if valid
        if ($valid && !$this->_checkDbConnection()) {
            $error = 'Could not connect to database, please make sure details '
                   . 'are correct and specified database exists';
            Ovoyo_ErrorHandler::addError($error);
            $valid = false;
        }

        return $valid;
    }

    /**
     * Create database
     *
     * @param   string $dbName
     * @return  void
     * @throws  Exception
     */
    protected function _createDatabase($dbName)
    {
        // connection
        $connection = $this->_db->getConnection();
        if (!$connection instanceof mysqli) {
            $exception = 'Can only create databases using a mysqli connection';
            throw new Exception($exception);
        }

        // sql to create database
        $sql = 'CREATE database `' . $dbName . '`;';

        $connection->query($sql);
    }

    /**
     * Drop database
     *
     * @param   string $dbName
     * @return  void
     * @throws  Exception
     */
    protected function _dropDatabase($dbName)
    {
        // connection
        $connection = $this->_db->getConnection();
        if (!$connection instanceof mysqli) {
            $exception = 'Can only drop databases using a mysqli connection';
            throw new Exception($exception);
        }

        // sql to drop database
        $sql = 'DROP database `' . $dbName . '`;';

        $connection->query($sql);
    }

    /**
     * Get enabled modules onworkspace
     *
     * @return  array
     */
    public function getEnabledModules()
    {
        if (!isset($this->_enabledModules)) {
            $workspaceModule = Ovoyo_Model::factory(
                'Ovoyo_Application_Hosted_WorkspaceModule',
                $this->getModelOptions()
            );

            $stmt = $workspaceModule->search(array(
                'enabled' => 1,
                'workspaceId' => $this->workspaceId
            ));

            $results = $stmt->fetchAll(Zend_Db::FETCH_OBJ);

            $this->_enabledModules = array();
            foreach ($results AS $row) {
                $this->_enabledModules[$row->module] = $row->module;
            }
        }

        return $this->_enabledModules;
    }

    /**
     * Set enabled modules
     *
     * $dbConnName should be the name of the database connection where the
     * workspaceModule table is held i.e. the table that holds the list of
     * which modules are enabled for each workspace
     *
     * @param array $modules
     * @param string $dbConnName
     * @return boolean
     */
    public function setEnabledModules($modules, $dbConnName = 'default')
    {
        $currentlyEnabled = $this->getEnabledModules();

        $availableModules = Ovoyo_WebContainer::getModules();

        try {
            // check module dependencies before enabling
            foreach ($modules AS $module) {
                if ($currentlyEnabled[$module]) {
                    continue;
                }

                $config = Ovoyo_WebContainer::getModuleConfig($module);
                if (!$this->_checkModuleDependencies($config, $modules)) {
                    return false;
                }
            }

            // build list of modules installed by default (used by db update
            // later on)
            $installedByDefault = array();
            foreach ($availableModules AS $module => $directory) {
                $config = Ovoyo_WebContainer::getModuleConfig($module);
                if ($config->installation->installedByDefault) {
                    $installedByDefault[] = $module;
                }
            }
            if (count($installedByDefault) == 0) {
                $installedByDefault = 0;
            } else {
                $installedByDefault = "'"
                                    . implode("','", $installedByDefault)
                                    . "'";
            }

            // enable modules
            $enabledModules = array();
            foreach ($modules AS $module) {
                // if currently enabled, no need to do anything
                if ($currentlyEnabled[$module]) {
                    $enabledModules[] = $module;
                    continue;
                }

                if (!$this->moduleEnable($module, $dbConnName, true)) {
                    return false;
                }

                $enabledModules[] = $module;
            }

            if (count($enabledModules) == 0) {
                $enabledModules = 0;
            } else {
                $enabledModules = "'" . implode($enabledModules, "','") . "'";
            }

            // update workspaceModule table list of modules enabled
            $db = Ovoyo_WebContainer::getDbConnection($dbConnName);
            $data = array('enabled' => 0);
            $where = 'workspaceId = ' . $this->workspaceId
                   .' AND `module` NOT IN (' . $enabledModules . ')'
                   .' AND `module` NOT IN (' . $installedByDefault . ')';

            $db->update('workspaceModule', $data, $where);

        } catch (Exception $e) {
            // list of enabled modules with ones that have been enabled
            // so far
            $db = Ovoyo_WebContainer::getDbConnection($dbConnName);
            $data = array('enabled' => 0);
            $where = 'workspaceId = ' . $this->workspaceId
                   .' AND `module` NOT IN (' . $enabledModules . ')'
                   .' AND `module` NOT IN (' . $installedByDefault . ')';

            $db->update('workspaceModule', $data, $where);

            throw $e;
        }

        return true;
    }

    /**
     * Check module dependency prior to installing
     *
     * $others holds a list of other modules that are also being
     * enabled at the same time
     *
     * @param Zend_Config $config Config of module to check
     * @param array $others
     * @returnboolean
     */
    protected function _checkModuleDependencies($config, $others = array())
    {
        // check dependencies
        if (!$config->installation->dependencies instanceOf Zend_Config) {
            return true;
        }

        $others = (is_array($others)) ? $others : array();

        $dependencies = $config->installation->dependencies->dependency->toArray();
        $key = key($dependencies);
        if ($key !== 0) {
            $dependencies = array($dependencies);
        }

        $modules = Ovoyo_WebContainer::getModules();
        $currentlyEnabled = $this->getEnabledModules();

        $moduleName = $config->definition->name;

        $valid = true;
        foreach ($dependencies AS $dependency) {
            $dependencyError = false;

            $dependentModule = Ovoyo_WebContainer::getModuleConfig($dependency['module']);
            $dependentModuleDefinition = $dependentModule->definition;
            $dependentModuleName = $dependentModuleDefinition->name;

            // module must already be enabled or in list of modules to be
            // enabled
            if (!in_array($dependency['module'], $others)) {

                $dependencyError = true;
                $valid = false;

            // module is enabled or in list of other modules to be
            // enabled, so check version numbers
            } else {
                $dependentModuleVersion = $dependentModuleDefinition->version;

                // check min version number
                if ($dependency['minVersion'] > 0) {
                    $minVersion = $dependency['minVersion'];
                    if ($dependentModuleVersion < $minVersion) {
                        $dependencyError = true;
                    }
                }

                // check max version number
                if ($dependency['maxVersion'] > 0) {
                    $maxVersion = $dependency['maxVersion'];
                    if ($dependentModuleVersion > $maxVersion) {
                        $dependencyError = true;
                    }
                }
            }

            if ($dependencyError) {
                $valid = false;

                if (!$dependentModuleName) {
                    $dependentModuleName = $dependency['module'];
                }

                $error = $moduleName . ' requires '
                       . $dependentModuleName . ' module version';
                if ($dependency['minVersion'] > 0) {
                    $error.= ' >= ' .  $dependency['minVersion'];
                }
                if ($dependency['maxVersion'] > 0) {
                    if ($dependency['minVersion'] > 0) {
                        $error.= ' and ';
                    }
                    $error.= ' <= ' .  $dependency['maxVersion'];
                }
                Ovoyo_ErrorHandler::addError($error);
                continue;
            }
        }

        return $valid;
    }

    /**
     * Enable a module in a workspace
     *
     * $dbConnName should be the name of the database connection where the
     * workspaceModule table is held i.e. the table that holds the list of
     * which modules are enabled for each workspace
     *
     * @param string $module
     * @param string $dbConnName
     * @param boolean $skipDependencyCheck
     * @return boolean
     */
    public function moduleEnable($module, $dbConnName = 'default', $skipDependencyCheck = false)
    {
        $modules = Ovoyo_WebContainer::getModules();
        if (!array_key_exists($module, $modules)) {
            $error = 'Trying to enable an invalid module (' . $module . ')';
            Ovoyo_ErrorHandler::addError($error);
            return false;
        }

        // get module config
        $config = Ovoyo_WebContainer::getModuleConfig($module);

        // do dependency check
        if (!$skipDependencyCheck) {
            $this->_checkModuleDependencies($config);
        }

        // install module if not already installed
        $workspaceModule = Ovoyo_Model::factory(
            'Ovoyo_Application_Hosted_WorkspaceModule',
            array('connName' => $dbConnName)
        );
        $workspaceModule->fetchBy(array(
            'workspaceId' => $this->workspaceId,
            'module' => $module
        ));

        if (!$workspaceModule->module) {
            return $this->moduleInstall($module);
        }

        // module is already installed, so just re-enable
        $workspaceModule->enabled = 1;

        return $workspaceModule->update();
    }


    /**
     * Apply an sql file to a workspace database
     *
     * @param   string $sqlFile Path to sql file
     * @return  boolean
     */
    protected function _applySqlFile($sqlFile)
    {
        $dbConfig = $this->getDbConfig(true);
        return Ovoyo_Utilities_Shell::applySqlFile($sqlFile, $dbConfig,
                                                   'workspace');
    }

    /**
     * Check database connection is valid
     *
     * @return  boolean
     */
    protected function _checkDbConnection()
    {
        try {
            $dbConfig = $this->getDbConfig();
            Ovoyo_WebContainer::getInstance()->dbConnectionManager->testConfig(
                $dbConfig
            );
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * Create user in workspace
     *
     * Given a user instance, will insert the new user and assign to the current workspace
     *
     * @param   Ovoyo_Application_Hosted_User $user
     * @return  boolean
     * @throws  Exception
     */
    public function createUserInWorkspace(Ovoyo_Application_Hosted_User $user)
    {
        $this->beginTransaction();

        try {
            $user->active = 1;
            if (!$user->insert()) {
                $this->rollback();
                return false;
            }

            $this->assignUser($user->userId);

            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }

        return true;
    }

    /**
     * Get users associated with workspace
     *
     * Results returned as an array, with an option to fetch users NOT assigned to workspace instead
     *
     * @param   boolean $getUnassigned
     * @return  array
     * @throws  Exception
     */
    public function getUsers($getUnassigned = false)
    {
        $users  = array();

        try {
            $select = $this->_db->select();

            // get unassigned users
            if ($getUnassigned) {
                $select->from(array('u' => $this->_fullTableName('user')))
                       ->joinLeft(array('uw' => $this->_fullTableName('userWorkspace')), 'uw.userId=u.userId AND uw.workspaceId = ' . $this->workspaceId, array())
                       ->where('u.deleted=0')
                       ->where('u.superuser=0')
                       ->where('uw.workspaceId IS NULL');

            // get assigned users
            } else {
                $select->from(array('uw' => $this->_fullTableName('userWorkspace')))
                       ->join(array('u' => $this->_fullTableName('user')), 'u.userId=uw.userId', array('email', 'firstName', 'lastName'))
                       ->where('u.deleted=0')
                       ->where('u.superuser=0')
                       ->where('uw.workspaceId = ?', $this->workspaceId);
            }

            $select->order('u.email');

            $rows = $this->_db->fetchAll($select, array(), Zend_Db::FETCH_OBJ);
            foreach ($rows AS $row) {
                    $name = $row->lastName;
                    if ($row->firstName) {
                       $name = ($name) ? "$name, $row->firstName" : $row->firstName;
                    }
                    $value = "$name ($row->email)";

                $users[$row->userId] = $value;
            }
        } catch (Exception $e) {
            throw $e;
        }

        return $users;
    }

    /**
     * Set users who are assigned to workspace
     *
     * @param   array $users
     * @return  boolean
     * @throws  Exception
     */
    public function setAssignedUsers(array $users)
    {
            $userIds = array_flip($users);

            $this->beginTransaction();
            try {
                    $assigned = $this->getUsers();

                    // first remove any users that are now longer in assigned list
                    foreach ($assigned AS $userId => $label) {
                            if (array_key_exists($userId, $userIds)) {
                                continue;
                            }

                            $this->unassignUser($userId);
                    }

                    // add new users
                    foreach ($users AS $userId) {
                            if (array_key_exists($userId, $assigned)) {
                                continue;
                            }

                            $this->assignUser($userId);
                    }

            $this->commit();
            } catch (Exception $e) {
                    $this->rollback();
                    throw $e;
            }

            return true;
    }

    /**
     * Unassign user from an workspace
     *
     * @param   int $userId
     * @return  boolean
     * @throws  Exception
     */
    public function unassignUser($userId)
    {
        $this->beginTransaction();
            try {
            $deleteBy = array(
                'userId' => $userId, 'workspaceId' => $this->workspaceId
            );

                    // delete user workspace
            $userWorkspace = Ovoyo_Model::factory(
                'Ovoyo_Application_Hosted_UserWorkspace',
                $this->_modelOptions()
            );
            if (!$userWorkspace->deleteBy($deleteBy)) {
                $this->rollback();
                return false;
            }

            // delete user role entries for workspace
            $userRole = Ovoyo_Model::factory(
                'Ovoyo_Application_Hosted_UserRole',
                $this->_modelOptions()
            );
            if (!$userRole->deleteBy($deleteBy)) {
                $this->rollback();
                return false;
            }

            $this->commit();
            } catch (Exception $e) {
                    $this->rollback();
                    throw $e;
            }

            return true;
    }

    /**
     * Assign user to an workspace
     *
     * @param   int $userId
     * @return  boolean
     * @throws  Exception
     */
    public function assignUser($userId)
    {
            $userWorkspace = Ovoyo_Model::factory(
               'Ovoyo_Application_Hosted_UserWorkspace', $this->_modelOptions()
            );
            $userWorkspace->userId = $userId;
            $userWorkspace->workspaceId = $this->workspaceId;

            return $userWorkspace->insert();
    }


    /**
     * Install module to workspace
     *
     * @param string $module
     * @return boolean
     */
    public function moduleInstall($module)
    {

        // get module config
        $config = Ovoyo_WebContainer::getModuleConfig($module);

        $installation = $config->installation;

        // apply any sql file
        if (isset($installation->sqlFile)) {
            $modules = Ovoyo_WebContainer::getModules();

            $sqlFile = $modules[$module]
                     . DIRECTORY_SEPARATOR
                     . $installation->sqlFile;

           if (!$this->_applySqlFile($sqlFile)) {
                return false;
           }
        }

        $workspaceModule = Ovoyo_Model::factory(
            'Ovoyo_Application_Hosted_WorkspaceModule',
            $this->getModelOptions()
        );
        $workspaceModule->workspaceId = $this->workspaceId;
        $workspaceModule->module = $module;
        $workspaceModule->enabled = 1;

        return $workspaceModule->insert();
    }

    /**
     * Get connection to workspace db
     *
     * @return Ovoyo_Db
     */
    public function getDbConnection()
    {
        if (isset($this->_workspaceDb)) {
            return $this->_workspaceDb;
        }

        $webContainer = Ovoyo_WebContainer::getInstance();

        // connection to workspace, and install db
        $dbConfig = $this->getDbConfig();

        // make sure dbname has been defined (maybe a newly created workspace)
        if (!$dbConfig->params->dbname) {
            if (!$this->dbName) {
                $exception = 'Can not create db connection which does not '
                           . 'have a db name assigned';
                throw new Exception($exception);
            }

            $dbConfig = $this->getDbConfig(true);
        }

        $webContainer->addDbConnection($dbConfig);
        $workspaceDb = $webContainer->getDbConnection($this->getConnName());

        $this->_workspaceDb = $workspaceDb;

        return $this->_workspaceDb;
    }

    /**
     * Get workspace db config
     *
     * @param   boolean $rebuild
     * @return  Zend_Config
     */
    public function getDbConfig($rebuild = false)
    {
        if (!isset($this->_dbConfig) || $rebuild) {
            $webContainer = Ovoyo_WebContainer::getInstance();

            // get current db config object so we can get missing connection
            // details from it
            $currentDbConfig = $webContainer->dbConnectionManager->getDbConfig();
            $currentParams = $currentDbConfig->params;

            $host = (isset($this->host)) ? $this->host : $currentParams->host;
            $username = (isset($this->username)) ? $this->username
                                                 : $currentParams->username;
            $password = (isset($this->password)) ? $this->password
                                                 : $currentParams->password;

            $dbConfigArray = array(
                'connName' => $this->getConnName(),
                'adapter'  => $currentDbConfig->adapter,
                'params'   => array(
                    'host'     => $host,
                    'username' => $username,
                    'password' => $password,
                    'dbname'   => $this->dbName
                )
            );

            require_once 'Zend/Config.php';
            $this->_dbConfig = new Zend_Config($dbConfigArray);
        }

        return $this->_dbConfig;
    }


}
